//
//  ServerToken.h
//  Bancomer
//
//  Created by Mike on 01/06/15.
//  Copyright (c) 2015 GoNet. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseViewController.h"
#import "Server.h"


#define CLOSE_SESSION_OPERATION                                     11

/*
 * Consultas para Softtoken
 */
#define AUTENTICAR_TOKEN                                             43
#define SINCRONIZA_TOKEN_ST                                          44
#define EXPORTA_TOKEN_ST                                             45
#define CONSULTA_TIPO_SOLICITUD_ST                                   46
#define CAMBIO_TELEFONO_ASOCIADO_ST                                  49
#define SOLICITUD_ST                                                 50
#define FINALIZAR_CONTRATACION_ST                                    51
#define CONTRATACION_ENROLAMIENTO                                    54
#define SINCRONIZACION_EXPORTACION_TOKEN                             92

@interface ServerToken : NSObject



+ (ServerToken*) getInstance;

//linea comentada METODO ANTERIOR
-(NSInteger) autenticarTokenConTarjeta: (NSString*)numTarjeta
                                  aOTP: (NSString*)otp
                            numCelular: (NSString*)numCel
                            numCliente: (NSString*)numCliente
                                   NIP: (NSString*)nip
                                conCVV: (NSString*)codigoCVV
                           companiaTel: (NSString*)compania
                            tSolicitud: (NSString*)tipoSolicitud
                                 email: (NSString*)email
                                nombre: (NSString*)nombreCliente
                                  vApp: (NSString*)versionApp
                             forClient: (id<ServerClient>) client;

//linea comentada METODO NUEVO
-(NSInteger) autenticarTokenConTarjetaN: (NSString*)numTarjeta
                                  aOTP: (NSString*)otp
                            numCelular: (NSString*)numCel
                            numCliente: (NSString*)numCliente
                                   NIP: (NSString*)nip
                                conCVV: (NSString*)codigoCVV
                            tSolicitud: (NSString*)tipoSolicitud
                                 email: (NSString*)email
                                nombre: (NSString*)nombreCliente
                             forClient: (id<ServerClient>) client;


-(NSInteger) sincronizaToken: (NSString*)nombreToken
                       aOTP1: (NSString*)otp1
                       aOTP2: (NSString*)otp2
                   forClient: (id<ServerClient>) client;

-(NSInteger)exportaToken: (NSString*)numCliente
              numCelular: (NSString*)numCelular
              numTarjeta: (NSString*)numTarjeta
             nombreToken: (NSString*)nombreToken
              aIndicador: (NSString*)indicador
             aVersionAPP: (NSString*)versionAPP
               forClient: (id<ServerClient>) client;

//linea comentada METODO ANTERIOR
-(NSInteger) consultaTipoSolicitudConNumCelular: (NSString*) numCelular
                                  andNumTarjeta: (NSString*) numTarjeta
                             andCompaniaCelular: (NSString*) companiaCelular
                                           vApp: (NSString*) versionApp
                                      forClient: (id<ServerClient>) client;


//linea comentada METODO NUEVO
-(NSInteger) consultaTipoSolicitudConNumCelular: (NSString*) numCelular
                                  andNumTarjeta: (NSString*) numTarjeta
                                      forClient: (id<ServerClient>) client;


-(NSInteger) cambioTelefonoAsociado: (NSString*)numeroTelefono
                    companiaCelular: (NSString*)companiaCelular
                          codigoOTP: (NSString*)codigoOTP
                      numeroCliente: (NSString*)numeroCliente
                      numeroTarjeta: (NSString*)numeroTarjeta
                          forClient: (id<ServerClient>) client;

-(NSInteger) solicitudSTConNumeroTarjeta: (NSString*)numeroTarjeta
                       andNumeroTelefono: (NSString*)numeroTelefono
                      andCompaniaCelular: (NSString*)companiaCelular
                                andEmail: (NSString*)email
                        andNumeroCliente: (NSString*)numeroCliente
                               forClient: (id<ServerClient>) client;

//LINEA COMENTADA METODO ANTERIOR
-(NSInteger) finalizaContratacionST: (NSString*)numeroTarjeta
                  conNumeroTelefono: (NSString*)numeroTelefono
                             conIum: (NSString*)ium
                 conCompaniaCelular: (NSString*)companiaCelular
                       conCodigoOPT: (NSString*)codigootp
                        paraCliente: (id<ServerClient>)cliente;


//LINEA COMENTADA METODO NUEVO
-(NSInteger) finalizaContratacionSTN: (NSString*)numeroTarjeta
                  conNumeroTelefono: (NSString*)numeroTelefono
                             conIum: (NSString*)ium
                       conCodigoOPT: (NSString*)codigootp
                        paraCliente: (id<ServerClient>)cliente;

-(NSInteger) contratacionEnrolamiento: (NSString *)numeroTelefono
                            cveAcceso: (NSString*)cveAcceso
                        numeroTarjeta: (NSString*)numeroTarjeta
                        perfilCliente: (NSString*)perfilCliente
                      companiaCelular: (NSString*)companiaCelular
                                email: (NSString*)email
            aceptoTerminosCondiciones: (NSString*)aceptoTerminosCondiciones
                            forClient: (id<ServerClient>)cliente;

-(NSInteger) sincronizeAndExportToken: (NSString*)numeroTarjeta
                       numeroTelefono: (NSString*)aNumeroTelefono
                        tipoSolicitud: (NSString*)aTipoSolicitud
                          nombreToken: (NSString*)aNombreToken
                                 otp1: (NSString*)aOtp1
                                 otp2: (NSString*)aOtp2
                                  ium: (NSString*)aIUM
                           versionApp: (NSString*)aVersionApp
                            forClient: (id<ServerClient>)cliente;

@end
