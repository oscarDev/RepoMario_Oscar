//
//  ServerToken.m
//  Bancomer
//
//  Created by Mike on 01/06/15.
//  Copyright (c) 2015 GoNet. All rights reserved.
//

#import "ServerToken.h"
#import "Server.h"
#import "ServerTools.h"
#import "AutenticacionTokenResult.h"
#import "TipoSolicitudResult.h"
#import "SincronizaTokenResult.h"
#import "ExportacionTokenResult.h"
#import "CambioTelefonoAsociadoSTResult.h"
#import "SolicitudSTResult.h"
#import "FinalizarContratacionSTResult.h"
#import "ContratacionEnrolamientoResult.h"
#import "SincronizacionExportacionTokenResult.h"
#import "Tools.h"
#import "EncripcionUtilities.h"
#import "ConstantsToken.h"
#import "APIToken.h"

@implementation ServerToken

/**
 * ServerToken singleton only instance
 */
static ServerToken* _serverInstance = nil;

/*
 * Returns the singleton only instance
 */
+ (ServerToken*) getInstance {
    if (_serverInstance == nil) {
        @synchronized ([Server class]) {
            if (_serverInstance == nil) {
                _serverInstance = [[ServerToken alloc] init];
            }
        }
    }
    return _serverInstance;
}


//linea comentada METODO ANTERIOR
-(NSInteger) autenticarTokenConTarjeta: (NSString*)numTarjeta
                                  aOTP: (NSString*)otp
                            numCelular: (NSString*)numCel
                            numCliente: (NSString*)numCliente
                                   NIP: (NSString*)nip
                                conCVV: (NSString*)codigoCVV
                           companiaTel: (NSString*)compania
                            tSolicitud: (NSString*)tipoSolicitud
                                 email: (NSString*)email
                                nombre: (NSString*)nombreCliente
                                  vApp: (NSString*)versionApp
                             forClient: (id<ServerClient>) client {
    
   
    
    NSMutableDictionary* parametros = [[NSMutableDictionary alloc] init];
    
    if([ServerTools isEncriptar]) {
    
        NSMutableString* cadenaEncriptada = [NSMutableString stringWithFormat:@"\"operacion\":\"autenticacionToken\",\"numeroCelular\":\"%@\",\"numeroTarjeta\":\"%@\"", numCel, numTarjeta];
        
        if ([versionApp isEqualToString:@""]) {
            
            [cadenaEncriptada appendString:[NSString stringWithFormat:@",\"numeroCliente\":\"%@\",\"nombreCliente\":\"%@\"",numCliente,[[nombreCliente componentsSeparatedByString:@" "] objectAtIndex:0]]];
            
        } else {
            
            [cadenaEncriptada appendString:[NSString stringWithFormat:@",\"codigoCVV2\":\"%@\"",codigoCVV]];
           
        }
        [cadenaEncriptada appendString:[NSString stringWithFormat:@",\"codigoNIP\":\"%@\",\"codigoOTP\":\"%@\",\"tipoSolicitud\":\"%@\",\"email\":\"%@\"", nip, otp, tipoSolicitud, email]];
        
        double version = [APIToken getInstance].appVersion;
        version = version*100;
        
        NSString *miVersion=[NSString stringWithFormat: @"%.0f", version];
        [cadenaEncriptada appendString:[NSString stringWithFormat:@",\"versionApp\":\"%@\"",miVersion]];
        
      [parametros setObject:[NSString stringWithString:[EncripcionUtilities encriptarDatos:cadenaEncriptada]] forKey:@"EN"];
    
    } else {
    
        [parametros setObject:@"autenticacionToken" forKey:@"operacion"];
        [parametros setObject:numCel forKey:@"numeroCelular"];
        [parametros setObject:numTarjeta forKey:@"numeroTarjeta"];
        
        if ([versionApp isEqualToString:@""]) {
            [parametros setObject:numCliente forKey:@"numeroCliente"];
            [parametros setObject:[[nombreCliente componentsSeparatedByString:@" "] objectAtIndex:0] forKey:@"nombreCliente"];
        } else {
            [parametros setObject:codigoCVV forKey:@"codigoCVV2"];
        }
        [parametros setObject:nip forKey:@"codigoNIP"];
        
        [parametros setObject:otp forKey:@"codigoOTP"];
        [parametros setObject:tipoSolicitud forKey:@"tipoSolicitud"];
        
        [parametros setObject:email forKey:@"email"];
        double version = [APIToken getInstance].appVersion;
        version = version*100;
        
        NSString *miVersion=[NSString stringWithFormat: @"%.0f", version];
        
        [parametros setObject:miVersion forKey:@"versionApp"];

    }
    return [[Server getInstance] doGenericNetworkRequestForOperation:@"autenticacionToken" withParameters:parametros andResponseHandler:[[AutenticacionTokenResult alloc] init] forClient:client];
}

//linea comentada METODO NUEVO

-(NSInteger) autenticarTokenConTarjetaN: (NSString*)numTarjeta
                                  aOTP: (NSString*)otp
                            numCelular: (NSString*)numCel
                            numCliente: (NSString*)numCliente
                                   NIP: (NSString*)nip
                                conCVV: (NSString*)codigoCVV
                            tSolicitud: (NSString*)tipoSolicitud
                                 email: (NSString*)email
                                nombre: (NSString*)nombreCliente
                             forClient: (id<ServerClient>) client {
    
    
    
    NSMutableDictionary* parametros = [[NSMutableDictionary alloc] init];
    
    if([ServerTools isEncriptar]) {
        
        NSMutableString* cadenaEncriptada = [NSMutableString stringWithFormat:@"\"operacion\":\"autenticacionToken\",\"numeroCelular\":\"%@\",\"numeroTarjeta\":\"%@\"", numCel, numTarjeta];
        
        [cadenaEncriptada appendString:[NSString stringWithFormat:@",\"codigoNIP\":\"%@\",\"codigoOTP\":\"%@\",\"tipoSolicitud\":\"%@\",\"email\":\"%@\"", nip, otp, tipoSolicitud, email]];
        
        double version = [APIToken getInstance].appVersion;
        version = version*100;
        
        [parametros setObject:[NSString stringWithString:[EncripcionUtilities encriptarDatos:cadenaEncriptada]] forKey:@"EN"];
        
    } else {
        
        [parametros setObject:@"autenticacionToken" forKey:@"operacion"];
        [parametros setObject:numCel forKey:@"numeroCelular"];
        [parametros setObject:numTarjeta forKey:@"numeroTarjeta"];
        
//        if ([versionApp isEqualToString:@""]) {
//            [parametros setObject:numCliente forKey:@"numeroCliente"];
//            [parametros setObject:[[nombreCliente componentsSeparatedByString:@" "] objectAtIndex:0] forKey:@"nombreCliente"];
//        } else {
//            [parametros setObject:codigoCVV forKey:@"codigoCVV2"];
//        }
//      LINEA AGREGADA SE PUSO AQUI ABAJO
        [parametros setObject:codigoCVV forKey:@"codigoCVV2"];
        [parametros setObject:nip forKey:@"codigoNIP"];
        
        [parametros setObject:otp forKey:@"codigoOTP"];
        [parametros setObject:tipoSolicitud forKey:@"tipoSolicitud"];
        
        [parametros setObject:email forKey:@"email"];
        
    }
    return [[Server getInstance] doGenericNetworkRequestForOperation:@"autenticacionToken" withParameters:parametros andResponseHandler:[[AutenticacionTokenResult alloc] init] forClient:client];
}



-(NSInteger) sincronizaToken: (NSString*)nombreToken
                       aOTP1: (NSString*)otp1
                       aOTP2: (NSString*)otp2
                   forClient: (id<ServerClient>) client {
    
   NSLog(@"Server sincronizaToken");
    
    NSMutableDictionary* parametros = [[NSMutableDictionary alloc] init];
    
    [parametros setObject:@"203" forKey:@"OP"];
    [parametros setObject:nombreToken forKey:@"NA"];
    [parametros setObject:otp1 forKey:@"01"];
    [parametros setObject:otp1 forKey:@"02"];
   
    
    return [[Server getInstance] doGenericNetworkRequestForOperation:@"203" withParameters:parametros andResponseHandler:[[SincronizaTokenResult alloc] init] forClient:client];
}

-(NSInteger)exportaToken: (NSString*)numCliente
              numCelular: (NSString*)numCelular
              numTarjeta: (NSString*)numTarjeta
             nombreToken: (NSString*)nombreToken
              aIndicador: (NSString*)indicador
             aVersionAPP: (NSString*)versionAPP
               forClient: (id<ServerClient>) client {
    NSLog(@"Server exportaToken");
   
    
    NSMutableDictionary* parametros = [[NSMutableDictionary alloc] init];
    [parametros setObject:@"204" forKey:@"OP"];
    [parametros setObject:numTarjeta forKey:@"NJ"];
    [parametros setObject:numCelular forKey:@"NT"];
    
    [parametros setObject:@"AAAAAAAA" forKey:@"TE"];
    [parametros setObject:indicador forKey:@"TS"];
    [parametros setObject:nombreToken forKey:@"NA"];
   
    double version = [APIToken getInstance].appVersion;
    version = version*100;
    [parametros setObject: [NSString stringWithFormat: @"%.0f", version] forKey:@"AV"];
   
    return [[Server getInstance] doGenericNetworkRequestForOperation:@"204" withParameters:parametros andResponseHandler:[[ExportacionTokenResult alloc] init] forClient:client];
}

//linea comentada METODO ANTERIOR
-(NSInteger) consultaTipoSolicitudConNumCelular: (NSString*) numCelular
                                  andNumTarjeta: (NSString*) numTarjeta
                             andCompaniaCelular: (NSString*) companiaCelular
                                           vApp: (NSString*) versionApp
                                      forClient: (id<ServerClient>) client {
    
 
    NSMutableDictionary* parametros = [[NSMutableDictionary alloc] init];
    
    
    // Validamos el Switch encriptar
    if([ServerTools isEncriptar]){
        
        NSMutableString* cadenaEncriptada = [NSMutableString stringWithFormat:@"\"operacion\":\"consultaTarjetaST\",\"numeroTelefono\":\"%@\",\"numeroTarjeta\":\"%@\",\"companiaCelular\":\"%@\"", numCelular, numTarjeta, companiaCelular];
        
        if (![versionApp isEqualToString:@""]) {
            double version = [APIToken getInstance].appVersion;
            version = version*100;
            
            NSString *miVersion=[NSString stringWithFormat: @"%.0f", version];
            [cadenaEncriptada appendString:[NSString stringWithFormat:@",\"versionApp\":\"%@\"",miVersion]];
        }
        [parametros setObject:[NSString stringWithString:[EncripcionUtilities encriptarDatos:cadenaEncriptada]] forKey:@"EN"];
        
    } else {
        
        [parametros setObject:@"consultaTarjetaST" forKey:@"operacion"];
        [parametros setObject:numTarjeta forKey:@"numeroTarjeta"];
        [parametros setObject:numCelular forKey:@"numeroTelefono"];
        [parametros setObject:companiaCelular forKey:@"companiaCelular"];
        
        if (![versionApp isEqualToString:@""]) {
            double version = [APIToken getInstance].appVersion;
            version = version*100;
            
            NSString *miVersion=[NSString stringWithFormat: @"%.0f", version];
            
            [parametros setObject:miVersion forKey:@"versionApp"];
        }

    }
    
    return [[Server getInstance] doGenericNetworkRequestForOperation:@"consultaTarjetaST" withParameters:parametros andResponseHandler:[[TipoSolicitudResult alloc] init] forClient:client];
}

//linea comentada METODO NUEVO

-(NSInteger) consultaTipoSolicitudConNumCelular: (NSString*) numCelular
                                  andNumTarjeta: (NSString*) numTarjeta
                                      forClient: (id<ServerClient>) client {
    
    
    NSMutableDictionary* parametros = [[NSMutableDictionary alloc] init];
    
    
    // Validamos el Switch encriptar
    if([ServerTools isEncriptar]){
        
        NSMutableString* cadenaEncriptada = [NSMutableString stringWithFormat:@"\"operacion\":\"consultaTarjetaST\",\"numeroTelefono\":\"%@\",\"numeroTarjeta\":\"%@\"", numCelular, numTarjeta];
        
//        if (![versionApp isEqualToString:@""]) {
//            double version = [APIToken getInstance].appVersion;
//            version = version*100;
//            
//            NSString *miVersion=[NSString stringWithFormat: @"%.0f", version];
//            [cadenaEncriptada appendString:[NSString stringWithFormat:@",\"versionApp\":\"%@\"",miVersion]];
//        }
        [parametros setObject:[NSString stringWithString:[EncripcionUtilities encriptarDatos:cadenaEncriptada]] forKey:@"EN"];
        
    } else {
        
        [parametros setObject:@"consultaTarjetaST" forKey:@"operacion"];
        [parametros setObject:numTarjeta forKey:@"numeroTarjeta"];
        [parametros setObject:numCelular forKey:@"numeroTelefono"];

//        
//        if (![versionApp isEqualToString:@""]) {
//            double version = [APIToken getInstance].appVersion;
//            version = version*100;
//            
//            NSString *miVersion=[NSString stringWithFormat: @"%.0f", version];
//            
//            [parametros setObject:miVersion forKey:@"versionApp"];
//        }
        
    }
    
    return [[Server getInstance] doGenericNetworkRequestForOperation:@"consultaTarjetaST" withParameters:parametros andResponseHandler:[[TipoSolicitudResult alloc] init] forClient:client];
}





-(NSInteger) cambioTelefonoAsociado: (NSString*)numeroTelefono
                    companiaCelular: (NSString*)companiaCelular
                          codigoOTP: (NSString*)codigoOTP
                      numeroCliente: (NSString*)numeroCliente
                      numeroTarjeta: (NSString*)numeroTarjeta
                          forClient: (id<ServerClient>) client {

    NSMutableDictionary* parametros = [[NSMutableDictionary alloc] init];
    
    [parametros setObject:@"cambioTelefonoAsociadoE" forKey:@"operacion"];
    [parametros setObject:numeroTelefono forKey:@"numeroTelefono"];
    [parametros setObject:companiaCelular forKey:@"companiaCelular"];
    [parametros setObject:codigoOTP forKey:@"codigoOTP"];
    [parametros setObject:numeroCliente forKey:@"numeroCliente"];
    [parametros setObject:numeroTarjeta forKey:@"numeroTarjeta"];
  
    return [[Server getInstance] doGenericNetworkRequestForOperation:@"cambioTelefonoAsociadoE" withParameters:parametros andResponseHandler:[[CambioTelefonoAsociadoSTResult alloc] init] forClient:client];
    
}

-(NSInteger) solicitudSTConNumeroTarjeta: (NSString*)numeroTarjeta
                       andNumeroTelefono: (NSString*)numeroTelefono
                      andCompaniaCelular: (NSString*)companiaCelular
                                andEmail: (NSString*)email
                        andNumeroCliente: (NSString*)numeroCliente
                               forClient: (id<ServerClient>) client {
  
    NSMutableDictionary* parametros = [[NSMutableDictionary alloc] init];
    
    
    // Validamos el Switch encriptar
    if([ServerTools isEncriptar]){
        
        NSMutableString* cadenaEncriptada = [NSMutableString stringWithFormat:@"\"operacion\":\"%@\",\"numeroTarjeta\":\"%@\",\"numeroTelefono\":\"%@\",\"companiaCelular\":\"%@\",\"email\":\"%@\",\"numeroCliente\":\"%@\"", @"solicitudST", numeroTarjeta, numeroTelefono, companiaCelular, email, numeroCliente];
        [parametros setObject:[NSString stringWithString:[EncripcionUtilities encriptarDatos:cadenaEncriptada]] forKey:@"EN"];
    
    } else {
    
        [parametros setObject:@"solicitudST" forKey:@"operacion"];
        [parametros setObject:numeroTarjeta forKey:@"numeroTarjeta"];
        [parametros setObject:numeroTelefono forKey:@"numeroTelefono"];
        [parametros setObject:companiaCelular forKey:@"companiaCelular"];
        [parametros setObject:email forKey:@"email"];
        [parametros setObject:numeroCliente forKey:@"numeroCliente"];
    
    }

    return [[Server getInstance] doGenericNetworkRequestForOperation:@"solicitudST" withParameters:parametros andResponseHandler:[[SolicitudSTResult alloc] init] forClient:client];
}


//LINEA COMENTADA METODO ANTERIOR
-(NSInteger)finalizaContratacionST:(NSString*)numeroTarjeta conNumeroTelefono:(NSString*)numeroTelefono conIum:(NSString*)ium conCompaniaCelular:(NSString*)companiaCelular conCodigoOPT:(NSString*)codigootp paraCliente:(id<ServerClient>)cliente {
  
    NSMutableDictionary* parametros = [[NSMutableDictionary alloc] init];
    
    
    
    // Validamos el Switch encriptar
    if([ServerTools isEncriptar]){
        
        NSMutableString* cadenaEncriptada = [NSMutableString stringWithFormat:@"\"operacion\":\"finalizarContratacionST\",\"numeroTarjeta\":\"%@\",\"numeroTelefono\":\"%@\",\"IUM\":\"%@\",\"companiaCelular\":\"%@\",\"codigoOTP\":\"%@\"", numeroTarjeta, numeroTelefono, ium, companiaCelular, codigootp];
        [parametros setObject:[NSString stringWithString:[EncripcionUtilities encriptarDatos:cadenaEncriptada]] forKey:@"EN"];
        
    } else {
        
        [parametros setObject:@"finalizarContratacionST" forKey:@"operacion"];
        [parametros setObject:numeroTarjeta forKey:@"numeroTarjeta"];
        [parametros setObject:numeroTelefono forKey:@"numeroTelefono"];
        [parametros setObject:ium forKey:@"IUM"];
        [parametros setObject:companiaCelular forKey:@"companiaCelular"];
        [parametros setObject:codigootp forKey:@"codigoOTP"];
        
    }

    return [[Server getInstance] doGenericNetworkRequestForOperation:@"finalizarContratacionST" withParameters:parametros andResponseHandler:[[FinalizarContratacionSTResult alloc] init] forClient:cliente];
}


//LINEA COMENTADA METODO NUEVO
-(NSInteger)finalizaContratacionSTN:(NSString*)numeroTarjeta conNumeroTelefono:(NSString*)numeroTelefono conIum:(NSString*)ium conCodigoOPT:(NSString*)codigootp paraCliente:(id<ServerClient>)cliente {
    
    NSMutableDictionary* parametros = [[NSMutableDictionary alloc] init];
    
    
    
    // Validamos el Switch encriptar
    if([ServerTools isEncriptar]){
        
        NSMutableString* cadenaEncriptada = [NSMutableString stringWithFormat:@"\"operacion\":\"finalizarContratacionST\",\"numeroTarjeta\":\"%@\",\"numeroTelefono\":\"%@\",\"IUM\":\"%@\",\"codigoOTP\":\"%@\"", numeroTarjeta, numeroTelefono, ium, codigootp];
        [parametros setObject:[NSString stringWithString:[EncripcionUtilities encriptarDatos:cadenaEncriptada]] forKey:@"EN"];
        
    } else {
        
        [parametros setObject:@"finalizarContratacionST" forKey:@"operacion"];
        [parametros setObject:numeroTarjeta forKey:@"numeroTarjeta"];
        [parametros setObject:numeroTelefono forKey:@"numeroTelefono"];
        [parametros setObject:ium forKey:@"IUM"];
        [parametros setObject:codigootp forKey:@"codigoOTP"];
        
    }
    
    return [[Server getInstance] doGenericNetworkRequestForOperation:@"finalizarContratacionST" withParameters:parametros andResponseHandler:[[FinalizarContratacionSTResult alloc] init] forClient:cliente];
}




-(NSInteger)contratacionEnrolamiento:(NSString *)numeroTelefono cveAcceso:(NSString*)cveAcceso numeroTarjeta:(NSString*)numeroTarjeta companiaCelular:(NSString*)perfilCliente:(NSString*)perfilCliente email:(NSString*)email aceptoTerminosCondiciones:(NSString*)aceptoTerminosCondiciones forClient:(id<ServerClient>)cliente {
    
    NSMutableDictionary* parametros = [[NSMutableDictionary alloc] init];
    
    [parametros setObject:@"contratacionE" forKey:@"operacion"];
    [parametros setObject:PERFIL_BASICO forKey:@"perfilCliente"];
    [parametros setObject:aceptoTerminosCondiciones forKey:@"aceptoTerminosCondiciones"];
    [parametros setObject:[APIToken getInstance].versionFlujoContratacion forKey:@"versionFlujo"];
    // Validamos el Switch encriptar
    if([ServerTools isEncriptar]){
        
        [parametros setObject:@"" forKey:@"numeroTelefono"];
        [parametros setObject:@"" forKey:@"cveAcceso"];
        [parametros setObject:@"" forKey:@"numeroTarjeta"];
        [parametros setObject:@"" forKey:@"companiaCelular"];
        [parametros setObject:@"" forKey:@"email"];
        //marcador
        //NSString* cadenaEncriptada = [NSString stringWithFormat:@"\"numeroTelefono\":\"%@\",\"cveAcceso\":\"%@\",\"numeroTarjeta\":\"%@\",\"companiaCelular\":\"%@\",\"email\":\"%@\"", numeroTelefono, cveAcceso, numeroTarjeta, /*companiaCelular*/,email];
        NSString* cadenaEncriptada = [NSString stringWithFormat:@"\"numeroTelefono\":\"%@\",\"cveAcceso\":\"%@\",\"numeroTarjeta\":\"%@\",\"email\":\"%@\"", numeroTelefono, cveAcceso, numeroTarjeta, /*companiaCelular*/email];

        
        [parametros setObject:[NSString stringWithString:[EncripcionUtilities encriptarDatos:cadenaEncriptada]] forKey:@"EN"];
        
    } else {
    
        [parametros setObject:numeroTelefono forKey:@"numeroTelefono"];
        [parametros setObject:cveAcceso forKey:@"cveAcceso"];
        [parametros setObject:numeroTarjeta forKey:@"numeroTarjeta"];
        //[parametros setObject:companiaCelular forKey:@"companiaCelular"];
        [parametros setObject:email forKey:@"email"];
        [parametros setObject:@"" forKey:@"EN"];
    }
    
    return [[Server getInstance] doGenericNetworkRequestForOperation:@"contratacionE" withParameters:parametros andResponseHandler:[[ContratacionEnrolamientoResult alloc] init] forClient:cliente];
}

-(NSInteger)sincronizeAndExportToken:(NSString*)numeroTarjeta numeroTelefono:(NSString*)aNumeroTelefono tipoSolicitud: (NSString*)aTipoSolicitud nombreToken:(NSString*)aNombreToken otp1:(NSString*)aOtp1 otp2:(NSString*)aOtp2 ium:(NSString*)aIUM versionApp:(NSString*)aVersionApp forClient:(id<ServerClient>)cliente {
    
   
    NSMutableDictionary* parametros = [[NSMutableDictionary alloc] init];
    
    [parametros setObject:@"sincronizaExportaToken" forKey:@"operacion"];
    [parametros setObject:numeroTarjeta forKey:@"numeroTarjeta"];
    [parametros setObject:aNumeroTelefono forKey:@"numeroTelefono"];
    [parametros setObject:aTipoSolicitud forKey:@"tipoSolicitud"];
    [parametros setObject:aNombreToken forKey:@"nombreToken"];
    [parametros setObject:aOtp1 forKey:@"OTP1"];
    [parametros setObject:aOtp2 forKey:@"OTP2"];
    [parametros setObject:aIUM forKey:@"IUM"];
    [parametros setObject:aVersionApp forKey:@"versionApp"];
    
    
    
    return [[Server getInstance] doGenericNetworkRequestForOperation:@"sincronizaExportaToken" withParameters:parametros andResponseHandler:[[SincronizacionExportacionTokenResult alloc] init] forClient:cliente];
}

@end
