//
//  ToolsAPIToken.m
//  APIToken
//
//  Created by Driss on 18/4/16.
//  Copyright © 2016 CGI. All rights reserved.
//

#import "ToolsAPIToken.h"

@implementation ToolsAPIToken

+ (NSString*) createPUK {
    unichar tmp[32];
    int length = 4;
    int i;
    for (i = 0; i < length; i++) {
        tmp[i] = (abs(rand()) % 10) + '0';
    }
    return [[NSString alloc] initWithCharacters: tmp length: length];
}

+ (NSString*) getTemporalSTFilePath {
    return [[[ToolsAPIToken getDocumentsDirPath] stringByAppendingPathComponent: PERSISTENCE_DIRECTORY]
            stringByAppendingPathComponent: TEMPORAL_ST_FILE_NAME];
}

+ (NSString*) getBanderasBMovilFilePath
{
    return [[[ToolsAPIToken getDocumentsDirPath] stringByAppendingPathComponent: PERSISTENCE_DIRECTORY]
            stringByAppendingPathComponent: BANDERAS_BMOVIL_FILE_NAME];
}

/*
 * Constructs and returs the App States storage file path.
 */
+ (NSString*) getAppStatusStorageFilePath
{
    return [[[ToolsAPIToken getDocumentsDirPath] stringByAppendingPathComponent: PERSISTENCE_DIRECTORY]
            stringByAppendingPathComponent: APP_STATUS_STORAGE_FILE_NAME];
}

/*
 * Constructs and returns the application's session file path. This file is used
 * by Session singleton
 */
+ (NSString*) getApplicationSessionFilePath {
    return [[[ToolsAPIToken getDocumentsDirPath] stringByAppendingPathComponent: PERSISTENCE_DIRECTORY]
            stringByAppendingPathComponent: APPLICATION_SESSION_FILE_NAME];
}


+ (NSString*) getTemporalCambioTelefonoFilePath
{
    return [[[ToolsAPIToken getDocumentsDirPath] stringByAppendingPathComponent: PERSISTENCE_DIRECTORY]
            stringByAppendingPathComponent: TEMPORAL_CAMBIO_TELEFONO_FILE_NAME];
}

+ (NSString*) getPendienteDescargaFilePath {
    return [[[ToolsAPIToken getDocumentsDirPath] stringByAppendingPathComponent: PERSISTENCE_DIRECTORY]
            stringByAppendingPathComponent: PENDIENTE_DESCARGA_FILE_NAME];
}

/*
 * Returns the application's Documents directory path
 */
+ (NSString*) getDocumentsDirPath {
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    return [paths objectAtIndex: 0];
}


@end
