//
//  APIToken.h
//  APIToken
//
//  Created by Driss on 14/4/16.
//  Copyright © 2016 CGI. All rights reserved.
//

#import <Foundation/Foundation.h>
@class BaseViewController;
@class BaseDelegate;
@class ServerResponse;

@class SofttokenApp, Autenticacion, TipoSolicitudResult, AutenticacionTokenResult;

@protocol APITokenDelegate

- (void)bmovilLaunched;
- (void)persistTipoSolicitud:(TipoSolicitudResult *)tipoSolicitud;
- (void) resetSession;
- (void) storeSession;
- (void) setApplicationActivated:(BOOL) status;
- (void) setIUMWithUser: (NSString*) aUser andActivationTime: (double) anActivationTime;
- (void) setReglasDeAutenticacion:(Autenticacion *) reglasAutenticacion;
- (NSString*) computeIUMWithUser: (NSString*) aUser andActivationTime: (double) anActivationTime;
- (void) setIumST:(NSString*)iumSt;
- (void) setUserActivationST:(NSString*) userActivationST;
- (void) setUserNumTarjetaST:(NSString*) numTarjetaST;
- (void) setUserCompaniaST:(NSString*) companiaST;
- (void) setCveReactivacionST:(NSString*) cveReactivacionST;
- (void) setUserName:(NSString*) userName;
- (void) setInvalidSessionStatus;
- (void) setValidSessionStatus;
- (void) setNoneSessionStatus;
- (void) setEmailST:(NSString*) emailST;
- (void) invokeAutomaticLogout;

@end


@protocol APITokenDataSource

- (NSString *) nombreCliente;
- (NSString *) getIUM;
- (NSString *) compania;
- (NSString *) email;
- (NSString *) clientNumber;
- (BOOL) isAppActivated;
- (TipoSolicitudResult *) tipoSolicitud;
- (NSString *) userName;
- (double) activationTime;
- (NSString *) getIumST;
- (AutenticacionTokenResult*) getAutenticacionToken;
- (NSString *) getCveReactivacionST;
- (NSString*) getSemillaST;
- (NSString*) getAppName;

@end


@interface APIToken : NSObject

@property (weak) id <APITokenDelegate> apiTokenDelegate;
@property (weak) id <APITokenDataSource> apiTokenDataSource;

@property(nonatomic,strong)SofttokenApp* softApp;

@property (nonatomic) double appVersion;
@property (nonatomic, retain) NSString* appName;
@property (nonatomic) double appVersionConsulta;
@property (nonatomic, retain) NSString* versionFlujoContratacion;


+ (APIToken*) getInstance;

- (NSString*) generaOTPTiempo;
- (BOOL) mtCoreFactoryResetDatabaseWithPath:(NSString*) path;
- (void)showPantallaInicialASMWithStatusST:(BOOL) statusST;
- (void)showSoftTokenWithStatus:(BOOL)status;
- (void)showSoftTokenWithAutenticacionST;
- (void)initSofttokenAppWithStatus:(BOOL)status;
- (BOOL)leeEstatusSTActivado;

- (void)guardaEstatusTokenMovil:(BOOL)status;


- (NSString*) leerDeKeyChain:(NSString *)clave;
- (void) guardarEnKeyChain:(NSString *)valor clave:(NSString *)clave;
- (void) borrarDeKeyChain:(NSString*)clave;

- (BOOL)isIVR;
- (void)guardarEstatusIVR:(BOOL)estatusIVR;

/* Métodos que son invocados desde fuera del API */
- (void) invokeContratacionEnrolamientoConDelegate:(BaseDelegate*)delegate
                                  conNumeroCelular:(NSString*)numeroCelular
                                  conNumeroTarjeta:(NSString*)numeroTarjeta
                                  conNumeroCliente:(NSString*)numeroCliente
                                   conEmailCliente:(NSString*)emailCliente
                                  conNombreCliente:(NSString*)nombreCliente
                                conCompaniaCelular:(NSString*)companiaCelular
                                     conVersionApp:(NSString*)versionApp;

- (void) invokeOperacionIngresoConDelegate:(BaseDelegate*)delegate
                          conNumeroCelular:(NSString*)numeroCelular
                          conNumeroTarjeta:(NSString*)numeroTarjeta
                        andCompaniaCelular:(NSString*)companiaCelular;

- (void) analyzeServerResponseConsultaTarjetaSTConTipoSolicitudResult:(TipoSolicitudResult*)tsResult
                                                      andBaseDelegate:(BaseDelegate*)delegate;

- (void) analyzeServerResponse:(ServerResponse*)aServerResponse;


- (void) showConfirmacionSTConTipoSolicitud:(NSString*)tipoSolicitud;
+ (void)resetToken;

@end
