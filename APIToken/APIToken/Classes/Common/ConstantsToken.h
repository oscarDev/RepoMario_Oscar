//
//  ConstantsToken.h
//  Bancomer
//
//  Created by Alberto Martínez Fernández on 27/01/16.
//  Copyright (c) 2015 CGI. All rights reserved.
//


#define PERFIL_AVANZADO                 @"MF03"

#define ST_MSG_AYUDA_CONTRATACION               @"Aplicación para generar códigos de seguridad.\nSi ya cuentas con Token de teclado numérico, no es necesario contratar Token móvil, pero si deseas sustituirlo acude a una sucursal con tu Token físico, lo necesitarás para activarlo"

#define ST_HEADER_ACTIVACION                    @"Activación"
#define ST_CHALLENGE_ERROR                      @"El campo número de tarjeta es de 5 dígitos, por favor verifica e intenta de nuevo"


//ST
#define CHALLENGE_ST_REFERENCE_LENGTH                                        5
#define LAT_MARGIN                              20
#define DISTANCE_BTW_COMP                       20

#define SCREEN_WIDTH                            320
#define SCREEN_HEIGHT                           480
#define SCREEN_HEIGHT_I5                        568
#define HEADER_HEIGHT                           81
#define LBL_HEIGHT                              21
#define COMPONENT_WIDTH                         280
#define CORNER_RADIOUS                          3
#define BORDER_WIDTH                            1.0
#define CVE_ACTIVACION_ST_REFERENCE_LENGTH                                  10

/*
 * Define el nombre de la fuente utilizada en la aplicación
 */
#define HELVETICA_NEUE_LT_STD_55_ROMAN                                  @"HelveticaNeueLTStd-Roman"
#define FUENTE_CUERPO                           [UIFont fontWithName:HELVETICA_NEUE_LT_STD_55_ROMAN size:16.0F];
#define FUENTE_CUERPO_2                         [UIFont fontWithName:HELVETICA_NEUE_LT_STD_55_ROMAN size:12.0F];



#define ST_INGRESADATOS_PHONENUMBER_ERROR       @"El campo número celular es de 10 dígitos, por favor verifica e intenta de nuevo."

#define ST_INGRESADATOS_CARDNUMBER_ERROR        @"El campo número de tarjeta es de 16 dígitos, por favor verifica e intenta de nuevo"

#define ST_NIP_ERROR                            @"El campo NIP es de 4 dígitos, por favor verifica e intenta de nuevo"


#define ST_CVV_ERROR  @"El campo CVV es de 3 dígitos, por favor verifica e intenta de nuevo"

#define ST_TEXTO_VALIDACION_ALERTAS_00          @"Para continuar con la contratación de tu Token móvil es necesario que también actives tu servicio de Bancomer móvil en tu celular."


#define LABEL_INFORMATION                   @"¡Importante!"
#define ERROR_FORMAT                        @"Error de formato"


#define ST_CLAVEACESSO_SEGURO_ERROR         @"El campo clave de acceso seguro es de 8 dígitos, por favor verifica e intenta de nuevo"
#define ST_CLAVE_ACTIVACION_ERROR           @"El campo clave de activación es de 10 dígitos, por favor verifica e intenta de nuevo"

#define ST_TEXTO_SWITCH_ENROLAMIENTO_APAGADO          @"Por ahora solo puedes contratar Bancomer móvil sin Token, para concluir tu activación vuelve a ingresar a Banca móvil"
#define COMMON_ACCEPT                       @"Aceptar"
#define COMMON_CANCEL                       @"Cancelar"

#define ST_TEXTO_VALIDACION_ALERTAS_01                @"Para contratar este servicio es necesario contar con Alertas Bancomer, que por tu seguridad, puedes activar en cualquier cajero automático Bancomer"

#define ST_TEXTO_VALIDACION_ALERTAS_02_03             @"Para continuar con la contratación, es necesario que el número celular ingresado y la compañía telefónica coincidan con el registrado en Alertas Bancomer con terminación %@. Puedes actualizar tu número de Alertas en cualquier cajero automático Bancomer"

#define ST_TEXTO_VALIDACION_ALERTAS_04                @"El número celular registrado en Banca móvil no coincide con el registrado en Alertas Bancomer. ¿Deseas que tu número de Banca móvil sea sustituido por el de Alertas?"

#define ST_TEXTO_VALIDACION_ALERTAS_05                @"La compañía celular registrada en Banca móvil no coincide con el registrado en Alertas Bancomer. ¿Deseas que tu compañía celular de Banca móvil sea sustituido por el de Alertas?"
#define ST_MSG_ALERT_BIENVENIDA_BANCA_MOVIL_TOKEN_MOVIL @"La contratación y activación de tus servicios de Bancomer móvil y Token móvil fue realizada con éxito. Ahora podrás hacer uso de ellos en tu celular."

#define TEXTO_ALERT_ROLLBACK      @"No se ha logrado realizar la activación de tus Servicios Banca Movil y Token Movil"

#define ST_TITLE_ALERT_BIENVENIDA               @"¡Felicidades!"
#define ST_MSG_ALERT_BIENVENIDA_TOKEN_MOVIL     @"La activación de tu Token móvil fue realizada con éxito. Ahora podrás hacer uso de él en tu celular."
#define ST_MSG_ALERT_BIENVENIDA_REACTIVA_2X1    @"Se ha realizado la activación de tus Servicios Banca Movil y Token Movil"

#define SUITEMENU_NOT_REACHABLE_CONNECTION      @"Para ingresar a Bancomer móvil requieres una conexión a internet."
/*
 * Tableta colores guia de estilos.
 */
#define COLOR_1ER_AZUL                                      [UIColor colorWithRed:9.0f/255.0f green:79.0f/255.0f blue:164.0f/255.0f alpha:1.0f]
#define COLOR_2DO_AZUL                                      [UIColor colorWithRed:0.0f/255.0f green:110.0f/255.0f blue:193.0f/255.0f alpha:1.0f]
#define COLOR_3ER_AZUL                                      [UIColor colorWithRed:0.0f/255.0f green:158.0f/255.0f blue:229.0f/255.0f alpha:1.0f]
#define COLOR_4TO_AZUL                                      [UIColor colorWithRed:82.0f/255.0f green:188.0f/255.0f blue:236.0f/255.0f alpha:1.0f]
#define COLOR_5TO_AZUL                                      [UIColor colorWithRed:137.0f/255.0f green:209.0f/255.0f blue:243.0f/255.0f alpha:1.0f]
#define COLOR_6TO_AZUL                                      [UIColor colorWithRed:181.0f/255.0f green:229.0f/255.0f blue:249.0f/255.0f alpha:1.0f]

#define COLOR_MAGENTA                                       [UIColor colorWithRed:200.0f/255.0f green:23.0f/255.0f blue:94.0f/255.0f alpha:1.0f]
#define COLOR_NARANJA                                       [UIColor colorWithRed:246.0f/255.0f green:137.0f/255.0f blue:30.0f/255.0f alpha:1.0f]
#define COLOR_AMARILLO                                      [UIColor colorWithRed:253.0f/255.0f green:189.0f/255.0f blue:44.0f/255.0f alpha:1.0f]
#define COLOR_VERDE                                         [UIColor colorWithRed:134.0f/255.0f green:200.0f/255.0f blue:45.0f/255.0f alpha:1.0f]
#define COLOR_VERDE_LIMON                                   [UIColor colorWithRed:183.0f/255.0f green:194.0f/255.0f blue:4.0f/255.0f alpha:1.0f]
#define COLOR_TURQUESA                                      [UIColor colorWithRed:62.0f/255.0f green:182.0f/255.0f blue:187.0f/255.0f alpha:1.0f]

#define COLOR_GRIS_TEXTO_1                                  [UIColor colorWithRed:60.0f/255.0f green:60.0f/255.0f blue:60.0f/255.0f alpha:1.0f]
#define COLOR_GRIS_TEXTO_2                                  [UIColor colorWithRed:77.0f/255.0f green:77.0f/255.0f blue:77.0f/255.0f alpha:1.0f]
#define COLOR_GRIS_TEXTO_3                                  [UIColor colorWithRed:102.0f/255.0f green:102.0f/255.0f blue:102.0f/255.0f alpha:1.0f]
#define COLOR_GRIS_TEXTO_4                                  [UIColor colorWithRed:176.0f/255.0f green:176.0f/255.0f blue:176.0f/255.0f alpha:1.0f]
#define COLOR_GRIS_FONDO                                    [UIColor colorWithRed:245.0f/255.0f green:245.0f/255.0f blue:245.0f/255.0f alpha:1.0f]
#define COLOR_AZUL_FONDO                                    [UIColor colorWithRed:240.0f/255.0f green:249.0f/255.0f blue:254.0f/255.0f alpha:1.0f]

#define COLOR_GRIS_BORDE_TABLA                              [UIColor colorWithRed:128.0f/255.0f green:128.0f/255.0f blue:128.0f/255.0f alpha:1.0f]


#define CORNER_RADIOUS                          3
#define BORDER_WIDTH                            1.0
#define CARD_NUMBER_LENGTH													16
#define NIP_REFERENCE_LENGTH                                                4
#define CVV_REFERENCE_LENGTH                                                3
#define CVE_AS_REFERENCE_LENGTH                                             8

#define CONTRATAR_BMOVIL_KEY                                                @"contratarBMovil"
#define CAMBIO_DE_PERFIL_KEY                                                @"cambioPerfil"
#define TOKEN_MOVIL                             @"Token Móvil"

#define TEMPORALST_NUM_CELULAR                                              @"numCelular"
#define TEMPORALST_NUM_TARJETA                                              @"numeroTarjeta"
#define TEMPORALST_COMPANIA_CELULAR                                         @"companiaCelular"
#define TEMPORALST_CONTRASENA                                               @"contrasena"
#define TEMPORALST_CORREO                                                   @"correo"
#define TEMPORALST_PERFIL                                                   @"perfil"
#define CONTRATACION_2X1                                                    @"contratacion2x1"
#define CAMBIO_CELULAR_KEY                                                  @"cambioCelular"
#define TARJETA_TEMPORAL_CAMBIO_TELEFONO_KEY                                @"tarjeta"

/*
 Indicadores de solicitud de softtoken
 */
#define INDICADOR_SOLICITUD_TOKEN_NUEVO                                     @"N"
#define INDICADOR_SOLICITUD_REACTIVACION                                    @"R"
#define INDICADOR_SOLICITUD_SUSTITUCION                                     @"S"
#define INDICADOR_SOLICITUD_NO_EXISTE_SOLICITUD                             @"X"

/*
 Indicadores de estatus de validación (Consulta Tarjeta ST)
 */
#define ESTATUS_VALIDACION_BMOVIL_CONTRATADO                                @"00"
#define ESTATUS_VALIDACION_SIN_ALERTAS_CONTRATADAS                          @"01"
#define ESTATUS_VALIDACION_TELEFONO_INGRESADO_NO_COINCIDE                   @"02"
#define ESTATUS_VALIDACION_COMPANIA_INGRESADA_NO_COINCIDE                   @"03"
#define ESTATUS_VALIDACION_TELEFONO_ASOCIADO_NO_COINCIDE                    @"04"
#define ESTATUS_VALIDACION_COMPANIA_ASOCIADA_NO_COINCIDE                    @"05"
#define ESTATUS_VALIDACION_EFECTUAR_SOLICITUD_SOFTTOKEN                     @"06"


#define ESTATUS_A1    @"A1" //Activo
#define ESTATUS_PE    @"PE"
#define ESTATUS_PA    @"PA"
#define ESTATUS_BI    @"BI" //“Desbloqueo de Contraseñas”
#define ESTATUS_S4    @"S4"
#define ESTATUS_C4    @"C4"
#define ESTATUS_CN    @"CN"
#define ESTATUS_PS    @"PS"
#define ESTATUS_II    @"II"
#define ESTATUS_PB    @"PB" //Bloqueo de nip o cvv
#define ESTATUS_NE    @"NE"

#define BMOVIL_ESTATUS_KEY                                                  @"bmovilActivado"
#define SOFTOKEN_ESTATUS_KEY                                                @"softTokenActivado"

#define kStatusST                           @"softokenActivado"

#define TIPO_INSTRUMENTO_OCRA           @"T6"
#define TIPO_INSTRUMENTO_DP270          @"T3"
#define TIPO_INSTRUMENTO_SOFTOKEN       @"S1"

#define PENDIENTE_DESCARGA_NUMERO_TARJETA_KEY                               @"NumeroTarjeta"
#define PENDIENTE_DESCARGA_NUMERO_TELEFONO_KEY                              @"NumeroTelefono"
#define PENDIENTE_DESCARGA_NUMERO_CLIENTE_KEY                               @"NumeroCliente"
#define PENDIENTE_DESCARGA_TIPO_SOLICITUD_KEY                               @"TipoSolicitud"
#define PENDIENTE_DESCARGA_NOMBRE_TOKEN_KEY                                 @"NombreToken"
#define PENDIENTE_DESCARGA_NUMERO_SERIE_KEY                                 @"NumeroSerie"
#define PENDIENTE_DESCARGA_CORREO_KEY                                       @"Correo"
#define PENDIENTE_DESCARGA_COMPANIA_CELULAR_KEY                             @"CompaniaCelular"

/*
 * Constantes usadas en simulación
 */
static NSString *const AutenticarTokenResponseKey = @"AutenticarTokenResponse";
static NSString *const SincronizarTokenResponseKey = @"SincronizarTokenResponse";
static NSString *const ExportarTokenResponseKey = @"ExportarTokenResponse";
static NSString *const ConsultarTipoSolicitudResponseKey = @"ConsultarTipoSolicitudResponse";
static NSString *const CambiarTelefonoAsociadoSTResponseKey = @"CambiarTelefonoAsociadoSTResponse";
static NSString *const SolicitudSTResponseKey = @"SolicitudSTResponse";
static NSString *const FinalizarContratacionSTResponseKey = @"FinalizarContratacionSTResponse";
static NSString *const ContratacionEnrolamientoResponseKey = @"ContratacionEnrolamientoResponse";
static NSString *const SincronizarExportarTokenResponseKey = @"SincronizarExportarTokenResponse";
static NSString *const OkKey = @"OK";
static NSString *const ErrorKey = @"ERROR";