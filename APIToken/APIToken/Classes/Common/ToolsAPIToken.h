//
//  ToolsAPIToken.h
//  APIToken
//
//  Created by Driss on 18/4/16.
//  Copyright © 2016 CGI. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * Defines persistence directory (special directory inside Documents directory)
 */
#define PERSISTENCE_DIRECTORY									@"Persistence"
#define BANDERAS_BMOVIL_FILE_NAME                               @"BanderasBMovil"
/**
 * Defines the App Status storage file name (stored inside persistence directory)
 */
#define APP_STATUS_STORAGE_FILE_NAME							@"EstatusAplicaciones"

/**
 * Defines the application's session file name (stored inside persistence directory)
 */
#define APPLICATION_SESSION_FILE_NAME							@"AppSession"

#define TEMPORAL_CAMBIO_TELEFONO_FILE_NAME                      @"TemporalCambioTelefono"
#define TEMPORAL_ST_FILE_NAME                                   @"TemporalST"
#define PENDIENTE_DESCARGA_FILE_NAME                            @"PendienteDescarga"



@interface ToolsAPIToken : NSObject

+ (NSString*) createPUK;
+ (NSString*) getTemporalSTFilePath;
+ (NSString*) getBanderasBMovilFilePath;
/*
 * Constructs and returs the App States storage file path.
 */
+ (NSString*) getAppStatusStorageFilePath;

/*
 * Constructs and returns the application's session file path. This file is used
 * by Session singleton
 */
+ (NSString*) getApplicationSessionFilePath;


+ (NSString*) getTemporalCambioTelefonoFilePath;

+ (NSString*) getPendienteDescargaFilePath;
@end
