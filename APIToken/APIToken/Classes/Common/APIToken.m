//
//  APIToken.m
//  APIToken
//
//  Created by Driss on 14/4/16.
//  Copyright © 2016 CGI. All rights reserved.
//

#import "APIToken.h"
#import <mtcore-ios/MTCoreFactory.h>
#import "SofttokenApp.h"
#import "KeyChainHandler.h"
#import "GeneraOTPSTDelegate.h"
#import "ContratacionSTDelegate.h"
#import "SoftokenViewsController.h"
#import "Softtoken.h"
#import "ConstantsToken.h"
#import "ToolsAPIToken.h"

@implementation APIToken {
    
    KeyChainHandler *keychainHandler;
    ContratacionSTDelegate *_contratacionSTDelegate;
}

#pragma mark Singleton Methods

+ (APIToken*) getInstance {
    
    static APIToken *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (id)init {
    if (self = [super init]) {
         keychainHandler = [[KeyChainHandler alloc] init];
    }
    return self;
}

- (BOOL) mtCoreFactoryResetDatabaseWithPath:(NSString*) path {
    return [MTCoreFactory resetDatabase: path];
    
}

- (NSString*) generaOTPTiempo {
    return [GeneraOTPSTDelegate generaOTPTiempo];
}

-(void)showSoftTokenWithStatus:(BOOL)status {
    if(_softApp) {
        [_softApp softLaunchedwithStatus:status];
        
    } else {
        _softApp = [[SofttokenApp alloc] initWithStatus:status];
    }
}

-(void)showSoftTokenWithAutenticacionST {
    _softApp = [[SofttokenApp alloc] initAutenticacionST];
}

-(void)showPantallaInicialASMWithStatusST:(BOOL) statusST
{
    NSLog(@"ASM elegido");
    [self showSoftTokenWithStatus:statusST];
}

- (void) initSofttokenAppWithStatus:(BOOL)status {
    _softApp = [[SofttokenApp alloc] initWithStatus:status];
}

-(BOOL)leeEstatusSTActivado{
    
   BOOL isSofttokenActivado;
    
    // Fichero de SuiteBMóvil
    NSString* documentsDirectory = [[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject] path];
    NSFileManager* manager = [NSFileManager defaultManager];
    NSString* estatusSTPath = [documentsDirectory stringByAppendingPathComponent:@"EstatusSoftToken"];
    
    // Fichero de Enrolamiento BMóvil
    NSString* estatusAplicacionesFilePath = [ToolsAPIToken getAppStatusStorageFilePath];
    NSMutableDictionary* estatusAplicacionesDictionary = [[NSMutableDictionary alloc] initWithDictionary:[NSDictionary dictionaryWithContentsOfFile: estatusAplicacionesFilePath]];
    
    if([manager fileExistsAtPath: estatusSTPath]){
        // Había instalada una versión de SuiteBMovil
        NSDictionary* estatusST = [NSKeyedUnarchiver unarchiveObjectWithFile:estatusSTPath];
        NSNumber* valor = [estatusST objectForKey:kStatusST];
        isSofttokenActivado = [valor boolValue];
        
        // Guardo el estatus en el fichero de Enrolamiento BMóvil
        [estatusAplicacionesDictionary setObject:[NSNumber numberWithBool:isSofttokenActivado] forKey:SOFTOKEN_ESTATUS_KEY];
        [estatusAplicacionesDictionary writeToFile: estatusAplicacionesFilePath atomically: YES];
        
        //Borro el fichero de Suite BMóvil
        [manager removeItemAtPath: estatusSTPath error: NULL];
        
    } else {
        isSofttokenActivado = [[estatusAplicacionesDictionary objectForKey:SOFTOKEN_ESTATUS_KEY] boolValue];
    }
    
    return isSofttokenActivado;
}


-(NSString*) leerDeKeyChain:(NSString *)clave {

    return [keychainHandler leerDeKeyChain:clave];

}

- (void) guardarEnKeyChain:(NSString *)valor clave:(NSString *)clave {

    [keychainHandler guardarEnKeyChain:valor clave:clave];
}

- (void) borrarDeKeyChain:(NSString*)clave {
    [keychainHandler borrarDeKeyChain:clave];
}

- (BOOL)isIVR {
    return [ContratacionSTDelegate isIVR];
}
- (void)guardarEstatusIVR:(BOOL)estatusIVR {
    [ContratacionSTDelegate guardarEstatusIVR:estatusIVR];
}

- (void) inicializarControlador {
    if (_contratacionSTDelegate == nil) {
        _contratacionSTDelegate = [[ContratacionSTDelegate alloc] init];
    }
    
    if (_contratacionSTDelegate.objetoSofttoken == nil) {
        _contratacionSTDelegate.objetoSofttoken = [[Softtoken alloc] init];
    }
    
    if (_softApp == nil) {
        _softApp = [[SofttokenApp alloc] initWithStatus:NO andMostrarPantallaToken:NO];
    }
    
    _softApp.controladorSofttoken.delegateContratacionST = _contratacionSTDelegate;

}

- (void) invokeContratacionEnrolamientoConDelegate:(BaseDelegate*)delegate
                                  conNumeroCelular:(NSString*)numeroCelular
                                  conNumeroTarjeta:(NSString*)numeroTarjeta
                                  conNumeroCliente:(NSString*)numeroCliente
                                   conEmailCliente:(NSString*)emailCliente
                                  conNombreCliente:(NSString*)nombreCliente
                                conCompaniaCelular:(NSString*)companiaCelular
                                     conVersionApp:(NSString*)versionApp {
    
    [self inicializarControlador];
    
    _contratacionSTDelegate.objetoSofttoken.numCelular = numeroCelular;
    _contratacionSTDelegate.objetoSofttoken.numTarjeta = numeroTarjeta;
    _contratacionSTDelegate.objetoSofttoken.numeroCliente = numeroCliente;
    _contratacionSTDelegate.objetoSofttoken.correo = emailCliente;
    _contratacionSTDelegate.objetoSofttoken.nombreCliente = nombreCliente;
    _contratacionSTDelegate.objetoSofttoken.companiaCelular = companiaCelular;
    _contratacionSTDelegate.objetoSofttoken.versionApp =versionApp;
    
    [_contratacionSTDelegate invokeContratacionEnrolamiento:delegate];
}

- (void) invokeOperacionIngresoConDelegate:(BaseDelegate*)delegate
                          conNumeroCelular:(NSString*)numeroCelular
                          conNumeroTarjeta:(NSString*)numeroTarjeta
                        andCompaniaCelular:(NSString*)companiaCelular {
    
    [self inicializarControlador];
    
    _contratacionSTDelegate.objetoSofttoken.numCelular = numeroCelular;
    _contratacionSTDelegate.objetoSofttoken.numTarjeta = numeroTarjeta;
    _contratacionSTDelegate.objetoSofttoken.companiaCelular = companiaCelular;
    
    [_contratacionSTDelegate invokeOperacionIngreso:delegate andNumCelular:numeroCelular andTarjeta:numeroTarjeta andCompaniaCelular:companiaCelular];
}

- (void) analyzeServerResponseConsultaTarjetaSTConTipoSolicitudResult:(TipoSolicitudResult*)tsResult
                                                      andBaseDelegate:(BaseDelegate*)delegate {
    [self inicializarControlador];
    
    UINavigationController *uiNavController = (UINavigationController*)[[[UIApplication sharedApplication] delegate] window].rootViewController;
    BaseViewController *view = uiNavController.viewControllers.lastObject;
    //view.controladorSofttoken = _softApp.controladorSofttoken;
    _softApp.controladorSofttoken.delegateContratacionST.viewController = view;
    
    [_softApp.controladorSofttoken.delegateContratacionST analyzeServerResponseConsultaTarjetaST:tsResult conCliente:delegate];
}

- (void) analyzeServerResponse:(ServerResponse*)aServerResponse {
    [_softApp.controladorSofttoken.delegateContratacionST analyzeServerResponse:aServerResponse];
}



- (void) showConfirmacionSTConTipoSolicitud:(NSString*)tipoSolicitud {
    
    [self inicializarControlador];
    
    Softtoken *objetoSoftToken =[[Softtoken alloc] init];
    objetoSoftToken.tipoSolicitud = tipoSolicitud;
    
    [_softApp.controladorSofttoken showConfirmacionST:objetoSoftToken];
}

+ (void)resetToken {
    [ContratacionSTDelegate borrarToken];
}

-(void)guardaEstatusTokenMovil:(BOOL)status {

    [ContratacionSTDelegate guardaEstatusTokenMovil:status];

}

@end
