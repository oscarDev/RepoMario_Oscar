//
//  GeneraOTPSTViewController.m
//  Bancomer
//
//  Created by Francisco.Garcia on 28/05/13.
//
//
#import <UIKit/UIKit.h>
#import "GeneraOTPSTViewController.h"
#import "GeneraOTPSTDelegate.h"
#import "Constants.h"
#import "ConstantsToken.h"
#import "SoftokenViewsController.h"
#import "MBProgressHUD.h"
#import "SofttokenApp.h"
#import "APIToken.h"


#define GENERA_CLV              @"GENERA"
#define GENERA_TXT              @"Generar código de operación"
#define REGSITRA_CLV            @"REGISTRA"
#define REGISTRA_TXT            @"Registrar cuenta/tarjeta/teléfono"
#define TABLE_HEADER_TEXT       @"Selecciona el tipo de operación"
#define LINK_BORRAR_TEXT        @"Borrar datos asociados"
#define IMG_ASM                 @"IoIcAcceso.png"

// Modificación 50683
#define ERROR_CAMARA            @"Camara no disponible"
#define ERROR_CAMARA_2          @"Se cerro por tiempo de espera."
#define ERROR_CAMARA_3          @"Codigo erroneo"
#define CAMARA_NAVIGATION_HEIGHT 50
#define LOGO_IMG                @"logotipoBBVABancomer.png"
#define ATRAS_IMG               @"btn-15.png"
#define ATRAS_FRAME             CGRectMake(4, 18, 64, 28)
#define TIME_INTERVAL           30
// Termina Modificación 50683
#define CODIGO_QR_INVALIDO      @"Código QR inválido"

@interface GeneraOTPSTViewController ()

@end

@implementation GeneraOTPSTViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [self setBotones];
    [self setLinkBorrarDatos];
    [super viewDidLoad];
    _selectedPurchaseOptionIndex = 0;
    [self setHeaderTitle: TOKEN_MOVIL];
	[self setHeaderImage:[UIImage imageNamed:IMG_ASM]];
    [self showHeader:YES];
    
    
}

-(void)setBotones
{
    UILabel *labelCuentaDeposito = [[UILabel alloc]init];
    UIFont *fuenteCuerpo = [UIFont fontWithName:HELVETICA_NEUE_LT_STD_55_ROMAN size:17.5F];
    labelCuentaDeposito.font = fuenteCuerpo;
    labelCuentaDeposito.backgroundColor = [UIColor clearColor];
    labelCuentaDeposito.text =@"Selecciona tipo de operación";
    labelCuentaDeposito.textColor = COLOR_1ER_AZUL;
    labelCuentaDeposito.numberOfLines = 0;
    [labelCuentaDeposito sizeToFit];
    [labelCuentaDeposito setFrame:CGRectMake(20,20,320,25)];
    [self.view addSubview:labelCuentaDeposito];

    
    UIButton *btnGenerar =[[UIButton alloc] init];
    
    [btnGenerar setFrame:CGRectMake(20,60,285,60)];
    [btnGenerar setImage:[UIImage imageNamed:@"IoBTGenerar.png"] forState:UIControlStateNormal];
    [btnGenerar addTarget:self action:@selector(generar) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnGenerar];
    
    UIButton *btnRegistrar =[[UIButton alloc] init];
    //el valor 280 (190 por default)
    [btnRegistrar setFrame:CGRectMake(20,140,285,60)];
    [btnRegistrar setImage:[UIImage imageNamed:@"IoBTSoftregistrar.png"] forState:UIControlStateNormal];
    [btnRegistrar addTarget:self action:@selector(registrar) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:btnRegistrar];

    // Modificación 50683
    UIButton *btnQR =[[UIButton alloc] init];
    [btnQR setFrame:CGRectMake(20,220,285,60)];
    [btnQR setImage:[UIImage imageNamed:@"ios_btn_QR.png"] forState:UIControlStateNormal];
    [btnQR addTarget:self action:@selector(generaCodigoOperacionQR) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:btnQR];
    
    
}

-(void)setLinkBorrarDatos
{
    _lblBorrar.text = LINK_BORRAR_TEXT;
    _lblBorrar.frame = CGRectMake(76,300, COMPONENT_WIDTH, LBL_HEIGHT);
    _lblBorrar.font = FUENTE_CUERPO;
    _lblBorrar.backgroundColor = [UIColor clearColor];
    _lblBorrar.textColor = COLOR_MAGENTA;
    [_lblBorrar sizeToFit];
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = COLOR_MAGENTA;
    lineView.frame = CGRectMake(_lblBorrar.frame.origin.x , _lblBorrar.frame.origin.y + _lblBorrar.frame.size.height -3, _lblBorrar.frame.size.width, 1);
    [self.view addSubview:lineView];
    _btnBorrar.frame = _lblBorrar.frame;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewDidUnload {
    [self setLblBorrar:nil];
    [self setBtnBorrar:nil];
    [super viewDidUnload];
}

#pragma mark -

-(void) generar
{
    _generaOTPSTDelegate.seleccionToken = claveSimple;
    [self startActivityIndicator];
    [_generaOTPSTDelegate muestraOTP:[GeneraOTPSTDelegate generaOTPTiempo]];
    [self stopActivityIndicator];

}
-(void) registrar
{
    _generaOTPSTDelegate.seleccionToken = claveConSemilla;
    [[APIToken getInstance].softApp.controladorSofttoken showRegistraCuentaST];
  //  [self.controladorSofttoken showRegistraCuentaST];
}


- (IBAction)borrarDatosAsociados:(id)sender {
    NSLog(@"Borrar datos asociados touch");
    [_generaOTPSTDelegate  muestraAlertConfirmacionBorrarDatos];
}

/**
 * Acción de botón para generar una OTP Transaction Signing apartir de un Código QR. -Modificación 50683
 */
- (void)generaCodigoOperacionQR{
    
    if(![ZBarReaderController isSourceTypeAvailable:
         UIImagePickerControllerSourceTypeCamera]) {
        [self showErrorMessage:ERROR_CAMARA];
        return;
    }
    reader = [ZBarReaderViewController new];
   
    reader.readerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, [UIScreen mainScreen].bounds.size.height );
    reader.readerView.torchMode = 2;
    reader.readerDelegate = self;
    reader.showsZBarControls = NO;
    reader.showsHelpOnFail = NO;
    [reader setTracksSymbols: YES];
    /**/
    UIView *navigation = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, CAMARA_NAVIGATION_HEIGHT)];
    navigation.backgroundColor = [UIColor whiteColor];
    [reader.readerView addSubview:navigation];
    UIImageView *bancomerLogo = [[UIImageView alloc] initWithFrame:CGRectMake(0, 8, SCREEN_WIDTH, CAMARA_NAVIGATION_HEIGHT)];
    bancomerLogo.image = [UIImage imageNamed:LOGO_IMG];
    [navigation addSubview:bancomerLogo];
    UIButton *atras = [UIButton buttonWithType:UIButtonTypeCustom];
    [atras addTarget:self action:@selector(cancelBarcodeReader:) forControlEvents:UIControlEventTouchDown];
    UIImage *buttonBackground = [UIImage imageNamed:ATRAS_IMG];
    [atras setBackgroundImage:buttonBackground forState:UIControlStateNormal];
    atras.frame = ATRAS_FRAME;
    [navigation addSubview:atras];
    
    [self presentViewController:reader
                       animated:NO
                     completion:^{
                         
                         timeoutReader =  [NSTimer scheduledTimerWithTimeInterval:TIME_INTERVAL
                                                                           target:self
                                                                         selector:@selector(cancelBarcodeReader:)
                                                                         userInfo:nil
                                                                          repeats:NO];
                     }];
}

/**
 * Cancelar camara -Modificación 50683
 */
-(void)cancelBarcodeReader:(id)sender{
    if([sender isKindOfClass:[NSTimer class]]){
        [self showErrorMessage:ERROR_CAMARA_2];
    }
    
    if(timeoutReader && timeoutReader.isValid)
        [timeoutReader invalidate];
    
    if(reader && reader == self.presentedViewController){
        [reader.readerView stop];
        [reader dismissViewControllerAnimated:YES completion:nil];
        
    }
    
    reader.readerView.torchMode = 0;
}

/**
 *  Leer codigo - Modificación 50683
 */
- (void) imagePickerController: (UIImagePickerController*) readerZ didFinishPickingMediaWithInfo: (NSDictionary*) info {
    [timeoutReader invalidate];
    timeoutReader = nil;
    id<NSFastEnumeration> results = [info objectForKey: ZBarReaderControllerResults];
    
    ZBarSymbol *symbol = nil;
    for(symbol in results)
        break;
    NSString* qrCode = symbol.data;
    if(qrCode){
        NSLog(@"El Codigo QR leído es: %@", qrCode);
        [self startActivityIndicator];
        
        NSMutableDictionary *datosQR = [GeneraOTPSTDelegate generaOperacionTransactionSigning:qrCode];
        
        NSString *tipoOperacion =[[NSString alloc]initWithFormat:@"%@",[datosQR objectForKey:@"0x07"]];
        
        if (datosQR != nil && ([tipoOperacion isEqualToString:@"01"] || [tipoOperacion isEqualToString:@"02"] || [tipoOperacion isEqualToString:@"03"] || [tipoOperacion isEqualToString:@"04"] || [tipoOperacion isEqualToString:@"05"])) {
            [_generaOTPSTDelegate muestraVistaAltaRegistro:datosQR qrLeido:qrCode]; //SofftokenOptica ODT2 -KSRB
            [self stopActivityIndicator];
        } else {
            [self stopActivityIndicator];
            [timeoutReader invalidate];
            [reader.readerView stop];
            [reader dismissViewControllerAnimated:YES completion:nil];
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            
            // Configure for text only and offset down
            hud.mode = MBProgressHUDModeText;
            hud.labelText = @"Código QR inválido";
            hud.margin = 10.f;
            hud.yOffset = 120.f;
            hud.removeFromSuperViewOnHide = YES;
            
            [hud hide:YES afterDelay:3];
        }
        
    } else {
        [self showErrorMessage:ERROR_CAMARA_3];
    }
    
    [readerZ dismissViewControllerAnimated:YES completion:nil];
}

@end
