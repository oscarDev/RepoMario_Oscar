//
//  RegistraCuentaSTViewController.m
//  SuiteBancomer
//
//  Created by JuanRa Marin  on 10/10/13.
//  Copyright (c) 2013 GoNet. All rights reserved.
//

#import "RegistraCuentaSTViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "GeneraOTPSTDelegate.h"
#import "Constants.h"
#import "ConstantsToken.h"

#define LBL_TITULO_TEXT                 @"Ingresa datos"
#define LBL_INFO_TEXT                   @"Ingresa los últimos 5 dígitos de la cuenta, tarjeta o teléfono a registrar."
#define TXT_HEIGHT                      36
#define TABLE_BORDER_COLOR_RED          128.0f/255.0f
#define TABLE_BORDER_COLOR_GREEN        128.0f/255.0f
#define TABLE_BORDER_COLOR_BLUE         128.0f/255.0f
#define IMG_ASM                         @"IoIcAcceso.png"

@interface RegistraCuentaSTViewController ()

@end

@implementation RegistraCuentaSTViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [self setLblTitulo];
    [self setTxtCuenta];
    [self setLblInfo];
    [super viewDidLoad];
    [self setHeaderTitle: TOKEN_MOVIL];
	[self setHeaderImage:[UIImage imageNamed:IMG_ASM]];
    [self showHeader:YES];
    [self muestraBotonDerecho:BOTON_SIGUIENTE];
}

-(void)setLblTitulo
{
    _lblTitulo.text = LBL_TITULO_TEXT;
    _lblTitulo.frame = CGRectMake(LAT_MARGIN,DISTANCE_BTW_COMP, COMPONENT_WIDTH, LBL_HEIGHT);
    _lblTitulo.font = FUENTE_CUERPO;
    _lblTitulo.backgroundColor = [UIColor clearColor];
    _lblTitulo.textColor = COLOR_1ER_AZUL;
}
-(void)setTxtCuenta
{
    _txtCuenta.delegate = self;
    _txtCuenta.frame = CGRectMake(LAT_MARGIN, _lblTitulo.frame.origin.y+_lblTitulo.frame.size.height, COMPONENT_WIDTH, TXT_HEIGHT);
    _txtCuenta.layer.cornerRadius = CORNER_RADIOUS;
    _txtCuenta.layer.borderWidth = BORDER_WIDTH;
    _txtCuenta.layer.borderColor = [UIColor colorWithRed:TABLE_BORDER_COLOR_RED green:TABLE_BORDER_COLOR_GREEN blue:TABLE_BORDER_COLOR_BLUE alpha:1].CGColor;
}
-(void)setLblInfo
{
    _lblInfo.text = LBL_INFO_TEXT;
    _lblInfo.frame = CGRectMake(LAT_MARGIN,_txtCuenta.frame.origin.y + _txtCuenta.frame.size.height + DISTANCE_BTW_COMP, COMPONENT_WIDTH, LBL_HEIGHT*2);
    _lblInfo.font = FUENTE_CUERPO;
    _lblInfo.numberOfLines = 2;
    _lblInfo.backgroundColor = [UIColor clearColor];
    _lblInfo.textColor = COLOR_GRIS_TEXTO_1;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)ejecutaAccionBotonDerecho
{
    if([_delegateGeneraOTPST validaTextoChallenge]){
        [_txtCuenta resignFirstResponder];
        [_delegateGeneraOTPST muestraOTP:[GeneraOTPSTDelegate generaOTPChallenge:_txtCuenta.text]];
    }
}

/**
 * Invoked by framework when a text field asks to start editing. Pop buttons view state is updated accodingly
 *
 * @param textField The text field asking to start editing
 * @return YES if editing can start, NO otherwise
 */
 - (BOOL) textFieldShouldBeginEditing: (UITextField*) textField {
 BOOL result = YES;
 
 if (textField == _txtCuenta) {
 [_popButtonsView enableNextResponderButton:NO];
 [_popButtonsView enablePreviousResponderButton: NO];
 _editedView = _txtCuenta;
 _infoEdited = rcstiee_NumCard;
 } else {
 [_popButtonsView enableNextResponderButton: NO];
 [_popButtonsView enablePreviousResponderButton: NO];
 _editedView = nil;
 _infoEdited = rcstiee_None;
 result = NO;
 }
 
 return result;
 }

/**
 * Child classes informs editable view controller whether the provided text field contains
 * an integer number. Both text field in view controller are integer numbers
 *
 * @param aTextField The text field to test
 * @return YES in all cases
 */
 - (BOOL) textFieldIsInteger: (UITextField*) aTextField {
 return YES;
 }

/**/
 - (NSInteger) maxSizeForTextField: (UITextField*) aTextField {
 NSInteger result = -1;
 
 if (aTextField == _txtCuenta) {
 result = CHALLENGE_ST_REFERENCE_LENGTH;
 }
 return result;
 }

/**
 * Informs the pop buttons view delegate that OK button was clicked. Keypad is dismissed
 *
 * @param aPopButtonsView The pop buttons view triggering the event
 */
 - (void) okButtonClickedInPopButtonsView: (PopButtonsView*) aPopButtonsView {
 [super okButtonClickedInPopButtonsView: aPopButtonsView];
 
 _editedView = nil;
 
 if (_infoEdited == rcstiee_NumCard) {
 [_txtCuenta resignFirstResponder];
 }
 }

 



- (void)viewDidUnload {
    [self setLblTitulo:nil];
    [self setTxtCuenta:nil];
    [self setLblInfo:nil];
    [super viewDidUnload];
}
@end
