//
//  AutenticacionSTViewController.h
//  APIToken
//
//  Created by Alberto Martínez Fernández on 7/1/16.
//  Copyright © 2016 CGI. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EditableViewController.h"

@class ContratacionSTDelegate;

#define AUTENTICACION_ST_XIB  @"AutenticacionSTView"

typedef enum {
    liee_nil = 0,
    liee_NumeroTarjeta,
    liee_nip,
    liee_3DigitosTarjeta
} AutenticacionSTInformationEditingEnum;

@interface AutenticacionSTViewController : EditableViewController {
    AutenticacionSTInformationEditingEnum _infoEdited;
}

@property (nonatomic,strong) ContratacionSTDelegate *contratacionSTDelegate;
@property (nonatomic) NSInteger operationDelivered;
@property (nonatomic, strong) IBOutlet UITextField *txtNumeroTarjeta;
@property (nonatomic, strong) IBOutlet UITextField *txtNIP;
@property (nonatomic, strong) IBOutlet UITextField *txt3DigitosTarjeta;

@end
