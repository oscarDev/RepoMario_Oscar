//
//  OpenOCSTesting.m
//  Bancomer
//
//  Created by Karen Romero on 20/07/15.
//  Copyright (c) 2015 GoNet. All rights reserved.
//

#import "OpenOCSTesting.h"
#import "ocsralph.h"
#import "ocscodec.h"
#import "ocsb2nco.h"
#import "ocsb64al.h"
#import "Tools.h"

#define RandomAlphabetGeneratorTest_CHARS "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
#define RandomAlphabetGeneratorTest_CHARS_SIZE 26
const static char * CSArrayAlphabetTest_CHARS = "ABCDEFGHIJ";
#define CSArrayAlphabetTest_CHARS_SIZE 10
#define OCSBase2NCodecTest_SAMPLES_SIZE 8
static const char * OCSBase2NCodecTest_SAMPLES[8][3] = {
    { "", "", "" },
    { "f", "Zg==", "Zg" },
    { "fo", "Zm8=", "Zm8" },
    { "foo", "Zm9v", "Zm9v" },
    { "foob", "Zm9vYg==", "Zm9vYg" },
    { "fooba", "Zm9vYmE=", "Zm9vYmE" },
    { "foobar", "Zm9vYmFy", "Zm9vYmFy"},
    { "This is just a test...\n", "VGhpcyBpcyBqdXN0IGEgdGVzdC4uLgo=", "VGhpcyBpcyBqdXN0IGEgdGVzdC4uLgo"}
};
/**
 * List of blank characters.
 */
#define OCSBase2NCodec_SPACES " \n\r\f\v\t"

#define QRCODE_ALPHANUMERIC_NO_SPACE "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ$%*+-./:"
#define QRCODE_ALPHANUMERIC_NO_SPACE_CHARS_SIZE 44

@implementation OpenOCSTesting

- (NSData *)obfuscatorWithString : (NSString *)qr {

    
    const char *stringSrc = [qr UTF8String];
    
    char srcAlphabet[QRCODE_ALPHANUMERIC_NO_SPACE_CHARS_SIZE + 1] ;
    char dstAlphabet[QRCODE_ALPHANUMERIC_NO_SPACE_CHARS_SIZE + 1];
    int retval;
    
    // Ensure that src remain unchanged
    memcpy(srcAlphabet, QRCODE_ALPHANUMERIC_NO_SPACE, sizeof(char) * QRCODE_ALPHANUMERIC_NO_SPACE_CHARS_SIZE);
    retval = RandomAlphabetGenerator_generateRandom(96158743, 100,
                                                    srcAlphabet, QRCODE_ALPHANUMERIC_NO_SPACE_CHARS_SIZE,
                                                    dstAlphabet, 32);
    //[Tools writeLog:[NSString stringWithFormat:@"%s", dstAlphabet]];
    
    OCSArrayAlphabet *ocsAlphabet;
    OCSAlphabet *ocsBase;
    
    retval = OCSArrayAlphabet_New(&ocsAlphabet, dstAlphabet, 32, false);
    ocsBase = (OCSAlphabet*)ocsAlphabet;
    //[Tools writeLog:[NSString stringWithFormat:@"base: %d", ocsBase->_size]];
    
    OCSBase2NCodec *ocsBase2NCodec;
    OCSCodec *baseOCSCodec;
    
    retval = OCSBase2NCodec_New(&ocsBase2NCodec, (OCSAlphabet*)ocsBase, '=', 0, NULL);
    //[Tools writeLog:[NSString stringWithFormat:@"%d", ocsBase2NCodec->alphabet->_size]];
    baseOCSCodec = (OCSCodec *)ocsBase2NCodec;
    
    char dst[256];
    int srcSize;
    int dstSize;
    int dstSize2;
    
    srcSize = (int)strlen(stringSrc);
    //[Tools writeLog:[NSString stringWithFormat:@"%s", stringSrc]];
    dstSize = baseOCSCodec->getDecodedSize(baseOCSCodec, srcSize);
    memset(dst, '*', sizeof(dst));
    dstSize2 = dstSize;
    retval = baseOCSCodec->decode(baseOCSCodec, stringSrc, srcSize, dst, &dstSize2);
    
    /*
    [Tools writeLog:[NSString stringWithFormat:@"********  SALIDA  ********"]];
    
    for (int i = 0; i < dstSize; i++) {
        [Tools writeLog:[NSString stringWithFormat:@"%c", dst[i]]];
    }
    */
    NSData *data = [NSData dataWithBytes:dst length:sizeof(dst)];

   
    dst[dstSize2] = 0;
    
    srcAlphabet[QRCODE_ALPHANUMERIC_NO_SPACE_CHARS_SIZE] = 0;
    OCSObjectDelete((OCSObject*)ocsBase2NCodec);

    
    return data;
}

- (void)generateRandom {
    char src[QRCODE_ALPHANUMERIC_NO_SPACE_CHARS_SIZE + 1] ;
    char dst[QRCODE_ALPHANUMERIC_NO_SPACE_CHARS_SIZE + 1];
    int retval;
    int size;
    
    // Ensure that src remain unchanged
    memcpy(src, QRCODE_ALPHANUMERIC_NO_SPACE, sizeof(char) * QRCODE_ALPHANUMERIC_NO_SPACE_CHARS_SIZE);
    retval = RandomAlphabetGenerator_generateRandom(96158743, 100,
                                                    src, QRCODE_ALPHANUMERIC_NO_SPACE_CHARS_SIZE,
                                                    dst, 32);
    NSLog(@"%s", dst);
    src[QRCODE_ALPHANUMERIC_NO_SPACE_CHARS_SIZE] = 0;
    
    // Ensure that all characters in dst are indeed part of src
    for (size = 1; size <= QRCODE_ALPHANUMERIC_NO_SPACE_CHARS_SIZE; size++) {
        memcpy(src, QRCODE_ALPHANUMERIC_NO_SPACE, sizeof(char) * QRCODE_ALPHANUMERIC_NO_SPACE_CHARS_SIZE);
        retval = RandomAlphabetGenerator_generateRandom(size, 100,
                                                        src, QRCODE_ALPHANUMERIC_NO_SPACE_CHARS_SIZE,
                                                        dst, size);
        
    }
}

- (void)constructorOCSAlphabet {
    OCSArrayAlphabet * a;
    OCSAlphabet * base;
    int retval;
    
    retval = OCSArrayAlphabet_New(&a, CSArrayAlphabetTest_CHARS, CSArrayAlphabetTest_CHARS_SIZE, false);
    base = (OCSAlphabet*)a;
    NSLog(@"base: %d", base->_size);
    OCSObjectDelete((OCSObject*)a);
}

- (void)constructorNoPaddingNoIgnore {
    OCSBase64Alphabet * a;
    OCSBase2NCodec * c;
    OCSCodec * base;
    int retval;
    
    retval = OCSBase64Alphabet_New(&a, false);
   NSLog(@"%s", a->base.alphabet);
    retval = OCSBase2NCodec_New(&c, (OCSAlphabet*)a, 0, 0, NULL);
   
    
    base = (OCSCodec *)c;
    OCSObjectDelete((OCSObject *) c);
}

- (void)decodingNoPadding {
    OCSBase64Alphabet * a;
    OCSBase2NCodec * c;
    OCSCodec * base;
    const char ** sample;
    char dst[256];
    int srcSize;
    int dstSize;
    int dstSize2;
    int retval;
    int i;
    
    retval = OCSBase64Alphabet_New(&a, false);
    retval = OCSBase2NCodec_New(&c, (OCSAlphabet*)a, '=', 0, NULL);
    base = (OCSCodec *)c;
    //    for (i = 0 ; i < OCSBase2NCodecTest_SAMPLES_SIZE; i++) {
    i = 7;
    sample = OCSBase2NCodecTest_SAMPLES[i];
    srcSize = strlen(sample[2]);
   
    dstSize = base->getDecodedSize(base, srcSize);
    //        for (tmpSize = 0; tmpSize < dstSize; tmpSize++) {
    //            dstSize2 = tmpSize;
    //            retval = base->decode(base, sample[2], srcSize, dst, &dstSize2);
    //        }
    memset(dst, '*', sizeof(dst));
    dstSize2 = dstSize;
    retval = base->decode(base, sample[2], srcSize, dst, &dstSize2);
   
    dst[dstSize2] = 0;
    //    }
    OCSObjectDelete((OCSObject *) c);
}

@end
