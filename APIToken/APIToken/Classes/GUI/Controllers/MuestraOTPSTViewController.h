//
//  MuestraOTPSTViewController.h
//  Bancomer
//
//  Created by Francisco.Garcia on 28/05/13.
//
//

#import "BaseViewController.h"
#define MUESTRA_OTPST_XIB           @"MuestraOTPSTView"
@class GeneraOTPSTDelegate;
@interface MuestraOTPSTViewController : BaseViewController{
    GeneraOTPSTDelegate* __strong _generaOTPSTDelegate;
}

@property (nonatomic,strong) GeneraOTPSTDelegate* generaOTPSTDelegate;
@property (strong, nonatomic) IBOutlet UILabel *lblInfo;
@property (strong, nonatomic) IBOutlet UITextField *txtToken;
@property (strong, nonatomic) IBOutlet UILabel *lblInfo2;
@property (strong, nonatomic) IBOutlet UILabel *lblOpExitosa;
@property (strong, nonatomic) IBOutlet UILabel *lblInfo3;
//array estados
@property (nonatomic,retain) NSMutableArray *estados;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andToken:(NSString*)token;

//- (id)initWithArray:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andToken:(NSString*)token array:(NSMutableArray*)array;

@end
