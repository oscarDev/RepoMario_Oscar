//
//  MuestraOTPSTViewController.m
//  Bancomer
//
//  Created by Francisco.Garcia on 28/05/13.
//
//

#import "MuestraOTPSTViewController.h"
#import "Constants.h"
#import "GeneraOTPSTDelegate.h"
#import <QuartzCore/QuartzCore.h>
#import "ConstantsToken.h"
#import "SofttokenApp.h"
#import "APIToken.h"
#import "SoftokenViewsController.h"
#import "GeneraOTPSTViewController.h"


#define IMG_ASM                 @"IoIcAcceso.png"
#define LBL_OPERACION_TEXT      @"Operación exitosa"
#define LBL_INFO_TEXT           @""
#define LBL_INFO2_TEXT          @"Tu Código de seguridad es:"
#define LBL_INFO3_TEXT          @"Se utiliza para autorizar una operación bancaria."
#define LBL_INFO3_TEXT_2        @"Se utiliza para registrar los datos de un tercero y enviarle dinero, pagar un servicio o comprar tiempo aire."
#define TXT_HEIGHT                  36
#define TABLE_BORDER_COLOR_RED                          128.0f/255.0f
#define TABLE_BORDER_COLOR_GREEN                        128.0f/255.0f
#define TABLE_BORDER_COLOR_BLUE                         128.0f/255.0f

@interface MuestraOTPSTViewController ()
{

    NSString* token ;
}
@end

@implementation MuestraOTPSTViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andToken:(NSString*)token1
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        token = [token1 copy];
    }
    return self;
}

- (void)viewDidLoad
{
    [self setLblOpExitosa];
    [self setLblInfo];
    [self setLblInfo2];
    [self setTxtToken];
    [self setLblInfo3];
    [super viewDidLoad];
    [self setHeaderTitle:TOKEN_MOVIL];
	[self setHeaderImage:[UIImage imageNamed:IMG_ASM]];
    [self showHeader:YES];
    [self muestraBotonDerecho:BOTON_MENU];
    //[self ocultarBotonAtras:YES];
}

-(void)setLblOpExitosa
{
    _lblOpExitosa.text = LBL_OPERACION_TEXT;
    _lblOpExitosa.frame = CGRectMake(LAT_MARGIN,DISTANCE_BTW_COMP, COMPONENT_WIDTH, LBL_HEIGHT);
    _lblOpExitosa.font = FUENTE_CUERPO;
    _lblOpExitosa.backgroundColor = [UIColor clearColor];
    _lblOpExitosa.textColor = COLOR_VERDE_LIMON;
}

-(void)setLblInfo
{
    _lblInfo.text = LBL_INFO_TEXT;
    _lblInfo.frame = CGRectMake(LAT_MARGIN,_lblOpExitosa.frame.origin.y + _lblOpExitosa.frame.size.height + DISTANCE_BTW_COMP, COMPONENT_WIDTH, LBL_HEIGHT*2);
    _lblInfo.font = FUENTE_CUERPO;
    _lblInfo.numberOfLines = 2;
    _lblInfo.backgroundColor = [UIColor clearColor];
    _lblInfo.textColor = COLOR_GRIS_TEXTO_1;
}

-(void)setLblInfo2
{
    _lblInfo2.text = LBL_INFO2_TEXT;
    _lblInfo2.frame = CGRectMake(LAT_MARGIN,_lblInfo.frame.origin.y + _lblInfo.frame.size.height + DISTANCE_BTW_COMP, COMPONENT_WIDTH, LBL_HEIGHT);
    _lblInfo2.font = FUENTE_CUERPO;
    _lblInfo2.backgroundColor = [UIColor clearColor];
    _lblInfo2.textColor = COLOR_GRIS_TEXTO_1;
}

-(void)setTxtToken
{
    _txtToken.frame = CGRectMake(LAT_MARGIN, _lblInfo2.frame.origin.y+_lblInfo2.frame.size.height, COMPONENT_WIDTH, TXT_HEIGHT);
    _txtToken.layer.cornerRadius = CORNER_RADIOUS;
    _txtToken.layer.borderWidth = BORDER_WIDTH;
    _txtToken.layer.borderColor = [UIColor colorWithRed:TABLE_BORDER_COLOR_RED green:TABLE_BORDER_COLOR_GREEN blue:TABLE_BORDER_COLOR_BLUE alpha:1].CGColor;
   
}

-(void)setLblInfo3
{
    NSInteger lineas = 0;
    if(_generaOTPSTDelegate.seleccionToken == claveConSemilla)
    {
        _lblInfo3.text = LBL_INFO3_TEXT_2;
        lineas = 3;
    }
    else if(_generaOTPSTDelegate.seleccionToken == claveSimple)
    {
        _lblInfo3.text = LBL_INFO3_TEXT;
        lineas = 2;
        
    }
    _lblInfo3.numberOfLines = lineas;
    
    _lblInfo3.frame = CGRectMake(LAT_MARGIN,_txtToken.frame.origin.y + _txtToken.frame.size.height + DISTANCE_BTW_COMP, COMPONENT_WIDTH, LBL_HEIGHT*lineas);
    _lblInfo3.font = FUENTE_CUERPO;
    _lblInfo3.backgroundColor = [UIColor clearColor];
    _lblInfo3.textColor = COLOR_MAGENTA;
    
}

- (void) accionBotonDerecho {

    [self accionBotonIzquierdo];
}

- (void) accionBotonIzquierdo {
    
    [self.navigationController popToViewController:[APIToken getInstance].softApp.controladorSofttoken.controladorGeneraOTPS animated:YES];
    
}

- (void)viewDidAppear:(BOOL)animated{

    [super viewDidAppear:animated];
    [_txtToken setText:token];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setLblInfo:nil];
    [self setTxtToken:nil];
    [self setLblInfo2:nil];
    [self setLblOpExitosa:nil];
    [self setLblInfo3:nil];
    [super viewDidUnload];
}
@end
