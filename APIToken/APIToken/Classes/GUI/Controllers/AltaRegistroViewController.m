//
//  AltaRegistroViewController.m
//  Bancomer
//
//  Created by Karen Romero on 19/06/15.
//  Copyright (c) 2015 GoNet. All rights reserved.
//

#import "AltaRegistroViewController.h"
#import "GeneraOTPSTDelegate.h"
#import "SoftokenViewsController.h"
#import "Constants.h"
#import "ConstantsToken.h"


#define IMG_ASM                 @"IoIcAcceso.png"
#define ATRAS_FRAME             CGRectMake(4, 18, 64, 28)
#define TXT_INFO01_CUENTA       @"¿Confirmas el Alta de la cuenta"
#define TXT_INFO02_CUENTA       @"a nombre de:"
#define TXT_INFO03_CUENTA       @"Para realizar transferencias\nde dinero?"
#define TXT_INFO01_BANCO        @"del banco:"
#define TXT_INFO01_CONV         @"¿Confirmas el Alta del convenio"
#define TXT_INFO02_CONV         @"del servicio:"
#define TXT_INFO03_CONV         @"Para realizar pagos\nde servicios con dinero?"
#define TXT_INFO01_CEL          @"¿Confirmas el Alta del celular"
#define TXT_INFO02_CEL          @"de la compañía telefónica:"
#define TXT_INFO03_CEL          @"Para envíos de dinero o\ncompra de tiempo aire?"



@interface AltaRegistroViewController ()

@end

@implementation AltaRegistroViewController
@synthesize generaOTPSTDelegate;
@synthesize lblAlta,btnCancelarOperacion;
@synthesize lblfromAccount,lblInfo01,lblInfo02,lblInfo03,lbltoAccount;
- (id)initWithOperacion:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil operacion:(NSMutableDictionary *)operacion qrLeido: (NSString *)qr{
    
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        operacionAlta = [[NSMutableDictionary alloc] initWithDictionary:operacion];
        qrLeido = qr;
        // Custom initialization
    }
    return self;
}



- (void)viewDidLoad {
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setHeaderTitle: TOKEN_MOVIL];
    [self setHeaderImage:[UIImage imageNamed:IMG_ASM]];
    [self showHeader:YES];
    [self ocultarBotonAtras:NO];  //Mod 50635
    [self muestraBotonDerecho:BOTON_CONFIRMAR] ; //Mod 50635
    [self setLabelTitulo];
    [self setLabelInfo01];
    [self setLabelFromAccount];
    [self setLabelInfo02];
    [self setLabelToAccount];
    [self setLabelInfo03];
    
    [self recalculateScroll]; //Mod 50635
    [self setTransparentScrollContentSize]; //Mod 50635
    
}

/**
 * Calcula el scroll. Mod 50635
 */

-(void)setTransparentScrollContentSize
{
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    
    btnCancelarOperacion.frame = CGRectMake(btnCancelarOperacion.frame.origin.x,  screenHeight - HEADER_HEIGHT - 77, btnCancelarOperacion.frame.size.width, btnCancelarOperacion.frame.size.height);
        _transparentScroll.frame = CGRectMake(0, HEADER_HEIGHT, SCREEN_WIDTH, screenHeight - HEADER_HEIGHT);
}

- (NSString *)getQRLeido {
    
    return qrLeido;
}

- (NSMutableDictionary *)getOperacionAlta {
    
    return operacionAlta;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setLabelTitulo
{
    lblAlta.text = @"Alta de Registro";
    lblAlta.font = FUENTE_CUERPO;
    lblAlta.backgroundColor = [UIColor clearColor];
    lblAlta.textColor = COLOR_2DO_AZUL;
    lblAlta.textAlignment = NSTextAlignmentLeft;
}
- (void)setLabelInfo01;
{
    
    NSString *tipoOperacion = [[self getOperacionAlta] objectForKey:@"0x07"];

    if ([tipoOperacion isEqualToString:@"01"] || [tipoOperacion isEqualToString:@"02"]) { //Terceros Bancomer y Terceros Interbancarios
        
        lblInfo01.text = TXT_INFO01_CUENTA;
        
    }else if ([tipoOperacion isEqualToString:@"03"]){ //Pago de servicios
        
        lblInfo01.text = TXT_INFO01_CONV;
        
    }else if ([tipoOperacion isEqualToString:@"04"] || [tipoOperacion isEqualToString:@"05"]){ //Dinero Movil y Compra tiempo aire
        
        lblInfo01.text = TXT_INFO01_CEL;
    }
    
    lblInfo01.font = [UIFont fontWithName:HELVETICA_NEUE_LT_STD_55_ROMAN size:19.0];
    lblInfo01.backgroundColor = [UIColor clearColor];
    lblInfo01.textColor = COLOR_GRIS_TEXTO_2;
    lblInfo01.textAlignment = NSTextAlignmentCenter;
}

- (void)setLabelFromAccount;
{
    

    NSString *info = [[self getOperacionAlta] objectForKey:@"0x02"];

    NSMutableAttributedString *commentString = [[NSMutableAttributedString alloc] initWithString:info];
    UIColor* textColor = COLOR_NARANJA;
    
    [commentString setAttributes:@{NSForegroundColorAttributeName:textColor,NSFontAttributeName:[UIFont boldSystemFontOfSize:25.0],NSUnderlineStyleAttributeName:[NSNumber numberWithInteger:NSUnderlineStyleSingle]} range:NSMakeRange(0,[commentString length])];
    
    lblfromAccount.font = [UIFont boldSystemFontOfSize: 25.0];
    [lblfromAccount setTextAlignment:NSTextAlignmentCenter];
    [lblfromAccount setAttributedText:commentString];

   
}

- (void)setLabelInfo02;
{
    NSString *tipoOperacion =[[self getOperacionAlta] objectForKey:@"0x07"];
    
    if ([tipoOperacion isEqualToString:@"01"]) { //Terceros Bancomer
        
        lblInfo02.text = TXT_INFO02_CUENTA;
        
    }else if ([tipoOperacion isEqualToString:@"02"]){ //Terceros Interbancarios
        
        lblInfo02.text = TXT_INFO01_BANCO;
    
    }else if ([tipoOperacion isEqualToString:@"03"]){ //Pago de servicios
        
        lblInfo02.text = TXT_INFO02_CONV;
        
    }else if ([tipoOperacion isEqualToString:@"04"] || [tipoOperacion isEqualToString:@"05"]){ //Dinero Movil y Compra tiempo aire
        
        lblInfo02.text = TXT_INFO02_CEL;
    }

    
    lblInfo02.font = [UIFont fontWithName:HELVETICA_NEUE_LT_STD_55_ROMAN size:19.0];
    lblInfo02.backgroundColor = [UIColor clearColor];
    lblInfo02.textColor = COLOR_GRIS_TEXTO_2;
    lblInfo02.textAlignment = NSTextAlignmentCenter;
}

- (void)setLabelToAccount;
{
    lbltoAccount.text = [[self getOperacionAlta] objectForKey:@"0x04"];
    lbltoAccount.lineBreakMode = NSLineBreakByWordWrapping;
    lbltoAccount.numberOfLines = 0;
    lbltoAccount.font = [UIFont boldSystemFontOfSize: 25.0];
    lbltoAccount.backgroundColor = [UIColor clearColor];
    lbltoAccount.textColor = COLOR_NARANJA;
    lbltoAccount.textAlignment = NSTextAlignmentCenter;
}

- (void)setLabelInfo03;
{
    NSString *tipoOperacion =[[self getOperacionAlta] objectForKey:@"0x07"];
    
    if ([tipoOperacion isEqualToString:@"01"] || [tipoOperacion isEqualToString:@"02"]) { //Terceros Bancomer y Terceros
        
        lblInfo03.text = TXT_INFO03_CUENTA;
        
    }else if ([tipoOperacion isEqualToString:@"03"]){ //Pago de servicios
        
        lblInfo03.text = TXT_INFO03_CONV;
        
    }else if ([tipoOperacion isEqualToString:@"04"] || [tipoOperacion isEqualToString:@"05"]){ //Dinero Movil y Compra tiempo aire
        
        lblInfo03.text = TXT_INFO03_CEL;
    }
    
    lblInfo03.font = [UIFont fontWithName:HELVETICA_NEUE_LT_STD_55_ROMAN size:19.0];
    lblInfo03.numberOfLines = 2;
    lblInfo03.backgroundColor = [UIColor clearColor];
    lblInfo03.textColor = COLOR_GRIS_TEXTO_2;
    lblInfo03.textAlignment = NSTextAlignmentCenter;
}

//Mod 50635
-(void)ejecutaAccionBotonDerecho
{
    [generaOTPSTDelegate muestraOTP:[GeneraOTPSTDelegate generaOTPTransactionSigning:[self getQRLeido]]];
}
- (IBAction)accionCancelarOperacion:(id)sender {
    
    [super accionBotonIzquierdo];
}

@end
