//
//  GeneraOTPSTViewController.h
//  Bancomer
//
//  Created by Francisco.Garcia on 28/05/13.
//
//

#import "EditableViewController.h"
#import "ZBarSDK.h"  //Modificación 50683

@class GeneraOTPSTDelegate;

#define GENERA_OTPST_XIB  @"GeneraOTPSTView"

//Modificación 50683
@interface GeneraOTPSTViewController : EditableViewController<ZBarReaderDelegate>{

   
	NSInteger _selectedPurchaseOptionIndex;
    
    ZBarReaderViewController *reader; //Modificación 50683
    
    NSTimer* timeoutReader;  //Modificación 50683
}

@property (nonatomic,strong) GeneraOTPSTDelegate* generaOTPSTDelegate;

@property (strong, nonatomic) IBOutlet UILabel *lblBorrar;
@property (strong, nonatomic) IBOutlet UIButton *btnBorrar;



@end
