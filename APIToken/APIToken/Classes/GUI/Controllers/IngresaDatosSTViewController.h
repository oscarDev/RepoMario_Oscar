//
//  IngresaDatosSTViewController.h
//  Bancomer
//
//  Created by Francisco.Garcia on 27/05/13.
//
//

#import "EditableViewController.h"
#import "SeleccionHorizontalViewController.h"

@class ContratacionSTDelegate;
#define INGRESA_DATOS_ST_XIB            @"ContratarToken"
typedef enum {
	idiee_None = 0, //!<No information is being edited
	idiee_NumCel, //
	idiee_NumCard //
} IngresaDatosInformationEditingEnum;
@interface IngresaDatosSTViewController : EditableViewController<SeleccionHorizontalDelegate>
{
@private
    ContratacionSTDelegate* _contratacionSTDelegate;
    IngresaDatosInformationEditingEnum _infoEdited;
}
@property (strong, nonatomic)ContratacionSTDelegate* contratacionSTDelegate;
@property (nonatomic, strong) SeleccionHorizontalViewController *controladorSeleccionHorizontal;
@property (nonatomic, strong) NSMutableArray *selectedItem;



@property (strong, nonatomic) IBOutlet UILabel *LblNumero;

@property (strong, nonatomic) IBOutlet UILabel *lblGetNumero;

@property (strong, nonatomic) IBOutlet UITextField *txtTarjeta;
@property (strong, nonatomic) IBOutlet UIButton *btnContinuarOutlet;

- (IBAction)bntContinuarAccion:(id)sender;

///////////////OUTLETS ANTERIORES////////////////////
//@property (strong, nonatomic) IBOutlet UILabel *lblIngresaDatos;
//@property (strong, nonatomic) IBOutlet UILabel *lblNumCelular;
//@property (strong, nonatomic) IBOutlet UITextField *txtNumCelular;
//@property (strong, nonatomic) IBOutlet UILabel *lblInfoCll;
//
//@property (strong, nonatomic) IBOutlet UILabel *lblNumTarjeta;
//
//@property (strong, nonatomic) IBOutlet UITextField *txtNumTarjeta;
//@property (unsafe_unretained, nonatomic) IBOutlet UILabel *lblInfoRed;

@property (strong, nonatomic) NSMutableArray *companias;

//array estados
//@property (nonatomic,retain) NSMutableArray *estados;
//- (id)initWithArray:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil array:(NSMutableArray*)array;

@end
