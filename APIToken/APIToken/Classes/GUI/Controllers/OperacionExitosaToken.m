//
//  OperacionExitosaToken.m
//  APIToken
//
//  Created by OscarO on 11/07/16.
//  Copyright © 2016 CGI. All rights reserved.
//

#import "OperacionExitosaToken.h"
#import "ConstantsToken.h"
#import "SoftokenViewsController.h"
#import "ContratacionSTDelegate.h"
#import <QuartzCore/QuartzCore.h>
#import "Softtoken.h"
#import "Tools.h"
#import "ToolsAPIToken.h"
#import "APIToken.h"

#define DISTANCE_FROM_TOP           30
#define LABEL_WIDTH                 245
#define LABEL_MARGIN           35
#define IMG_ASM                     @"icon_paloma_aceptado.png"
#define LBL_FELICIDADES_TEXT        @"¡OPERACIÓN EXITOSA!"
#define TXT_HEIGHT                  36
#define LBL_INFO_TEXT               @"“La activación fue realizada con éxito, a partir de este momento puedes utilizar tu servicio móvil”"
#define LBL_INFO_TEXT_REACTIVACION  @"La reactivación fue realizada con éxito, a partir de este momento puedes utilizar tu servicio móvil"
#define LBL_INFO_TEXT_ALT           @"La contratación y activación fue realizada con éxito, a partir de este momento puedes utilizar tu servicio móvil"

#define TABLE_BORDER_COLOR_RED                          128.0f/255.0f
#define TABLE_BORDER_COLOR_GREEN                        128.0f/255.0f
#define TABLE_BORDER_COLOR_BLUE                         128.0f/255.0f

@interface OperacionExitosaToken ()

@end

@implementation OperacionExitosaToken
@synthesize lblOperacionExitosa;
@synthesize lblActivacionExito;
@synthesize imgFondo;
@synthesize imgExitosa;
@synthesize btnAceptar;

//NSString* const SplashExit = @"icon_paloma_aceptado.png";


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    
    //////////////
    
    //Aqui hay que meter los metodos set de los elementos de la interfaz.
    //PENDIENTE
    ///////////////
    //[self setImgFondo];
    
    [self setLblOperacionExitosa];
    [self setImgExitosa];
    [self setLblActivacionExito];
    [self setBtnAceptar];
 
    
    [super viewDidLoad];
    
    [self ocultaEncabezado:YES];
    [self showHeader:NO];

    // Do any additional setup after loading the view.
}

//- (void) viewDidAppear:(BOOL)animated {
//    [super viewDidAppear:animated];
//    
//    [UIView animateWithDuration:1.3 delay:2 options:UIViewAnimationOptionCurveLinear animations:^{
//        [self.imgFondo setAlpha:0.7f];
//    } completion:^(BOOL finished) {
//        //
//    }];
//}

-(void)setLblOperacionExitosa{
    lblOperacionExitosa.text = LBL_FELICIDADES_TEXT;
    lblOperacionExitosa.frame = CGRectMake(LABEL_MARGIN, DISTANCE_FROM_TOP+30, LABEL_WIDTH, LBL_HEIGHT);
    lblOperacionExitosa.font = FUENTE_CUERPO;
    lblOperacionExitosa.backgroundColor = [UIColor clearColor];
    lblOperacionExitosa.textColor = [UIColor colorWithRed:66.0f/255.0f green:139.0f/255.0f blue:18.0f/255.0f alpha:1.0f];

    //[_viewSplashExito addSubview:lblOperacionExitosa];
}

-(void)setLblActivacionExito{
    if(_contratacionSTDelegate.es2x1){
        lblActivacionExito.text = LBL_INFO_TEXT_ALT;
    } else if (_contratacionSTDelegate.esReactiva2x1) {
        lblActivacionExito.text = LBL_INFO_TEXT_REACTIVACION;
    } else {
        lblActivacionExito.text = LBL_INFO_TEXT;
    }
    
    lblActivacionExito.font = FUENTE_CUERPO;
    lblActivacionExito.frame = CGRectMake(LABEL_MARGIN, imgExitosa.frame.origin.y +60, LABEL_WIDTH, LBL_HEIGHT*8);
    lblOperacionExitosa.numberOfLines = 2;
    lblOperacionExitosa.backgroundColor = [UIColor clearColor];
    lblOperacionExitosa.textColor = [UIColor blackColor];

    //[_viewSplashExito addSubview:lblActivacionExito];
}


-(void) setBtnAceptar {
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    //Modificacion pantalla
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        //its iPhone. Find out which one?
        
        CGSize resultS = [[UIScreen mainScreen] bounds].size;
        if(resultS.height == 480)
        {
            // iPhone Classic
            btnAceptar.frame = CGRectMake(LAT_MARGIN, 435/*TextCodigo.frame.origin.y+TextCodigo.frame.size.height+100*/, screenWidth -40, 40 );
            NSLog(@"iphone 4s");
        }
        else if(resultS.height == 568.0f)
        {
            btnAceptar.frame = CGRectMake(LAT_MARGIN, lblActivacionExito.frame.origin.y+160/*TextCodigo.frame.origin.y+TextCodigo.frame.size.height+100*/, screenWidth -30, 40 );
            NSLog(@"iphone 5");
        }
        else if(resultS.height == 667.0f)
        {
            btnAceptar.frame = CGRectMake(LAT_MARGIN, lblActivacionExito.frame.origin.y+160/*TextCodigo.frame.origin.y+TextCodigo.frame.size.height+100*/, screenWidth -40, 40 );
            NSLog(@"iphone 6");
        }
        else if(resultS.height == 736.0f)
        {
            btnAceptar.frame = CGRectMake(LAT_MARGIN, lblActivacionExito.frame.origin.y+160/*TextCodigo.frame.origin.y+TextCodigo.frame.size.height+100*/, screenWidth -40, 40 );
            NSLog(@"iphone 6+");
        }
    }
    //[_viewSplashExito addSubview:btnAceptar];

}

-(void)setImgExitosa {
    UIImage* im = [UIImage imageNamed:IMG_ASM];
    CGFloat imgHeight = im.size.height;
    CGFloat imgWidth = im.size.width;
    imgExitosa.frame = CGRectMake(LAT_MARGIN + 85, lblOperacionExitosa.frame.origin.y+DISTANCE_BTW_COMP+90, imgWidth, imgHeight);
    imgExitosa.image = [UIImage imageNamed:IMG_ASM];
    //[_viewSplashExito addSubview:imgExitosa];
    
}

//-(void)setImgFondo {
//    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
//    //CGSize screenHeight = [[UIScreen mainScreen] bounds].size;
//    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
//    
//    imgFondo.frame = CGRectMake(0, 0, screenWidth, screenHeight);
//    imgFondo.backgroundColor = [UIColor lightGrayColor];
//    imgFondo.alpha = 0.5f;
//
//    [imgFondo addSubview:_viewSplashExito];
//    //[self.view addSubview:imgFondo];
//
//}

-(void)ejecutaAccionBotonDerecho{
    NSString* banderasBMovilFilePath = [ToolsAPIToken getBanderasBMovilFilePath];
    NSDictionary* banderasBMovilDictionary = [NSDictionary dictionaryWithContentsOfFile: banderasBMovilFilePath];
    NSLog(@"banderasBMovilDictionary%@",banderasBMovilDictionary);
    
    
    //Mostramos el menuSuite
    [self.navigationController popToRootViewControllerAnimated:YES];
    
    // Si se cumple EA#7 mostramos pantalla inicial
    if ([[banderasBMovilDictionary objectForKey:CAMBIO_DE_PERFIL_KEY] boolValue]) {
        [[APIToken getInstance].apiTokenDelegate bmovilLaunched];
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidUnload {
    [self setBtnAceptar:nil];
    [self setLblActivacionExito:nil];
    [self setLblOperacionExitosa:nil];
    [self setImgFondo:nil];
    [self setImgExitosa:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void) presentSplash {
    [_viewSplashExito setFrame:CGRectMake(0.0f, 0.0f, self.view.frame.size.width, self.view.frame.size.height)];
    [self.view addSubview:_viewSplashExito];
}

- (IBAction)accionAceptar:(id)sender {
    [self ejecutaAccionBotonDerecho];
}
@end
