//
//  IngresaDatosSTViewController.m
//  Bancomer
//
//  Created by Francisco.Garcia on 27/05/13.
//
//

#import "IngresaDatosSTViewController.h"
#import "Constants.h"
#import "ConstantsToken.h"
#import "ContratacionSTDelegate.h"
#import "Compania.h"
#import "Tools.h"
//#import "TelefonicasMantenimiento.h"
#import "EditableViewController+protectedCategory.h"
#import <QuartzCore/QuartzCore.h>
#import "ServerToken.h"
#import "APIToken.h"

#define LBL_INGRESA_TEXT            @"Ingresa tus datos"
#define LBL_NUM_CLL_TEXT            @"Número celular"
#define LBL_NUM_TRJTA_TEXT          @"Número de tarjeta"
#define IMG_ASM                 @"barra_azul.png"
#define TXT_HEIGHT                  36
#define TABLE_BORDER_COLOR_RED                          128.0f/255.0f
#define TABLE_BORDER_COLOR_GREEN                        128.0f/255.0f
#define TABLE_BORDER_COLOR_BLUE                         128.0f/255.0f
#define LBL_INFO_CLL_TEXT           @"Es el número celular de 10 dígitos que registraste en sucursal."
#define LBL_INFO_TRJTA_TEXT         @"Es el número de tu Tarjeta de Débito asociada a la cuenta que registraste en la sucursal."
#define LBL_INFO_RED_TEXT           @"Asegúrate de tener una buena señal de 3G ó WiFi"

@interface IngresaDatosSTViewController ()

@end

@implementation IngresaDatosSTViewController
@synthesize contratacionSTDelegate = _contratacionSTDelegate;

//LINEA Comentada linea nueva
UIImageView *imagenUsuario;
UIImageView *imagenBarra;
UIImageView *imagenPaloma;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    //[self setHeaderTitle:ST_HEADER_ACTIVACION];
    //[self setHeaderImage:[UIImage imageNamed:IMG_ASM]];
    
    //[self muestraBotonDerecho:BOTON_SIGUIENTE];
    
    
   
    [self setLblNumero];
    [self setLblGetNumero];
    [self setTxtTarjeta];
    [self setEncabezado];
    [self setBotonContinuarOutlet];
//    [self setLblIngresaDatos];
//    [self setLblNumCelular];
//    [self setTxtNumCelular];
//    [self setLblInfoCll];
//     [self muestraSeleccionHorizontal];
//    [self setLblNumTarjeta];
//    [self setTxtNumTarjeta];
//    //[self setLblInfoTrjta];
//    [self setLblInfoRed];
    [super viewDidLoad];
    
    [self showHeader:NO];
    [self ocultaEncabezado:YES]; //linea nueva
    [self configView];
    
 
    _infoEdited = idiee_None;
 
//    [self setHeaderTitle:ST_HEADER_ACTIVACION];
//	[self setHeaderImage:[UIImage imageNamed:IMG_ASM]];
//    [self showHeader:NO];
//    [self ocultaEncabezado:YES]; //linea nueva
//    [self muestraBotonDerecho:BOTON_SIGUIENTE];
    
}

//metodo mario
//- (void) setImgSuperiores {
//    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
//    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
//    
//    _imgIdentificate.frame = CGRectMake(LAT_MARGIN+10, (screenHeight/8), TXT_HEIGHT, TXT_HEIGHT);//imagen identificate
//    _imgAutoriza.frame = CGRectMake(screenWidth-LAT_MARGIN-50, (screenHeight/8), TXT_HEIGHT, TXT_HEIGHT);//imagen Autoriza
//    _imgLinea.frame = CGRectMake(screenWidth-(screenWidth-60), (screenHeight/8)+15, screenWidth-125, 6);//imagen linea
//    _lblIdentificate.frame = CGRectMake(LAT_MARGIN, (screenHeight/8)+30, 60, 20);//labelIdentificate
//    _lblAutoriza.frame = CGRectMake(screenWidth-LAT_MARGIN-60, (screenHeight/8)+30, 60, 20);//labelAutoriza
//}


-(void)setEncabezado {
    //UIImage *imUsuario = [UIImage imageNamed:@"icon_usuario.png"];
    //UIImage *imPaloma = [UIImage imageNamed:@"icon_paloma_inactiva.png"];
    //UIImage *imBarra = [UIImage imageNamed:@"barra_azul.png"];


    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    
    UILabel *lblIdentificate = [[UILabel alloc]initWithFrame:CGRectMake(LAT_MARGIN + 5, (screenHeight/7)+ 35, 60, 20)];
    lblIdentificate.text = @"Identificate";
    lblIdentificate.font = [UIFont fontWithName:HELVETICA_NEUE_LT_STD_55_ROMAN size:10.0F];
    lblIdentificate.backgroundColor = [UIColor clearColor];
    lblIdentificate.textColor = COLOR_1ER_AZUL;

    UILabel *lblAutoriza = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth-LAT_MARGIN-52, (screenHeight/7)+ 35, 60, 20)];
    lblAutoriza.text = @"Autoriza";
    lblAutoriza.font = [UIFont fontWithName:HELVETICA_NEUE_LT_STD_55_ROMAN size:10.0F];
    lblAutoriza.backgroundColor = [UIColor clearColor];
    lblAutoriza.textColor = COLOR_1ER_AZUL;

    
    imagenUsuario = [[UIImageView alloc]initWithFrame:CGRectMake(LAT_MARGIN+10, (screenHeight/7), TXT_HEIGHT, TXT_HEIGHT)];
    [imagenUsuario setImage:[UIImage imageNamed:@"icon_usuario.png"]];
    
    imagenPaloma = [[UIImageView alloc]initWithFrame:CGRectMake(screenWidth-LAT_MARGIN-50, (screenHeight/7), TXT_HEIGHT, TXT_HEIGHT)];
    [imagenPaloma setImage: [UIImage imageNamed: @"icon_paloma_inactiva.png"]];

    
    imagenBarra = [[UIImageView alloc]initWithFrame:CGRectMake(screenWidth-(screenWidth-60), (screenHeight/7)+15, screenWidth-125, 3 )];
    [imagenBarra setImage:[UIImage imageNamed:@"barra_azul.png"]];
    imagenBarra.alpha = 0.3f;
    
    //lblIdentificate.frame = CGRectMake(LAT_MARGIN, (screenHeight/7)+30, 60, 20);//labelIdentificate
    //lblAutoriza.frame = CGRectMake(screenWidth-LAT_MARGIN-60, (screenHeight/7)+30, 60, 20);//labelAutoriza
    [self.view addSubview:imagenBarra];
    [self.view addSubview:imagenUsuario];
    [self.view addSubview:imagenPaloma];
    [self.view addSubview:lblIdentificate];
    [self.view addSubview:lblAutoriza];
}

-(void)setLblNumero{
    _LblNumero.text = @"Tu número celular";
    _LblNumero.frame = CGRectMake(LAT_MARGIN, _txtTarjeta.frame.origin.y - 130, COMPONENT_WIDTH, LBL_HEIGHT);
    _LblNumero.font = FUENTE_CUERPO_2;
    _LblNumero.backgroundColor = [UIColor clearColor];
    _LblNumero.textColor = COLOR_1ER_AZUL;
}


///////////////METODO ORIGINAL////////////////////

//-(void)setLblIngresaDatos
//{
//    _lblIngresaDatos.text = LBL_INGRESA_TEXT;
//    _lblIngresaDatos.frame = CGRectMake(LAT_MARGIN,DISTANCE_BTW_COMP, COMPONENT_WIDTH, LBL_HEIGHT);
//    _lblIngresaDatos.font = FUENTE_CUERPO;
//    _lblIngresaDatos.backgroundColor = [UIColor clearColor];
//    _lblIngresaDatos.textColor = COLOR_1ER_AZUL;
//}

-(void)setLblGetNumero{
    
    _lblGetNumero.frame = CGRectMake(LAT_MARGIN, _LblNumero.frame.origin.y +15+ DISTANCE_BTW_COMP, COMPONENT_WIDTH, LBL_HEIGHT);
    _lblGetNumero.font = FUENTE_CUERPO;
    _lblGetNumero.backgroundColor = [UIColor clearColor];
    _lblGetNumero.textColor = [UIColor blackColor];
    
    NSString * userName = [[APIToken getInstance].apiTokenDataSource userName];
    
        if (userName.length == 10) {
            _lblGetNumero.text = userName;
        } else {
            _lblGetNumero.text = @"";
        }
}


-(void) setBotonContinuarOutlet{
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    //Modificacion pantalla
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        //its iPhone. Find out which one?
        
        CGSize resultS = [[UIScreen mainScreen] bounds].size;
        if(resultS.height == 480)
        {
            // iPhone Classic
            _btnContinuarOutlet.frame = CGRectMake(LAT_MARGIN, 435/*TextCodigo.frame.origin.y+TextCodigo.frame.size.height+100*/, screenWidth -40, 40 );
            NSLog(@"iphone 4s");
        }
        else if(resultS.height == 568.0f)
        {
            _btnContinuarOutlet.frame = CGRectMake(LAT_MARGIN, _txtTarjeta.frame.origin.y+_txtTarjeta.frame.size.height+140/*TextCodigo.frame.origin.y+TextCodigo.frame.size.height+100*/, screenWidth -40, 40 );
            NSLog(@"iphone 5");
        }
        else if(resultS.height == 667.0f)
        {
            _btnContinuarOutlet.frame = CGRectMake(LAT_MARGIN, _txtTarjeta.frame.origin.y+_txtTarjeta.frame.size.height+140, screenWidth -40, 40 );
            NSLog(@"iphone 6");
        }
        else if(resultS.height == 736.0f)
        {
            _btnContinuarOutlet.frame = CGRectMake(LAT_MARGIN, _txtTarjeta.frame.origin.y+_txtTarjeta.frame.size.height+140, screenWidth -40, 40 );
            NSLog(@"iphone 6+");
        }
    }
    //_btnActivarOutlet.frame = CGRectMake(LAT_MARGIN, self.view.bounds.size.height - 130/*TextCodigo.frame.origin.y+TextCodigo.frame.size.height+100*/, screenWidth -40, 40 );
}



///////////////METODO ORIGINAL////////////////////

//-(void)setLblNumCelular
//{
//    _lblNumCelular.text = LBL_NUM_CLL_TEXT;
//    _lblNumCelular.frame = CGRectMake(LAT_MARGIN,_lblIngresaDatos.frame.origin.y + _lblIngresaDatos.frame.size.height + DISTANCE_BTW_COMP, COMPONENT_WIDTH, LBL_HEIGHT);
//    _lblNumCelular.font = FUENTE_CUERPO;
//    _lblNumCelular.backgroundColor = [UIColor clearColor];
//    _lblNumCelular.textColor = COLOR_GRIS_TEXTO_1;
//}

///////////////METODO ORIGINAL////////////////////

//-(void)setTxtNumCelular
//{
//    _txtNumCelular.delegate = self;
//    _txtNumCelular.frame = CGRectMake(LAT_MARGIN, _lblNumCelular.frame.origin.y+_lblNumCelular.frame.size.height, COMPONENT_WIDTH, TXT_HEIGHT);
//    _txtNumCelular.layer.cornerRadius = CORNER_RADIOUS;
//    _txtNumCelular.layer.borderWidth = BORDER_WIDTH;
//    _txtNumCelular.layer.borderColor = [UIColor colorWithRed:TABLE_BORDER_COLOR_RED green:TABLE_BORDER_COLOR_GREEN blue:TABLE_BORDER_COLOR_BLUE alpha:1].CGColor;
//    
//    NSString * userName = [[APIToken getInstance].apiTokenDataSource userName];
//    
//    if (userName.length == 10) {
//        _txtNumCelular.text = userName;
//    } else {
//        _txtNumCelular.text = @"";
//    }
//}

//-(void)inicializaComponenteHorizontal
//{
//    
//    NSString *nombre = nil;
//    NSString *imagen = nil;
//    NSString *clave = nil;
//    int orden = 0;
//    Compania *compania = nil;
//    _companias = [[NSMutableArray alloc] init];
//    
//    
//    nombre = @"TELCEL";
//    imagen = @"telcel.png";
//    clave = @"0003";
//    orden = 1;
//    
//    compania = [[Compania alloc] initWithNombre:nombre imagen:imagen clave:clave importes:nil orden:orden];
//    [_companias addObject: compania];
//    
//    nombre = @"MOVISTAR";
//    imagen = @"movistar.png";
//    clave = @"0001";
//    orden = 2;
//    
//    compania = [[Compania alloc] initWithNombre:nombre imagen:imagen clave:clave importes:nil orden:orden];
//    [_companias addObject: compania];
//    
//    nombre = @"IUSACELL";
//    imagen = @"iusacell.png";
//    clave = @"0004";
//    orden = 3;
//    
//    compania = [[Compania alloc] initWithNombre:nombre imagen:imagen clave:clave importes:nil orden:orden];
//    [_companias addObject: compania];
//    
//    nombre = @"UNEFON";
//    imagen = @"unefon.png";
//    clave = @"0002";
//    orden = 4;
//    
//    compania = [[Compania alloc] initWithNombre:nombre imagen:imagen clave:clave importes:nil orden:orden];
//    [_companias addObject: compania];
//    
//    nombre = @"NEXTEL";
//    imagen = @"nextel.png";
//    clave = @"0005";
//    orden = 5;
//    
//    compania = [[Compania alloc] initWithNombre:nombre imagen:imagen clave:clave importes:nil orden:orden];
//    [_companias addObject: compania];
//    
//}


//-(void)muestraSeleccionHorizontal
//{
//    _controladorSeleccionHorizontal = [[SeleccionHorizontalViewController alloc] initWithNibName:SELECCION_HORIZONTAL_XIB bundle:[NSBundle mainBundle]];
//    
//    
////    TelefonicasMantenimiento *catalogo = session.telefonicasMantenimiento;
//    
//    NSMutableArray *lista = [[NSMutableArray alloc]init];
////    NSString *nombreCompania = session.compania;
//    //int tag = 0;
//    int numberTag = 0;
//    
//    [self inicializaComponenteHorizontal];
//    
//    for (Compania *compania in _companias)
//    {
//        numberTag++;
//        NSMutableArray *registro = [[NSMutableArray alloc] init];
//        [registro addObject:@""];
//        [registro addObject:compania.nombre];
//        [registro addObject:compania.imagen];
//        [registro addObject:@""];
//        [registro addObject:[NSNumber numberWithInt:compania.orden]];
//        [lista addObject:registro];
////        if ([nombreCompania isEqualToString:compania.nombre]) {
////            //tag = numberTag;
////        }
//    }
//    
//    lista = [[NSMutableArray alloc]initWithArray:[lista sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
//        NSArray *first = (NSArray*)a ;
//        NSArray *second =(NSArray*)b ;
//        NSNumber *primerNumero = [first objectAtIndex:4];
//        NSNumber *segundoNumero = [second objectAtIndex:4];
//        return [primerNumero compare:segundoNumero];
//    }]];
//    
//    _controladorSeleccionHorizontal.lista = lista;
//    _controladorSeleccionHorizontal.delegate = self;
//    [_controladorSeleccionHorizontal setViewFrame:20 andOriginY:160];
//    [self.view addSubview:_controladorSeleccionHorizontal.view];
//    
//    [_controladorSeleccionHorizontal changeSelection:(UIButton *)[_controladorSeleccionHorizontal.view viewWithTag:1]];
//  
//}

//-(void)getServicio
//{
//    _selectedItem = [_controladorSeleccionHorizontal getSelectedItem];
//    
//    NSLog(@"item seleccionado: %@",_selectedItem);
//    //_selectedCompanyIndex = 1;
//}


//-(void)setLblInfoCll
//{
//    _lblInfoCll.text = @"Compañía celular";
//    _lblInfoCll.frame = CGRectMake(LAT_MARGIN,_txtNumCelular.frame.origin.y + _txtNumCelular.frame.size.height +5, COMPONENT_WIDTH, LBL_HEIGHT*2);
//    _lblInfoCll.font = FUENTE_CUERPO;
//    _lblInfoCll.backgroundColor = [UIColor clearColor];
//    _lblInfoCll.textColor = COLOR_GRIS_TEXTO_1;
//
//}



///////////////METODO ORIGINAL////////////////////

//-(void)setLblNumTarjeta
//{
//    _lblNumTarjeta.text = LBL_NUM_TRJTA_TEXT;
//    _lblNumTarjeta.frame = CGRectMake(LAT_MARGIN,_controladorSeleccionHorizontal.view.frame.origin.y  + _controladorSeleccionHorizontal.view.frame.size.height + DISTANCE_BTW_COMP, COMPONENT_WIDTH, LBL_HEIGHT);
//    _lblNumTarjeta.font = FUENTE_CUERPO;
//    _lblNumTarjeta.backgroundColor = [UIColor clearColor];
//    _lblNumTarjeta.textColor = COLOR_GRIS_TEXTO_1;
//}


-(void)setTxtTarjeta
{
    _txtTarjeta.delegate = self;
    //_txtTarjeta.center = self.view.center;
    _txtTarjeta.frame = CGRectMake(LAT_MARGIN, _lblGetNumero.frame.origin.y+_lblGetNumero.frame.size.height+100, COMPONENT_WIDTH-50, TXT_HEIGHT);
    _txtTarjeta.layer.cornerRadius = CORNER_RADIOUS;
    _txtTarjeta.layer.borderWidth = BORDER_WIDTH;
    _txtTarjeta.layer.borderColor = [UIColor colorWithRed:TABLE_BORDER_COLOR_RED green:TABLE_BORDER_COLOR_GREEN blue:TABLE_BORDER_COLOR_BLUE alpha:1].CGColor;
}


///////////////METODO ORIGINAL////////////////////

//-(void)setTxtNumTarjeta
//{
//    _txtNumTarjeta.delegate = self;
//    _txtNumTarjeta.frame = CGRectMake(LAT_MARGIN, _lblNumTarjeta.frame.origin.y+_lblNumTarjeta.frame.size.height, COMPONENT_WIDTH, TXT_HEIGHT);
//    _txtNumTarjeta.layer.cornerRadius = CORNER_RADIOUS;
//    _txtNumTarjeta.layer.borderWidth = BORDER_WIDTH;
//    _txtNumTarjeta.layer.borderColor = [UIColor colorWithRed:TABLE_BORDER_COLOR_RED green:TABLE_BORDER_COLOR_GREEN blue:TABLE_BORDER_COLOR_BLUE alpha:1].CGColor;
//}


///////////////ESTE METODO YA ESTABA COMENTADO////////////////////

//-(void)setLblInfoTrjta
//{
//    _lblInfoTrjta.text = LBL_INFO_TRJTA_TEXT;
//    _lblInfoTrjta.frame = CGRectMake(LAT_MARGIN,_txtNumTarjeta.frame.origin.y + _txtNumTarjeta.frame.size.height +5, COMPONENT_WIDTH, LBL_HEIGHT*3);
//    _lblInfoTrjta.font = FUENTE_CUERPO;
//    _lblInfoTrjta.backgroundColor = [UIColor clearColor];
//    _lblInfoTrjta.textColor = COLOR_GRIS_TEXTO_4;
//    _lblInfoTrjta.numberOfLines = 3;
//}


///////////////METODO ORIGINAL////////////////////

//-(void)setLblInfoRed
//{
//    _lblInfoRed.text = LBL_INFO_RED_TEXT;
//    _lblInfoRed.frame = CGRectMake(LAT_MARGIN, _txtNumTarjeta.frame.origin.y + _txtNumTarjeta.frame.size.height + 5, COMPONENT_WIDTH, LBL_HEIGHT*3);
//    _lblInfoRed.backgroundColor = [UIColor clearColor];
//    _lblInfoRed.numberOfLines = 3;
//    _lblInfoRed.textColor = UIColorFromRGBWithAlpha(0xC8175E,1);
//}

/**
 * Notifies the view controller that its view is about to be dismissed, covered, or otherwise hidden from view.
 * Keyboard is dismissed
 *
 * @param animated	If YES, the disappearance of the view is being animated
 */
//se copia
-(void)viewWillDisappear:(BOOL)animated{

    //[self okButtonClickedInPopButtonsView:nil];
    //[_txtNumCelular resignFirstResponder]; linea comentada
    [_txtTarjeta resignFirstResponder];
    [super viewWillDisappear:animated];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {

    ///////////////METODO ORIGINAL////////////////////

    [self setLblNumero: nil];
    [self setLblGetNumero: nil];
    [self setTxtTarjeta:nil];
    
    //[self setLblIngresaDatos:nil];
    //[self setLblNumCelular:nil];
    //[self setTxtNumCelular:nil];
    //[self setLblNumTarjeta:nil];
    //[self setTxtNumTarjeta:nil];
    //[self setLblInfoCll:nil];
   // [self setLblInfoTrjta:nil];
    [super viewDidUnload];
}

/*#pragma mark - respuesta de servidor

- (NSInteger) checkDownloadId: (NSInteger) aDownloadId {
	return CONSULTA_TIPO_SOLICITUD_ST;
}

-(void)networkResponse:(ServerResponse *)aServerResponse{
    
    [Tools writeLog:[NSString stringWithFormat:@"Respuesta llega a ingresa datos st."]];
//    [self stopActivityIndicator];
    [Session getInstance].userActivationST = _txtNumCelular.text;
    [Session getInstance].numTarjetaST = _txtNumTarjeta.text;
    [Session getInstance].companiaST =[_selectedItem objectAtIndex:1];
    
    [_contratacionSTDelegate analyzeServerResponse:aServerResponse];
}
*/

////////////////////////////AQUI VOY//////////////////////////////
#pragma mark -
-(void)ejecutaAccionBotonDerecho{
 
    //[_txtNumCelular resignFirstResponder]; //LINEA COMENTADA CAMBIO
    
    [_txtTarjeta resignFirstResponder];
    if([ContratacionSTDelegate validaCampos:self])
    {
        NSArray *itemSelected = [_controladorSeleccionHorizontal getSelectedItem];
        NSString *compania = [itemSelected objectAtIndex:1];
        
        //Objeto Softtoken
        //[_contratacionSTDelegate setObjetoSofttoken:_txtNumCelular.text andCompaniaCelular:compania andNumTarjeta:_txtNumTarjeta.text]; ///CAMBIO
        
        //[_contratacionSTDelegate consultaSolicitud:_txtNumCelular.text andTarjeta:_txtNumTarjeta.text andCompaniaCelular:compania onViewController:self]; //CAMBIO
        
        [_contratacionSTDelegate setObjetoSofttokenN:_lblGetNumero.text andNumTarjeta:_txtTarjeta.text];
        
        [_contratacionSTDelegate consultaSolicitudN:_lblGetNumero.text andTarjeta:_txtTarjeta.text onViewController:self];
    }
}


-(void)ejecutaAccionBotonDerechoN{
    
    //[_txtNumCelular resignFirstResponder]; //LINEA COMENTADA CAMBIO
    
    [_txtTarjeta resignFirstResponder];
    if([ContratacionSTDelegate validaCampos:self])
    {
        NSArray *itemSelected = [_controladorSeleccionHorizontal getSelectedItem];
        NSString *compania = [itemSelected objectAtIndex:1];
        
        //Objeto Softtoken
        //[_contratacionSTDelegate setObjetoSofttoken:_txtNumCelular.text andCompaniaCelular:compania andNumTarjeta:_txtNumTarjeta.text]; ///CAMBIO
        
        //[_contratacionSTDelegate consultaSolicitud:_txtNumCelular.text andTarjeta:_txtNumTarjeta.text andCompaniaCelular:compania onViewController:self]; //CAMBIO
        
        [_contratacionSTDelegate setObjetoSofttokenN:_lblGetNumero.text andNumTarjeta:_txtTarjeta.text];
        
        [_contratacionSTDelegate consultaSolicitudN:_lblGetNumero.text andTarjeta:_txtTarjeta.text onViewController:self];
    }
}


#pragma mark -

- (NSInteger) maxSizeForTextField: (UITextField*) aTextField {
	NSInteger result = -1;
	
	if (aTextField == _txtTarjeta) {
		result = CARD_NUMBER_LENGTH;
	} //else if (aTextField == _txtNumCelular) { //CAMBIO
		//result = TELEPHONE_NUMBER_LENGTH; //CAMBIO
	//} //CAMBIO
	
	return result;
}

/**
 * Child classes informs editable view controller whether the provided text field contains
 * an integer number. Both text field in view controller are integer numbers
 *
 * @param aTextField The text field to test
 * @return YES in all cases
 */
- (BOOL) textFieldIsInteger: (UITextField*) aTextField {
	return YES;
}




/**
 * Invoked by framework when a text field asks to start editing. Pop buttons view state is updated accodingly
 *
 * @param textField The text field asking to start editing
 * @return YES if editing can start, NO otherwise
 */
- (BOOL) textFieldShouldBeginEditing: (UITextField*) textField {
	BOOL result = YES;
	
	if (textField == _txtTarjeta) {
		[_popButtonsView enableNextResponderButton: NO]; //CAMBIO YES por NO
		[_popButtonsView enablePreviousResponderButton: NO];
        _editedView = _txtTarjeta;
        //_infoEdited = idiee_NumCel;
		_infoEdited = idiee_NumCard;
    }
//	} else if (textField == _txtNumTarjeta) {
//		[_popButtonsView enableNextResponderButton: NO];
//		[_popButtonsView enablePreviousResponderButton: YES];  //CAMBIO
//		_editedView = _txtNumTarjeta;
//		_infoEdited = idiee_NumCard;
//	 else {
//		[_popButtonsView enableNextResponderButton: NO];
//		[_popButtonsView enablePreviousResponderButton: NO];
//		_editedView = nil;
//		_infoEdited = idiee_None;
//		result = NO;
//	}
	
	return result;
}



/**
 * Informs the delegate that next responder button was clicked. Sets password text field
 * as next first responder
 *
 * @param aPopButtonsView The pop buttons view triggering the event
 */
- (void) nextButtonClickedInPopButtonsView: (PopButtonsView*) aPopButtonsView {
	if (_infoEdited == idiee_NumCard) {
		[_txtTarjeta becomeFirstResponder];
	}
}

/**
 * Informs the pop buttons view delegate that OK button was clicked. Keypad is dismissed
 *
 * @param aPopButtonsView The pop buttons view triggering the event
 */
- (void) okButtonClickedInPopButtonsView: (PopButtonsView*) aPopButtonsView {
	[super okButtonClickedInPopButtonsView: aPopButtonsView];
	
	_editedView = nil;
	
	if (_infoEdited == idiee_NumCard) {
		[_txtTarjeta resignFirstResponder];
    }
//	} else if (_infoEdited == idiee_NumCel) {
//		[_txtNumCelular resignFirstResponder];
//	}
}

/**
 * Informs the delegate that previous responder button was clicked. Sets user text field as
 * next first responder
 *
 * @param aPopButtonsView The pop buttons view triggering the event
 */
- (void) previousButtonClickedInPopButtonsView: (PopButtonsView*) aPopButtonsView {
	if (_infoEdited == idiee_NumCard) {
		[_txtTarjeta becomeFirstResponder];
	}
}

-(void) accionBotonIzquierdo {
    
    if([self.navigationController.viewControllers count] == 2)
    {
        [[APIToken getInstance].apiTokenDelegate invokeAutomaticLogout];
       
    }
    [self.navigationController popViewControllerAnimated:YES];
    
}


//--------------------ENCABEZADO-------------------------

- (void)configView {
    
    UIImageView *bgImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 20, _headerView.bounds.size.width, /*_headerView.bounds.size.height*/ 40)];
    [bgImg setImage:[UIImage imageNamed:IMG_ASM]];
    [self.view addSubview:bgImg];
    
    UIView *bar = [[UIView alloc] initWithFrame:CGRectMake(0, 20, _headerView.bounds.size.width, _headerView.bounds.size.height)];
    [bar setBackgroundColor:[UIColor colorWithRed:30/255.0 green:80/255.0 blue:130.0/255.0 alpha:0]];
    
    for (UIView *v in [_headerView subviews]) {
        if ([v isKindOfClass:[UIImageView class]]) {
            [v removeFromSuperview];
        }
    }
    UIButton *btnBack = [[UIButton alloc] initWithFrame:CGRectMake(20, 7, 30, 28)];
    
    [btnBack setImage:[UIImage imageNamed:@"al_ic_regresar.png"] forState:UIControlStateNormal];
    [btnBack setTitle:@"" forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(accionBotonIzquierdo) forControlEvents:UIControlEventTouchUpInside];
    [bar addSubview:btnBack];
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-110, 13, 220, 21)];
    [title setText:@"CONTRATACIÓN"];
    [title setTextColor:[UIColor whiteColor]];
    [title setTextAlignment:NSTextAlignmentCenter];
    [title setFont:[UIFont fontWithName:HELVETICA_NEUE_LT_STD_55_ROMAN size:16.0f]];
    [title setAdjustsFontSizeToFitWidth:YES];
    [title setMinimumScaleFactor:0.7];
    [bar addSubview:title];
    
    [self.view addSubview:bar];
}

/////////////////////////header mario





- (IBAction)bntContinuarAccion:(id)sender {
    [self ejecutaAccionBotonDerecho];
}
@end
