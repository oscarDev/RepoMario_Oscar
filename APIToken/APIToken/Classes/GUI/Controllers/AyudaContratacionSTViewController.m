//
//  AyudaContratacionSTViewController.m
//  Bancomer
//
//  Created by Francisco.Garcia on 27/05/13.
//
//

#import "AyudaContratacionSTViewController.h"
#import "ConstantsToken.h"
//#import "Constants.h"
#import "SoftokenViewsController.h"
#import "SofttokenApp.h"
#import "APIToken.h"




@implementation AyudaContratacionSTViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    [_lblBienvenida setText:ST_MSG_AYUDA_CONTRATACION];
    _lblBienvenida.textColor = COLOR_GRIS_TEXTO_3;
    _lblTitulo.textColor = COLOR_1ER_AZUL;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
- (IBAction)atrasMenuSuiteTouch:(id)sender {
    NSLog(@"Atras a menu suite touch");
  //  [self.softDelegate.controladorSofttoken removeAyudaST];
}

- (IBAction)btnContinuarTouch:(id)sender
{
    NSLog(@"Continuar ASM touch");
    [[APIToken getInstance].softApp.controladorSofttoken showIngresaDatosST];
}


- (void)viewDidUnload {
    [self setLblBienvenida:nil];
    [self setLblTitulo:nil];
    [self setBtnContinuar:nil];
    [self setLblBienvenida2:nil];
    [super viewDidUnload];
}
@end
