//
//  AlertaEnvio.h
//  APIToken
//
//  Created by OscarO on 08/07/16.
//  Copyright © 2016 CGI. All rights reserved.
//

#import <UIKit/UIKit.h>



//
//  AlertaIncVC.h
//  APIToken
//
//  Created by OscarO on 06/07/16.
//  Copyright © 2016 CGI. All rights reserved.
//

@protocol AlertaEnvioDelegate

- (void)popUpButtonTouchUpInside:(id)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;

@end

@interface AlertaEnvio : UIView<AlertaEnvioDelegate>

@property (nonatomic, retain) UIView *parentView;    // The parent view this 'dialog' is attached to
@property (nonatomic, retain) UIView *dialogView;    // Dialog's container view
@property (nonatomic, retain) UIView *containerView; // Container within the dialog (place your ui elements here)
@property (nonatomic, retain) UILabel *lblTitulo;

@property (nonatomic, assign) id<AlertaEnvioDelegate> delegate;
@property (nonatomic, retain) NSArray *buttonTitles;
@property (nonatomic, assign) BOOL useMotionEffects;
@property (nonatomic, assign) BOOL closable;

@property (copy) void (^onButtonTouchUpInside)(AlertaEnvio *alertView, int buttonIndex) ;

- (id)init;
- (id)initWithTitle:(NSString*) aTitle andImageName:(NSString*) aImage andMessage:(NSString*) aMessage closable:(BOOL) aClosable;

//- (id)initWithTitle:(NSString*) aTitle andImageName:(NSString*) aImage andMessage:(NSString*) aMessage closable:(BOOL) aClosable aButton1:(UIButton*) aGenerar aButton2:(UIButton*) aCancelar;

- (void)show;
- (void)close;

- (IBAction)popUpButtonTouchUpInside:(id)sender;
- (void)setOnButtonTouchUpInside:(void (^)(AlertaEnvio *alertView, int buttonIndex))onButtonTouchUpInside;

- (void)deviceOrientationDidChange:(NSNotification *)notification;
- (void)dealloc;
- (void)setColorTitulo:(UIColor *)color;
-(void)setColorSub: (UIColor *)color;

@end

