//
//  claveActivacionViewController.m
//  APIToken
//
//  Created by Sergio Berlanga Alvarez on 05/07/16.
//  Copyright © 2016 CGI. All rights reserved.
//

#import "claveActivacionViewController.h"
#import "ConstantsToken.h"
#import "ContratacionSTDelegate.h"
#import "EditableViewController+protectedCategory.h"
#import <QuartzCore/QuartzCore.h>
#import "APIToken.h"

//-----------2do xib--------------
//#import "ConstantsToken.h"
#import "SoftokenViewsController.h"
#import "ContratacionSTDelegate.h"
#import "Softtoken.h"
#import "Tools.h"
#import "ToolsAPIToken.h"


#define IMG_ASM                 @"barra_azul.png" //imagen header
#define LBL_CVE_ACT_TEXT            @"Activación"
#define LBL_INFO_TEXT               @"A continuación, debéras introducir tu código de activación que te llego vía SMS."
#define TXT_HEIGHT                  36
#define TABLE_BORDER_COLOR_RED                          128.0f/255.0f
#define TABLE_BORDER_COLOR_GREEN                        128.0f/255.0f
#define TABLE_BORDER_COLOR_BLUE                         128.0f/255.0f
#define LINK_TEXT                   @"¿Necesitas reenviar el código de activación?"
#define SOLICITAR_CODIGO_TEL_NUMBER        @"tel://+525552262685,#39"

//---------CONSTANTES 2DO XIB---------------
#define DISTANCE_FROM_TOP           30
#define LABEL_WIDTH                 245
#define LABEL_MARGIN           35
#define IMG_EXITO                    @"icon_paloma_aceptado.png"
#define LBL_FELICIDADES_TEXT        @"¡OPERACIÓN EXITOSA!"
#define TXT_HEIGHT                  36
#define LBL_TITULO_TEXT               @"“La activación fue realizada con éxito, a partir de este momento puedes utilizar tu servicio móvil”"
#define LBL_INFO_TEXT_REACTIVACION  @"La reactivación fue realizada con éxito, a partir de este momento puedes utilizar tu servicio móvil"
#define LBL_INFO_TEXT_ALT           @"La contratación y activación fue realizada con éxito, a partir de este momento puedes utilizar tu servicio móvil"

#define TABLE_BORDER_COLOR_RED                          128.0f/255.0f
#define TABLE_BORDER_COLOR_GREEN                        128.0f/255.0f
#define TABLE_BORDER_COLOR_BLUE                         128.0f/255.0f

//---------------FIN CONSTANTES 2DO XIB-----------------------


@interface claveActivacionViewController ()
@end

@implementation claveActivacionViewController
@synthesize TextCodigo;
@synthesize lblIntroducircodigo;
@synthesize contratacionSTDelegate = _contratacionSTDelegate;
UIImageView *imageView;


//---------------variables 2do XIB--------------------
@synthesize lblTitulo;
@synthesize lblSubtitulo;
@synthesize viewExito;
@synthesize btnExitoOutlet;
@synthesize imgExito;


//linea comentada solicitar codigo
- (IBAction)btnActivar:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:SOLICITAR_CODIGO_TEL_NUMBER]];
    [self ejecutaAccionBotonActivar];
}

- (IBAction)reenviarTouch:(id)sender {
    [_contratacionSTDelegate mostrarAlertCambioNumeroCompaniaDesdeReenviarCodigo];
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    
    //[self setLblActivacion];
    [self setImgCelularActivacion];
    [self setLblIntroducircodigo];
    [self setTextCodigo];
    [self setLink];
    [self setBotonActivar];
    [super viewDidLoad];
    
    //[_headerView setHeaderTitle:@"Activación 1"];
    //[_headerView setHeaderImageBackground:[UIImage imageNamed:@"barra_azul.png"]];

    //[_headerView setBtnIzquierdoImage:[UIImage imageNamed:@"al_ic_regresar.png"]];
    
    [self showHeader:NO];
    [self ocultaEncabezado:YES];
    [self configView];

    
    //[self setHeaderTitle: ST_HEADER_ACTIVACION];
    //[self setHeaderImage:[UIImage imageNamed:IMG_ASM]];
    //[self showHeader:YES];
    [self activarBotonActivar:YES];
    // Do any additional setup after loading the view from its nib.
    
    //Se comento la linea de abajo por que su evento esta comentado, es el de habilitar el boton continuar
    //[TextCodigo addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    // Guardo sesión para poder guardar el número de teléfono y poder precargarlo en pantalla si el usuario se sale ahora
    
    BOOL estatusIVR = [ContratacionSTDelegate isIVR];
    [[APIToken getInstance].apiTokenDelegate storeSession];
    [ContratacionSTDelegate guardarEstatusIVR:estatusIVR];
    
}

-(void) setBotonActivar{
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    //Modificacion pantalla
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        //its iPhone. Find out which one?
        
        CGSize resultS = [[UIScreen mainScreen] bounds].size;
        if(resultS.height == 480)
        {
            // iPhone Classic
            _btnActivarOutlet.frame = CGRectMake(LAT_MARGIN, 435/*TextCodigo.frame.origin.y+TextCodigo.frame.size.height+100*/, screenWidth -40, 40 );
            NSLog(@"iphone 4s");
        }
        else if(resultS.height == 568.0f)
        {
            _btnActivarOutlet.frame = CGRectMake(LAT_MARGIN, TextCodigo.frame.origin.y+TextCodigo.frame.size.height+140/*TextCodigo.frame.origin.y+TextCodigo.frame.size.height+100*/, screenWidth -40, 40 );
            NSLog(@"iphone 5");
        }
        else if(resultS.height == 667.0f)
        {
            _btnActivarOutlet.frame = CGRectMake(LAT_MARGIN, TextCodigo.frame.origin.y+TextCodigo.frame.size.height+140, screenWidth -40, 40 );
            NSLog(@"iphone 6");
        }
        else if(resultS.height == 736.0f)
        {
            _btnActivarOutlet.frame = CGRectMake(LAT_MARGIN, TextCodigo.frame.origin.y+TextCodigo.frame.size.height+140, screenWidth -40, 40 );
            NSLog(@"iphone 6+");
        }
    }
    //_btnActivarOutlet.frame = CGRectMake(LAT_MARGIN, self.view.bounds.size.height - 130/*TextCodigo.frame.origin.y+TextCodigo.frame.size.height+100*/, screenWidth -40, 40 );
}

-(void) setBotonSolicitarClave {
    //    [_btnSolicitarCodigo setHidden:![_contratacionSTDelegate mostrarBotonSlicitarClave]];
    //    [_btnSolicitarCodigo setHidden:[_contratacionSTDelegate mostrarBotonSlicitarClave]];
}

-(void)setLblActivacion
{
    _lblActivacion.text = LBL_CVE_ACT_TEXT;
    _lblActivacion.frame = CGRectMake(LAT_MARGIN,DISTANCE_BTW_COMP, COMPONENT_WIDTH, LBL_HEIGHT);
    _lblActivacion.font = FUENTE_CUERPO;
    _lblActivacion.backgroundColor = [UIColor clearColor];
    _lblActivacion.textColor = COLOR_GRIS_TEXTO_1;
}

-(void)setImgCelularActivacion{
    UIImage* im = [UIImage imageNamed:@"icon_celular_candado"];
    CGFloat imgHeight = im.size.height;
    CGFloat imgWidth = im.size.width;
    /*UIImageView **/imageView = [[UIImageView alloc] initWithFrame:CGRectMake(LAT_MARGIN + 85, DISTANCE_BTW_COMP + 90, imgWidth-80, imgHeight-125)];
    [imageView setImage:[UIImage imageNamed:@"icon_celular_candado"]];
    //imageView = [[UIImageView alloc] initWithFrame:CGRectMake(LAT_MARGIN + 80, DISTANCE_BTW_COMP + _lblActivacion.frame.size.height + 40, imgWidth-175, imgHeight-250)];
    [self.view addSubview:imageView];
}

-(void)setLblIntroducircodigo
{
    lblIntroducircodigo.text = LBL_INFO_TEXT;
    lblIntroducircodigo.frame = CGRectMake(LAT_MARGIN, imageView.frame.origin.y + 160 +DISTANCE_BTW_COMP, COMPONENT_WIDTH, LBL_HEIGHT*3);
    lblIntroducircodigo.numberOfLines = 3;
    lblIntroducircodigo.font = FUENTE_CUERPO;
    lblIntroducircodigo.backgroundColor = [UIColor clearColor];
    lblIntroducircodigo.textColor = COLOR_GRIS_TEXTO_1;
}

-(void)setTextCodigo
{
    TextCodigo.delegate = self;
    TextCodigo.frame = CGRectMake(LAT_MARGIN, lblIntroducircodigo.frame.origin.y+lblIntroducircodigo.frame.size.height, COMPONENT_WIDTH, TXT_HEIGHT);
    TextCodigo.layer.cornerRadius = CORNER_RADIOUS;
    TextCodigo.layer.borderWidth = BORDER_WIDTH;
    TextCodigo.layer.borderColor = [UIColor colorWithRed:TABLE_BORDER_COLOR_RED green:TABLE_BORDER_COLOR_GREEN blue:TABLE_BORDER_COLOR_BLUE alpha:1].CGColor;
}

//configurar boton de nuevo codigo
-(void)setLink
{

    _lblReenviar.text = LINK_TEXT;
    _lblReenviar.frame = CGRectMake(40, TextCodigo.frame.origin.y + TextCodigo.frame.size.height + DISTANCE_BTW_COMP, 280, LBL_HEIGHT);
    _lblReenviar.font = FUENTE_CUERPO_2;
    _lblReenviar.textColor = COLOR_1ER_AZUL;
    [_lblReenviar sizeToFit];
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = COLOR_1ER_AZUL;
    lineView.frame = CGRectMake(_lblReenviar.frame.origin.x , _lblReenviar.frame.origin.y + _lblReenviar.frame.size.height -3, _lblReenviar.frame.size.width, 1);
    [self.view addSubview:lineView];
    _btnReenviar.frame = _lblReenviar.frame;
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [self stopActivityIndicator];
    [super viewWillDisappear:animated];
}

//Esto se ejecuta en la etiqueta
//- (IBAction)reenviarTouch:(id)sender {
//    [_contratacionSTDelegate mostrarAlertCambioNumeroCompaniaDesdeReenviarCodigo];
//}


-(void)ejecutaAccionBotonActivar{
    
    [TextCodigo resignFirstResponder];
    
    if([ContratacionSTDelegate validaCampos:self]){
        [_contratacionSTDelegate borraPendienteDescarga];
        [[APIToken getInstance].apiTokenDelegate setCveReactivacionST:TextCodigo.text];
        [self startActivityIndicator];
        [_contratacionSTDelegate continuarActivacion];
        _contratacionSTDelegate.claveActivacionViewController = self;
        //[self presentSplash];
        
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setLblActivacion:nil];
    [self setTextCodigo:nil];
    [self setLblIntroducircodigo:nil];
    [self setLblReenviar:nil];
    [self setBtnReenviar:nil];
    [self setBtnActivarOutlet:nil];
    [super viewDidUnload];
}

#pragma mark -

#pragma mark -

- (NSInteger)maxSizeForTextField:(UITextField *)aTextField {
    NSInteger result = -1;
    
    if (aTextField == TextCodigo) {
        result = CVE_ACTIVACION_ST_REFERENCE_LENGTH;
    }
    return result;
}

/**
 * Child classes informs editable view controller whether the provided text field contains
 * an integer number. Both text field in view controller are integer numbers
 *
 * @param aTextField The text field to test
 * @return YES in all cases
 */
- (BOOL) textFieldIsInteger: (UITextField*) aTextField {
    return YES;
}

/**
 * Invoked by framework when a text field asks to start editing. Pop buttons view state is updated accodingly
 *
 * @param textField The text field asking to start editing
 * @return YES if editing can start, NO otherwise
 */
- (BOOL) textFieldShouldBeginEditing: (UITextField*) textField {
    BOOL result = YES;
    
    if (textField == TextCodigo) {
        [_popButtonsView enableNextResponderButton:NO];
        [_popButtonsView enablePreviousResponderButton: NO];
        _editedView = TextCodigo;
        _infoEdited = cast_cveActivacion;
    } else {
        [_popButtonsView enableNextResponderButton: NO];
        [_popButtonsView enablePreviousResponderButton: NO];
        _editedView = nil;
        _infoEdited = cast_None;
        result = NO;
    }
    
    return result;
}

//habilita el boton de continuar una vez que se llena el text field
- (void)textFieldDidChange:(UITextField *)textField {
    NSInteger result = -1;
    
    if (textField == TextCodigo) {
        result = CVE_ACTIVACION_ST_REFERENCE_LENGTH;
        [self activarBotonActivar:textField.text.length == result]; //linea comentada

    }
}


/**
 * Informs the pop buttons view delegate that OK button was clicked. Keypad is dismissed
 *
 * @param aPopButtonsView The pop buttons view triggering the event
 */
- (void) okButtonClickedInPopButtonsView: (PopButtonsView*) aPopButtonsView {
    [super okButtonClickedInPopButtonsView: aPopButtonsView];
    
    _editedView = nil;
    
    if (_infoEdited == cast_cveActivacion) {
        [TextCodigo resignFirstResponder];
    }
}




//- (IBAction)solicitarCodigo:(id)sender {
//    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:SOLICITAR_CODIGO_TEL_NUMBER]];
//    
//}
//@end




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

// ---------------------------------------------------------------

- (void)activarBotonActivar:(BOOL)enable {
    _btnActivarOutlet.enabled = enable;
    //_headerView.btnMenu.enabled = enable;
}


//          Metodo encabezado Lord
// --------------------------------------------

- (void)configView {
    
    UIImageView *bgImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 20, _headerView.bounds.size.width, _headerView.bounds.size.height)];
    [bgImg setImage:[UIImage imageNamed:IMG_ASM]];
    [self.view addSubview:bgImg];
    
    UIView *bar = [[UIView alloc] initWithFrame:CGRectMake(0, 20, _headerView.bounds.size.width, _headerView.bounds.size.height)];
    [bar setBackgroundColor:[UIColor colorWithRed:30/255.0 green:80/255.0 blue:130.0/255.0 alpha:0]];
    
    for (UIView *v in [_headerView subviews]) {
        if ([v isKindOfClass:[UIImageView class]]) {
            [v removeFromSuperview];
        }
    }
    UIButton *btnBack = [[UIButton alloc] initWithFrame:CGRectMake(20, 7, 30, 28)];
    
    [btnBack setImage:[UIImage imageNamed:@"al_ic_regresar.png"] forState:UIControlStateNormal];
    [btnBack setTitle:@"" forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(accionBotonIzquierdo) forControlEvents:UIControlEventTouchUpInside];
    [bar addSubview:btnBack];
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-110, 13, 220, 21)];
    [title setText:LBL_CVE_ACT_TEXT];
    [title setTextColor:[UIColor whiteColor]];
    [title setTextAlignment:NSTextAlignmentCenter];
    [title setFont:[UIFont fontWithName:HELVETICA_NEUE_LT_STD_55_ROMAN size:16.0f]];
    [title setAdjustsFontSizeToFitWidth:YES];
    [title setMinimumScaleFactor:0.7];
    [bar addSubview:title];
    
    [self.view addSubview:bar];
//    if ([Tools isIphone5]) {
//        [_transparentScroll setFrame:CGRectMake(0, 180, _transparentScroll.frame.size.width, self.view.frame.size.height-180)];
//    } else {
//        [_transparentScroll setFrame:CGRectMake(0, 180, _transparentScroll.frame.size.width, self.view.frame.size.height-240)];
//    }
//    
//    
//    _generaOTP = NO;
//    [self hideAuthFields];
//    
//    _frameGuia = 0;
//    _instrumento.secureTextEntry = YES;
//    
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(consultaTerminosYCondiciones)];
//    [self.lblTerms setUserInteractionEnabled:YES];
//    [self.lblTerms addGestureRecognizer:tap];
}


//---------------------------------------------------------------
//---------EMPIEZA 2DO XIB --------------------------------------

-(void)setLblTitulo{
    lblTitulo.text = LBL_FELICIDADES_TEXT;
    lblTitulo.frame = CGRectMake(LABEL_MARGIN+40, DISTANCE_FROM_TOP+80, LABEL_WIDTH, LBL_HEIGHT);
    lblTitulo.font = FUENTE_CUERPO;
    lblTitulo.backgroundColor = [UIColor clearColor];
    lblTitulo.textColor = [UIColor colorWithRed:66.0f/255.0f green:139.0f/255.0f blue:18.0f/255.0f alpha:1.0f];
    
}

-(void)setLblSubtitulo{
    if(_contratacionSTDelegate.es2x1){
        lblSubtitulo.text = LBL_INFO_TEXT_ALT;
    } else if (_contratacionSTDelegate.esReactiva2x1) {
        lblSubtitulo.text = LBL_INFO_TEXT_REACTIVACION;
    } else {
        lblSubtitulo.text = LBL_TITULO_TEXT;
    }
    
    lblSubtitulo.font = FUENTE_CUERPO;
    lblSubtitulo.frame = CGRectMake(LABEL_MARGIN, imgExito.frame.origin.y +90, LABEL_WIDTH, LBL_HEIGHT*8);
    lblSubtitulo.numberOfLines = 4;
    lblSubtitulo.backgroundColor = [UIColor clearColor];
    lblSubtitulo.textColor = [UIColor blackColor];
    
}


-(void) setBtnExitoOutlet {
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    
    //Modificacion pantalla
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        //its iPhone. Find out which one?
        
        CGSize resultS = [[UIScreen mainScreen] bounds].size;
        if(resultS.height == 480)
        {
            // iPhone Classic
            btnExitoOutlet.frame = CGRectMake(LAT_MARGIN, 435/*TextCodigo.frame.origin.y+TextCodigo.frame.size.height+100*/, screenWidth -40, 40 );
            NSLog(@"iphone 4s");
        }
        else if(resultS.height == 568.0f)
        {
            btnExitoOutlet.frame = CGRectMake(LAT_MARGIN, lblSubtitulo.frame.origin.y+190/*TextCodigo.frame.origin.y+TextCodigo.frame.size.height+100*/, screenWidth -40, 40 );
            NSLog(@"iphone 5");
        }
        else if(resultS.height == 667)
        {
            btnExitoOutlet.frame = CGRectMake(LAT_MARGIN, lblSubtitulo.frame.origin.y+190/*TextCodigo.frame.origin.y+TextCodigo.frame.size.height+100*/, screenWidth -40, 40 );
            NSLog(@"iphone 6");
        }
        else if(resultS.height == 736)
        {
            btnExitoOutlet.frame = CGRectMake(LAT_MARGIN, lblSubtitulo.frame.origin.y+190/*TextCodigo.frame.origin.y+TextCodigo.frame.size.height+100*/, screenWidth -40, 40 );
            NSLog(@"iphone 6+");
        }
    }
 
    
}

-(void)setImgExito {
    UIImage* im = [UIImage imageNamed:IMG_EXITO];
    CGFloat imgHeight = im.size.height;
    CGFloat imgWidth = im.size.width;
    imgExito.frame = CGRectMake(LAT_MARGIN + 50, lblTitulo.frame.origin.y+DISTANCE_BTW_COMP+10, imgWidth/3, imgHeight/3);
    imgExito.image = [UIImage imageNamed:IMG_EXITO];
    
}

//-(void)setImgFondo {
//    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
//    //CGSize screenHeight = [[UIScreen mainScreen] bounds].size;
//    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
//    
//    imgFondo.frame = CGRectMake(0, 0, screenWidth, screenHeight);
//    imgFondo.backgroundColor = [UIColor lightGrayColor];
//    imgFondo.alpha = 0.5f;
//    
//    [imgFondo addSubview:_viewSplashExito];
//    [self.view addSubview:imgFondo];
//    
//}

-(void)ejecutaAccionBotonDerecho{
    NSString* banderasBMovilFilePath = [ToolsAPIToken getBanderasBMovilFilePath];
    NSDictionary* banderasBMovilDictionary = [NSDictionary dictionaryWithContentsOfFile: banderasBMovilFilePath];
    NSLog(@"banderasBMovilDictionary%@",banderasBMovilDictionary);
    
    
    //Mostramos el menuSuite
    [self.navigationController popToRootViewControllerAnimated:YES];
    
    // Si se cumple EA#7 mostramos pantalla inicial
    if ([[banderasBMovilDictionary objectForKey:CAMBIO_DE_PERFIL_KEY] boolValue]) {
        [[APIToken getInstance].apiTokenDelegate bmovilLaunched];
    }
    
}

-(void)presentSplash {
    [viewExito setFrame:CGRectMake(0.0f, 0.0f, self.view.frame.size.width, self.view.frame.size.height)];
    [self setLblTitulo];
    [self setImgExito];
    [self setLblSubtitulo];
    [self setBtnExitoOutlet];
    [self.view addSubview:_viewContenedorExito];
    //[_viewContenedorExito addSubview:lblTitulo];
    //[_viewContenedorExito addSubview:lblSubtitulo];
    //[_viewContenedorExito addSubview:imgExito];
    

}






- (IBAction)btnAccionExito:(id)sender {
    [self ejecutaAccionBotonDerecho];
}
@end
