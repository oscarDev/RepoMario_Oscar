//
//  AltaRegistroViewController.h
//  Bancomer
//
//  Created by Karen Romero on 19/06/15.
//  Copyright (c) 2015 GoNet. All rights reserved.
//

#import "EditableViewController.h"

#define ALTA_REGISTRO_XIB           @"AltaRegistroView"

@class GeneraOTPSTDelegate;
@interface AltaRegistroViewController : EditableViewController {
    
    NSMutableDictionary *operacionAlta;
    NSString *qrLeido;
}

@property (nonatomic,strong) GeneraOTPSTDelegate *generaOTPSTDelegate;
@property (strong, nonatomic) IBOutlet UILabel *lblAlta;

//@property (strong, nonatomic) IBOutlet UIButton *btnConfirmarOperacion; //Mod 50635
@property (strong, nonatomic) IBOutlet UIButton *btnCancelarOperacion;

@property (strong, nonatomic) IBOutlet UILabel *lblInfo01;
@property (strong, nonatomic) IBOutlet UILabel *lblfromAccount;
@property (strong, nonatomic) IBOutlet UILabel *lblInfo02;
@property (strong, nonatomic) IBOutlet UILabel *lbltoAccount;
@property (strong, nonatomic) IBOutlet UILabel *lblInfo03;


- (id)initWithOperacion:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil operacion:(NSMutableDictionary *)operacion qrLeido: (NSString *)qr;
- (IBAction)accionCancelarOperacion:(id)sender;

@end
