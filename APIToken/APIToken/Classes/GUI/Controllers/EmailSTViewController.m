//
//  EmailSTViewController.m
//  Bancomer
//
//  Created by Francisco.Garcia on 28/05/13.
//
//

#import "EmailSTViewController.h"
//#import "Session.h"
#import "ConstantsToken.h"
#import "SoftokenViewsController.h"
#import "ContratacionSTDelegate.h"
#import <QuartzCore/QuartzCore.h>
#import "Softtoken.h"
#import "Tools.h"
#import "ToolsAPIToken.h"
#import "APIToken.h"

#define DISTANCE_FROM_TOP           30
#define LABEL_WIDTH                 245
#define LABEL_MARGIN           35
#define IMG_ASM                     @"IoIcAcceso.png"
#define LBL_FELICIDADES_TEXT        @"¡Felicidades!"
#define TXT_HEIGHT                  36
#define LBL_INFO_TEXT               @"La activación de tu Token Móvil fue realizada con éxito. Ahora podrás hacer uso de él en tu celular."
#define LBL_INFO_TEXT_REACTIVACION  @"La reactivación de tus servicios de Bancomer móvil y Token móvil fue realizada con éxito. Ahora podrás hacer uso de ellos en tu celular."
#define LBL_INFO_TEXT_ALT           @"La contratación y activación de tus servicios de Bancomer móvil y Token Móvil fue realizada con éxito. Ahora podrás hacer uso de ellos en tu celular."

#define TABLE_BORDER_COLOR_RED                          128.0f/255.0f
#define TABLE_BORDER_COLOR_GREEN                        128.0f/255.0f
#define TABLE_BORDER_COLOR_BLUE                         128.0f/255.0f

@interface EmailSTViewController ()

@end

@implementation EmailSTViewController
@synthesize lblFelicidades;
@synthesize lblInfo;
@synthesize contratacionSTDelegate = _contratacionSTDelegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [self setLblFelicidades];
    [self setLblInfo];
    [super viewDidLoad];
   
    
    [self setHeaderTitle: ST_HEADER_ACTIVACION];
	[self setHeaderImage:[UIImage imageNamed:IMG_ASM]];
    [self showHeader:YES];
    [self muestraBotonDerecho:BOTON_SIGUIENTE];
    [self ocultarBotonAtras:YES];
}

-(void)setLblFelicidades
{
    lblFelicidades.text = LBL_FELICIDADES_TEXT;
    lblFelicidades.frame = CGRectMake(LABEL_MARGIN, DISTANCE_FROM_TOP, LABEL_WIDTH, LBL_HEIGHT);
    lblFelicidades.font = FUENTE_CUERPO;
    lblFelicidades.backgroundColor = [UIColor clearColor];
    lblFelicidades.textColor = [UIColor colorWithRed:66.0f/255.0f green:139.0f/255.0f blue:18.0f/255.0f alpha:1.0f];
}

-(void)setLblInfo
{
    if(_contratacionSTDelegate.es2x1){
        lblInfo.text = LBL_INFO_TEXT_ALT;
    } else if (_contratacionSTDelegate.esReactiva2x1) {
        lblInfo.text = LBL_INFO_TEXT_REACTIVACION;
    } else {
        lblInfo.text = LBL_INFO_TEXT;
    }
    
    lblInfo.font = FUENTE_CUERPO;
    lblInfo.frame = CGRectMake(LABEL_MARGIN,lblFelicidades.frame.origin.y /*+ LBL_HEIGHT*/, LABEL_WIDTH, LBL_HEIGHT*8);
    lblInfo.numberOfLines = 0;
    lblInfo.backgroundColor = [UIColor clearColor];
    lblInfo.textColor = [UIColor blackColor];
}

-(void)ejecutaAccionBotonDerecho{
    NSString* banderasBMovilFilePath = [ToolsAPIToken getBanderasBMovilFilePath];
    NSDictionary* banderasBMovilDictionary = [NSDictionary dictionaryWithContentsOfFile: banderasBMovilFilePath];
    NSLog(@"banderasBMovilDictionary%@",banderasBMovilDictionary);
    
    
    //Mostramos el menuSuite
    [self.navigationController popToRootViewControllerAnimated:YES];
    
    // Si se cumple EA#7 mostramos pantalla inicial
    if ([[banderasBMovilDictionary objectForKey:CAMBIO_DE_PERFIL_KEY] boolValue]) {
        [[APIToken getInstance].apiTokenDelegate bmovilLaunched];
    }
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setLblFelicidades:nil];
    [self setLblInfo:nil];
    [super viewDidUnload];
}
@end
