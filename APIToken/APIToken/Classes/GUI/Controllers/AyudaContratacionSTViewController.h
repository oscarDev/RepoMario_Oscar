//
//  AyudaContratacionSTViewController.h
//  Bancomer
//
//  Created by Francisco.Garcia on 27/05/13.
//
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@class ContratacionSTDelegate;

#define AYUDA_CONTRATACION_ST_XIB         @"AyudaContratacionSTView"

@interface AyudaContratacionSTViewController : BaseViewController

@property(nonatomic,strong)ContratacionSTDelegate* contratacionSTDelegate;
@property (strong, nonatomic) IBOutlet UILabel *lblTitulo;
@property (strong, nonatomic) IBOutlet UILabel *lblBienvenida;
@property (strong, nonatomic) IBOutlet UILabel *lblBienvenida2;
@property (strong, nonatomic) IBOutlet UIButton *btnContinuar;
@end
