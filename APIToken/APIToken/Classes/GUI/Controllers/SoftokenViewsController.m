//
//  SoftokenViewsController.m
//  Bancomer
//
//  Created by Francisco.Garcia on 27/05/13.
//
//

#import "SoftokenViewsController.h"
#import "AyudaContratacionSTViewController.h"
#import "IngresaDatosSTViewController.h" // linea a reemplazar
#import "ContratarTokenController.h" //linea nueva
#import "TipoSolicitudResult.h"
#import "ConfirmacionSTViewController.h"
#import "AutenticacionSTViewController.h"
#import "EmailSTViewController.h"  //linea comentada, VC a reemplazar
//#import "OperacionExitosaToken.h"
//#import "ClaveActivacionSTViewController.h"
#import "GeneraOTPSTViewController.h"
#import "MuestraOTPSTViewController.h"
#import "ContratacionSTDelegate.h"
//#import "ClaveActivacionSTViewController.h"
#import "GeneraOTPSTDelegate.h"
#import "RegistraCuentaSTViewController.h"
#import "ConstantsToken.h"
#import "Reachability.h"
#import "Softtoken.h"
#import "AltaRegistroViewController.h"   // Modificación 50683
#import "Tools.h"
#import "ServerToken.h"
#import "SofttokenApp.h"
#import "APIToken.h"
#import "ToolsAPIToken.h"
#import "ServerResponse.h"
#import "claveActivacionViewController.h"
#import "AlertaEnvio.h"

@implementation SoftokenViewsController

- (id)initWithSoftDelegate:(SofttokenApp*)softAppDelegate andStatus:(BOOL)status andMostrarPantallaToken:(BOOL)mostrarPantallaToken {
    
    self = [super init];
    if (self) {
        self.softDelegate = softAppDelegate;
        if(status) {
            [self showGeneraOTPS];
            
        } else {
            
            //EA#10
            Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
            NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
            
            if (networkStatus == NotReachable) {
                
                [[[UIAlertView alloc] initWithTitle: AVISO message: SUITEMENU_NOT_REACHABLE_CONNECTION delegate:self cancelButtonTitle:COMMON_ACCEPT otherButtonTitles:nil , nil]  show];
                
            } else {
                // Continúa el flujo normal del EA#2: paso 6
                _delegateContratacionST = [[ContratacionSTDelegate alloc]init];
                if ([_delegateContratacionST consultaArchivoPendienteDescarga]) {
                    [_delegateContratacionST cargaArchivoPendienteDescarga];
                    
                    if (!_controladorConfirmacionST) {
                        _controladorConfirmacionST = [[ConfirmacionSTViewController alloc] initWithNibName:CONFIRMACION_ST_XIB bundle:[NSBundle mainBundle]];
                    }
                    _delegateContratacionST.confirmacionST = _controladorConfirmacionST;
                    _delegateContratacionST.confirmacionST.tiposolicitud = REACTIVACION;
                    [self showClaveActivacionST];   // modif, linea comentada para llamar a la nueva
                    
                    
                    NSMutableDictionary* dictionaryAppSession;
                    NSString* appSessionFilePath = [ToolsAPIToken getApplicationSessionFilePath];
                    
                    dictionaryAppSession = [NSMutableDictionary dictionaryWithContentsOfFile: appSessionFilePath];

                    if ([[dictionaryAppSession objectForKey:IVR_KEY] boolValue]) {
                        //linea comentada se agrego lo de abajo hasta el mensaje
                        
                        AlertaEnvio *sp_AlertViewEnvio = [[AlertaEnvio alloc] initWithTitle: AVISO andImageName:@"Ios_Im_godinbmovil" andMessage:@"En breve recibirás un mensaje con el código de activación para comenzar a utilizar tu servicio móvil." closable:NO];
                        sp_AlertViewEnvio.tag = TAG_ALERTA_SOLICITAR_COD_ACTIVACION;
                        [sp_AlertViewEnvio setDelegate:_delegateContratacionST];
                        [sp_AlertViewEnvio setColorTitulo:COLOR_1ER_AZUL];
                        [sp_AlertViewEnvio show];

//                        PopUpImageViewController *sp_AlertView = [[PopUpImageViewController alloc] initWithTitle: AVISO andImageName:@"Ios_Im_godinbmovil" andMessage:@"En breve recibirás un mensaje con el código de activación para comenzar a utilizar tu servicio móvil." closable:NO];
//                        sp_AlertView.backgroundColor = [UIColor blackColor];
//                        sp_AlertView.alpha = 0.5f;
//                        sp_AlertView.tag = TAG_ALERTA_SOLICITAR_COD_ACTIVACION;
//                        [sp_AlertView setDelegate:_delegateContratacionST];
//                        [sp_AlertView setColorTitulo:COLOR_1ER_AZUL];
//                        [sp_AlertView show];
                    }
                } else {
                    if (mostrarPantallaToken) {
                        [self showIngresaDatosST];
                    } else {
                        //¿Hago algo?
                    }
                }
            }
            
            
        }
    }
    return self;
}

- (id)initAutenticacionSTWithSoftDelegate:(SofttokenApp*)softAppDelegate {
    self = [super init];
    if (self) {
        self.softDelegate = softAppDelegate;
        [self showAutenticacionST];
    }
    
    return self;
}

/**
 * Se inicializa con el delegado de soft token
 */
//- (id)initWithSoftDelegate:(SofttokenApp*)softAppDelegate andStatus:(BOOL)status {
//    return [self initWithSoftDelegate:softAppDelegate andStatus:status andMostrarPantallaToken:YES];
//}

/**
 * The request was cancelled. Removes the activity indicator
 *
 * @param aServer The server triggering the event
 * @param aDownloadId The download identification that was cancelled
 */
- (void) cancelledServer: (Server*) aServer download: (NSInteger) aDownloadId {
    //[self.appDelegate.controladorBmovil.controladorBase stopActivityIndicator];
}

/**
 * The request finished correctly
 *
 * @param aServer The server triggering the event
 * @param aDownloadId The download identification that finished correctly
 * @param aServerResponse The server response information
 */
- (void) server: (Server*) aServer download: (NSInteger) aDownloadId finishedWithResponse: (ServerResponse*) aServerResponse {
    
    //[self.appDelegate.controladorBmovil.controladorBase stopActivityIndicator];
    
    NSInteger operationType = [self checkDownloadId: aDownloadId];
    
	if (operationType >= 0)
    {
		if ([self.softDelegate analyzeServerResponse: aServerResponse forOperation: operationType] == YES)
        {
			[self networkResponse: aServerResponse];
		}
        else if (aServerResponse.status == OPERATION_ERROR)
        {
            [self serverError];
		}
	}
}



/**
 * The request finished with an error. An error message is shown to the user
 *
 * @param aServer The server triggering the event
 * @param aDownloadId The download identification that finished with an error
 */
- (void) erroredServer: (Server*) aServer download: (NSInteger) aDownloadId {
    
    [_controladorBase stopActivityIndicator];
    [self.softDelegate showCommunicationErrorAlert];
	[self serverError];
    
}

/*
 * Checks whether the download iddentification is valid, and returns the operation type associated
 */
- (NSInteger) checkDownloadId: (NSInteger) aDownloadId {
	return CLOSE_SESSION_OPERATION;
}

/*
 * Child classes must implement this method to send the server response to delegate
 */
- (void) networkResponse: (ServerResponse*) aServerResponse
{
    NSLog(@"_softDelegate analyzeResponse:aServerResponse");
   // [_softDelegate analyzeResponse:aServerResponse];
}

/*
 * Child classes must implement this method to respond to server error responses
 */
- (void) serverError {
}

-(void)showIngresaDatosST
{
    [self removeIngresaDatosST];
    _controladorIngresaDatosST = [[IngresaDatosSTViewController alloc]initWithNibName:INGRESA_DATOS_ST_XIB bundle:[NSBundle mainBundle]];
    _delegateContratacionST = [[ContratacionSTDelegate alloc] init];

    _controladorIngresaDatosST.contratacionSTDelegate = _delegateContratacionST;
    _delegateContratacionST.controladorIngresaDatosST = _controladorIngresaDatosST;

    [(UINavigationController *)([[UIApplication sharedApplication].delegate window].rootViewController)  pushViewController:_controladorIngresaDatosST animated:YES];
   // [self removeAyudaST];
    
}

-(void)removeIngresaDatosST
{
    if(_controladorIngresaDatosST)
    {
        //[_controladorIngresaDatosST release];
        _controladorIngresaDatosST = nil;
    }
}

-(void)showConfirmacionST:(Softtoken*)result
{
    [self removeConfirmacionST];
    _controladorConfirmacionST = [[ConfirmacionSTViewController alloc] initWithNibName:CONFIRMACION_ST_XIB bundle:[NSBundle mainBundle]];

    if(!_delegateContratacionST)
    {
        _delegateContratacionST = [[ContratacionSTDelegate alloc] init];
    }
    _controladorConfirmacionST.contratacionSTDelegate = _delegateContratacionST;
    _delegateContratacionST.confirmacionST = _controladorConfirmacionST;
 /*   _controladorConfirmacionST.softDelegate = _softDelegate;
    _controladorConfirmacionST.controladorSofttoken = self;
    _controladorConfirmacionST.controladorSuite = _controladorSuite;*/
    NSLog(@"Indicador de solicitud %@",result.tipoSolicitud);
    
    [[APIToken getInstance].apiTokenDelegate setEmailST:result.correo];
    
    if([result.tipoSolicitud isEqualToString:@"N"])
    {
        [_controladorConfirmacionST configuraParaOperacion:NUEVO];
    }
    else if([result.tipoSolicitud  isEqualToString:@"R"])
    {
        [_controladorConfirmacionST configuraParaOperacion:REACTIVACION];
    }
    else if([result.tipoSolicitud  isEqualToString:@"S"])
    {
        [_controladorConfirmacionST configuraParaOperacion:SUSTITUCION];
    }
    
    _delegateContratacionST.viewController = _controladorConfirmacionST;
    
    [(UINavigationController *)([[UIApplication sharedApplication].delegate window].rootViewController)  pushViewController:_controladorConfirmacionST animated:YES];
}

-(void)removeConfirmacionST
{
    if(_controladorConfirmacionST)
    {
        _controladorConfirmacionST = nil;
    }
}

-(void)showAutenticacionST {
    [self removeAutenticacionST];
    
    _controladorAutenticacionST = [[AutenticacionSTViewController alloc] initWithNibName:AUTENTICACION_ST_XIB bundle:[NSBundle mainBundle]];
    
    if (!_delegateContratacionST) {
        _delegateContratacionST = [[ContratacionSTDelegate alloc] init];
    }
    _controladorAutenticacionST.contratacionSTDelegate = _delegateContratacionST;
    _delegateContratacionST.autenticacionSTViewController = _controladorAutenticacionST;
  /*  _controladorAutenticacionST.softDelegate = _softDelegate;
    _controladorAutenticacionST.controladorSofttoken = self;
    _controladorAutenticacionST.controladorSuite = _controladorSuite;*/
    
    _delegateContratacionST.viewController = _controladorAutenticacionST;
    
    [(UINavigationController *)([[UIApplication sharedApplication].delegate window].rootViewController)  pushViewController:_controladorAutenticacionST animated:YES];
}

-(void)removeAutenticacionST {
    if (_controladorAutenticacionST) {
        _controladorAutenticacionST = nil;
    }
}

//Aqui se manda llamar la ventana ClaveActivacionSTView
-(void)showClaveActivacionST
{
    
    [self removeClaveActivacionST];
    _controladorClaveActivacionST = [[claveActivacionViewController alloc]initWithNibName:CLV_ACTIVACION_ST_XIB bundle:[NSBundle mainBundle]]; // linea comentada se cambio ClaveActivacionSTViewController
    //_controladorClaveActivacionST = [[claveActivacionViewController alloc]initWithArray:CLV_ACTIVACION_ST_XIB bundle:[NSBundle mainBundle] array:_estados]; // se cambio ClaveActivacionSTViewController pero esta linea queda comentada
    if(!_delegateContratacionST)
        {
          _delegateContratacionST = [[ContratacionSTDelegate alloc] init];
        }
    _controladorClaveActivacionST.contratacionSTDelegate = _delegateContratacionST;
    _delegateContratacionST.claveActivacionViewController = _controladorClaveActivacionST;
    //_controladorClaveActivacionST.softDelegate = _softDelegate;
    //_controladorClaveActivacionST.controladorSofttoken = self;
    //s_controladorClaveActivacionST.controladorSuite = _controladorSuite;*/
    //ejemplo [_contratacionSTDelegate borraPendienteDescarga];
    
    
    _delegateContratacionST.viewController = _controladorClaveActivacionST;
    
    //linea comentada, probar el splash view de exito
    [(UINavigationController *)([[UIApplication sharedApplication].delegate window].rootViewController)  pushViewController:_controladorClaveActivacionST animated:YES];
    
}

    
    //LO SIGUIENTE ES EL CÓDIGO ORIGINAL DE ESTA FUNCIÓN COMENTADO:
//    [self removeClaveActivacionST];
//    _controladorClaveActivacionST = [[ClaveActivacionSTViewController alloc]initWithNibName:CLV_ACTIVACION_ST_XIB bundle:[NSBundle mainBundle]];
//  //  _controladorClaveActivacionST = [[ClaveActivacionSTViewController alloc]initWithArray:CLV_ACTIVACION_ST_XIB bundle:[NSBundle mainBundle] array:_estados];
//
//    if(!_delegateContratacionST)
//    {
//        _delegateContratacionST = [[ContratacionSTDelegate alloc] init];
//    }
//    _controladorClaveActivacionST.contratacionSTDelegate = _delegateContratacionST;
//    _delegateContratacionST.claveActivacionSTViewController = _controladorClaveActivacionST;
// /*   _controladorClaveActivacionST.softDelegate = _softDelegate;
//    _controladorClaveActivacionST.controladorSofttoken = self;
//    _controladorClaveActivacionST.controladorSuite = _controladorSuite;*/
//    
//    _delegateContratacionST.viewController = _controladorClaveActivacionST;
//    
//    [(UINavigationController *)([[UIApplication sharedApplication].delegate window].rootViewController)  pushViewController:_controladorClaveActivacionST animated:YES];
//}

-(void)removeClaveActivacionST
{
    if(_controladorClaveActivacionST)
    {
        //[_controladorClaveActivacionST release];
        _controladorClaveActivacionST = nil;
    }
}

-(void)showEmailST
{
//-----------------INICIO METODO MODIFICADO COMENTADO----------------------
    
//    [self removeEmailST];
//    //Se cambio el xib de abajo modif splash
//    _controladorEmailST = [[EmailSTViewController alloc] initWithNibName:EMAIL_ST_XIB bundle:[NSBundle mainBundle]];
//    //AMZ
//    //Linea comentada Ya estaba comentado esto // _controladorEmailST = [[EmailSTViewController alloc] initWithArray:EMAIL_ST_XIB bundle:[NSBundle mainBundle] array: _estados];
//    
//    if(!_delegateContratacionST)
//    {
//        _delegateContratacionST = [[ContratacionSTDelegate alloc] init];
//    }
//    //linea comentada 6:12 11/07 modif splash
//    _controladorExitosa.contratacionSTDelegate = _delegateContratacionST;
//    _delegateContratacionST.controladorExitosa = _controladorExitosa;
//    
//    //linea comentada para habilitar la de arriba
//    //_controladorClaveActivacionST.contratacionSTDelegate = _delegateContratacionST;
//    //_delegateContratacionST.claveActivacionViewController = _controladorClaveActivacionST;
//    
//    
//    //linea comentada lo de abajo ya estaba comentada
//    /*  _controladorEmailST.softDelegate = _softDelegate;
//     _controladorEmailST.controladorSofttoken = self;
//     _controladorEmailST.controladorSuite = _controladorSuite;*/
//    //linea comentada FIN
//    
//    //Linea comentada probando para el splash email/token exito
//    [(UINavigationController *)([[UIApplication sharedApplication].delegate window].rootViewController)  pushViewController:_controladorExitosa animated:YES];
// 
//    
//    //[(UINavigationController *)([[UIApplication sharedApplication].delegate window].rootViewController)  presentViewController: _controladorExitosa animated:YES completion: nil];
//    
//    //modif splash
//    _delegateContratacionST.viewController = _controladorExitosa;
//    
//    //_delegateContratacionST.viewController = _controladorClaveActivacionST;
    
//-----------------------------FIN COMENTARIO METODO MODIFICADO-------------------
    
    
//      METODO ORIGINAL
    [self removeEmailST];
    _controladorEmailST = [[EmailSTViewController alloc] initWithNibName:EMAIL_ST_XIB bundle:[NSBundle mainBundle]];
    //AMZ
   //Linea comentada Ya estaba comentado esto // _controladorEmailST = [[EmailSTViewController alloc] initWithArray:EMAIL_ST_XIB bundle:[NSBundle mainBundle] array: _estados];

    if(!_delegateContratacionST)
    {
        _delegateContratacionST = [[ContratacionSTDelegate alloc] init];
    }
    _controladorEmailST.contratacionSTDelegate = _delegateContratacionST;
    _delegateContratacionST.controladorEmailST = _controladorEmailST;
    //linea comentada lo de abajo ya estaba comentada
  /*  _controladorEmailST.softDelegate = _softDelegate;
    _controladorEmailST.controladorSofttoken = self;
    _controladorEmailST.controladorSuite = _controladorSuite;*/
    //linea comentada FIN
    [(UINavigationController *)([[UIApplication sharedApplication].delegate window].rootViewController)  pushViewController:_controladorEmailST animated:YES];
    
    _delegateContratacionST.viewController = _controladorEmailST;
    
}

-(void)removeEmailST
{
    if(_controladorEmailST) //linea comentada se reemplazo _controladorEmailST por por _controladorExitosa          modif splash
    {
        //[_controladorEmailST release];
        _controladorEmailST = nil; //linea comentada se reemplazo _controladorEmailST por _controladorExitosa       modif splash
    }
}

-(void)showGeneraOTPS
{
    [self removeGeneraOTPS];
    _controladorGeneraOTPS = [[GeneraOTPSTViewController alloc]initWithNibName:GENERA_OTPST_XIB bundle:[NSBundle mainBundle]];
    if(!_delegateGeneraOTPS)
    {
        _delegateGeneraOTPS = [[GeneraOTPSTDelegate alloc]init];
    }
    _controladorGeneraOTPS.generaOTPSTDelegate = _delegateGeneraOTPS;
    _delegateGeneraOTPS.generaOTPSTViewController = _controladorGeneraOTPS;
   /* _controladorGeneraOTPS.softDelegate = _softDelegate;
    _controladorGeneraOTPS.controladorSofttoken = self;
    _controladorGeneraOTPS.controladorSuite = _controladorSuite;*/
    [(UINavigationController *)([[UIApplication sharedApplication].delegate window].rootViewController)  pushViewController:_controladorGeneraOTPS animated:YES];
    
}

-(void)removeGeneraOTPS
{
    if(_controladorGeneraOTPS)
    {
        _controladorGeneraOTPS = nil;
    }
}

-(void)showMuestraOTPS:(NSString *)otp
{
    [self removeMuestraOTPS];
    _controladorMuestraOTPS = [[MuestraOTPSTViewController alloc]initWithNibName:MUESTRA_OTPST_XIB bundle:[NSBundle mainBundle] andToken:otp];
    //AMZ
   // _controladorMuestraOTPS = [[MuestraOTPSTViewController alloc]initWithArray:MUESTRA_OTPST_XIB bundle:[NSBundle mainBundle] andToken:otp array:_estados];

    if(!_delegateGeneraOTPS)
    {
        _delegateGeneraOTPS = [[GeneraOTPSTDelegate alloc]init];
    }
    _controladorMuestraOTPS.generaOTPSTDelegate = _delegateGeneraOTPS;
    _delegateGeneraOTPS.muestraOTPSTViewController = _controladorMuestraOTPS;
  /*  _controladorMuestraOTPS.softDelegate = _softDelegate;
    _controladorMuestraOTPS.controladorSofttoken = self;
    _controladorMuestraOTPS.controladorSuite = _controladorSuite;*/
    [(UINavigationController *)([[UIApplication sharedApplication].delegate window].rootViewController)  pushViewController:_controladorMuestraOTPS animated:YES];
}

-(void)removeMuestraOTPS
{
    if(_controladorMuestraOTPS)
    {
        _controladorMuestraOTPS = nil;
    }
}

-(void)showRegistraCuentaST
{
    [self removeRegistraCuentaST];
    _controladorRegistraCuentaST = [[RegistraCuentaSTViewController alloc]initWithNibName:REGISTRA_CUENTA_ST_XIB bundle:[NSBundle mainBundle]];
   

    if(!_delegateGeneraOTPS)
    {
        _delegateGeneraOTPS = [[GeneraOTPSTDelegate alloc]init];
    }
    _controladorRegistraCuentaST.delegateGeneraOTPST = _delegateGeneraOTPS;
    _delegateGeneraOTPS.controladorRegistraCuentasST = _controladorRegistraCuentaST;
   /* _controladorRegistraCuentaST.softDelegate = _softDelegate;
    _controladorRegistraCuentaST.controladorSofttoken = self;
    _controladorRegistraCuentaST.controladorSuite = _controladorSuite;*/
    [(UINavigationController *)([[UIApplication sharedApplication].delegate window].rootViewController)  pushViewController:_controladorRegistraCuentaST animated:YES];
}

-(void)removeRegistraCuentaST
{
    if(_controladorRegistraCuentaST)
    {
        _controladorRegistraCuentaST = nil;
    }
}

-(void)touchAtras{
   
}

// Modificación 50683
-(void)muestraAltaRegistro:(NSMutableDictionary *)operacion qrLeido:(NSString *)qr;
{
    [self removeMuestraAltaRegistro];
    _controladorAltaRegistro = [[AltaRegistroViewController alloc]initWithOperacion:ALTA_REGISTRO_XIB bundle:[NSBundle mainBundle] operacion:operacion qrLeido:qr];
    if(!_delegateGeneraOTPS)
    {
        _delegateGeneraOTPS = [[GeneraOTPSTDelegate alloc]init];
    }
    _controladorAltaRegistro.generaOTPSTDelegate = _delegateGeneraOTPS;
    _delegateGeneraOTPS.altaRegistroViewController = _controladorAltaRegistro;
   /* _controladorAltaRegistro.softDelegate = _softDelegate;
    _controladorAltaRegistro.controladorSofttoken = self;
    _controladorAltaRegistro.controladorSuite = _controladorSuite;*/
    [(UINavigationController *)([[UIApplication sharedApplication].delegate window].rootViewController)  pushViewController:_controladorAltaRegistro animated:YES];
}
// Modificación 50683
-(void)removeMuestraAltaRegistro
{
    if(_controladorAltaRegistro)
    {
        _controladorAltaRegistro= nil;
    }
}

+(BOOL) esPantallaSofttoken:(BaseViewController*)clasePantalla {
    BOOL esPantallaToken = NO;
    
    if (([clasePantalla isKindOfClass:[AltaRegistroViewController class]])
        || ([clasePantalla isKindOfClass:[AyudaContratacionSTViewController class]])
        || ([clasePantalla isKindOfClass:[claveActivacionViewController class]])
        || ([clasePantalla isKindOfClass:[ConfirmacionSTViewController class]])
        //|| ([clasePantalla isKindOfClass:[OperacionExitosaToken class]])   //modif splash
        ||([clasePantalla isKindOfClass:[EmailSTViewController class]])//linea comentada VC a reemplazar
        || ([clasePantalla isKindOfClass:[GeneraOTPSTViewController class]])
        || ([clasePantalla isKindOfClass:[IngresaDatosSTViewController class]])
        || ([clasePantalla isKindOfClass:[MuestraOTPSTViewController class]])
        || ([clasePantalla isKindOfClass:[RegistraCuentaSTViewController class]])) {  //// linea comentada, se cambio ClaveActivacionSTViewController
        
        esPantallaToken = YES;
    }
    
    return esPantallaToken;
}

@end
