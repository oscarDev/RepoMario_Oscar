//
//
//  Bancomer
//
//  Created by Francisco.Garcia on 27/05/13.
//
//

#import "BaseViewController.h"
#import "IngresaDatosSTViewController.h" // linea comentada se va a reemplazar este vc 1er pantalla
//#import "ClaveActivacionSTViewController.h" linea comentada se quita la referencia al vc a reemplazar
#import "claveActivacionViewController.h" // se agrego esta linea
#import "ContratarTokenController.h" //linea nueva 1er pantalla

@class BancomerAppDelegate;
@class TipoSolicitudResult;
@class ContratacionSTDelegate;
@class GeneraOTPSTDelegate;

@class SuiteViewsController;
@class AyudaContratacionSTViewController;
@class SofttokenApp;
@class ConfirmacionSTViewController;
@class AutenticacionSTViewController;
@class EmailSTViewController; //linea comentada VC a reemplazar
//@class OperacionExitosaToken; //linea nueva
@class GeneraOTPSTViewController;
@class MuestraOTPSTViewController;
@class RegistraCuentaSTViewController;
@class BaseViewController;
@class Softtoken;
@class AltaRegistroViewController;    // Modificación 50683


@interface SoftokenViewsController : NSObject

/**
 * Referencia del delegado de SoftToken
 */
@property(nonatomic,strong)SofttokenApp *softDelegate;

/**
 * Referencia del controlador principal de la Suite
 */
@property(nonatomic,strong)SuiteViewsController *controladorSuite;

@property(nonatomic,strong)AyudaContratacionSTViewController *controladorAyudaST;

@property(nonatomic,strong)IngresaDatosSTViewController *controladorIngresaDatosST;  //linea comentada 1er pantalla VC a reemplazar

@property (nonatomic, strong)ContratarTokenController *controladorContratarST; //linea nueva 1er pantalla

@property(nonatomic,strong)ContratacionSTDelegate *delegateContratacionST;

@property(nonatomic,strong)ConfirmacionSTViewController *controladorConfirmacionST;

//@property(nonatomic,strong)ClaveActivacionSTViewController  *controladorClaveActivacionST;  linea comentada, se quita la referencia al vc anterior

@property(nonatomic,strong)claveActivacionViewController *controladorClaveActivacionST;  //linea comentadase agrego esta linea

@property(nonatomic,strong)AutenticacionSTViewController *controladorAutenticacionST;

@property(nonatomic,strong)EmailSTViewController *controladorEmailST;  //linea comentada VC a reemplazar

//@property(nonatomic, strong)OperacionExitosaToken *controladorExitosa; //linea comentada nuevo VC

@property(nonatomic,strong)GeneraOTPSTViewController *controladorGeneraOTPS;

@property(nonatomic,strong)GeneraOTPSTDelegate *delegateGeneraOTPS;

@property(nonatomic,strong)MuestraOTPSTViewController *controladorMuestraOTPS;

@property(nonatomic,strong)RegistraCuentaSTViewController *controladorRegistraCuentaST;

@property(nonatomic,strong)BaseViewController *controladorBase;

@property(nonatomic,strong)AltaRegistroViewController *controladorAltaRegistro; // Modificación 50683



- (id)initWithSoftDelegate:(SofttokenApp*)softAppDelegate andStatus:(BOOL)status andMostrarPantallaToken:(BOOL)mostrarPantallaToken;
- (id)initAutenticacionSTWithSoftDelegate:(SofttokenApp*)softAppDelegate;

//-(void)showAyudaST;

//-(void)removeAyudaST;

-(void)showIngresaDatosST;

-(void)removeIngresaDatosST;

-(void)showConfirmacionST:(Softtoken*)result;
//-(void)showConfirmacionST:(TipoSolicitudResult*)result;

-(void)removeConfirmacionST;

-(void)showAutenticacionST;

-(void)removeAutenticacionST;

-(void)showClaveActivacionST;

-(void)removeClaveActivacionST;

-(void)showEmailST;

-(void)removeEmailST;

-(void)showGeneraOTPS;

-(void)removeGeneraOTPS;

-(void)showMuestraOTPS:(NSString *)otp;

-(void)removeMuestraOTPS;

-(void)showRegistraCuentaST;

-(void)removeRegistraCuentaST;

-(void)touchAtras;

-(void)muestraAltaRegistro:(NSMutableDictionary *)operacion qrLeido:(NSString *)qr; // Modificación 50683

-(void)removeMuestraAltaRegistro; // Modificación 50683

+(BOOL)esPantallaSofttoken:(BaseViewController*)clasePantalla;

@end
