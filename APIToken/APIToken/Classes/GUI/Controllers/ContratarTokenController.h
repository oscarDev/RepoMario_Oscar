//
//  ContratarTokenController.h
//  APIToken
//
//  Created by OscarO on 12/07/16.
//  Copyright © 2016 CGI. All rights reserved.
//

#import "EditableViewController.h"

@class ContratacionSTDelegate;
#define INGRESA_DATOS_STN_XIB            @"ContratarToken"
typedef enum {
    idiee_NoneN = 0, //!<No information is being edited
    idiee_NumCelN, //
    idiee_NumCardN //
} IngresaDatosInformationEditingEnumN;

@interface ContratarTokenController : EditableViewController{
@private
    ContratacionSTDelegate* _contratacionSTDelegate;
    IngresaDatosInformationEditingEnumN _infoEdited;
}

@property (strong, nonatomic)ContratacionSTDelegate* contratacionSTDelegate;

@property (strong, nonatomic) IBOutlet UILabel *lblNumero;
@property (strong, nonatomic) IBOutlet UILabel *lblGetNumero;

@property (strong, nonatomic) IBOutlet UITextField *textNumTarjeta;

@property (strong, nonatomic) IBOutlet UIButton *btnContinuarOutlet;




- (IBAction)btnContinuarAccion:(id)sender;

@end
