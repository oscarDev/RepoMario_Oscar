//
//  OpenOCSTesting.h
//  Bancomer
//
//  Created by Karen Romero on 20/07/15.
//  Copyright (c) 2015 GoNet. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OpenOCSTesting : NSObject

- (void)generateRandom;
- (void)constructorOCSAlphabet;
- (void)constructorNoPaddingNoIgnore;
- (void)decodingNoPadding;
- (NSData *)obfuscatorWithString : (NSString *)qr ;
@end
