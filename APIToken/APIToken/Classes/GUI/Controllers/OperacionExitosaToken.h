//
//  OperacionExitosaToken.h
//  APIToken
//
//  Created by OscarO on 11/07/16.
//  Copyright © 2016 CGI. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EditableViewController.h"
#define CONF_ACT_TKN_XIB    @"OperacionExitosaToken"
@class ContratacionSTDelegate;

@interface OperacionExitosaToken : EditableViewController <UIAlertViewDelegate>{
    ContratacionSTDelegate* __strong _contratacionSTDelegate;
}

@property (nonatomic, strong) ContratacionSTDelegate* contratacionSTDelegate;
@property (strong, nonatomic) IBOutlet UILabel *lblOperacionExitosa;
@property (strong, nonatomic) IBOutlet UIImageView *imgExitosa;
@property (strong, nonatomic) IBOutlet UILabel *lblActivacionExito;
@property (strong, nonatomic) IBOutlet UIButton *btnAceptar;

@property (strong, nonatomic) IBOutlet UIImageView *imgFondo;
@property (strong, nonatomic) IBOutlet UIView *viewSplashExito;

- (IBAction)accionAceptar:(id)sender;

-(void)presentSplash;
@end
