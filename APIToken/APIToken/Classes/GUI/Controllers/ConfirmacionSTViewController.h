//
//  ConfirmacionSTViewController.h
//  Bancomer
//
//  Created by Francisco.Garcia on 27/05/13.
//
//

#import "EditableViewController.h"
@class ContratacionSTDelegate;
#define CONFIRMACION_ST_XIB  @"ConfirmacionSTView"
typedef enum {
    NUEVO,
    REACTIVACION,
    SUSTITUCION
}tipoSolicitud;

typedef enum {
	liee_Null = 0, //!<No information is being edited
	liee_NIP, //
    liee_CVV, //Hasta que se repare la transacción CVV
	liee_CVEAcesso //
} ConfirmacionSTInformationEditingEnum;
@interface ConfirmacionSTViewController : EditableViewController{

    NSInteger tiposolicitud;
    ContratacionSTDelegate* __strong _contratacionSTDelegate;
    ConfirmacionSTInformationEditingEnum _infoEdited;
    
}
//array estados
@property (nonatomic,retain) NSMutableArray *estados;

//STNuevo
@property (nonatomic, strong)IBOutlet UILabel* lblNIP;
@property (nonatomic, strong)IBOutlet UITextField* txtNIP;
@property (nonatomic, strong)IBOutlet UILabel* lblInfo;
@property (nonatomic, strong)IBOutlet UILabel* lblInfo2;
@property (nonatomic, strong)IBOutlet UIImageView *fondoAcceso;
@property (strong, nonatomic) IBOutlet UILabel *lblinfored;
// Transacción CVV
 @property (weak, nonatomic) IBOutlet UILabel *lblCVV;
 @property (weak, nonatomic) IBOutlet UITextField *txtCVV;
 

//STSustitucion

@property (nonatomic, strong)IBOutlet UILabel* lblCveAccesoSeg;
@property (nonatomic, strong)IBOutlet UITextField* txtCveAccesoSeg;
@property (strong, nonatomic) IBOutlet UILabel *lblAyudaCve;

//STReactivacion




@property (nonatomic, assign) NSInteger tiposolicitud;
@property (nonatomic, strong) ContratacionSTDelegate* contratacionSTDelegate;


-(void)configuraParaOperacion:(NSInteger)operation;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil;

@end
