//
//  AutenticacionSTViewController.m
//  APIToken
//
//  Created by Alberto Martínez Fernández on 7/1/16.
//  Copyright © 2016 CGI. All rights reserved.
//

#import "AutenticacionSTViewController.h"
#import "ContratacionSTDelegate.h"
#import "ConstantsToken.h"
#import "ServerToken.h"
#import "ServerResponse.h"
#import "SolicitudSTResult.h"
#import "AutenticacionTokenResult.h"
#import "Softtoken.h"
#import "APIToken.h"

#define IMG_ASM                 @"IoIcAcceso.png"

#define TABLE_BORDER_COLOR_RED                          128.0f/255.0f
#define TABLE_BORDER_COLOR_GREEN                        128.0f/255.0f
#define TABLE_BORDER_COLOR_BLUE                         128.0f/255.0f

@implementation AutenticacionSTViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    _infoEdited = liee_nil;
    
    [self setTxtNumeroTarjeta];
    [self setTxtNIP];
    [self setTxt3DigitosTarjeta];
    
    [self setHeaderTitle: ST_HEADER_ACTIVACION];
    [self setHeaderImage:[UIImage imageNamed:IMG_ASM]];
    [self showHeader:YES];
    [self muestraBotonDerecho:BOTON_CONFIRMAR];

}

-(void)setTxtNumeroTarjeta {
    _txtNumeroTarjeta.delegate = self;

    _txtNumeroTarjeta.layer.cornerRadius = CORNER_RADIOUS;
    _txtNumeroTarjeta.layer.borderWidth = BORDER_WIDTH;
    _txtNumeroTarjeta.layer.borderColor = [UIColor colorWithRed:TABLE_BORDER_COLOR_RED green:TABLE_BORDER_COLOR_GREEN blue:TABLE_BORDER_COLOR_BLUE alpha:1].CGColor;
}

-(void)setTxtNIP {
    _txtNIP.delegate = self;

    _txtNIP.layer.cornerRadius = CORNER_RADIOUS;
    _txtNIP.layer.borderWidth = BORDER_WIDTH;
    _txtNIP.layer.borderColor = [UIColor colorWithRed:TABLE_BORDER_COLOR_RED green:TABLE_BORDER_COLOR_GREEN blue:TABLE_BORDER_COLOR_BLUE alpha:1].CGColor;
}

-(void)setTxt3DigitosTarjeta {
    _txt3DigitosTarjeta.delegate = self;
    
    _txt3DigitosTarjeta.layer.cornerRadius = CORNER_RADIOUS;
    _txt3DigitosTarjeta.layer.borderWidth = BORDER_WIDTH;
    _txt3DigitosTarjeta.layer.borderColor = [UIColor colorWithRed:TABLE_BORDER_COLOR_RED green:TABLE_BORDER_COLOR_GREEN blue:TABLE_BORDER_COLOR_BLUE alpha:1].CGColor;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self recalculateScroll];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self stopActivityIndicator];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)ejecutaAccionBotonDerecho {
    
    _editedView = nil;
    _infoEdited = liee_nil;
    
    if(_txtNumeroTarjeta.isFirstResponder) {
        [_txtNumeroTarjeta resignFirstResponder];
    }
    if(_txtNIP.isFirstResponder) {
        [_txtNIP resignFirstResponder];
    }
    if (_txt3DigitosTarjeta.isFirstResponder) {
        [_txt3DigitosTarjeta resignFirstResponder];
    }
    
    if([ContratacionSTDelegate validaCampos:self]){
        NSLog(@"Continuar AutenticacionST");
        [self solicitarSofttoken];
    }
}

-(void)solicitarSofttoken {
    _contratacionSTDelegate.objetoSofttoken = [[Softtoken alloc]init];
    
    _contratacionSTDelegate.objetoSofttoken.numTarjeta = _txtNumeroTarjeta.text;
    _contratacionSTDelegate.objetoSofttoken.numCelular = [[APIToken getInstance].apiTokenDataSource userName];
    _contratacionSTDelegate.objetoSofttoken.companiaCelular = [[APIToken getInstance].apiTokenDataSource compania];
    _contratacionSTDelegate.objetoSofttoken.correo = [[APIToken getInstance].apiTokenDataSource email];
    _contratacionSTDelegate.objetoSofttoken.numeroCliente = [[APIToken getInstance].apiTokenDataSource clientNumber];
    
    _operationDelivered = SOLICITUD_ST;
    
    [_contratacionSTDelegate invokeSolicitudST:_contratacionSTDelegate];
}

- (NSInteger) maxSizeForTextField: (UITextField*) aTextField {
    NSInteger result = -1;
    
    if (aTextField == _txtNumeroTarjeta) {
        result = CARD_NUMBER_LENGTH;
        
    } else if (aTextField == _txtNIP) {
        result = NIP_REFERENCE_LENGTH;
        
    }else if (aTextField == _txt3DigitosTarjeta) {
        result = CVV_REFERENCE_LENGTH;
    }
    
    return result;
}

/**
 * Child classes informs editable view controller whether the provided text field contains
 * an integer number. Both text field in view controller are integer numbers
 *
 * @param aTextField The text field to test
 * @return YES in all cases
 */
- (BOOL) textFieldIsInteger: (UITextField*) aTextField {
    return YES;
}

/**
 * Invoked by framework when a text field asks to start editing. Pop buttons view state is updated accodingly
 *
 * @param textField The text field asking to start editing
 * @return YES if editing can start, NO otherwise
 */
- (BOOL) textFieldShouldBeginEditing: (UITextField*) textField {
    BOOL result = YES;
    
    if (textField == _txtNumeroTarjeta) {
        [_popButtonsView enableNextResponderButton: YES];
        [_popButtonsView enablePreviousResponderButton: NO];
        _editedView = _txtNumeroTarjeta;
        _infoEdited = liee_NumeroTarjeta;
        
    }else if (textField == _txtNIP) { //-- Transacción CVV
        [_popButtonsView enableNextResponderButton: YES];
        [_popButtonsView enablePreviousResponderButton: YES];
        _editedView = _txtNIP;
        _infoEdited = liee_nip;
        
    } else if (textField == _txt3DigitosTarjeta) {
        [_popButtonsView enableNextResponderButton: NO];
        [_popButtonsView enablePreviousResponderButton: YES];
        _editedView = _txt3DigitosTarjeta;
        _infoEdited = liee_3DigitosTarjeta;
        
    } else {
        [_popButtonsView enableNextResponderButton: NO];
        [_popButtonsView enablePreviousResponderButton: NO];
        _editedView = nil;
        _infoEdited = liee_nil;
        result = NO;
    }
    
    return result;
}


/**
 * Informs the delegate that next responder button was clicked. Sets password text field
 * as next first responder
 *
 * @param aPopButtonsView The pop buttons view triggering the event
 */
- (void) nextButtonClickedInPopButtonsView: (PopButtonsView*) aPopButtonsView {
    if (_infoEdited == liee_NumeroTarjeta) {
        [_txtNIP becomeFirstResponder];
    } else if (_infoEdited == liee_nip) {
        [_txt3DigitosTarjeta becomeFirstResponder];
    }
}

/**
 * Informs the pop buttons view delegate that OK button was clicked. Keypad is dismissed
 *
 * @param aPopButtonsView The pop buttons view triggering the event
 */
- (void) okButtonClickedInPopButtonsView: (PopButtonsView*) aPopButtonsView {
    [super okButtonClickedInPopButtonsView: aPopButtonsView];
    
    _editedView = nil;
    
    if (_infoEdited == liee_NumeroTarjeta) {
        [_txtNumeroTarjeta resignFirstResponder];
    }else if (_infoEdited == liee_nip) { //--- Transacción CVV
        [_txtNIP resignFirstResponder];
    }  else if (_infoEdited == liee_3DigitosTarjeta) {
        [_txt3DigitosTarjeta resignFirstResponder];
    }
}

/**
 * Informs the delegate that previous responder button was clicked. Sets user text field as
 * next first responder
 *
 * @param aPopButtonsView The pop buttons view triggering the event
 */
- (void) previousButtonClickedInPopButtonsView: (PopButtonsView*) aPopButtonsView {
    if (_infoEdited == liee_nip) {
        [_txtNumeroTarjeta becomeFirstResponder];
    } else if (_infoEdited == liee_3DigitosTarjeta) {
        [_txtNIP becomeFirstResponder];
    }
}

@end
