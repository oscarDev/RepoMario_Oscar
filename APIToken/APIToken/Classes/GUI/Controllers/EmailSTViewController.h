//
//  EmailSTViewController.h
//  Bancomer
//
//  Created by Francisco.Garcia on 28/05/13.
//
//

#import "EditableViewController.h"
#define EMAIL_ST_XIB    @"EmailSTView"
@class ContratacionSTDelegate;

@interface EmailSTViewController : EditableViewController<UIAlertViewDelegate>{

    ContratacionSTDelegate* __strong _contratacionSTDelegate;
}
@property (nonatomic, strong) ContratacionSTDelegate* contratacionSTDelegate;
@property (strong, nonatomic) IBOutlet UILabel *lblFelicidades;
@property (weak, nonatomic) IBOutlet UIImageView *imgFelicidades;
@property (strong, nonatomic) IBOutlet UILabel *lblInfo;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil;

@end
