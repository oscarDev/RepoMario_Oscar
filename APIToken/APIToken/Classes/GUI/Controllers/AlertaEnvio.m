//
//  AlertaEnvio.m
//  APIToken
//
//  Created by OscarO on 08/07/16.
//  Copyright © 2016 CGI. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import "AlertaEnvio.h"

@implementation AlertaEnvio


const static CGFloat kCustomIOSAlertViewDefaultButtonHeight       = 45;
const static CGFloat kCustomIOSAlertViewDefaultButtonSpacerHeight = 1;
const static CGFloat kCustomIOSAlertViewCornerRadius              = 7;
const static CGFloat kCustomIOS7MotionEffectExtent                = 10.0;


CGFloat buttonHeightEnvio = 0;
CGFloat buttonSpacerHeightEnvio = 0;


- (id)init
{
    self = [super init];
    if (self) {
        self.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
        
        _delegate = self;
        _useMotionEffects = false;
        _buttonTitles = @[@"Close"];
        
        [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deviceOrientationDidChange:) name:UIDeviceOrientationDidChangeNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    }
    return self;
}

- (id)initWithTitle:(NSString*) aTitle andImageName:(NSString*) aImage andMessage:(NSString*) aMessage closable:(BOOL) aClosable
{
    self = [self init];
    if (self) {
        
        // Add some custom content to the alert view
        _containerView = [self createPopUpContentViewWithTitle:aTitle andImageName:aImage andMessage:aMessage];
        
        // Modify the parameters
        _buttonTitles = [NSMutableArray arrayWithObjects:@"Aceptar", nil];
        
        
        // You may use a Block, rather than a delegate.
        [self setOnButtonTouchUpInside:^(AlertaEnvio *alertView, int buttonIndex) {
            NSLog(@"Block: Button at position %d is clicked on alertView %d.", buttonIndex, (int)[alertView tag]);
            [alertView close];
        }];
        
        _closable = aClosable;
        
        [self setUseMotionEffects:false];
        
        
    }
    return self;
}

// Create the dialog view, and animate opening the dialog
- (void)show
{
    _dialogView = [self createContainerView];
    
    if(_closable){
        
        UIButton * closeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        
        closeBtn.frame = CGRectMake(5, 5, 30, 30); // position in the parent view and set the size of the button
        [closeBtn setBackgroundImage:[UIImage imageNamed:@"Ios_Ic_cerrar"] forState:UIControlStateNormal];
        // add targets and actions
        // [closeBtn addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
        
        [closeBtn addTarget:self action:@selector(popUpButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
        [closeBtn setTag:1];
        
        // add to a view
        [_dialogView addSubview:closeBtn];
        
        
    }
    
    _dialogView.layer.shouldRasterize = YES;
    _dialogView.layer.rasterizationScale = [[UIScreen mainScreen] scale];
    
    self.layer.shouldRasterize = YES;
    self.layer.rasterizationScale = [[UIScreen mainScreen] scale];
    
#if (defined(__IPHONE_7_0))
    if (_useMotionEffects) {
        [self applyMotionEffects];
    }
#endif
    //alfa y color de fondo
    //self.backgroundColor = [UIColor clearColor];
    //self.alpha = 1; //alpha aqui
    self.backgroundColor = [UIColor colorWithRed:0 green:0 blue: 0 alpha:0.85f]; //modif val
    
    [self addSubview:_dialogView];
    
    // Can be attached to a view or to the top most window
    // Attached to a view:
    if (_parentView != NULL) {
        [_parentView addSubview:self];
        
        // Attached to the top most window
    } else {
        
        // On iOS7, calculate with orientation
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_7_1) {
            
            UIInterfaceOrientation interfaceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
            switch (interfaceOrientation) {
                case UIInterfaceOrientationLandscapeLeft:
                    self.transform = CGAffineTransformMakeRotation(M_PI * 270.0 / 180.0);
                    break;
                    
                case UIInterfaceOrientationLandscapeRight:
                    self.transform = CGAffineTransformMakeRotation(M_PI * 90.0 / 180.0);
                    break;
                    
                case UIInterfaceOrientationPortraitUpsideDown:
                    self.transform = CGAffineTransformMakeRotation(M_PI * 180.0 / 180.0);
                    break;
                    
                default:
                    break;
            }
            
            [self setFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
            
            // On iOS8, just place the dialog in the middle
        } else {
            
            CGSize screenSize = [self countScreenSize];
            CGSize dialogSize = [self countDialogSize];
            CGSize keyboardSize = CGSizeMake(0, 0);
            
            _dialogView.frame = CGRectMake((screenSize.width - dialogSize.width) / 2, (screenSize.height - keyboardSize.height - dialogSize.height) / 2, dialogSize.width, dialogSize.height);
            
        }
        
        [[[[UIApplication sharedApplication] windows] firstObject] addSubview:self];
    }
    
    _dialogView.layer.opacity = 0.8f;
    _dialogView.layer.transform = CATransform3DMakeScale(1.3f, 1.3f, 1.0);
    
    [UIView animateWithDuration:0.2f delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         //self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4f];
                         //self.backgroundColor = [UIColor lightGrayColor];
                         //MODIF VAL
                         self.backgroundColor = [UIColor colorWithRed:0 green:0 blue: 0 alpha:0.85f];
                         _dialogView.layer.opacity = 0.5f;
                         _dialogView.layer.transform = CATransform3DMakeScale(1, 1, 1);
                     }
                     completion:NULL
     ];
    
}

// Button has been touched
- (IBAction)popUpButtonTouchUpInside:(id)sender
{
    if (_delegate != NULL) {
        [_delegate popUpButtonTouchUpInside:self clickedButtonAtIndex:[sender tag]];
    }
    
    if (_onButtonTouchUpInside != NULL) {
        _onButtonTouchUpInside(self, (int)[sender tag]);
    }
}

// Default button behaviour
- (void)popUpButtonTouchUpInside: (AlertaEnvio *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"Button Clicked! %d, %d", (int)buttonIndex, (int)[alertView tag]);
    [self close];
}

// Dialog close animation then cleaning and removing the view from the parent
- (void)close
{
    CATransform3D currentTransform = _dialogView.layer.transform;
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_7_1) {
        CGFloat startRotation = [[_dialogView valueForKeyPath:@"layer.transform.rotation.z"] floatValue];
        CATransform3D rotation = CATransform3DMakeRotation(-startRotation + M_PI * 270.0 / 180.0, 0.0f, 0.0f, 0.0f);
        
        _dialogView.layer.transform = CATransform3DConcat(rotation, CATransform3DMakeScale(1, 1, 1));
    }
    
    _dialogView.layer.opacity = 1.0f;
    
    [UIView animateWithDuration:0.2f delay:0.0 options:UIViewAnimationOptionTransitionNone
                     animations:^{
                         self.backgroundColor = [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.0f];
                         _dialogView.layer.transform = CATransform3DConcat(currentTransform, CATransform3DMakeScale(0.6f, 0.6f, 1.0));
                         _dialogView.layer.opacity = 0.0f;
                     }
                     completion:^(BOOL finished) {
                         for (UIView *v in [self subviews]) {
                             [v removeFromSuperview];
                         }
                         [self removeFromSuperview];
                     }
     ];
}

- (void)setSubView: (UIView *)subView
{
    _containerView = subView;
}

// Creates the container view here: create the dialog, then add the custom content and buttons
- (UIView *)createContainerView
{
    if (_containerView == NULL) {
        _containerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 300, 250)];
    }
    CGSize screenSize = [self countScreenSize];
    CGSize dialogSize = [self countDialogSize];
    
    // For the black background
    [self setFrame:CGRectMake(0, 0, screenSize.width, screenSize.height)];
    
    // This is the dialog's container; we attach the custom content and the buttons to this one
    UIView *dialogContainer = [[UIView alloc] initWithFrame:CGRectMake((screenSize.width - dialogSize.width) / 2, (screenSize.height - dialogSize.height) / 2, dialogSize.width, dialogSize.height)];
    
    // First, we style the dialog to match the iOS7 UIAlertView >>>
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = dialogContainer.bounds;
    gradient.colors = [NSArray arrayWithObjects:
                       //(id)[[UIColor colorWithRed:218.0/255.0 green:218.0/255.0 blue:218.0/255.0 alpha:1.0f] CGColor],
                       //(id)[[UIColor colorWithRed:233.0/255.0 green:233.0/255.0 blue:233.0/255.0 alpha:1.0f] CGColor],
                       //(id)[[UIColor colorWithRed:218.0/255.0 green:218.0/255.0 blue:218.0/255.0 alpha:1.0f] CGColor],
                       //nil];
                       (id)[[UIColor colorWithRed:0 green:0 blue: 0 alpha:0.85f] CGColor],
                       (id)[[UIColor colorWithRed:0 green:0 blue: 0 alpha:0.85f] CGColor],
                       (id)[[UIColor colorWithRed:0 green:0 blue: 0 alpha:0.85f] CGColor],
                       nil];

    
    CGFloat cornerRadius = kCustomIOSAlertViewCornerRadius;
    gradient.cornerRadius = cornerRadius;
    [dialogContainer.layer insertSublayer:gradient atIndex:0];
    
    dialogContainer.layer.cornerRadius = cornerRadius;
    dialogContainer.layer.borderColor = [[UIColor colorWithRed:198.0/255.0 green:198.0/255.0 blue:198.0/255.0 alpha:1] CGColor]; //alpha aqui
    //dialogContainer.layer.borderColor = [[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:1.0f] CGColor];
    dialogContainer.layer.borderWidth = 1;
    dialogContainer.layer.shadowRadius = cornerRadius + 5;
    dialogContainer.layer.shadowOpacity = 0.1f;
    dialogContainer.layer.shadowOffset = CGSizeMake(0 - (cornerRadius+5)/2, 0 - (cornerRadius+5)/2);
    dialogContainer.layer.shadowColor = [UIColor blackColor].CGColor;
    dialogContainer.layer.shadowPath = [UIBezierPath bezierPathWithRoundedRect:dialogContainer.bounds cornerRadius:dialogContainer.layer.cornerRadius].CGPath;
    
    // There is a line above the button
    //UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, dialogContainer.bounds.size.height - buttonHeightAI - buttonSpacerHeightAI, dialogContainer.bounds.size.width, buttonSpacerHeightAI)];
    //    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, dialogContainer.bounds.size.height - buttonHeightAI - buttonSpacerHeightAI, dialogContainer.bounds.size.width, buttonSpacerHeightAI)];
    
    //CGRectMake( 30, (i * buttonHeightAI) +345 , buttonWidth, buttonHeightAI)];
    //    lineView.backgroundColor = [UIColor colorWithRed:198.0/255.0 green:198.0/255.0 blue:198.0/255.0 alpha:1.0f];
    //    [dialogContainer addSubview:lineView];
    // ^^^
    
    // Add the custom container if there is any
    [dialogContainer addSubview:_containerView];
    
    // Add the buttons too
    [self addButtonsToView:dialogContainer];
    
    return dialogContainer;
}

// Helper function: add buttons to container
//Se generan los botones
- (void)addButtonsToView: (UIView *)container
{
    if (_buttonTitles==NULL) { return; }
    
    CGFloat buttonWidth = container.bounds.size.width - 15; /// [_buttonTitles count];
    
    for (int i=0; i<[_buttonTitles count]; i++) {
        // Se crean y añaden al view los botones btnNuevoCodigo, btnNuevoIntento
        UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        //CGFloat buttonY = (i * buttonHeightEnvio) + (i*8) +335;
        
        //[closeButton setFrame:CGRectMake( 15, buttonY, buttonWidth, buttonHeightEnvio)];
        //        CGFloat buttonY;
        //
        //        if (i==0) {
        //            buttonY = (i * buttonHeightAI) +335;
        //        }else{
        //            buttonY = (i * buttonHeightAI) +345;
        //        }
        //
        //        [closeButton setFrame:CGRectMake( 15, buttonY, buttonWidth, buttonHeightAI)];
        //
        [closeButton setFrame:CGRectMake( 10, 455 , buttonWidth, buttonHeightEnvio)];/*CGRectMake(i * buttonWidth, container.bounds.size.height - buttonHeightAI, buttonWidth, buttonHeightAI)];*/
        [closeButton addTarget:self action:@selector(popUpButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
        [closeButton setTag:i];
        
        [closeButton setTitle:[_buttonTitles objectAtIndex:i] forState:UIControlStateNormal];
        [closeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [closeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted]; //colorWithRed:0.0f green:0.5f blue:1.0f alpha:1.0f
        //colorWithRed:0.2f green:0.2f blue:0.2f alpha:0.5f
        //[title setTextColor:[UIColor whiteColor]]; ejemplo
        [closeButton setBackgroundColor: [UIColor colorWithRed:9.0f/255.0f green:79.0f/255.0f blue:164.0f/255.0f alpha:1.0f]];
        [closeButton.titleLabel setFont:[UIFont boldSystemFontOfSize:12.0f]];
        [closeButton.layer setCornerRadius:kCustomIOSAlertViewCornerRadius];
        
        [container addSubview:closeButton];
    }
}

// Helper function: count and return the dialog's size
- (CGSize)countDialogSize
{
    CGFloat dialogWidth = _containerView.frame.size.width;
    CGFloat dialogHeight = _containerView.frame.size.height + buttonHeightEnvio + buttonSpacerHeightEnvio +buttonHeightEnvio + 100;
    
    return CGSizeMake(dialogWidth, dialogHeight);
}

// Helper function: count and return the screen's size
- (CGSize)countScreenSize
{
    if (_buttonTitles!=NULL && [_buttonTitles count] > 0) {
        buttonHeightEnvio     = kCustomIOSAlertViewDefaultButtonHeight;
        buttonSpacerHeightEnvio = kCustomIOSAlertViewDefaultButtonSpacerHeight;
    } else {
        buttonHeightEnvio = 0;
        buttonSpacerHeightEnvio = 0;
    }
    
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    
    // On iOS7, screen width and height doesn't automatically follow orientation
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_7_1) {
        UIInterfaceOrientation interfaceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
        if (UIInterfaceOrientationIsLandscape(interfaceOrientation)) {
            CGFloat tmp = screenWidth;
            screenWidth = screenHeight;
            screenHeight = tmp;
        }
    }
    
    return CGSizeMake(screenWidth, screenHeight);
}

#if (defined(__IPHONE_7_0))
// Add motion effects
- (void)applyMotionEffects {
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        return;
    }
    
    UIInterpolatingMotionEffect *horizontalEffect = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.x"
                                                                                                    type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
    horizontalEffect.minimumRelativeValue = @(-kCustomIOS7MotionEffectExtent);
    horizontalEffect.maximumRelativeValue = @( kCustomIOS7MotionEffectExtent);
    
    UIInterpolatingMotionEffect *verticalEffect = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.y"
                                                                                                  type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis];
    verticalEffect.minimumRelativeValue = @(-kCustomIOS7MotionEffectExtent);
    verticalEffect.maximumRelativeValue = @( kCustomIOS7MotionEffectExtent);
    
    UIMotionEffectGroup *motionEffectGroup = [[UIMotionEffectGroup alloc] init];
    motionEffectGroup.motionEffects = @[horizontalEffect, verticalEffect];
    
    [_dialogView addMotionEffect:motionEffectGroup];
}
#endif

- (void)dealloc
{
    [[UIDevice currentDevice] endGeneratingDeviceOrientationNotifications];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
}

// Rotation changed, on iOS7
- (void)changeOrientationForIOS7 {
    
    UIInterfaceOrientation interfaceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    
    CGFloat startRotation = [[self valueForKeyPath:@"layer.transform.rotation.z"] floatValue];
    CGAffineTransform rotation;
    
    switch (interfaceOrientation) {
        case UIInterfaceOrientationLandscapeLeft:
            rotation = CGAffineTransformMakeRotation(-startRotation + M_PI * 270.0 / 180.0);
            break;
            
        case UIInterfaceOrientationLandscapeRight:
            rotation = CGAffineTransformMakeRotation(-startRotation + M_PI * 90.0 / 180.0);
            break;
            
        case UIInterfaceOrientationPortraitUpsideDown:
            rotation = CGAffineTransformMakeRotation(-startRotation + M_PI * 180.0 / 180.0);
            break;
            
        default:
            rotation = CGAffineTransformMakeRotation(-startRotation + 0.0);
            break;
    }
    
    [UIView animateWithDuration:0.2f delay:0.0 options:UIViewAnimationOptionTransitionNone
                     animations:^{
                         _dialogView.transform = rotation;
                         
                     }
                     completion:nil
     ];
    
}

// Rotation changed, on iOS8
- (void)changeOrientationForIOS8: (NSNotification *)notification {
    
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    
    [UIView animateWithDuration:0.2f delay:0.0 options:UIViewAnimationOptionTransitionNone
                     animations:^{
                         CGSize dialogSize = [self countDialogSize];
                         CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
                         self.frame = CGRectMake(0, 0, screenWidth, screenHeight);
                         _dialogView.frame = CGRectMake((screenWidth - dialogSize.width) / 2, (screenHeight - keyboardSize.height - dialogSize.height) / 2, dialogSize.width, dialogSize.height);
                     }
                     completion:nil
     ];
    
    
}

// Handle device orientation changes
- (void)deviceOrientationDidChange: (NSNotification *)notification
{
    // If dialog is attached to the parent view, it probably wants to handle the orientation change itself
    if (_parentView != NULL) {
        return;
    }
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_7_1) {
        [self changeOrientationForIOS7];
    } else {
        [self changeOrientationForIOS8:notification];
    }
}

// Handle keyboard show/hide changes
- (void)keyboardWillShow: (NSNotification *)notification
{
    CGSize screenSize = [self countScreenSize];
    CGSize dialogSize = [self countDialogSize];
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIInterfaceOrientation interfaceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (UIInterfaceOrientationIsLandscape(interfaceOrientation) && NSFoundationVersionNumber <= NSFoundationVersionNumber_iOS_7_1) {
        CGFloat tmp = keyboardSize.height;
        keyboardSize.height = keyboardSize.width;
        keyboardSize.width = tmp;
    }
    
    [UIView animateWithDuration:0.2f delay:0.0 options:UIViewAnimationOptionTransitionNone
                     animations:^{
                         _dialogView.frame = CGRectMake((screenSize.width - dialogSize.width) / 2, (screenSize.height - keyboardSize.height - dialogSize.height) / 2, dialogSize.width, dialogSize.height);
                     }
                     completion:nil
     ];
}

- (void)keyboardWillHide: (NSNotification *)notification
{
    CGSize screenSize = [self countScreenSize];
    CGSize dialogSize = [self countDialogSize];
    
    [UIView animateWithDuration:0.2f delay:0.0 options:UIViewAnimationOptionTransitionNone
                     animations:^{
                         _dialogView.frame = CGRectMake((screenSize.width - dialogSize.width) / 2, (screenSize.height - dialogSize.height) / 2, dialogSize.width, dialogSize.height);
                     }
                     completion:nil
     ];
}

- (UIView *)createPopUpContentViewWithTitle:(NSString*) aTitle andImageName:(NSString*) aImage andMessage:(NSString*) aMessage
{
    UIImage* im = [UIImage imageNamed:aImage];
    
    CGFloat imgHeight = im.size.height;
    UIView *demoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, 40 + imgHeight + [self heightForText:aMessage] +10)];
    
    _lblTitulo = [[UILabel alloc] initWithFrame:CGRectMake(10, 50, 270, 20)];
    _lblTitulo.textAlignment = NSTextAlignmentCenter;
    _lblTitulo.text = aTitle;
    
    _lblTitulo.font = [UIFont boldSystemFontOfSize:16.0f];
    _lblTitulo.textColor =[UIColor colorWithRed:1.0f green:1.0f blue:1.0f alpha:1.0f];
    //[UIColor colorWithRed:106.0f/255.0f green:168.0f/255.0f blue:79.0f/255.0f alpha:1.0];
    [demoView addSubview:_lblTitulo];
    
    UIImageView *imageViewFondo = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, _containerView.bounds.size.width, _containerView.bounds.size.height)];
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, _lblTitulo.frame.origin.y, 270, imgHeight)];
    //Crear image contenedor

    //imageViewFondo.backgroundColor = [UIColor lightGrayColor];
    //imageViewFondo.alpha = 0.5f;
    //MODIF VAL
    imageViewFondo.backgroundColor = [UIColor colorWithRed:0 green:0 blue: 0 alpha:0.5f];
    
    [imageView setImage:[UIImage imageNamed:aImage]];
    imageView.alpha = 1; //alpha aqui ****
    
    //texto subtitulo AVISO: lblMensCodIncorrecto
    UILabel* textSub = [[UILabel alloc] initWithFrame:CGRectMake(10, imgHeight - 53, 260, [self heightForText:aMessage])];
    textSub.numberOfLines = 0;
    textSub.textAlignment = NSTextAlignmentCenter;
    textSub.text = @"";
    textSub.font = [UIFont systemFontOfSize:18.0f];
    textSub.textColor = [UIColor colorWithRed:1.0f green:1.0f blue:1.0f alpha:1.0f];
    //[UIColor colorWithRed:200.0f/255.0f green:23.0f/255.0f blue:94.0f/255.0f alpha:1.0];
    //Fin lblMensCodIncorrecto
    
    //label subtitulo
    UILabel * text = [[UILabel alloc] initWithFrame:CGRectMake(10, 40 + imgHeight +25, 260, [self heightForText:aMessage])];
    text.numberOfLines = 0;
    text.textAlignment = NSTextAlignmentCenter;
    text.text = aMessage;
    text.textColor = [UIColor colorWithRed:1.0f green:1.0f blue:1.0f alpha:1.0f];
    text.font = [UIFont systemFontOfSize:15.0f];
    
    
    //[demoView addSubview:imageViewFondo];

    [demoView addSubview:imageView];
    [demoView addSubview:textSub];
    [demoView addSubview:text];
    return demoView;
}

- (CGFloat)heightForText:(NSString *)bodyText
{
    UIFont *cellFont = [UIFont systemFontOfSize:15.0f];
    
    CGSize constraintSize = CGSizeMake(260, MAXFLOAT);
    CGSize labelSize = [bodyText sizeWithFont:cellFont constrainedToSize:constraintSize lineBreakMode:NSLineBreakByWordWrapping];
    CGFloat height = labelSize.height + 10;
    NSLog(@"height=%f", height);
    return height;
}

- (void)setColorTitulo:(UIColor*) color {
    _lblTitulo.textColor = [UIColor whiteColor];
}


//-------------------------


@end

