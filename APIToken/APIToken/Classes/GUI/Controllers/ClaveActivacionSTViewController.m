//
//  ClaveActivacionSTViewController.m
//  Bancomer
//
//  Created by Francisco.Garcia on 28/05/13.
//
//
/*
#import "ClaveActivacionSTViewController.h"
#import "ConstantsToken.h"
#import "ContratacionSTDelegate.h"
#import "EditableViewController+protectedCategory.h"
#import <QuartzCore/QuartzCore.h>
#import "APIToken.h"

#define IMG_ASM                 @"IoIcAcceso.png"
#define LBL_CVE_ACT_TEXT            @"Ingresa la clave de activación"
#define LBL_INFO_TEXT               @"Ingresa la clave de activación de 10 dígitos que recibiste por medio de un SMS."
#define TXT_HEIGHT                  36
#define TABLE_BORDER_COLOR_RED                          128.0f/255.0f
#define TABLE_BORDER_COLOR_GREEN                        128.0f/255.0f
#define TABLE_BORDER_COLOR_BLUE                         128.0f/255.0f
#define LINK_TEXT                   @"¿Necesitas reenviar el código de activación?"
#define SOLICITAR_CODIGO_TEL_NUMBER        @"tel://+525552262685,#39"

@interface ClaveActivacionSTViewController ()

@end

@implementation ClaveActivacionSTViewController
@synthesize lblCveActivacion;
@synthesize lblInfo;
@synthesize txtCveActivacion;
@synthesize contratacionSTDelegate = _contratacionSTDelegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [self setLblCveActivacion];
    [self setTxtCveActivacion];
    [self setLblInfo];
    [self setLink];
    [super viewDidLoad];
     
    [self setHeaderTitle: ST_HEADER_ACTIVACION];
	[self setHeaderImage:[UIImage imageNamed:IMG_ASM]];
    [self showHeader:YES];
    [self muestraBotonDerecho:BOTON_CONFIRMAR];
    [self activarBotonDerecho:NO];
    [self setBotonSolicitarClave];
    
    [txtCveActivacion addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    // Guardo sesión para poder guardar el número de teléfono y poder precargarlo en pantalla si el usuario se sale ahora

    BOOL estatusIVR = [ContratacionSTDelegate isIVR];
    [[APIToken getInstance].apiTokenDelegate storeSession];
    [ContratacionSTDelegate guardarEstatusIVR:estatusIVR];
}

-(void) setBotonSolicitarClave {
//    [_btnSolicitarCodigo setHidden:![_contratacionSTDelegate mostrarBotonSlicitarClave]];
//    [_btnSolicitarCodigo setHidden:[_contratacionSTDelegate mostrarBotonSlicitarClave]];
}

-(void)setLblCveActivacion
{
    lblCveActivacion.text = LBL_CVE_ACT_TEXT;
    lblCveActivacion.frame = CGRectMake(LAT_MARGIN,DISTANCE_BTW_COMP, COMPONENT_WIDTH, LBL_HEIGHT);
    lblCveActivacion.font = FUENTE_CUERPO;
    lblCveActivacion.backgroundColor = [UIColor clearColor];
    lblCveActivacion.textColor = COLOR_GRIS_TEXTO_1;
}
-(void)setTxtCveActivacion
{
    txtCveActivacion.delegate = self;
    txtCveActivacion.frame = CGRectMake(LAT_MARGIN, lblCveActivacion.frame.origin.y+lblCveActivacion.frame.size.height, COMPONENT_WIDTH, TXT_HEIGHT);
    txtCveActivacion.layer.cornerRadius = CORNER_RADIOUS;
    txtCveActivacion.layer.borderWidth = BORDER_WIDTH;
    txtCveActivacion.layer.borderColor = [UIColor colorWithRed:TABLE_BORDER_COLOR_RED green:TABLE_BORDER_COLOR_GREEN blue:TABLE_BORDER_COLOR_BLUE alpha:1].CGColor;
}
-(void)setLblInfo  //setLblIntroducircodigo
{
    lblInfo.text = LBL_INFO_TEXT;
    lblInfo.frame = CGRectMake(LAT_MARGIN,txtCveActivacion.frame.origin.y + txtCveActivacion.frame.size.height + DISTANCE_BTW_COMP, COMPONENT_WIDTH, LBL_HEIGHT*3);
    lblInfo.numberOfLines = 3;
    lblInfo.font = FUENTE_CUERPO;
    lblInfo.backgroundColor = [UIColor clearColor];
    lblInfo.textColor = COLOR_GRIS_TEXTO_1;
}
//configurar boton de nuevo codigo
-(void)setLink
{
    _lblReenviar.text = LINK_TEXT;
    _lblReenviar.frame = CGRectMake(40, lblInfo.frame.origin.y + lblInfo.frame.size.height + DISTANCE_BTW_COMP, 280, LBL_HEIGHT);
    _lblReenviar.font = FUENTE_CUERPO_2;
    _lblReenviar.textColor = COLOR_MAGENTA;
    [_lblReenviar sizeToFit];
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = COLOR_MAGENTA;
    lineView.frame = CGRectMake(_lblReenviar.frame.origin.x , _lblReenviar.frame.origin.y + _lblReenviar.frame.size.height -3, _lblReenviar.frame.size.width, 1);
    [self.view addSubview:lineView];
    _btnReenviar.frame = _lblReenviar.frame;
}

-(void)viewWillDisappear:(BOOL)animated{

    [self stopActivityIndicator];
    [super viewWillDisappear:animated];
}

- (IBAction)reenviarTouch:(id)sender {
    [_contratacionSTDelegate mostrarAlertCambioNumeroCompaniaDesdeReenviarCodigo];
}


-(void)ejecutaAccionBotonDerecho{

    [txtCveActivacion resignFirstResponder];
    
    if([ContratacionSTDelegate validaCampos:self]){
        [_contratacionSTDelegate borraPendienteDescarga];
    [[APIToken getInstance].apiTokenDelegate setCveReactivacionST:txtCveActivacion.text];
    [self startActivityIndicator];
    [_contratacionSTDelegate continuarActivacion];
//        _contratacionSTDelegate.claveActivacionSTViewController = self;  linea comentada, tiraba error
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setLblCveActivacion:nil];
    [self setTxtCveActivacion:nil];
    [self setLblInfo:nil];
    [self setLblReenviar:nil];
    [self setBtnReenviar:nil];
    [super viewDidUnload];
}

#pragma mark - 

#pragma mark -

- (NSInteger)maxSizeForTextField:(UITextField *)aTextField {
	NSInteger result = -1;
	
	if (aTextField == txtCveActivacion) {
		result = CVE_ACTIVACION_ST_REFERENCE_LENGTH;
	}
	return result;
}


 * Child classes informs editable view controller whether the provided text field contains
 * an integer number. Both text field in view controller are integer numbers
 *
 * @param aTextField The text field to test
 * @return YES in all cases
 */
/* aqui
- (BOOL) textFieldIsInteger: (UITextField*) aTextField {
	return YES;
}
*/ //aqui
/**
 * Invoked by framework when a text field asks to start editing. Pop buttons view state is updated accodingly
 *
 * @param textField The text field asking to start editing
 * @return YES if editing can start, NO otherwise
 */
/* aqui
- (BOOL) textFieldShouldBeginEditing: (UITextField*) textField {
	BOOL result = YES;
	
	if (textField == txtCveActivacion) {
		[_popButtonsView enableNextResponderButton:NO];
		[_popButtonsView enablePreviousResponderButton: NO];
		_editedView = txtCveActivacion;
		_infoEdited = cast_cveActivacion;
	} else {
		[_popButtonsView enableNextResponderButton: NO];
		[_popButtonsView enablePreviousResponderButton: NO];
		_editedView = nil;
		_infoEdited = cast_None;
		result = NO;
	}
	
	return result;
}


- (void)textFieldDidChange:(UITextField *)textField {
    NSInteger result = -1;
    
    if (textField == txtCveActivacion) {
        result = CVE_ACTIVACION_ST_REFERENCE_LENGTH;
        [self activarBotonDerecho:textField.text.length == result];
    }
}

*/ //aqui
/**
 * Informs the pop buttons view delegate that OK button was clicked. Keypad is dismissed
 *
 * @param aPopButtonsView The pop buttons view triggering the event
 */
/*  aqui
- (void) okButtonClickedInPopButtonsView: (PopButtonsView*) aPopButtonsView {
	[super okButtonClickedInPopButtonsView: aPopButtonsView];
	
	_editedView = nil;
	
	if (_infoEdited == cast_cveActivacion) {
		[txtCveActivacion resignFirstResponder];
	} 
}

*/ //aqui

/* aqui
- (IBAction)solicitarCodigo:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:SOLICITAR_CODIGO_TEL_NUMBER]];
    
}
@end
*/