//
//  RegistraCuentaSTViewController.h
//  SuiteBancomer
//
//  Created by JuanRa Marin  on 10/10/13.
//  Copyright (c) 2013 GoNet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EditableViewController.h"

#define REGISTRA_CUENTA_ST_XIB  @"RegistraCuentaSTView"

typedef enum {
	rcstiee_None = 0, //!<No information is being edited
	rcstiee_NumCard //
} GOTPInformationEditingEnum;

@class GeneraOTPSTDelegate;

@interface RegistraCuentaSTViewController : EditableViewController
{
     GOTPInformationEditingEnum _infoEdited;
}

@property (nonatomic,strong) GeneraOTPSTDelegate *delegateGeneraOTPST;
@property (strong, nonatomic) IBOutlet UILabel *lblTitulo;
@property (strong, nonatomic) IBOutlet UITextField *txtCuenta;
@property (strong, nonatomic) IBOutlet UILabel *lblInfo;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil;

@end
