//
//  ConfirmacionSTViewController.m
//  Bancomer
//
//  Created by Francisco.Garcia on 27/05/13.
//
//

#import "ConfirmacionSTViewController.h"
#import "ConstantsToken.h"
#import "ContratacionSTDelegate.h"
#import <QuartzCore/QuartzCore.h>
#import "Constants.h"
//#import "Tools.h"

#define IMG_ASM                 @"IoIcAcceso.png"
#define LBL_NIP_TEXT                @"Ingresa el NIP que utilizas en el Cajero Automático"
#define LBL_CVV_TEXT                @"Ingresa los últimos 3 dígitos que se encuentran al reverso de tu tarjeta"
#define LBL_INFO_TEXT2               @"Al tercer intento fallido de tu NIP, tu servicio se bloqueará y tendrás que llamar a Línea Bancomer para desbloquearlo"
#define LBL_INFO_TEXT               @"Al tercer intento fallido de tu NIP o CVV2 tu servicio se bloqueará y tendrás que llamar a Línea Bancomer para desbloquearlo"

#define LBL_INFO_RED_TEXT           @"Esta operación puede tardar varios minutos, asegúrate de tener una buena señal de 3G ó WiFi."
#define LBL_INFO2_TEXT              @""
#define LBL_CVE_ACC_TEXT            @"Ingresa código del Token físico"
#define TXT_HEIGHT                  36
#define TABLE_BORDER_COLOR_RED                          128.0f/255.0f
#define TABLE_BORDER_COLOR_GREEN                        128.0f/255.0f
#define TABLE_BORDER_COLOR_BLUE                         128.0f/255.0f
#define TEXTO_AYUDA_IS_DP20   @"Para obtener el código de seguridad en tu Token: enciéndelo y presiona el botón 1"
#define TEXTO_AYUDA_IS_OCRA   @"Para obtener el código de seguridad en tu Token: enciéndelo y presiona el botón \"Código\""
#define TAMANO_FUENTE_AYUDA_CLAVE       12.0F


@implementation ConfirmacionSTViewController
@synthesize tiposolicitud;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [self setLblNIP];
    [self setTxtNIP];
    //Transacción CVV
    [self setLblCVV];
    [self setTxtCVV];
    [self setLblInfo];
    [self setLblInfo2];
    [self setLblCveAccesoSeg];
    [self setTxtCveAccesoSeg];
    [self setLblAyudaCve];
    [super viewDidLoad];
    _infoEdited = liee_Null;
    [self setHeaderTitle: ST_HEADER_ACTIVACION];
	[self setHeaderImage:[UIImage imageNamed:IMG_ASM]];
    [self showHeader:YES];
    [self muestraBotonDerecho:BOTON_CONFIRMAR];
    [self setLblInfoRed];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self recalculateScroll];
}

-(void)setLblNIP
{
    _lblNIP.text = LBL_NIP_TEXT;
    _lblNIP.frame = CGRectMake(LAT_MARGIN,DISTANCE_BTW_COMP, COMPONENT_WIDTH, LBL_HEIGHT*2);
    _lblNIP.numberOfLines = 2;
    _lblNIP.font = FUENTE_CUERPO;
    _lblNIP.backgroundColor = [UIColor clearColor];
    _lblNIP.textColor = COLOR_GRIS_TEXTO_1;
}
-(void)setTxtNIP
{
    _txtNIP.delegate = self;
    _txtNIP.frame = CGRectMake(LAT_MARGIN, _lblNIP.frame.origin.y+_lblNIP.frame.size.height, COMPONENT_WIDTH, TXT_HEIGHT);
    _txtNIP.layer.cornerRadius = CORNER_RADIOUS;
    _txtNIP.layer.borderWidth = BORDER_WIDTH;
    _txtNIP.layer.borderColor = [UIColor colorWithRed:TABLE_BORDER_COLOR_RED green:TABLE_BORDER_COLOR_GREEN blue:TABLE_BORDER_COLOR_BLUE alpha:1].CGColor;
}

// Transacción CVV
-(void)setLblCVV
{
    
    _lblCVV.text = LBL_CVV_TEXT;
    _lblCVV.frame = CGRectMake(LAT_MARGIN,_txtNIP.frame.origin.y + _txtNIP.frame.size.height + DISTANCE_BTW_COMP, COMPONENT_WIDTH, LBL_HEIGHT*2);
    _lblCVV.numberOfLines = 2;
    _lblCVV.font = FUENTE_CUERPO;
    _lblCVV.backgroundColor = [UIColor clearColor];
    _lblCVV.textColor = COLOR_GRIS_TEXTO_1;
    
    
}
-(void)setTxtCVV
{
    _txtCVV.delegate = self;
    _txtCVV.frame = CGRectMake(LAT_MARGIN, _lblCVV.frame.origin.y+_lblCVV.frame.size.height, COMPONENT_WIDTH, TXT_HEIGHT);
    _txtCVV.layer.cornerRadius = CORNER_RADIOUS;
    _txtCVV.layer.borderWidth = BORDER_WIDTH;
    _txtCVV.layer.borderColor = [UIColor colorWithRed:TABLE_BORDER_COLOR_RED green:TABLE_BORDER_COLOR_GREEN blue:TABLE_BORDER_COLOR_BLUE alpha:1].CGColor;
    
}

-(void)setLblInfo
{
    _lblInfo.text = LBL_INFO_TEXT;
        
    //Transacción CVV
    _lblInfo.frame = CGRectMake(LAT_MARGIN,_txtCVV.frame.origin.y + _txtCVV.frame.size.height + DISTANCE_BTW_COMP, COMPONENT_WIDTH, LBL_HEIGHT*4);
    _lblInfo.numberOfLines = 4;
    _lblInfo.font = FUENTE_CUERPO;
    _lblInfo.backgroundColor = [UIColor clearColor];
    _lblInfo.textColor = COLOR_GRIS_TEXTO_1;
}
-(void)setLblInfo2
{
    _lblInfo2.text = LBL_INFO2_TEXT;
    _lblInfo2.frame = CGRectMake(LAT_MARGIN,_lblInfo.frame.origin.y + _lblInfo.frame.size.height + DISTANCE_BTW_COMP, COMPONENT_WIDTH, LBL_HEIGHT*2);
    _lblInfo2.numberOfLines = 2;
    _lblInfo2.font = FUENTE_CUERPO;
    _lblInfo2.backgroundColor = [UIColor clearColor];
    _lblInfo2.textColor = COLOR_GRIS_TEXTO_1;
}
-(void)setLblInfoRed{
    _lblinfored.text = LBL_INFO_RED_TEXT;
    _lblinfored.font = FUENTE_CUERPO;
    _lblinfored.frame = CGRectMake(LAT_MARGIN,_lblInfo.frame.origin.y + _lblInfo.frame.size.height + DISTANCE_BTW_COMP, COMPONENT_WIDTH, LBL_HEIGHT*3);
    _lblinfored.backgroundColor = [UIColor clearColor];
    _lblinfored.textColor = COLOR_GRIS_TEXTO_1;
    _lblinfored.numberOfLines = 0;
    _lblinfored.lineBreakMode = NSLineBreakByWordWrapping;
}
-(void)setLblCveAccesoSeg
{
    if(tiposolicitud == SUSTITUCION)
    {
        _lblCveAccesoSeg.text = LBL_CVE_ACC_TEXT;
        _lblCveAccesoSeg.frame = CGRectMake(LAT_MARGIN,_lblinfored.frame.origin.y+_lblinfored.frame.size.height+ DISTANCE_BTW_COMP+10, 130, LBL_HEIGHT*2);
        _lblCveAccesoSeg.font = FUENTE_CUERPO;
        _lblCveAccesoSeg.numberOfLines = 2;
        _lblCveAccesoSeg.backgroundColor = [UIColor clearColor];
        _lblCveAccesoSeg.textColor = COLOR_1ER_AZUL;
        _fondoAcceso.frame = CGRectMake(0, _lblCveAccesoSeg.frame.origin.y-30, SCREEN_WIDTH, _fondoAcceso.frame.size.height);
    }
    else
    {
        _lblCveAccesoSeg.hidden = YES;
        _fondoAcceso.hidden = YES;
    }
}
-(void)setTxtCveAccesoSeg
{
    if(tiposolicitud == SUSTITUCION)
    {
        _txtCveAccesoSeg.delegate = self;
        _txtCveAccesoSeg.frame = CGRectMake(_lblCveAccesoSeg.frame.origin.x+_lblCveAccesoSeg.frame.size.width + 10, _lblCveAccesoSeg.frame.origin.y, 140, TXT_HEIGHT);
        _txtCveAccesoSeg.layer.cornerRadius = CORNER_RADIOUS;
        _txtCveAccesoSeg.layer.borderWidth = BORDER_WIDTH;
        _txtCveAccesoSeg.layer.borderColor = [UIColor colorWithRed:TABLE_BORDER_COLOR_RED green:TABLE_BORDER_COLOR_GREEN blue:TABLE_BORDER_COLOR_BLUE alpha:1].CGColor;
        _txtCveAccesoSeg.backgroundColor = [UIColor whiteColor];
    }
    else
    {
        _txtCveAccesoSeg.hidden = YES;
    }
}

-(void)setLblAyudaCve
{
    
    _lblAyudaCve.textColor = UIColorFromRGBWithAlpha(0x888888,1);
    _lblAyudaCve.font = [UIFont fontWithName:HELVETICA_NEUE_LT_STD_55_ROMAN size:TAMANO_FUENTE_AYUDA_CLAVE];
    [_lblAyudaCve setNumberOfLines:0];
    if([_contratacionSTDelegate.tipoInstrumento isEqualToString:@"T3"])
    {
        [_lblAyudaCve setText:TEXTO_AYUDA_IS_DP20];
    }
    else if([_contratacionSTDelegate.tipoInstrumento isEqualToString:@"T6"])
    {
        [_lblAyudaCve setText:TEXTO_AYUDA_IS_OCRA];
    }
    _lblAyudaCve.frame = CGRectMake(_lblCveAccesoSeg.frame.origin.x, _txtCveAccesoSeg.frame.origin.y+_lblCveAccesoSeg.frame.size.height +10, COMPONENT_WIDTH, TXT_HEIGHT);
    [_lblAyudaCve sizeToFit];
}

-(void)viewWillDisappear:(BOOL)animated{
   [super viewWillDisappear:animated];
    [self stopActivityIndicator];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidUnload{
    
    [self setLblNIP:nil];
    //Transacción CVV
    [self setLblCVV:nil];
    [self setLblInfo:nil];
    [self setLblCveAccesoSeg:nil];
    [self setLblInfo2:nil];
    [self setLblAyudaCve:nil];
    [super viewDidUnload];
}


#pragma mark - 

/*
 *
 */

-(void)configuraParaOperacion:(NSInteger)operation{

    tiposolicitud = operation;
    if(operation == NUEVO || operation == REACTIVACION)
    {
        [_lblNIP setHidden:NO];
        [_txtNIP setHidden:NO];
        //Transacción CVV
        [_lblCVV setHidden:NO];
        [_txtCVV setHidden:NO];
        [_lblInfo setHidden:NO];
        [_lblCveAccesoSeg setHidden:YES];
        [_txtCveAccesoSeg setHidden:YES];
        [_fondoAcceso setHidden:YES];
        
    }
    else if(operation == SUSTITUCION){
        
        [_lblNIP setHidden:NO];
        [_lblInfo setHidden:NO];
        [_txtNIP setHidden:NO];
        //Transacción CVV
        [_lblCVV setHidden:NO];
        [_txtCVV setHidden:NO];
        [_lblCveAccesoSeg setHidden:NO];
        [_txtCveAccesoSeg setHidden:NO];

    }
}

/*
 *
 */
-(void)ejecutaAccionBotonDerecho{
    //server ...
    NSLog(@"Continuar ConfirmacionST");
    _editedView = nil;
    _infoEdited = liee_Null;
    if(_txtNIP.isFirstResponder)
        [_txtNIP resignFirstResponder];
    //Transacción CVV
    if(tiposolicitud!=SUSTITUCION){
        if(_txtCVV.isFirstResponder)
            [_txtCVV resignFirstResponder];
    }
    if(_txtCveAccesoSeg.isFirstResponder)
        [_txtCveAccesoSeg resignFirstResponder];
    NSString *cveAccesoSeguro = @"";
    if(_txtCveAccesoSeg.text != nil)
    {
        cveAccesoSeguro = _txtCveAccesoSeg.text;
    }
        
    if([ContratacionSTDelegate validaCampos:self]){
            [_contratacionSTDelegate consultaAutenticacionN:_txtNIP.text andCVV:_txtCVV.text andOTP:cveAccesoSeguro onViewController:self];

    }
}

#pragma mark -

- (NSInteger) maxSizeForTextField: (UITextField*) aTextField {
	NSInteger result = -1;
	
	if (aTextField == _txtNIP) {
		result = NIP_REFERENCE_LENGTH;
    }else if (aTextField == _txtCVV) {    // --- Transacción CVV
      result = CVV_REFERENCE_LENGTH;
      } else if (aTextField == _txtCveAccesoSeg) {
		result = CVE_AS_REFERENCE_LENGTH;
	}	
	return result;
}

/**
 * Child classes informs editable view controller whether the provided text field contains
 * an integer number. Both text field in view controller are integer numbers
 *
 * @param aTextField The text field to test
 * @return YES in all cases
 */
- (BOOL) textFieldIsInteger: (UITextField*) aTextField {
	return YES;
}

/**
 * Invoked by framework when a text field asks to start editing. Pop buttons view state is updated accodingly
 *
 * @param textField The text field asking to start editing
 * @return YES if editing can start, NO otherwise
 */
- (BOOL) textFieldShouldBeginEditing: (UITextField*) textField {
	BOOL result = YES;
	
	if (textField == _txtNIP) {
		//[_popButtonsView enableNextResponderButton: !_txtCveAccesoSeg.isHidden];
        [_popButtonsView enableNextResponderButton: YES];
		[_popButtonsView enablePreviousResponderButton: NO];
		_editedView = _txtNIP;
		_infoEdited = liee_NIP;
    }else if (textField == _txtCVV) { //-- Transacción CVV
      [_popButtonsView enableNextResponderButton: !_txtCveAccesoSeg.isHidden];
      [_popButtonsView enablePreviousResponderButton: YES];
      _editedView = _txtCVV;
      _infoEdited = liee_CVV;
      } else if (textField == _txtCveAccesoSeg) {
		[_popButtonsView enableNextResponderButton: NO];
		[_popButtonsView enablePreviousResponderButton: YES];
		_editedView = _txtCveAccesoSeg;
		_infoEdited = liee_CVEAcesso;
	} else {
		[_popButtonsView enableNextResponderButton: NO];
		[_popButtonsView enablePreviousResponderButton: NO];
		_editedView = nil;
		_infoEdited = liee_Null;
		result = NO;
	}
	
	return result;
}



/**
 * Informs the delegate that next responder button was clicked. Sets password text field
 * as next first responder
 *
 * @param aPopButtonsView The pop buttons view triggering the event
 */
- (void) nextButtonClickedInPopButtonsView: (PopButtonsView*) aPopButtonsView {
	if (_infoEdited == liee_NIP) {
        [_txtCVV becomeFirstResponder];  //---Transacción CVV
         }else if (_infoEdited == liee_CVV) {
         
		[_txtCveAccesoSeg becomeFirstResponder];
	}
}

/**
 * Informs the pop buttons view delegate that OK button was clicked. Keypad is dismissed
 *
 * @param aPopButtonsView The pop buttons view triggering the event
 */
- (void) okButtonClickedInPopButtonsView: (PopButtonsView*) aPopButtonsView {
	[super okButtonClickedInPopButtonsView: aPopButtonsView];
	
	_editedView = nil;
	
	if (_infoEdited == liee_NIP) {
		[_txtNIP resignFirstResponder];
    }else if (_infoEdited == liee_CVV) { //--- Transacción CVV
      [_txtCVV resignFirstResponder];
      }  else if (_infoEdited == liee_CVEAcesso) {
		[_txtCveAccesoSeg resignFirstResponder];
	}
}

/**
 * Informs the delegate that previous responder button was clicked. Sets user text field as
 * next first responder
 *
 * @param aPopButtonsView The pop buttons view triggering the event
 */
- (void) previousButtonClickedInPopButtonsView: (PopButtonsView*) aPopButtonsView {
	
    if (_infoEdited == liee_CVV) { //---- Transacción CVV Reemplaza al de arriba
     [_txtNIP becomeFirstResponder];
     } else if (_infoEdited == liee_CVEAcesso) {
     [_txtCVV becomeFirstResponder];
     }
}




@end
