//
//  claveActivacionViewController.h
//  APIToken
//
//  Created by Sergio Berlanga Alvarez on 05/07/16.
//  Copyright © 2016 CGI. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EditableViewController.h"
@class ContratacionSTDelegate;
#define CLV_ACTIVACION_ST_XIB           @"claveActivacionViewController"
typedef enum {
    cast_None = 0,
    cast_cveActivacion
} CveActivacionInformationEditingEnum;
@interface claveActivacionViewController : EditableViewController <UIAlertViewDelegate> {
    
    ContratacionSTDelegate* _contratacionSTDelegate;
    CveActivacionInformationEditingEnum _infoEdited;
}




@property (strong, nonatomic) ContratacionSTDelegate* contratacionSTDelegate;

@property (strong, nonatomic) IBOutlet UILabel *lblActivacion;

@property (strong, nonatomic) IBOutlet UILabel *lblIntroducircodigo;
@property (strong, nonatomic) IBOutlet UITextField *TextCodigo;

@property (strong, nonatomic) IBOutlet UILabel *lblReenviar;

@property (strong, nonatomic) IBOutlet UIButton *btnReenviar;

- (IBAction)btnActivar:(id)sender;
- (IBAction)reenviarTouch:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *btnActivarOutlet;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil;

-(void)activarBotonActivar:(BOOL)enable;


//Segundo XIB
@property (strong, nonatomic) IBOutlet UILabel *lblTitulo;

@property (strong, nonatomic) IBOutlet UIView *viewExito;
@property (strong, nonatomic) IBOutlet UILabel *lblSubtitulo;
@property (strong, nonatomic) IBOutlet UIImageView *imgExito;

@property (strong, nonatomic) IBOutlet UIButton *btnExitoOutlet;
@property (strong, nonatomic) IBOutlet UIView *viewContenedorExito;

- (IBAction)btnAccionExito:(id)sender;

@end
