//
//  ContratarTokenController.m
//  APIToken
//
//  Created by OscarO on 12/07/16.
//  Copyright © 2016 CGI. All rights reserved.
//

#import "ContratarTokenController.h"

//#import "IngresaDatosSTViewController.h" VC anterior
#import "Constants.h"
#import "ConstantsToken.h"
#import "ContratacionSTDelegate.h"
#import "Compania.h"  //linea comentada se va a quitar
#import "Tools.h"
//#import "TelefonicasMantenimiento.h"
#import "EditableViewController+protectedCategory.h"
#import <QuartzCore/QuartzCore.h>
#import "ServerToken.h"
#import "APIToken.h"

//#define LBL_INGRESA_TEXT            @"Ingresa tus datos"
#define LBL_NUM_CLL_TEXT            @"Número celular"
//#define LBL_NUM_TRJTA_TEXT          @"Número de tarjeta"
#define IMG_ASM                 @"IoIcAcceso.png"
#define TXT_HEIGHT                  36
#define TABLE_BORDER_COLOR_RED                          128.0f/255.0f
#define TABLE_BORDER_COLOR_GREEN                        128.0f/255.0f
#define TABLE_BORDER_COLOR_BLUE                         128.0f/255.0f
#define LBL_INFO_CLL_TEXT           @"Es el número celular de 10 dígitos que registraste en sucursal."
#define LBL_INFO_TRJTA_TEXT         @"Es el número de tu Tarjeta de Débito asociada a la cuenta que registraste en la sucursal."
#define LBL_INFO_RED_TEXT           @"Asegúrate de tener una buena señal de 3G ó WiFi"
 
 
@interface ContratarTokenController ()

@end

@implementation ContratarTokenController
@synthesize contratacionSTDelegate = _contratacionSTDelegate;
UIImageView *imagenUsuario;
UIImageView *imagenBarra;
UIImageView *imagenPaloma;

 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
 
 
- (void)viewDidLoad {
    
    ///////////
    //Poner los metodos SET aqui
    ///////////
    [self setEncabezado];
    [self setLblNumero];
    [self setGetNumero];
    [self setBtnContinuarOutlet];
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self showHeader: NO];
    [self ocultaEncabezado:YES];
    [self configView];
    
}
 
 
 -(void)setLblNumero{
     _lblNumero.text = LBL_NUM_CLL_TEXT;
     _lblNumero.frame = CGRectMake(LAT_MARGIN+80, 70, COMPONENT_WIDTH, LBL_HEIGHT);
     _lblNumero.font = FUENTE_CUERPO;
     _lblNumero.backgroundColor = [UIColor clearColor];
     _lblNumero.textColor = COLOR_GRIS_TEXTO_1;
 }
 
 -(void)setGetNumero{
     _lblGetNumero.frame = CGRectMake(LAT_MARGIN +80, _lblNumero.frame.origin.y + 20, COMPONENT_WIDTH, LBL_HEIGHT+10);
     
     
     NSString * userName = [[APIToken getInstance].apiTokenDataSource userName];
     
     if (userName.length == 10) {
         _lblGetNumero.text = userName;
     } else {
         _lblGetNumero.text = @"";
     }

 }
 
 -(void)setBtnContinuarOutlet{
         CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
         //Modificacion pantalla
         if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
         {
             //its iPhone. Find out which one?
             
             CGSize resultS = [[UIScreen mainScreen] bounds].size;
             if(resultS.height == 480)
             {
                 // iPhone Classic
                 _btnContinuarOutlet.frame = CGRectMake(LAT_MARGIN, 435/*TextCodigo.frame.origin.y+TextCodigo.frame.size.height+100*/, screenWidth -40, 40 );
                 NSLog(@"iphone 4s");
             }
             else if(resultS.height == 568)
             {
                 _btnContinuarOutlet.frame = CGRectMake(LAT_MARGIN, _lblGetNumero.frame.origin.y+_lblGetNumero.frame.size.height+140/*TextCodigo.frame.origin.y+TextCodigo.frame.size.height+100*/, screenWidth -40, 40 );
                 NSLog(@"iphone 5");
             }
             else if(resultS.height == 667)
             {
                 _btnContinuarOutlet.frame = CGRectMake(LAT_MARGIN, _lblGetNumero.frame.origin.y+_lblGetNumero.frame.size.height+140, screenWidth -40, 40 );
                 NSLog(@"iphone 6");
             }
             else if(resultS.height == 736)
             {
                 _btnContinuarOutlet.frame = CGRectMake(LAT_MARGIN, _lblGetNumero.frame.origin.y+_lblGetNumero.frame.size.height+140, screenWidth -40, 40 );
                 NSLog(@"iphone 6+");
             }
         }
         //_btnActivarOutlet.frame = CGRectMake(LAT_MARGIN, self.view.bounds.size.height - 130/*TextCodigo.frame.origin.y+TextCodigo.frame.size.height+100*/, screenWidth -40, 40 );
     }
 

-(void)setEncabezado {
    UIImage *imUsuario = [UIImage imageNamed:@"icon_usuario.png"];
    UIImage *imPaloma = [UIImage imageNamed:@"icon_paloma_inactiva.png"];
    UIImage *imBarra = [UIImage imageNamed:@"barra_azul.png"];
    
    CGFloat usuarioWidth = imUsuario.size.width;
    CGFloat usuarioHeight = imUsuario.size.height;
    CGFloat palomaWidth = imPaloma.size.width;
    CGFloat palomaHeight = imPaloma.size.height;
    CGFloat barraWidth = imBarra.size.width;
    CGFloat barraHeight = imBarra.size.height;
    
    imagenUsuario = [[UIImageView alloc]initWithFrame:CGRectMake(0, 25, usuarioWidth-50, usuarioHeight-50)];
    [imagenUsuario setImage:[UIImage imageNamed:@"icon_usuario.png"]];
    
    imagenBarra = [[UIImageView alloc]initWithFrame:CGRectMake(5, 27, barraWidth-100, barraHeight-100 )];
    [imagenBarra setImage:[UIImage imageNamed:@"barra_azul.png"]];
    imagenPaloma = [[UIImageView alloc]initWithFrame:CGRectMake(self.view.bounds.size.width - 15, 25, palomaWidth-50, palomaHeight-50)];
    [imagenPaloma setImage: [UIImage imageNamed: @"icon_paloma_inactiva.png"]];
    
    
    [self.view addSubview:imagenUsuario];
    [self.view addSubview:imagenBarra];
    [self.view addSubview:imagenPaloma];
//    UIImage* im = [UIImage imageNamed:@"icon_celular_candado"];
//    CGFloat imgHeight = im.size.height;
//    CGFloat imgWidth = im.size.width;
//    /*UIImageView **/imageView = [[UIImageView alloc] initWithFrame:CGRectMake(LAT_MARGIN + 85, DISTANCE_BTW_COMP + 90, imgWidth-80, imgHeight-125)];
//    [imageView setImage:[UIImage imageNamed:@"icon_celular_candado"]];
//    //imageView = [[UIImageView alloc] initWithFrame:CGRectMake(LAT_MARGIN + 80, DISTANCE_BTW_COMP + _lblActivacion.frame.size.height + 40, imgWidth-175, imgHeight-250)];
//    [self.view addSubview:imageView];

}

-(void)viewWillDisappear:(BOOL)animated{
    
    //[self okButtonClickedInPopButtonsView:nil];
    [_textNumTarjeta resignFirstResponder];
    
    [super viewWillDisappear:animated];
}

- (void)viewDidUnload {

    [self setLblNumero:nil];
    [self setLblGetNumero:nil];
    [self setBtnContinuarOutlet:nil];
    
    
    [super viewDidUnload];
}






- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnContinuarAccion:(id)sender {
}

//--------------------ENCABEZADO-------------------------

- (void)configView {
    
    UIImageView *bgImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 20, _headerView.bounds.size.width, _headerView.bounds.size.height)];
    [bgImg setImage:[UIImage imageNamed:IMG_ASM]];
    [self.view addSubview:bgImg];
    
    UIView *bar = [[UIView alloc] initWithFrame:CGRectMake(0, 20, _headerView.bounds.size.width, _headerView.bounds.size.height)];
    [bar setBackgroundColor:[UIColor colorWithRed:30/255.0 green:80/255.0 blue:130.0/255.0 alpha:0]];
    
    for (UIView *v in [_headerView subviews]) {
        if ([v isKindOfClass:[UIImageView class]]) {
            [v removeFromSuperview];
        }
    }
    UIButton *btnBack = [[UIButton alloc] initWithFrame:CGRectMake(20, 7, 30, 28)];
    
    [btnBack setImage:[UIImage imageNamed:@"al_ic_regresar.png"] forState:UIControlStateNormal];
    [btnBack setTitle:@"" forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(accionBotonIzquierdo) forControlEvents:UIControlEventTouchUpInside];
    [bar addSubview:btnBack];
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-110, 13, 220, 21)];
    [title setText:@"CONTRATACIÓN"];
    [title setTextColor:[UIColor whiteColor]];
    [title setTextAlignment:NSTextAlignmentCenter];
    [title setFont:[UIFont fontWithName:HELVETICA_NEUE_LT_STD_55_ROMAN size:16.0f]];
    [title setAdjustsFontSizeToFitWidth:YES];
    [title setMinimumScaleFactor:0.7];
    [bar addSubview:title];
    
    [self.view addSubview:bar];
}


@end
