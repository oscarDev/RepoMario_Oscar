//
//  SofttokenApp.m
//  SuiteBancomer
//
//  Created by JuanRa Marin  on 07/10/13.
//  Copyright (c) 2013 GoNet. All rights reserved.
//

#import "SofttokenApp.h"
#import "SoftokenViewsController.h"
#import "ConstantsToken.h"
#import "TipoSolicitudResult.h"
#import "SincronizaTokenResult.h"
#import "FinalizarContratacionSTResult.h"
#import "ContratacionSTDelegate.h"
#import "SincronizacionExportacionTokenResult.h"
#import "ServerToken.h"
#import "Constants.h"
#import "APIToken.h"
#import "ToolsAPIToken.h"
#import "ServerResponse.h"

@implementation SofttokenApp

- (id)initWithStatus:(BOOL)status {
    return [self initWithStatus:status andMostrarPantallaToken:YES];
}

- (id)initWithStatus:(BOOL)status andMostrarPantallaToken:(BOOL) mostrarPantallaToken {
    self = [super init];
    if (self) {
        [self softLaunchedwithStatus:status andMostrarPantallaToken:mostrarPantallaToken];
    }
    return self;
}

-(id)initAutenticacionST {
    self = [super init];
    if (self) {
        [self softLaunchedAutenticacionST];
    }
    return self;
}

-(void)softLaunchedwithStatus:(BOOL)status andMostrarPantallaToken:(BOOL) mostrarPantallaToken {
    
    if (_controladorSofttoken) {
        _controladorSofttoken = nil;
    }
    _controladorSofttoken = [[SoftokenViewsController alloc] initWithSoftDelegate:self andStatus:status andMostrarPantallaToken:mostrarPantallaToken];
    
}


-(void)softLaunchedwithStatus:(BOOL)status {
    [self softLaunchedwithStatus:status andMostrarPantallaToken:YES];
}

-(void)softLaunchedAutenticacionST {
    
    if (_controladorSofttoken) {
        _controladorSofttoken = nil;
    }
    _controladorSofttoken = [[SoftokenViewsController alloc] initAutenticacionSTWithSoftDelegate:self];
}

/*
 * Shows an information alert view
 */
- (void) showInformationWithMessage: (NSString*) aMessage
{
	
	NSString* title = LABEL_INFORMATION;
	NSString* okButton = OK_KEY;
	
    _informationAlert = [[UIAlertView alloc] initWithTitle: title message: aMessage delegate: nil
                                         cancelButtonTitle: okButton otherButtonTitles: nil];
	[_informationAlert show];
    
}

/*
 * Shows an error message if the provided message length is not zero. The alert view title will
 * be "Error", and it will have one button to accept
 */
- (void) showErrorMessage: (NSString*) anErrorMessage {
	if ([anErrorMessage length] > 0) {
        
        NSString* alertTitle = AVISO;
		NSString* buttonText = OK_KEY;
		
        _errorAlert  = [[UIAlertView alloc] initWithTitle: alertTitle message: anErrorMessage delegate: nil cancelButtonTitle:buttonText otherButtonTitles: nil];
		[_errorAlert show];
	}
}

/*
 * Shows an error message if the provided message length is not zero. The alert view title will
 * be "Error", and it will have one button to accept
 */
- (void) showErrorMessage: (NSString*) anErrorMessage withErrorCode: (NSString*) anErrorCode{
	if ([anErrorMessage length] > 0) {
        
        
        NSString* alertTitle = AVISO;
        NSString* buttonText = OK_KEY;
        NSString* completeMessage;
        
        if ([anErrorCode length] > 0) {
            completeMessage = [NSString stringWithFormat:@"%@\n\n%@", anErrorMessage, anErrorCode];
        } else {
            completeMessage = anErrorMessage;
        }
        
        _errorAlert  = [[UIAlertView alloc] initWithTitle: alertTitle message: completeMessage delegate: nil cancelButtonTitle:buttonText otherButtonTitles: nil];
        [_errorAlert show];
	}
}

/*
 * Analyzes the general aspects of a server response
 */
- (BOOL) analyzeServerResponse: (ServerResponse*) aServerResponse forOperation: (NSInteger) anOperation
{
	BOOL result = NO;
    
	if (anOperation == CLOSE_SESSION_OPERATION) {
		[[Server getInstance] cancelPendingOperations];
		
        [[APIToken getInstance].apiTokenDelegate setInvalidSessionStatus];
		[[APIToken getInstance].apiTokenDelegate storeSession];
	} else {
		NSInteger resultCode = aServerResponse.status;
		
        
		switch (resultCode) {
			case OPERATION_SUCCESSFUL: {
			}
			case OPERATION_OPTIONAL_UPDATE: {
				result = YES;
				break;
			}
            case OPERATION_WARNING:
            {
                [self showErrorMessage:aServerResponse.messageText];
                //result = YES;
                break;
            }
			case OPERATION_ERROR: {
                
                
                if ([aServerResponse.responseHandler isKindOfClass:[SincronizacionExportacionTokenResult class]]) {
                    
                    result = YES;
                }else if (([aServerResponse.responseHandler isKindOfClass:[TipoSolicitudResult class]]) && ([aServerResponse.messageCode isEqualToString:CODIGO_ERROR_CNE1506])) {
                    
                    [self.controladorSofttoken.delegateContratacionST.viewController stopActivityIndicator];
                    result = YES;
                    
                } else if ([aServerResponse.responseHandler isKindOfClass:[FinalizarContratacionSTResult class]]) {
                    
                    // Borrar el archivo temporal ST
                    NSString* temporalSTFilePath = [ToolsAPIToken getTemporalSTFilePath];
                    [[NSFileManager defaultManager] removeItemAtPath: temporalSTFilePath error: NULL];
                    
                    // contratarBmovil = false
                    NSString* banderasBMovilFilePath = [ToolsAPIToken getBanderasBMovilFilePath];
                    NSMutableDictionary* banderasDictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:banderasBMovilFilePath];
                    
                    [banderasDictionary setObject:[NSNumber numberWithBool:NO] forKey:CONTRATAR_BMOVIL_KEY];
                    [banderasDictionary writeToFile: banderasBMovilFilePath atomically: YES];
                    
                    [[APIToken getInstance].apiTokenDelegate storeSession];
                    
                    [ ContratacionSTDelegate guardaEstatusTokenMovil: NO ];
                    
                    // mostrar pantalla de correo electrónico
                    [self.controladorSofttoken showEmailST];
                    
                    [self.controladorSofttoken.delegateContratacionST.viewController stopActivityIndicator];
                    [self showErrorMessage:aServerResponse.messageText withErrorCode:aServerResponse.messageCode];

                } else {
                    [self.controladorSofttoken.delegateContratacionST.viewController stopActivityIndicator];
                    [self showErrorMessage:aServerResponse.messageText withErrorCode:aServerResponse.messageCode];
                }
                
				break;
			}
			case OPERATION_SESSION_EXPIRED: {
				 [[APIToken getInstance].apiTokenDelegate setInvalidSessionStatus];
				break;
			}
			default: {
				NSString* errorMessage = ERROR_FORMAT;
				[self showErrorMessage: errorMessage];
				break;
			}
		}
	}
	
	return result;
}


//METODO NUEVO
/*
 * Analyzes the general aspects of a server response
 */
- (BOOL) analyzeServerResponseN: (ServerResponse*) aServerResponse forOperation: (NSInteger) anOperation
{
    BOOL result = NO;
    
    if (anOperation == CLOSE_SESSION_OPERATION) {
        [[Server getInstance] cancelPendingOperations];
        
        [[APIToken getInstance].apiTokenDelegate setInvalidSessionStatus];
        [[APIToken getInstance].apiTokenDelegate storeSession];
    } else {
        NSInteger resultCode = aServerResponse.status;
        
        
        switch (resultCode) {
            case OPERATION_SUCCESSFUL: {
            }
            case OPERATION_OPTIONAL_UPDATE: {
                result = YES;
                break;
            }
            case OPERATION_WARNING:
            {
                [self showErrorMessage:aServerResponse.messageText];
                //result = YES;
                break;
            }
            case OPERATION_ERROR: {
                
                
                if ([aServerResponse.responseHandler isKindOfClass:[SincronizacionExportacionTokenResult class]]) {
                    
                    result = YES;
                }else if (([aServerResponse.responseHandler isKindOfClass:[TipoSolicitudResult class]]) && ([aServerResponse.messageCode isEqualToString:CODIGO_ERROR_CNE1506])) {
                    
                    [self.controladorSofttoken.delegateContratacionST.viewController stopActivityIndicator];
                    result = YES;
                    
                } else if ([aServerResponse.responseHandler isKindOfClass:[FinalizarContratacionSTResult class]]) {
                    
                    // Borrar el archivo temporal ST
                    NSString* temporalSTFilePath = [ToolsAPIToken getTemporalSTFilePath];
                    [[NSFileManager defaultManager] removeItemAtPath: temporalSTFilePath error: NULL];
                    
                    // contratarBmovil = false
                    NSString* banderasBMovilFilePath = [ToolsAPIToken getBanderasBMovilFilePath];
                    NSMutableDictionary* banderasDictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:banderasBMovilFilePath];
                    
                    [banderasDictionary setObject:[NSNumber numberWithBool:NO] forKey:CONTRATAR_BMOVIL_KEY];
                    [banderasDictionary writeToFile: banderasBMovilFilePath atomically: YES];
                    
                    [[APIToken getInstance].apiTokenDelegate storeSession];
                    
                    [ ContratacionSTDelegate guardaEstatusTokenMovil: NO ];
                    
                    // mostrar pantalla de correo electrónico
                    [self.controladorSofttoken showEmailST];
                    
                    [self.controladorSofttoken.delegateContratacionST.viewController stopActivityIndicator];
                    [self showErrorMessage:aServerResponse.messageText withErrorCode:aServerResponse.messageCode];
                    
                } else {
                    [self.controladorSofttoken.delegateContratacionST.viewController stopActivityIndicator];
                    [self showErrorMessage:aServerResponse.messageText withErrorCode:aServerResponse.messageCode];
                }
                
                break;
            }
            case OPERATION_SESSION_EXPIRED: {
                [[APIToken getInstance].apiTokenDelegate setInvalidSessionStatus];
                break;
            }
            default: {
                NSString* errorMessage = ERROR_FORMAT;
                [self showErrorMessage: errorMessage];
                break;
            }
        }
    }
    
    return result;
}



/**
 * Shows a communication error alert
 */
- (void) showCommunicationErrorAlert {
	
	NSString* alertMessage = ERROR_COMUNICACIONES;
	NSString* alertTitle = ERROR;
	NSString* okButton = OK_KEY;
	
	UIAlertView* alertView = [[UIAlertView alloc] initWithTitle: alertTitle message: alertMessage delegate: nil
											   cancelButtonTitle: okButton otherButtonTitles: nil];
    
    
	[alertView show];
}

@end
