//
//  ContratacionSTDelegate.m
//  Bancomer
//
//  Created by Francisco.Garcia on 27/05/13.
//
//

#import "AutenticacionTokenResult.h"
#import "ConfirmacionSTViewController.h"
#import "AutenticacionSTViewController.h"
#import "ContratacionSTDelegate.h"

#import "IngresaDatosSTViewController.h" //linea comentada 1er pantalla
#import "ContratarTokenController.h" //linea agregada 1er pantalla

#import "DefinicionContrasenaViewController.h"
#import "Server.h"
#import "ServerToken.h"
#import "ServerAplicacionDesactivada.h"
#import "ServerResponse.h"
#import "SoftokenViewsController.h"
#import "TipoSolicitudResult.h"
#import "Tools.h"
#import "SincronizaTokenResult.h"
#import "ExportacionTokenResult.h"
#import "ConstantsToken.h"
//#import "ClaveActivacionSTViewController.h" linea comentada
#import "GeneraOTPSTDelegate.h"
#import "EmailSTViewController.h"
//#import "OperacionExitosaToken.h"
#import "CambioTelefonoAsociadoSTResult.h"
#import "Softtoken.h"
#import "SolicitudSTResult.h"
#import "IngresaDatosViewController.h"
#import "ContratacionDelegate.h"
#import "FinalizarContratacionSTResult.h"
#import "SincronizacionExportacionTokenResult.h"
#import "ConsultaEstatus.h"
#import "ContratacionViewsController.h"
#import "SofttokenApp.h"
#import "Autenticacion.h"
#import "APIToken.h"
#import "ToolsAPIToken.h"
#import "ServerTools.h"
#import "KeyChainHandler.h"

#import "claveActivacionViewController.h"  // linea comentada se agrega el import 


#define PENDIENTE_DESCARGA                  @"ArchivoPendienteDescarga"
#define PENDIENTE_DESCARGA_KEY              @"pendienteDescarga"
#define SMS_ALERT_TEXT                      @"En breve recibirás un mensaje con el código de activación para continuar el registro de la aplicación Token Móvil."

#define ESTATUS_A1    @"A1"
#define ESTATUS_PE    @"PE"
#define ESTATUS_PA    @"PA"

#define TEXTO_CAMBIO_NUMERO_COMPANIA        @"¿Has cambiado de número o compañía telefónica?"
#define TEXTO_VERIFICAR_DATOS               @"Por favor verifique los datos ingresados"
#define TEXTO_ACTUALIZAR_ALERTAS            @"Acude a tu cajero automático BBVA Bancomer más cercano para que actualices tus alertas Bancomer"

#define TAG_ALERTA_CAMBIO_NUMERO_COMPANIA_DESDE_REENVIAR_CODIGO         2
#define TAG_ALERTA_CAMBIO_NUMERO_COMPANIA_DESDE_CODIGO_ERROR_CNE1506    3
#define TAG_ALERTA_VERIFICAR_DATOS                                      4
#define TAG_ALERTA_ACTUALIZAR_ALERTAS_DESDE_REENVIAR_CODIGO             5
#define TAG_ALERTA_ACTUALIZAR_ALERTAS_DESDE_CODIGO_ERROR_CNE1506        6
#define TAG_ALERTA_IVR_FALSE                                            7

#define SOLICITAR_CODIGO_TEL_NUMBER        @"tel://+525552262685,#39"


@implementation ContratacionSTDelegate {
    PopUpImageViewController *sp_AlertView;
    AlertaIncVC *sp_AlertViewInc; //Se agrego esta linea
    AlertaEnvio *sp_AlertViewEnvio; //se agrego esta linea

}
@synthesize confirmacionST = _confirmacionST;
//@synthesize claveActivacionSTViewController = _claveActivacionSTViewController; linea comentada
@synthesize claveActivacionViewController = _claveActivacionViewController; //linea comentada se agrego
@synthesize autenticacionSTViewController = _autenticacionSTViewController;

bool isActivacionNuevoToken;
bool direcionarreactivar2x1=false;
double activationTimeBack;

+(NSInteger) getOperacionSolicitudSofttoken {
    return SOLICITUD_ST;
}

-(void)invokeContratacionEnrolamiento:(BaseDelegate*)client {
    NSLog(@"Contratación Enrolamiento con cliente genérico");
    
    NSString* temporalSTFilePath = [ToolsAPIToken getTemporalSTFilePath];
    NSDictionary* temporalSTDictionary = [NSDictionary dictionaryWithContentsOfFile: temporalSTFilePath];
    NSLog(@"temporalSTDictionary%@",temporalSTDictionary);
    
    NSInteger downloadId;
    
    downloadId = [[ServerToken getInstance] contratacionEnrolamiento:[temporalSTDictionary objectForKey:TEMPORALST_NUM_CELULAR]
                                                           cveAcceso:[temporalSTDictionary objectForKey:TEMPORALST_CONTRASENA]
                                                       numeroTarjeta:[temporalSTDictionary objectForKey:TEMPORALST_NUM_TARJETA]
                                                       perfilCliente:[temporalSTDictionary objectForKey:TEMPORALST_PERFIL]
                                                     companiaCelular:[temporalSTDictionary objectForKey:TEMPORALST_COMPANIA_CELULAR]
                                                               email:[temporalSTDictionary objectForKey:TEMPORALST_CORREO]
                                           aceptoTerminosCondiciones:@"si"
                                                           forClient:client];
    
    if (downloadId > 0) {
        [_controladorIngresaDatosST startActivityIndicator];
    } else {
        [_controladorIngresaDatosST showCommunicationErrorAlert];
    }
}

-(void)invokeOperacionIngreso:(BaseDelegate*)client andNumCelular:(NSString*)numCelular andTarjeta:(NSString*)numTarjeta andCompaniaCelular:(NSString*) companiaCelular {
    
    NSLog(@"ConsultaTarjetaST con cliente genérico");
    
    NSInteger downloadId;
    downloadId = [[ServerToken getInstance] consultaTipoSolicitudConNumCelular:numCelular andNumTarjeta:numTarjeta andCompaniaCelular:companiaCelular vApp:_objetoSofttoken.versionApp forClient:client];
    //client:contratacionDelegate
    
    if (downloadId > 0) {
        [_controladorIngresaDatosST startActivityIndicator];
    } else {
        [_controladorIngresaDatosST showCommunicationErrorAlert];
    }
}

//linea comentada METODO ANTERIOR
-(void)realizarConsultaSolicitud :(NSString*)numCelular andTarjeta:(NSString*)numTarjeta andCompaniaCelular:(NSString*) companiaCelular {
    
    NSInteger downloadId;
    _tipoOperacion=1;
    downloadId = [[ServerToken getInstance] consultaTipoSolicitudConNumCelular:numCelular andNumTarjeta:numTarjeta andCompaniaCelular:companiaCelular vApp:_objetoSofttoken.versionApp forClient:self]; //forClient:_controladorIngresaDatosST
    
    if (downloadId > 0) {
        [_controladorIngresaDatosST startActivityIndicator];
    } else {
        [_controladorIngresaDatosST showCommunicationErrorAlert];
    }
}

//linea comentada METODO NUEVO

-(void)realizarConsultaSolicitudN :(NSString*)numCelular andTarjeta:(NSString*)numTarjeta{
    
    NSInteger downloadId;
    _tipoOperacion=1;
    downloadId = [[ServerToken getInstance] consultaTipoSolicitudConNumCelular:numCelular andNumTarjeta:numTarjeta forClient:self]; //forClient:_controladorIngresaDatosST
    
    if (downloadId > 0) {
        [_controladorIngresaDatosST startActivityIndicator];
    } else {
        [_controladorIngresaDatosST showCommunicationErrorAlert];
    }
}


//linea comentada METODO ANTERIOR
-(void)consultaSolicitud:(NSString*)numCelular andTarjeta:(NSString*)numTarjeta andCompaniaCelular:(NSString*) companiaCelular onViewController:(IngresaDatosSTViewController*)viewController{
   
    _viewController = viewController;
    
    if([viewController isKindOfClass:[IngresaDatosSTViewController class]]){
        [self realizarConsultaSolicitud:_objetoSofttoken.numCelular andTarjeta:_objetoSofttoken.numTarjeta andCompaniaCelular:_objetoSofttoken.companiaCelular];
    }
}

//linea comentada METODO NUEVO
-(void)consultaSolicitudN:(NSString*)numCelular andTarjeta:(NSString*)numTarjeta onViewController:(IngresaDatosSTViewController*)viewController{
    
    _viewController = viewController;
    
    if([viewController isKindOfClass:[IngresaDatosSTViewController class]]){
        [self realizarConsultaSolicitudN:_objetoSofttoken.numCelular andTarjeta:_objetoSofttoken.numTarjeta];
    }
}

//linea comentada METODO ANTERIOR
-(void)setObjetoSofttoken:(NSString*)numeroCelular andCompaniaCelular:(NSString*)aCompaniaCelular andNumTarjeta:(NSString*)aNumTarjeta {
    
    if (!_objetoSofttoken) {
        _objetoSofttoken = [[Softtoken alloc]init];
    }
    
    if (numeroCelular != nil) {
        _objetoSofttoken.numCelular = numeroCelular;
    }
    if (aCompaniaCelular != nil) {
        _objetoSofttoken.companiaCelular = aCompaniaCelular;
    }
    if (aNumTarjeta != nil) {
        _objetoSofttoken.numTarjeta = aNumTarjeta;
    }
    
    float version = [APIToken getInstance].appVersion;
    version = version*100;
    _objetoSofttoken.versionApp =[NSString stringWithFormat:@"%.0f",version];
    
}

//linea nueva METODO NUEVO 
-(void)setObjetoSofttokenN:(NSString*)numeroCelular andNumTarjeta:(NSString*)aNumTarjeta {
    
    if (!_objetoSofttoken) {
        _objetoSofttoken = [[Softtoken alloc]init];
    }
    
    if (numeroCelular != nil) {
        _objetoSofttoken.numCelular = numeroCelular;
    }
    if (aNumTarjeta != nil) {
        _objetoSofttoken.numTarjeta = aNumTarjeta;
    }
    
    float version = [APIToken getInstance].appVersion;
    version = version*100;
    _objetoSofttoken.versionApp =[NSString stringWithFormat:@"%.0f",version];
    
}



+(BOOL)validaCampos:(id)viewController {
    
    BOOL isCamposOK = YES;
    
    if([viewController isKindOfClass:[IngresaDatosSTViewController class]]){ //IngresaDatos
        IngresaDatosSTViewController* ingresaDatos = viewController;
        
        NSString* msgError = @"";
        //if(!(ingresaDatos.txtNumCelular.text.length == TELEPHONE_NUMBER_LENGTH)){//linea comentada
        if(!(ingresaDatos.lblGetNumero.text.length == TELEPHONE_NUMBER_LENGTH)){ //LINEA NUEVA
            msgError = ST_INGRESADATOS_PHONENUMBER_ERROR;
            isCamposOK = NO;
            
        }else
            //if(!(ingresaDatos.txtNumTarjeta.text.length == CARD_NUMBER_LENGTH)){ //linea comentada
            if(!(ingresaDatos.txtTarjeta.text.length == CARD_NUMBER_LENGTH)){ //LINEA NUEVA
                msgError = ST_INGRESADATOS_CARDNUMBER_ERROR;
                isCamposOK = NO;
            } 
        [ingresaDatos showErrorMessage:msgError];
        
    } else if ([viewController isKindOfClass:[ConfirmacionSTViewController class]]) { // Confirmacion
        ConfirmacionSTViewController* confirmacionST = viewController;
        NSString* msgError = @"";
        if (!(confirmacionST.txtNIP.text.length == NIP_REFERENCE_LENGTH)) {
            msgError = ST_NIP_ERROR;
            isCamposOK = NO;
            
        } else if (!(confirmacionST.txtCVV.text.length == CVV_REFERENCE_LENGTH)) {
//            if(confirmacionST.tiposolicitud!=SUSTITUCION)
//            {
                msgError = ST_CVV_ERROR;
                isCamposOK = NO; //---- Transacción CVV
//            }else{
//                isCamposOK = YES;
//            }
            
        } else if (!confirmacionST.txtCveAccesoSeg.isHidden) {
            if(!(confirmacionST.txtCveAccesoSeg.text.length == CVE_AS_REFERENCE_LENGTH)){
                msgError = ST_CLAVEACESSO_SEGURO_ERROR;
                isCamposOK = NO;
            }
        }
        
        [confirmacionST showErrorMessage:msgError];
        
    } else if ([viewController isKindOfClass:[AutenticacionSTViewController class]]) {
        AutenticacionSTViewController* autenticacionST = viewController;
        NSString* msgError = @"";
        
        if (!(autenticacionST.txtNumeroTarjeta.text.length == CARD_NUMBER_LENGTH)){
            msgError = ST_INGRESADATOS_CARDNUMBER_ERROR;
            isCamposOK = NO;
            
        } else if (!(autenticacionST.txtNIP.text.length == NIP_REFERENCE_LENGTH)) {
            msgError = ST_NIP_ERROR;
            isCamposOK = NO;
            
        } else if (!(autenticacionST.txt3DigitosTarjeta.text.length == CVV_REFERENCE_LENGTH)) {
            msgError = ST_CVV_ERROR;
            isCamposOK = NO;
        }
        
        [autenticacionST showErrorMessage:msgError];
        //inicia modif:
    
    }else if ([viewController isKindOfClass:[claveActivacionViewController class]]) {
            claveActivacionViewController* viewActivacion = viewController;
            NSString* msgError = @"";
            if (!(viewActivacion.TextCodigo.text.length == CVE_ACTIVACION_ST_REFERENCE_LENGTH )) {
                msgError = ST_CLAVE_ACTIVACION_ERROR;
                isCamposOK = NO;
            }
            [viewActivacion showErrorMessage:msgError];
    }
    
//    linea comentada se comenta este bloque porque se va a reemplazar con el nuevo viewcontroller (AARIBA)
//    } else if ([viewController isKindOfClass:[ClaveActivacionSTViewController class]]) {
//        ClaveActivacionSTViewController* viewActivacion = viewController;
//        NSString* msgError = @"";
//        if (!(viewActivacion.txtCveActivacion.text.length == CVE_ACTIVACION_ST_REFERENCE_LENGTH )) {
//            msgError = ST_CLAVE_ACTIVACION_ERROR;
//            isCamposOK = NO;
//        }
//        [viewActivacion showErrorMessage:msgError];
//        
//    }
    
    return isCamposOK;
}

//linea comentada METODO ANTERIOR


//-(void)consultaAutenticacion:(NSString*)nip andOTP:(NSString*)otp onViewController:(ConfirmacionSTViewController*)viewController{
-(void)consultaAutenticacion:(NSString*)nip andCVV:cvv andOTP:(NSString*)otp onViewController:(BaseViewController*)viewController {
    
    if ([viewController isKindOfClass:[ConfirmacionSTViewController class]]) {
        NSLog(@"Autenticar token con tarjeta desde ConfirmacionSTViewController");
        _tipoOperacion=2;
        
    } else if ([viewController isKindOfClass:[AutenticacionSTViewController class]]) {
        NSLog(@"Autenticar token con tarjeta desde AutenticacionSTViewController");
      
        _tipoOperacion=3;
        
        _objetoSofttoken.tipoSolicitud = INDICADOR_SOLICITUD_TOKEN_NUEVO;
        _objetoSofttoken.nombreCliente = [[APIToken getInstance].apiTokenDataSource nombreCliente];
        _objetoSofttoken.versionApp = [NSString stringWithFormat:@"%.0f",[APIToken getInstance].appVersion * 100];
    }
    
    NSString* numTarjeta = _objetoSofttoken.numTarjeta;
    NSString* numCelular = _objetoSofttoken.numCelular;
    NSString* numCliente = _objetoSofttoken.numeroCliente;
    NSString* compania = _objetoSofttoken.companiaCelular;
    NSString* tSolicitud = _objetoSofttoken.tipoSolicitud;
    NSString* email = _objetoSofttoken.correo;
    NSString* nombre = _objetoSofttoken.nombreCliente;
    NSString* versionApp = _objetoSofttoken.versionApp;
    
    NSString* temporalSTFilePath = [ToolsAPIToken getTemporalSTFilePath];
    NSMutableDictionary* temporalSTDictionary = [NSMutableDictionary dictionaryWithCapacity: 3];
    
    [temporalSTDictionary setObject:numCelular forKey:TEMPORALST_NUM_CELULAR];
    [temporalSTDictionary setObject:numTarjeta forKey:TEMPORALST_NUM_TARJETA];
    [temporalSTDictionary setObject:compania forKey:TEMPORALST_COMPANIA_CELULAR];
    
    [temporalSTDictionary writeToFile: temporalSTFilePath atomically: YES];
    
    NSInteger downloadId = [[ServerToken getInstance] autenticarTokenConTarjeta:numTarjeta aOTP:otp numCelular:numCelular numCliente:numCliente NIP:nip conCVV:cvv companiaTel:compania tSolicitud:tSolicitud email:email nombre:nombre vApp:versionApp forClient:self];
    
    if (downloadId > 0) {
        [viewController startActivityIndicator];
    } else {
        [viewController showCommunicationErrorAlert];
    }
    
}


//linea comentada METODO NUEVO

-(void)consultaAutenticacionN:(NSString*)nip andCVV:cvv andOTP:(NSString*)otp onViewController:(BaseViewController*)viewController {
    
    if ([viewController isKindOfClass:[ConfirmacionSTViewController class]]) {
        NSLog(@"Autenticar token con tarjeta desde ConfirmacionSTViewController MN");
        _tipoOperacion=2;
        
    } else if ([viewController isKindOfClass:[AutenticacionSTViewController class]]) {
        NSLog(@"Autenticar token con tarjeta desde AutenticacionSTViewController MN");
        
        _tipoOperacion=3;
        
        _objetoSofttoken.tipoSolicitud = INDICADOR_SOLICITUD_TOKEN_NUEVO;
        _objetoSofttoken.nombreCliente = [[APIToken getInstance].apiTokenDataSource nombreCliente];
        _objetoSofttoken.versionApp = [NSString stringWithFormat:@"%.0f",[APIToken getInstance].appVersion * 100];
    }
    
    NSString* numTarjeta = _objetoSofttoken.numTarjeta;
    NSString* numCelular = _objetoSofttoken.numCelular;
    NSString* numCliente = _objetoSofttoken.numeroCliente;
   
    NSString* tSolicitud = _objetoSofttoken.tipoSolicitud;
    NSString* email = _objetoSofttoken.correo;
    NSString* nombre = _objetoSofttoken.nombreCliente;

    
    NSString* temporalSTFilePath = [ToolsAPIToken getTemporalSTFilePath];
    NSMutableDictionary* temporalSTDictionary = [NSMutableDictionary dictionaryWithCapacity: 2];
    
    [temporalSTDictionary setObject:numCelular forKey:TEMPORALST_NUM_CELULAR];
    [temporalSTDictionary setObject:numTarjeta forKey:TEMPORALST_NUM_TARJETA];
    
    [temporalSTDictionary writeToFile: temporalSTFilePath atomically: YES];
    
    NSInteger downloadId = [[ServerToken getInstance] autenticarTokenConTarjetaN:numTarjeta aOTP:otp numCelular:numCelular numCliente:numCliente NIP:nip conCVV:cvv tSolicitud:tSolicitud email:email nombre:nombre forClient:self];
    
    if (downloadId > 0) {
        [viewController startActivityIndicator];
    } else {
        [viewController showCommunicationErrorAlert];
    }
    
}


-(void)showPantallaConfirmacionST:(TipoSolicitudResult *)tipoSolicitud {
    
    [[APIToken getInstance].apiTokenDelegate persistTipoSolicitud:tipoSolicitud];
    
    _tipoInstrumento = tipoSolicitud.tipoInstrumento;
    
   // if (_controladorIngresaDatosST != nil) {
        // Flujo normal de Token Móvil
        [[APIToken getInstance].softApp.controladorSofttoken showConfirmacionST:_objetoSofttoken];
 /*   } else {
        //Vengo de Contratación
        
        [[APIToken getInstance].softApp.controladorSofttoken showConfirmacionST:_objetoSofttoken];
//        _appdelegate.controladorSuite.softApp.controladorSofttoken
//        [_viewController.appDelegate showErrorMessage:msg];
    }*/
    
}

-(void)analyzeServerResponseConsultaTarjetaST:(TipoSolicitudResult *)tipoSolicitud conCliente:(id<ServerClient>)cliente {
    //Revisar también esto...
    [[APIToken getInstance].apiTokenDelegate persistTipoSolicitud:tipoSolicitud];
    
    _objetoSofttoken.correo = tipoSolicitud.correoElectronico;
    _objetoSofttoken.numeroCliente = tipoSolicitud.numeroCliente;
    _objetoSofttoken.tipoSolicitud = tipoSolicitud.indicadorContratacion;
    _objetoSofttoken.nombreCliente = tipoSolicitud.nombreCliente;
    
    //Modificación Activación Softtoken, RN5
    _objetoSofttoken.estatusValidacion = tipoSolicitud.validacionAlertas;
    _objetoSofttoken.numAlertas = tipoSolicitud.numeroAlertas;
    _objetoSofttoken.companiaAlertas = tipoSolicitud.companiaAlertas;
    
    //Modificación Activación Softtoken, EA#9
    _objetoSofttoken.switchEnrolamiento = tipoSolicitud.switchEnrolamiento;
    
    if ([@"SI" isEqualToString:tipoSolicitud.dispositivoFisico] &&
        [INDICADOR_SOLICITUD_NO_EXISTE_SOLICITUD isEqualToString:tipoSolicitud.indicadorContratacion] &&
        [@"A1" isEqualToString:tipoSolicitud.estatusDispositivo] ) {
        
        // Incidencia #21981 - Quitar el indicador de actividad de definición de contraseña
        [_viewController stopActivityIndicator];

        _tempTipoSolicitud = tipoSolicitud;
        tipoSolicitud.switchEnrolamiento=@"S";
        _objetoSofttoken.estatusValidacion=@"06";
        if([@"S" isEqualToString:tipoSolicitud.switchEnrolamiento]){
            if([ESTATUS_VALIDACION_EFECTUAR_SOLICITUD_SOFTTOKEN isEqualToString:_objetoSofttoken.estatusValidacion]){
                [self analizarRespuestaNoExisteSolicitud];
            }
        }else {
            [self analizarRespuestaNoExisteSolicitud];
        }
        
    } else {
        [self analizarRespuestaConsultaTarjetaST:tipoSolicitud conCliente:cliente];
    }
}

-(void)analizarRespuestaConsultaTarjetaST:(TipoSolicitudResult *)tipoSolicitud conCliente:(id<ServerClient>)cliente {
    
    if (([INDICADOR_SOLICITUD_REACTIVACION isEqualToString:_objetoSofttoken.tipoSolicitud]) || ([INDICADOR_SOLICITUD_TOKEN_NUEVO isEqualToString:_objetoSofttoken.tipoSolicitud])) {
        
        if ([INDICADOR_SOLICITUD_REACTIVACION isEqualToString:_objetoSofttoken.tipoSolicitud] && ( [tipoSolicitud.estatusBmovil isEqualToString:ESTATUS_CN]||[tipoSolicitud.estatusBmovil isEqualToString:ESTATUS_C4])) {
            //El sistema ejecuta el caso de uso Solicitud softtoken EA#2 paso 6
            
            NSString* estatusFilePath = [ToolsAPIToken getAppStatusStorageFilePath];
            NSMutableDictionary* estatusDictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:estatusFilePath];
            [estatusDictionary setObject:[NSNumber numberWithBool:NO] forKey:BMOVIL_ESTATUS_KEY];
            [estatusDictionary writeToFile: estatusFilePath atomically: YES];
            
            [[APIToken getInstance].apiTokenDelegate resetSession];
            
            [[APIToken getInstance].apiTokenDelegate storeSession];
            
            [[APIToken getInstance].apiTokenDelegate setApplicationActivated:NO];
            
            [_viewController startActivityIndicator];
            
            [[ServerAplicacionDesactivada getInstance] consultaEstatusMantenimientoConNumeroTelefonico:_objetoSofttoken.numCelular
                                                                            conVersionCatalogoTelefono:@"0"
                                                                       conVersionCatalogoAutenticacion:@"0"
                                                                                 conVersionAppConsulta:[APIToken getInstance].appVersionConsulta
                                                                                        conIsEncriptar:[ServerTools isEncriptar]
                                                                                           paraCliente:cliente];
        } else {
            [_viewController stopActivityIndicator];
            [self showPantallaConfirmacionST:tipoSolicitud];
        }
    } else if ([INDICADOR_SOLICITUD_SUSTITUCION isEqualToString:_objetoSofttoken.tipoSolicitud]) {
        
        [_viewController stopActivityIndicator];
        [self showPantallaConfirmacionST:tipoSolicitud];
        
    } else if ([INDICADOR_SOLICITUD_NO_EXISTE_SOLICITUD isEqualToString:_objetoSofttoken.tipoSolicitud]) {
        
        if ([@"S" isEqualToString:_objetoSofttoken.switchEnrolamiento]) {
            // EA#4: Ejecutar Solicitud Softtoken
            NSString* banderasBMovilFilePath = [ToolsAPIToken getBanderasBMovilFilePath];
            NSDictionary* banderasBMovilDictionary = [NSDictionary dictionaryWithContentsOfFile: banderasBMovilFilePath];
            
            //Solicitud Softoken, Escenario Principal
            if (![[banderasBMovilDictionary objectForKey:CONTRATAR_BMOVIL_KEY] boolValue]) {
                [self analizarRespuestaNoExisteSolicitud];
            } else {
                // Solicitud Softtoken, EA#1
                _objetoSofttoken.tipoSolicitud = INDICADOR_SOLICITUD_TOKEN_NUEVO;
                [self invokeSolicitudST: self];
            }
            
        } else {
            [_viewController stopActivityIndicator];
            
            // EA#10
            NSString* banderasBMovilFilePath = [ToolsAPIToken getBanderasBMovilFilePath];
            NSMutableDictionary* banderasBMovilDictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:banderasBMovilFilePath];
            [banderasBMovilDictionary setObject:[NSNumber numberWithBool:NO] forKey:CONTRATAR_BMOVIL_KEY];
            [banderasBMovilDictionary writeToFile:banderasBMovilFilePath atomically:YES];
            
            NSLog(@"banderasBMovilDictionary%@",banderasBMovilDictionary);
            
            NSString* temporalSTFilePath = [ToolsAPIToken getTemporalSTFilePath];
            [[NSFileManager defaultManager] removeItemAtPath: temporalSTFilePath error: NULL];
            
            NSString* msg = ST_TEXTO_SWITCH_ENROLAMIENTO_APAGADO;
            [self mostrarMensaje:msg];
            
        }
    }
}

-(void)analizarRespuestaNoExisteSolicitud {
    
    //06
    if ([ESTATUS_VALIDACION_EFECTUAR_SOLICITUD_SOFTTOKEN isEqualToString:_objetoSofttoken.estatusValidacion]) {
        // Solicitud Softtoken, escenario principal, paso 6

        if([@"SI" isEqualToString:_tempTipoSolicitud.dispositivoFisico]&&[@"A1" isEqualToString:_tempTipoSolicitud.estatusDispositivo]){
            _objetoSofttoken.tipoSolicitud=INDICADOR_SOLICITUD_SUSTITUCION;
            
        }else{
            _objetoSofttoken.tipoSolicitud = INDICADOR_SOLICITUD_TOKEN_NUEVO;
        }
        [self invokeSolicitudSofttoken];
        //00
    } else if ([ESTATUS_VALIDACION_BMOVIL_CONTRATADO isEqualToString:_objetoSofttoken.estatusValidacion]) {
        // Solicitud Softtoken, EA#2
        [_viewController stopActivityIndicator];
        
        NSString* msg = ST_TEXTO_VALIDACION_ALERTAS_00;
        
        UIAlertView *alerta = [[UIAlertView alloc]initWithTitle: AVISO
                                                        message: msg
                                                       delegate: self
                                              cancelButtonTitle: nil
                                              otherButtonTitles: COMMON_ACCEPT, COMMON_CANCEL, nil];
        
        [alerta show];
    //01
    } else if ([ESTATUS_VALIDACION_SIN_ALERTAS_CONTRATADAS isEqualToString:_objetoSofttoken.estatusValidacion]) {
        // Solicitud Softtoken, EA#5
        [_viewController stopActivityIndicator];
        
        NSString* msg = ST_TEXTO_VALIDACION_ALERTAS_01;
        
        [self mostrarMensaje:msg];
    //02,03
    } else if (([ESTATUS_VALIDACION_TELEFONO_INGRESADO_NO_COINCIDE isEqualToString:_objetoSofttoken.estatusValidacion])
               || ([ESTATUS_VALIDACION_COMPANIA_INGRESADA_NO_COINCIDE isEqualToString:_objetoSofttoken.estatusValidacion])) {
        // Solicitud Softtoken, EA#6 y EA#7
        [_viewController stopActivityIndicator];
        
        NSString* msg = [NSString stringWithFormat:ST_TEXTO_VALIDACION_ALERTAS_02_03,_objetoSofttoken.numAlertas];
        
        [self mostrarMensaje:msg];
    //04
    } else if ([ESTATUS_VALIDACION_TELEFONO_ASOCIADO_NO_COINCIDE isEqualToString:_objetoSofttoken.estatusValidacion]) {
        // Solicitud Softtoken, EA#8
        [_viewController stopActivityIndicator];
        
        //Resolución incidencia #21963
        NSString* msg = ST_TEXTO_VALIDACION_ALERTAS_04;
        
        UIAlertView *alerta = [[UIAlertView alloc]initWithTitle: AVISO
                                                        message: msg
                                                       delegate: self
                                              cancelButtonTitle: nil
                                              otherButtonTitles: COMMON_ACCEPT, COMMON_CANCEL, nil];
        [alerta show];
    //05
    } else if ([ESTATUS_VALIDACION_COMPANIA_ASOCIADA_NO_COINCIDE isEqualToString:_objetoSofttoken.estatusValidacion]) {
        // Solicitud Softtoken, EA#9
        [_viewController stopActivityIndicator];
        
        NSString* msg = ST_TEXTO_VALIDACION_ALERTAS_05;
        
        UIAlertView *alerta = [[UIAlertView alloc]initWithTitle: AVISO
                                                        message: msg
                                                       delegate: self
                                              cancelButtonTitle: nil
                                              otherButtonTitles: COMMON_ACCEPT, COMMON_CANCEL, nil];
        [alerta show];
        
    }
}

//LINEA COMENTADA METODO ANTERIOR
-(void)analyzeServerResponse:(ServerResponse *)aServerResponse {
    
    //Consulta tipo solicitud
    if([aServerResponse.responseHandler isKindOfClass:[TipoSolicitudResult class]]) {
        
        if (aServerResponse.status == OPERATION_SUCCESSFUL) {
            
            [self analyzeServerResponseConsultaTarjetaST: (TipoSolicitudResult*) aServerResponse.responseHandler conCliente:self];
            
        } else if (aServerResponse.status == OPERATION_ERROR) {
            if ([aServerResponse.messageCode isEqualToString:CODIGO_ERROR_CNE1506]) {
                // P026 Activación Softtoken, EA#16
                UIAlertView *alert = [[UIAlertView alloc] init];
                
                alert.tag = TAG_ALERTA_CAMBIO_NUMERO_COMPANIA_DESDE_CODIGO_ERROR_CNE1506;
                [[alert initWithTitle:AVISO
                              message: TEXTO_CAMBIO_NUMERO_COMPANIA
                             delegate:self
                    cancelButtonTitle:NO_KEY
                    otherButtonTitles:SI, nil]
                 show];
            }
        }
        
    } else if([aServerResponse.responseHandler isKindOfClass:[AutenticacionTokenResult class]]) {
        [_viewController stopActivityIndicator];
        AutenticacionTokenResult *responseHandler = aServerResponse.responseHandler;
        
        _objetoSofttoken.nombreToken = responseHandler.nombreToken;
        _objetoSofttoken.numeroSerie = responseHandler.numeroSerie;
        
        if(responseHandler.ivr) {
            
            [ContratacionSTDelegate guardarEstatusIVR:YES];
           
           NSString* banderasBMovilFilePath = [ToolsAPIToken getBanderasBMovilFilePath];
           NSMutableDictionary* banderasBMovilDictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:banderasBMovilFilePath];
           NSLog(@"banderasBMovilDictionary%@",banderasBMovilDictionary);
           
           if ([[banderasBMovilDictionary objectForKey:CONTRATAR_BMOVIL_KEY] boolValue]) {
               [banderasBMovilDictionary setObject:[NSNumber numberWithBool:YES] forKey:CONTRATACION_2X1];
               [banderasBMovilDictionary writeToFile:banderasBMovilFilePath atomically:YES];
               
               [self creaArchivoPendienteDescarga]; //ANTERIOR
               
              
               
               [[APIToken getInstance] guardarEnKeyChain:[APIToken getInstance].appName clave:KEYCHAIN_CENTER_KEY];
               
               [_viewController showErrorMessage:SMS_ALERT_TEXT];
               [[APIToken getInstance].softApp.controladorSofttoken showClaveActivacionST];
           } else {
            
               NSString* contratacionBT = [[APIToken getInstance] leerDeKeyChain:KEYCHAIN_CONTRATACION_BT_KEY];
               
               if(contratacionBT==nil || ![contratacionBT isEqualToString:@""]) {
                    [self creaArchivoPendienteDescarga]; //ANTERIOR
                    [[APIToken getInstance] guardarEnKeyChain:[APIToken getInstance].appName clave:KEYCHAIN_CENTER_KEY];
                   //se cambio el mensaje de la alerta
                   sp_AlertViewEnvio = [[AlertaEnvio alloc] initWithTitle:AVISO andImageName:@"Ios_Im_godinbmovil" andMessage:@"En breve recibirás un mensaje con el código de activación para comenzar a utilizar tu servicio móvil." closable:NO];
                   ////el registro de la aplicación Token Móvil.
                   //sp_AlertView.backgroundColor = [UIColor blackColor];
                   //sp_AlertView.alpha = .5f;
                   //@"Ios_Im_godinbmovil"
                    sp_AlertViewEnvio.tag = TAG_ALERTA_SOLICITAR_COD_ACTIVACION;
                   [sp_AlertViewEnvio setDelegate:self];
                   [sp_AlertViewEnvio setColorTitulo:COLOR_1ER_AZUL];
                   [sp_AlertViewEnvio show];
                   [[APIToken getInstance].softApp.controladorSofttoken showClaveActivacionST];
               }
           }
            
        } else {
            
            [ContratacionSTDelegate guardarEstatusIVR:NO];
            
           NSString* banderasBMovilFilePath = [ToolsAPIToken getBanderasBMovilFilePath];
            NSMutableDictionary* banderasBMovilDictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:banderasBMovilFilePath];
            NSLog(@"banderasBMovilDictionary%@",banderasBMovilDictionary);
        
            if ([[banderasBMovilDictionary objectForKey:CONTRATAR_BMOVIL_KEY] boolValue]) {
                // Activación Softtoken, EA#11
                [banderasBMovilDictionary setObject:[NSNumber numberWithBool:YES] forKey:CONTRATACION_2X1];
                [banderasBMovilDictionary writeToFile:banderasBMovilFilePath atomically:YES];
            }
        
            [self creaArchivoPendienteDescarga]; //ANTERIOR
        
            
            UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle: AVISO message:SMS_ALERT_TEXT delegate:self cancelButtonTitle: OK_KEY otherButtonTitles: nil];
            errorAlert.tag = TAG_ALERTA_IVR_FALSE;
            [errorAlert show];
        }
        
    } else if ([aServerResponse.responseHandler isKindOfClass:[SincronizaTokenResult class]]) {
        [_viewController stopActivityIndicator];
        [self exportaToken];
        
    } else if ([aServerResponse.responseHandler isKindOfClass:[ExportacionTokenResult class]]) {
        [ContratacionSTDelegate guardaEstatusTokenMovil:YES];
        
        NSString* banderasBMovilFilePath = [ToolsAPIToken getBanderasBMovilFilePath];
        NSMutableDictionary* banderasBMovilDictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:banderasBMovilFilePath];
        NSLog(@"banderasBMovilDictionary%@",banderasBMovilDictionary);
        
        _es2x1 = [[banderasBMovilDictionary objectForKey:CONTRATACION_2X1] boolValue];
        if (_es2x1) {
            [banderasBMovilDictionary setObject:[NSNumber numberWithBool:NO] forKey:CONTRATACION_2X1];
            [banderasBMovilDictionary writeToFile:banderasBMovilFilePath atomically:YES];
        }
        
        if (![[banderasBMovilDictionary objectForKey:CONTRATAR_BMOVIL_KEY] boolValue]) {
            if (![[banderasBMovilDictionary objectForKey:CAMBIO_CELULAR_KEY] boolValue]) {
                // Mostrar pantalla emailST
                [_viewController stopActivityIndicator];
                [self mostrarPantallaEmailST];
            } else {
                // EA#6
                [self invokeCambioTelefonoAsociado];
            }
            
        } else {
            // EA#5
            NSString* temporalSTFilePath = [ToolsAPIToken getTemporalSTFilePath];
            NSDictionary* temporalSTDictionary = [NSDictionary dictionaryWithContentsOfFile: temporalSTFilePath];
           NSLog(@"temporalSTDictionary%@",temporalSTDictionary);
            
            //Se genera el IUM para BMOVIL
            double activationTime = [[NSDate date] timeIntervalSince1970];
            [[APIToken getInstance].apiTokenDelegate setIUMWithUser: [temporalSTDictionary objectForKey:TEMPORALST_NUM_CELULAR] andActivationTime: activationTime];
            
           
            NSLog(@"IUM = %@",[[APIToken getInstance].apiTokenDataSource getIUM]);
            
            NSInteger downloadId;
            
            //ClaveActivacionSTViewController tipo 4
            _tipoOperacion=4;
                
            
            downloadId = [[ServerToken getInstance] finalizaContratacionST:[temporalSTDictionary objectForKey:TEMPORALST_NUM_TARJETA]
                                                    conNumeroTelefono:[temporalSTDictionary objectForKey:TEMPORALST_NUM_CELULAR]
                                                               conIum:[[APIToken getInstance].apiTokenDataSource getIUM]
                                                   conCompaniaCelular:[temporalSTDictionary objectForKey:TEMPORALST_COMPANIA_CELULAR]
                                                         conCodigoOPT:[GeneraOTPSTDelegate generaOTPTiempo]
                                                          paraCliente:self];
            
             //client:_claveActivacionSTViewController
            
            
            if (downloadId > 0) {
                [_viewController startActivityIndicator];
            } else {
                [_viewController showCommunicationErrorAlert];
            }
        }
        
    } else if ([aServerResponse.responseHandler isKindOfClass:[CambioTelefonoAsociadoSTResult class]]) {
        
        NSString* banderasBMovilFilePath = [ToolsAPIToken getBanderasBMovilFilePath];
        
        NSMutableDictionary* banderasDictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:banderasBMovilFilePath];
        
        [banderasDictionary setObject:[NSNumber numberWithBool:NO] forKey:CAMBIO_CELULAR_KEY];
        [banderasDictionary writeToFile: banderasBMovilFilePath atomically: YES];
        
        // Modificación TemporalCambioTelefono
        NSString* temporalCambioTelefonoFilePath = [ToolsAPIToken getTemporalCambioTelefonoFilePath];
        [[NSFileManager defaultManager] removeItemAtPath: temporalCambioTelefonoFilePath error: NULL];
        
        if ([[APIToken getInstance].apiTokenDataSource isAppActivated]) {
            //RN33: bmovil=false
            NSString* estatusFilePath = [ToolsAPIToken getAppStatusStorageFilePath];
            NSMutableDictionary* estatusDictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:estatusFilePath];
            [estatusDictionary setObject:[NSNumber numberWithBool:NO] forKey:BMOVIL_ESTATUS_KEY];
            [estatusDictionary writeToFile: estatusFilePath atomically: YES];
            
            //RN33: borrar datos de DatosBMovil y borrar catálogos
           
            [[APIToken getInstance].apiTokenDelegate resetSession];
            [[APIToken getInstance].apiTokenDelegate storeSession];
            [[APIToken getInstance].apiTokenDelegate setApplicationActivated:NO];
        }
        
        //Paso 40 del escenario principal
        [_viewController stopActivityIndicator];
        [self mostrarPantallaEmailST];
        
    } else if ([aServerResponse.responseHandler isKindOfClass:[SolicitudSTResult class]]) {
        [_viewController stopActivityIndicator];
        [self showPantallaConfirmacionST:[[APIToken getInstance].apiTokenDataSource tipoSolicitud]];
        
    } else if ([aServerResponse.responseHandler isKindOfClass:[ConsultaEstatus class]]) {
        
        ConsultaEstatus *consultaEstatus = (ConsultaEstatus*)aServerResponse.responseHandler;
        consultaEstatus.numeroCelular = _objetoSofttoken.numCelular;
        consultaEstatus.numTarjeta = _objetoSofttoken.numTarjeta;
        consultaEstatus.companiaCelular = _objetoSofttoken.companiaCelular;
                
        if(isActivacionNuevoToken){
            isActivacionNuevoToken = NO;
            [self sincronizaToken];
            
           if(([consultaEstatus.estatusServicio isEqualToString:ESTATUS_A1] || [consultaEstatus.estatusServicio isEqualToString:ESTATUS_PA] || [consultaEstatus.estatusServicio isEqualToString:ESTATUS_PE]) && [consultaEstatus.tipoInstrumento isEqualToString:TIPO_INSTRUMENTO_SOFTOKEN]){
               
               direcionarreactivar2x1=true;
               
                // EA#12 Reactivacion 2X1
            } else{
                 direcionarreactivar2x1=false;
            }
            
           
        } else {
        
            
            // Solicitud Softtoken, RN16
            consultaEstatus.perfilCliente = avanzado;
            consultaEstatus.perfilAST = PERFIL_AVANZADO;
        
            if (([consultaEstatus.reglasDeAutenticacion.operacionesRecortado count]
                 + [consultaEstatus.reglasDeAutenticacion.operacionesBasico count]
                 + [consultaEstatus.reglasDeAutenticacion.operacionesAvanzado count]) >0) {
                    [[APIToken getInstance].apiTokenDelegate setReglasDeAutenticacion:[consultaEstatus.reglasDeAutenticacion copy]];
            }
        
            [_controladorIngresaDatosST startActivityIndicator];
        
            [self consultaTarjetaContratacionEConConsultaEstatus:consultaEstatus];
        }
        
    } else if([aServerResponse.responseHandler isKindOfClass:[SincronizacionExportacionTokenResult class]]) {
       
        if (aServerResponse.status == OPERATION_SUCCESSFUL) { // EA#12
            SincronizacionExportacionTokenResult* sincronizacionExportacionToken = (SincronizacionExportacionTokenResult*)aServerResponse.responseHandler;
        
            if ([sincronizacionExportacionToken.indReac2x1 isEqualToString:@"S"]) {
                [ self finalizaReactivacion2x1 ];
            } else {
                NSLog(@"indReac2x1 != S");
            }
        } else if ((aServerResponse.status == OPERATION_WARNING) || (aServerResponse.status == OPERATION_ERROR)) { // EA#15
            NSLog(@"ERROR EN LA PETICIÓN DE SINCRONIZACIÓN Y EXPORTACIÓN");
            
            [ self rollbackMovil ];
        }
    } else if ([aServerResponse.responseHandler isKindOfClass:[FinalizarContratacionSTResult class]]) {
        
        // Modificación TemporalCambioTelefono
        NSString* temporalSTFilePath = [ToolsAPIToken getTemporalSTFilePath];
        [[NSFileManager defaultManager] removeItemAtPath: temporalSTFilePath error: NULL];
        
        [self guardarEstatusBMovil:YES];
        
        // Guardar en KeyChain las variables número y seed:
        NSString* userName = [[APIToken getInstance].apiTokenDataSource userName];
        double activationTime = [[APIToken getInstance].apiTokenDataSource activationTime];
        [[APIToken getInstance] guardarEnKeyChain:userName clave:KEYCHAIN_TELEPHONE_KEY];
        [[APIToken getInstance] guardarEnKeyChain:[[NSNumber numberWithDouble: activationTime] stringValue] clave:KEYCHAIN_SEED_KEY];
        [[APIToken getInstance] guardarEnKeyChain:[APIToken getInstance].appName clave:KEYCHAIN_CENTER_KEY];
        
        NSString* banderasBMovilFilePath = [ToolsAPIToken getBanderasBMovilFilePath];
        NSMutableDictionary* banderasBMovilDictionary = [NSMutableDictionary dictionaryWithContentsOfFile: banderasBMovilFilePath];
        NSLog(@"banderasBMovilDictionary%@",banderasBMovilDictionary);
        
        // Resolución incidencia del 23/07/2014
        [banderasBMovilDictionary setObject:[NSNumber numberWithBool:NO] forKey:CONTRATAR_BMOVIL_KEY];
        [banderasBMovilDictionary writeToFile:banderasBMovilFilePath atomically:YES];
        
        if (![[banderasBMovilDictionary objectForKey:CAMBIO_CELULAR_KEY] boolValue]) {
            // Mostrar pantalla emailST
            _mensajeBienvenida = ST_MSG_ALERT_BIENVENIDA_BANCA_MOVIL_TOKEN_MOVIL;
            [_viewController stopActivityIndicator];
            [self mostrarPantallaEmailST];
        } else {
            // EA#6
            [self invokeCambioTelefonoAsociado];
        }
    }
}
//-----------------FIN METODO ANTERIOR------------------


//LINEA COMENTADA METODO NUEVO

-(void)analyzeServerResponseN:(ServerResponse *)aServerResponse {
    
    //Consulta tipo solicitud
    if([aServerResponse.responseHandler isKindOfClass:[TipoSolicitudResult class]]) {
        
        if (aServerResponse.status == OPERATION_SUCCESSFUL) {
            
            [self analyzeServerResponseConsultaTarjetaST: (TipoSolicitudResult*) aServerResponse.responseHandler conCliente:self];
            
        } else if (aServerResponse.status == OPERATION_ERROR) {
            if ([aServerResponse.messageCode isEqualToString:CODIGO_ERROR_CNE1506]) {
                // P026 Activación Softtoken, EA#16
                UIAlertView *alert = [[UIAlertView alloc] init];
                
                alert.tag = TAG_ALERTA_CAMBIO_NUMERO_COMPANIA_DESDE_CODIGO_ERROR_CNE1506;
                [[alert initWithTitle:AVISO
                              message: TEXTO_CAMBIO_NUMERO_COMPANIA
                             delegate:self
                    cancelButtonTitle:NO_KEY
                    otherButtonTitles:SI, nil]
                 show];
            }
        }
        
    } else if([aServerResponse.responseHandler isKindOfClass:[AutenticacionTokenResult class]]) {
        [_viewController stopActivityIndicator];
        AutenticacionTokenResult *responseHandler = aServerResponse.responseHandler;
        
        _objetoSofttoken.nombreToken = responseHandler.nombreToken;
        _objetoSofttoken.numeroSerie = responseHandler.numeroSerie;
        
        if(responseHandler.ivr) {
            
            [ContratacionSTDelegate guardarEstatusIVR:YES];
            
            NSString* banderasBMovilFilePath = [ToolsAPIToken getBanderasBMovilFilePath];
            NSMutableDictionary* banderasBMovilDictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:banderasBMovilFilePath];
            NSLog(@"banderasBMovilDictionaryN%@",banderasBMovilDictionary);
            
            if ([[banderasBMovilDictionary objectForKey:CONTRATAR_BMOVIL_KEY] boolValue]) {
                [banderasBMovilDictionary setObject:[NSNumber numberWithBool:YES] forKey:CONTRATACION_2X1];
                [banderasBMovilDictionary writeToFile:banderasBMovilFilePath atomically:YES];
                
                [self creaArchivoPendienteDescargaN];
                
                
                
                [[APIToken getInstance] guardarEnKeyChain:[APIToken getInstance].appName clave:KEYCHAIN_CENTER_KEY];
                
                [_viewController showErrorMessage:SMS_ALERT_TEXT];
                [[APIToken getInstance].softApp.controladorSofttoken showClaveActivacionST];
            } else {
                
                NSString* contratacionBT = [[APIToken getInstance] leerDeKeyChain:KEYCHAIN_CONTRATACION_BT_KEY];
                
                if(contratacionBT==nil || ![contratacionBT isEqualToString:@""]) {
                    [self creaArchivoPendienteDescargaN];
                    [[APIToken getInstance] guardarEnKeyChain:[APIToken getInstance].appName clave:KEYCHAIN_CENTER_KEY];
                    //se cambio el mensaje de la alerta
                    sp_AlertViewEnvio = [[AlertaEnvio alloc] initWithTitle:AVISO andImageName:@"Ios_Im_godinbmovil" andMessage:@"En breve recibirás un mensaje con el código de activación para comenzar a utilizar tu servicio móvil." closable:NO];
                    ////el registro de la aplicación Token Móvil.
                    //sp_AlertView.backgroundColor = [UIColor blackColor];
                    //sp_AlertView.alpha = .5f;
                    //@"Ios_Im_godinbmovil"
                    sp_AlertViewEnvio.tag = TAG_ALERTA_SOLICITAR_COD_ACTIVACION;
                    [sp_AlertViewEnvio setDelegate:self];
                    [sp_AlertViewEnvio setColorTitulo:COLOR_1ER_AZUL];
                    [sp_AlertViewEnvio show];
                    [[APIToken getInstance].softApp.controladorSofttoken showClaveActivacionST];
                }
            }
            
        } else {
            
            [ContratacionSTDelegate guardarEstatusIVR:NO];
            
            NSString* banderasBMovilFilePath = [ToolsAPIToken getBanderasBMovilFilePath];
            NSMutableDictionary* banderasBMovilDictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:banderasBMovilFilePath];
            NSLog(@"banderasBMovilDictionary%@",banderasBMovilDictionary);
            
            if ([[banderasBMovilDictionary objectForKey:CONTRATAR_BMOVIL_KEY] boolValue]) {
                // Activación Softtoken, EA#11
                [banderasBMovilDictionary setObject:[NSNumber numberWithBool:YES] forKey:CONTRATACION_2X1];
                [banderasBMovilDictionary writeToFile:banderasBMovilFilePath atomically:YES];
            }
            
            [self creaArchivoPendienteDescargaN];
            
            
            UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle: AVISO message:SMS_ALERT_TEXT delegate:self cancelButtonTitle: OK_KEY otherButtonTitles: nil];
            errorAlert.tag = TAG_ALERTA_IVR_FALSE;
            [errorAlert show];
        }
        
    } else if ([aServerResponse.responseHandler isKindOfClass:[SincronizaTokenResult class]]) {
        [_viewController stopActivityIndicator];
        [self exportaToken];
        
    } else if ([aServerResponse.responseHandler isKindOfClass:[ExportacionTokenResult class]]) {
        [ContratacionSTDelegate guardaEstatusTokenMovil:YES];
        
        NSString* banderasBMovilFilePath = [ToolsAPIToken getBanderasBMovilFilePath];
        NSMutableDictionary* banderasBMovilDictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:banderasBMovilFilePath];
        NSLog(@"banderasBMovilDictionary MNServerResponseN%@",banderasBMovilDictionary);
        
        _es2x1 = [[banderasBMovilDictionary objectForKey:CONTRATACION_2X1] boolValue];
        if (_es2x1) {
            [banderasBMovilDictionary setObject:[NSNumber numberWithBool:NO] forKey:CONTRATACION_2X1];
            [banderasBMovilDictionary writeToFile:banderasBMovilFilePath atomically:YES];
        }
        
        if (![[banderasBMovilDictionary objectForKey:CONTRATAR_BMOVIL_KEY] boolValue]) {
            if (![[banderasBMovilDictionary objectForKey:CAMBIO_CELULAR_KEY] boolValue]) {
                // Mostrar pantalla emailST
                [_viewController stopActivityIndicator];
                [self mostrarPantallaEmailST];
            } else {
                // EA#6
                [self invokeCambioTelefonoAsociado];
            }
            
        } else {
            // EA#5
            NSString* temporalSTFilePath = [ToolsAPIToken getTemporalSTFilePath];
            NSDictionary* temporalSTDictionary = [NSDictionary dictionaryWithContentsOfFile: temporalSTFilePath];
            NSLog(@"temporalSTDictionary%@",temporalSTDictionary);
            
            //Se genera el IUM para BMOVIL
            double activationTime = [[NSDate date] timeIntervalSince1970];
            [[APIToken getInstance].apiTokenDelegate setIUMWithUser: [temporalSTDictionary objectForKey:TEMPORALST_NUM_CELULAR] andActivationTime: activationTime];
            
            
            NSLog(@"IUM = %@",[[APIToken getInstance].apiTokenDataSource getIUM]);
            
            NSInteger downloadId;
            
            //ClaveActivacionSTViewController tipo 4
            _tipoOperacion=4;
            
            
            downloadId = [[ServerToken getInstance] finalizaContratacionSTN:[temporalSTDictionary objectForKey:TEMPORALST_NUM_TARJETA]
                                                         conNumeroTelefono:[temporalSTDictionary objectForKey:TEMPORALST_NUM_CELULAR]
                                                                    conIum:[[APIToken getInstance].apiTokenDataSource getIUM]
                                                              conCodigoOPT:[GeneraOTPSTDelegate generaOTPTiempo]
                                                               paraCliente:self];
            
            //client:_claveActivacionSTViewController
            
            
            if (downloadId > 0) {
                [_viewController startActivityIndicator];
            } else {
                [_viewController showCommunicationErrorAlert];
            }
        }
        
    } else if ([aServerResponse.responseHandler isKindOfClass:[CambioTelefonoAsociadoSTResult class]]) {
        
        NSString* banderasBMovilFilePath = [ToolsAPIToken getBanderasBMovilFilePath];
        
        NSMutableDictionary* banderasDictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:banderasBMovilFilePath];
        
        [banderasDictionary setObject:[NSNumber numberWithBool:NO] forKey:CAMBIO_CELULAR_KEY];
        [banderasDictionary writeToFile: banderasBMovilFilePath atomically: YES];
        
        // Modificación TemporalCambioTelefono
        NSString* temporalCambioTelefonoFilePath = [ToolsAPIToken getTemporalCambioTelefonoFilePath];
        [[NSFileManager defaultManager] removeItemAtPath: temporalCambioTelefonoFilePath error: NULL];
        
        if ([[APIToken getInstance].apiTokenDataSource isAppActivated]) {
            //RN33: bmovil=false
            NSString* estatusFilePath = [ToolsAPIToken getAppStatusStorageFilePath];
            NSMutableDictionary* estatusDictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:estatusFilePath];
            [estatusDictionary setObject:[NSNumber numberWithBool:NO] forKey:BMOVIL_ESTATUS_KEY];
            [estatusDictionary writeToFile: estatusFilePath atomically: YES];
            
            //RN33: borrar datos de DatosBMovil y borrar catálogos
            
            [[APIToken getInstance].apiTokenDelegate resetSession];
            [[APIToken getInstance].apiTokenDelegate storeSession];
            [[APIToken getInstance].apiTokenDelegate setApplicationActivated:NO];
        }
        
        //Paso 40 del escenario principal
        [_viewController stopActivityIndicator];
        [self mostrarPantallaEmailST];
        
    } else if ([aServerResponse.responseHandler isKindOfClass:[SolicitudSTResult class]]) {
        [_viewController stopActivityIndicator];
        [self showPantallaConfirmacionST:[[APIToken getInstance].apiTokenDataSource tipoSolicitud]];

    } else if ([aServerResponse.responseHandler isKindOfClass:[ConsultaEstatus class]]) {
        
        ConsultaEstatus *consultaEstatus = (ConsultaEstatus*)aServerResponse.responseHandler;
        consultaEstatus.numeroCelular = _objetoSofttoken.numCelular;
        consultaEstatus.numTarjeta = _objetoSofttoken.numTarjeta;
        //consultaEstatus.companiaCelular = _objetoSofttoken.companiaCelular;
        
        if(isActivacionNuevoToken){
            isActivacionNuevoToken = NO;
            [self sincronizaToken];
            
            if(([consultaEstatus.estatusServicio isEqualToString:ESTATUS_A1] || [consultaEstatus.estatusServicio isEqualToString:ESTATUS_PA] || [consultaEstatus.estatusServicio isEqualToString:ESTATUS_PE]) && [consultaEstatus.tipoInstrumento isEqualToString:TIPO_INSTRUMENTO_SOFTOKEN]){
                
                direcionarreactivar2x1=true;
                
                // EA#12 Reactivacion 2X1
            } else{
                direcionarreactivar2x1=false;
            }
            
            
        } else {
            
            
            // Solicitud Softtoken, RN16
            consultaEstatus.perfilCliente = avanzado;
            consultaEstatus.perfilAST = PERFIL_AVANZADO;
            
            if (([consultaEstatus.reglasDeAutenticacion.operacionesRecortado count]
                 + [consultaEstatus.reglasDeAutenticacion.operacionesBasico count]
                 + [consultaEstatus.reglasDeAutenticacion.operacionesAvanzado count]) >0) {
                [[APIToken getInstance].apiTokenDelegate setReglasDeAutenticacion:[consultaEstatus.reglasDeAutenticacion copy]];
            }
            
            [_controladorIngresaDatosST startActivityIndicator];
            
            [self consultaTarjetaContratacionEConConsultaEstatus:consultaEstatus];
        }
        
    } else if([aServerResponse.responseHandler isKindOfClass:[SincronizacionExportacionTokenResult class]]) {
        
        if (aServerResponse.status == OPERATION_SUCCESSFUL) { // EA#12
            SincronizacionExportacionTokenResult* sincronizacionExportacionToken = (SincronizacionExportacionTokenResult*)aServerResponse.responseHandler;
            
            if ([sincronizacionExportacionToken.indReac2x1 isEqualToString:@"S"]) {
                [ self finalizaReactivacion2x1 ];
            } else {
                NSLog(@"indReac2x1 != S");
            }
        } else if ((aServerResponse.status == OPERATION_WARNING) || (aServerResponse.status == OPERATION_ERROR)) { // EA#15
            NSLog(@"ERROR EN LA PETICIÓN DE SINCRONIZACIÓN Y EXPORTACIÓN");
            
            [ self rollbackMovil ];
        }
    } else if ([aServerResponse.responseHandler isKindOfClass:[FinalizarContratacionSTResult class]]) {
        
        // Modificación TemporalCambioTelefono
        NSString* temporalSTFilePath = [ToolsAPIToken getTemporalSTFilePath];
        [[NSFileManager defaultManager] removeItemAtPath: temporalSTFilePath error: NULL];
        
        [self guardarEstatusBMovil:YES];
        
        // Guardar en KeyChain las variables número y seed:
        NSString* userName = [[APIToken getInstance].apiTokenDataSource userName];
        double activationTime = [[APIToken getInstance].apiTokenDataSource activationTime];
        [[APIToken getInstance] guardarEnKeyChain:userName clave:KEYCHAIN_TELEPHONE_KEY];
        [[APIToken getInstance] guardarEnKeyChain:[[NSNumber numberWithDouble: activationTime] stringValue] clave:KEYCHAIN_SEED_KEY];
        [[APIToken getInstance] guardarEnKeyChain:[APIToken getInstance].appName clave:KEYCHAIN_CENTER_KEY];
        
        NSString* banderasBMovilFilePath = [ToolsAPIToken getBanderasBMovilFilePath];
        NSMutableDictionary* banderasBMovilDictionary = [NSMutableDictionary dictionaryWithContentsOfFile: banderasBMovilFilePath];
        NSLog(@"banderasBMovilDictionary%@",banderasBMovilDictionary);
        
        // Resolución incidencia del 23/07/2014
        [banderasBMovilDictionary setObject:[NSNumber numberWithBool:NO] forKey:CONTRATAR_BMOVIL_KEY];
        [banderasBMovilDictionary writeToFile:banderasBMovilFilePath atomically:YES];
        
        if (![[banderasBMovilDictionary objectForKey:CAMBIO_CELULAR_KEY] boolValue]) {
            // Mostrar pantalla emailST
            _mensajeBienvenida = ST_MSG_ALERT_BIENVENIDA_BANCA_MOVIL_TOKEN_MOVIL;
            [_viewController stopActivityIndicator];
            [self mostrarPantallaEmailST];
        } else {
            // EA#6
            [self invokeCambioTelefonoAsociado];
        }
    }
}

//---------------------FIN METODO NUEVO-------------------

+ (BOOL) isIVR {
    return [[[NSMutableDictionary dictionaryWithContentsOfFile: [ToolsAPIToken getApplicationSessionFilePath]] objectForKey:IVR_KEY] boolValue];
}

+ (void) guardarEstatusIVR:(BOOL)estatusIVR {
    NSFileManager* fileManager = [NSFileManager defaultManager];
    NSMutableDictionary* dictionaryAppSession;
    NSString* appSessionFilePath = [ToolsAPIToken getApplicationSessionFilePath];
    
    if ([fileManager fileExistsAtPath: appSessionFilePath] == YES) {
        dictionaryAppSession = [NSMutableDictionary dictionaryWithContentsOfFile: appSessionFilePath];
    } else {
        dictionaryAppSession = [NSMutableDictionary dictionaryWithCapacity: 1];
    }
    
    [dictionaryAppSession setObject: [NSNumber numberWithBool:estatusIVR] forKey: IVR_KEY];
    [dictionaryAppSession writeToFile: appSessionFilePath atomically: YES];
}

-(void)consultaTarjetaContratacionEConConsultaEstatus:(ConsultaEstatus*)consultaEstatus
{
    //Pasa a la aplicación Bmovil y delega la ejucion de la petición al delegate de contratacion
    
    IngresaDatosViewController* controladorIngresaDatos = [[IngresaDatosViewController alloc] initWithNibName: INGRESA_DATOS_XIB bundle: nil];
    ContratacionDelegate* delegateContratacion = [[ContratacionDelegate alloc] init];
    
    delegateContratacion.controladorIngresaDatos = controladorIngresaDatos;
    delegateContratacion.objConsultaEstatus = consultaEstatus;
    
    controladorIngresaDatos.delegateContratacion = delegateContratacion;
    
    
    [ContratacionViewsController getInstance].controladorIngresaDatos = controladorIngresaDatos;
    [ContratacionViewsController getInstance].delegateContratacion = delegateContratacion;

    delegateContratacion.textoTarjeta = _objetoSofttoken.numTarjeta;
    delegateContratacion.nombreCompania = _objetoSofttoken.companiaCelular;
    
    if([delegateContratacion validaTarjeta]) {
        delegateContratacion.perfilDeterminado = @"MF03";
        [delegateContratacion doNetworkOperationType:1 conPassword:nil nip:nil token:nil cvv:nil numerosTarjeta:nil];
    }
}


-(void)continuarActivacion {
    
    double activationTime = [[NSDate date] timeIntervalSince1970];
    //NSString* userNameForActivation = [Session getInstance].userActivationST;  JRMM Modificacion PendienteDescarga
    NSString* userNameForActivation = _objetoSofttoken.numCelular;
    
     NSString* ium = [[APIToken getInstance].apiTokenDelegate computeIUMWithUser:userNameForActivation andActivationTime:activationTime];
    
    [[APIToken getInstance].apiTokenDelegate setIumST:ium];
    [self descargaToken];
}

-(void)mostrarPantallaEmailST {
    
    [[APIToken getInstance].softApp.controladorSofttoken showEmailST];
    
   /* if ((_confirmacionST) && (_confirmacionST.tiposolicitud != REACTIVACION)) {
        [[APIToken getInstance].softApp.controladorSofttoken showEmailST];
        
    } else if(_claveActivacionSTViewController) {
        [[APIToken getInstance].softApp.controladorSofttoken showEmailST];
        
    } else if (_autenticacionSTViewController) {
        [[APIToken getInstance].softApp.controladorSofttoken showEmailST];
    }*/
}

-(void)invokeSolicitudST:(BaseDelegate*) client {
    NSLog(@"SolicitudST con cliente genérico");
    
    _tipoOperacion = 3;
    
    NSInteger downloadId;
    downloadId= [[ServerToken getInstance] solicitudSTConNumeroTarjeta:_objetoSofttoken.numTarjeta andNumeroTelefono:_objetoSofttoken.numCelular andCompaniaCelular:_objetoSofttoken.companiaCelular andEmail:_objetoSofttoken.correo andNumeroCliente:_objetoSofttoken.numeroCliente forClient: client];
    
    if (downloadId > 0) {
        [_viewController startActivityIndicator];
    } else {
        [_viewController showCommunicationErrorAlert];
    }
}

-(void)invokeSolicitudSofttoken {
    NSLog(@"SolicitudST");
    
    _tipoOperacion = 3;
    
    NSInteger downloadId;
    downloadId= [[ServerToken getInstance] solicitudSTConNumeroTarjeta:_objetoSofttoken.numTarjeta andNumeroTelefono:_objetoSofttoken.numCelular andCompaniaCelular:_objetoSofttoken.companiaCelular andEmail:_objetoSofttoken.correo andNumeroCliente:_objetoSofttoken.numeroCliente forClient: self];
    
    if (downloadId > 0) {
        [_viewController startActivityIndicator];
    } else {
        [_viewController showCommunicationErrorAlert];
    }
}

-(void)invokeCambioTelefonoAsociado {
    NSLog(@"Cambiar Teléfono Asociado");
    
    NSInteger downloadId;
    
    //Modificación: archivo TemporalCambioTelefono
    NSString* tctFilePath = [ToolsAPIToken getTemporalCambioTelefonoFilePath];
    NSDictionary* tctDictionary = [NSDictionary dictionaryWithContentsOfFile: tctFilePath];
    
    downloadId= [[ServerToken getInstance] cambioTelefonoAsociado: _objetoSofttoken.numCelular
                                                  companiaCelular: _objetoSofttoken.companiaCelular
                                                        codigoOTP: [GeneraOTPSTDelegate generaOTPTiempo]
                                                    numeroCliente: _objetoSofttoken.numeroCliente
                                                    numeroTarjeta: [tctDictionary objectForKey:TARJETA_TEMPORAL_CAMBIO_TELEFONO_KEY]
                                                        forClient: self];
    
    if (downloadId > 0) {
        [_viewController startActivityIndicator];
    } else {
        [_viewController showCommunicationErrorAlert];
    }
}

-(void)exportaToken {
    NSLog(@"Exportar Token");
    NSInteger downloadId;
    
    //claveActivacionSTViewController es tipo 4
    _tipoOperacion=4;
    
    downloadId= [[ServerToken getInstance] exportaToken: _objetoSofttoken.numeroCliente
                                             numCelular: _objetoSofttoken.numCelular
                                             numTarjeta: _objetoSofttoken.numTarjeta
                                            nombreToken: _objetoSofttoken.nombreToken
                                             aIndicador: _objetoSofttoken.tipoSolicitud
                                            aVersionAPP: _objetoSofttoken.versionApp
                                              forClient: self];
    //client:claveActivacionSTViewController
    
    if (downloadId > 0) {
        [_viewController startActivityIndicator];
    } else {
        [_viewController showCommunicationErrorAlert];
    }
}

-(void)descargaToken{
    NSLog(@"descarga Token");
    [self inicializaCore];
    [_viewController startActivityIndicator];
    if(_core.state == MTAppCoreState_NOT_INITIALIZED){
        NSLog(@"No inicializado");
        NSString* password = [[APIToken getInstance].apiTokenDataSource getIumST];
        NSString* puk = [ToolsAPIToken createPUK];
        [_core initializeWithPassword: password andPUK: puk];
        
        id <MTSerialActivation> serialActivation;
        //Session* session = [Session getInstance];
        serialActivation = [MTCoreFactory createSerialActivation];
        
        NSString* serial = [[APIToken getInstance].apiTokenDataSource getAutenticacionToken].numeroSerie;
        NSString* cveActivacion = [[APIToken getInstance].apiTokenDataSource getCveReactivacionST];
        //NSString* serial = @"20000189";
        //NSString* cveActivacion = @"1111111111";
        
        if(serial == nil) {
            serial = _objetoSofttoken.numeroSerie;
        }

        
        if ([serialActivation setSerial: serial]) {
            if ([serialActivation setActivation: cveActivacion]) {
                deployer = [_core deployTokenForSerialActivation:serialActivation];
                if (deployer) {
                    [deployer startUsingDelegate: self];
                } else {
                    NSLog(@"No se inicializa descarga");
                    [_viewController stopActivityIndicator];
                }
            } else {
                [_viewController stopActivityIndicator];
                if (_claveActivacionViewController) {  //Checar este bloque, aqui probablemente aqui ira el remplazado
                    [_claveActivacionViewController stopActivityIndicator]; //linea comentada se modifico el viewcontroller 
                }
                NSLog(@"Activacion Invalida");
                
                //inicia modif
            
                sp_AlertViewInc = [[AlertaIncVC alloc] initWithTitle: AVISO  andImageName:@"Ios_Im_godinincorrecto" andMessage:@"Recuerda que al tercer intento tu servicio de Banca móvil será bloqueado." closable:NO];
              // @"Aceptar", nil];
                
                [sp_AlertViewInc setButtonTitles: [NSMutableArray arrayWithObjects:@"Intentar nuevamente",@"Generar nuevo código",nil]];
                
                sp_AlertViewInc.tag = TAG_ALERTA_COD_ACTIVACION_INCORRECTO;
                [sp_AlertViewInc setDelegate:self];
                [sp_AlertViewInc setColorTitulo:COLOR_1ER_AZUL];
            
                [sp_AlertViewInc show];
                //fin modif
                
            }
        } else {
            NSLog(@"Numero de serie invalido");
            [_viewController stopActivityIndicator];
        }
        
    }
    else if (_core.state == MTAppCoreState_BLOCKED){
        NSLog(@"MTAppCoreState_BLOCKED");
    }
    else if (_core.state == MTAppCoreState_DEAD){
        NSLog(@"MTAppCoreState_DEAD");
    }
    else if(_core.state == MTAppCoreState_READY){
    
       GeneraOTPSTDelegate *delegateOTP = [[GeneraOTPSTDelegate alloc]init];
        if ([MTCoreFactory resetDatabase: [delegateOTP dbPath]]) {
            [ContratacionSTDelegate guardaEstatusTokenMovil:NO];
            [self cierraCore];
            NSLog(@"Token borrado");
        }
        [self descargaToken];
        
    }
    
    
    
}

+ (NSString *) dbPath {
    //NSString* path = [NSString stringWithFormat:@"%@/Library/tokendb/%@",NSHomeDirectory(),[[UIDevice currentDevice] uniqueIdentifier]];
    NSString* path = [NSString stringWithFormat:@"%@/Library/tokendb/%@",NSHomeDirectory(),@"222222"];
    return path;
}

+(void)borrarToken {
    if ([MTCoreFactory resetDatabase: [self dbPath]]) {
        [self guardaEstatusTokenMovil:NO];
        NSLog(@"Token borrado");
    }
}

-(void)borraToken{
    
    if ([MTCoreFactory resetDatabase: [ContratacionSTDelegate dbPath]]) {
        [ContratacionSTDelegate guardaEstatusTokenMovil:NO];
        [self cierraCore];
        NSLog(@"Token borrado");
        [self borraPendienteDescarga];
    }
    [(UINavigationController *)([[UIApplication sharedApplication].delegate window].rootViewController) popToRootViewControllerAnimated:YES];

}

-(void)cierraCore{
    if(_core!= nil)
    {
        _core = nil;
    }
}

-(void)inicializaCore{
    
    NSString* dbPath  = [NSString stringWithFormat:@"%@/Library/tokendb/%@",NSHomeDirectory(),@"222222"];

    if(!_core)
    {
        // Set Legacy mode ON
#ifdef MTCoreFactory_API_VERSION // Se agrego esta línea
        [MTCoreFactory setLegacyMode:YES]; // Se agrego esta línea
#endif // Se agrego esta línea
        _core = [MTCoreFactory createCore: [[APIToken getInstance].apiTokenDataSource getSemillaST] usingDatabase: dbPath];
    }
    
    if ([[Server getInstance] getEnvironment] == 0) {
        [_core setDownloadURL: @"https://www.bancomermovil.com/mbank/mtbrokerext/AccountDownload.aspx"];
    } else {
        [_core setDownloadURL: @"https://www.bancomermovil.net:11443/mbank/mtbrokerext/AccountDownload.aspx"];
    }
}



// Deployment listener
- (void) deploymentStepCompleted: (MTDeploymentStep) step fromSource: (id) source {
    //[_confirmacionST stopActivityIndicator];
    //float prog;
    
    switch (step) {
        case MTDeploymentStep_STARTING:
            //prog = 0;
            break;
        case MTDeploymentStep_REGISTRATION:
            //prog = 0.20;
            break;
        case MTDeploymentStep_BROKER_HANDSHAKE:
            //prog = 0.40;
            break;
        case MTDeploymentStep_ENTERPRISE_REQUEST:
            //prog = 0.60;
            break;
        case MTDeploymentStep_ENTERPRISE_HANDSHAKE:
            //prog = 0.80;
            break;
        case MTDeploymentStep_ACCOUNT_REQUEST:
            //prog = 1;
            break;
        case MTDeploymentStep_COMPLETED:
            //prog = 1;
            if ([deployer commitDeployment]){
                
                NSLog(@"Un nuevo token se ha agregado");
               
                _core = nil;
                
                [_viewController startActivityIndicator];
                isActivacionNuevoToken = YES;
                
                //cliente:claveActivacionSTViewController tipo 4
                _tipoOperacion=4;
                
                [[ServerAplicacionDesactivada getInstance] consultaEstatusMantenimientoConNumeroTelefonico:_objetoSofttoken.numCelular
                                             conVersionCatalogoTelefono:@"0"
                                        conVersionCatalogoAutenticacion:@"0"
                                                  conVersionAppConsulta:[APIToken getInstance]. appVersionConsulta
                                                         conIsEncriptar:[ServerTools isEncriptar]
                                                            paraCliente:self];
             
                
               
            } else {
                NSLog( @"No es posible guardar el token en la base de adatos");
               
            }
            break;
    }
    
}


- (void) deploymentError: (NSError *) error fromSource: (id) source {
   NSLog(@"Activacion Invalida del servidor");
    if(_claveActivacionViewController) // linea comentada, se cambio _claveActivacionSTViewController
    {
        [_claveActivacionViewController stopActivityIndicator];  // linea comentada, se cambio _claveActivacionSTViewController
        [_claveActivacionViewController showErrorMessage:error.description];  // linea comentada, se cambio _claveActivacionSTViewController
    }
    else
    {
        [_viewController stopActivityIndicator];
        [_viewController showErrorMessage:error.description];
    }
    
}

- (void) deploymentCanceledFromSource: (id) source{

    
}


-(void)sincronizaToken{
    [_viewController startActivityIndicator];
    
    if (_confirmacionST) {
        [_confirmacionST.txtNIP resignFirstResponder];
    } else if (_autenticacionSTViewController != nil) {
        [_autenticacionSTViewController.txtNIP resignFirstResponder];
    }
    [self inicializaCore];
    if(_core.state == MTAppCoreState_READY)
    {
        if([_core usingPassword]){
            NSLog(@"%@", ([_core loginWithPassword:[[APIToken getInstance].apiTokenDataSource getIumST]]?@"ok":@"no"));
        }
        
        id <MTEnterpriseList> eList;
        id <MTAccountList> aList;
        //obtener empresas
        eList = [_core loadEnterpriseList];
        
        if (!eList.count>0) {
            NSLog(@"Lista de empresas vacia");
            
        }else {
            NSLog(@"Lista de empresas mayor a cero");
            
            aList = [_core loadAccountListForEnterprise: [eList enterpriseAt: 0]];
            if (!aList.count>0) {
                NSLog(@"Lista de tokens vacia");
                
            }else{
                id <MTAccount> a = [aList accountAt:0];
                NSLog(@"%@",[a name]);
                t1 =   [_core loadTokenForAccount: a];
                [t1 updateTimeOTP];
                
                timerObtenerToken = [NSTimer scheduledTimerWithTimeInterval:15 target:self selector:@selector(obtenerOtroToken) userInfo:nil repeats:NO];
            }
        }
        
    }
    
    else if (_core.state == MTAppCoreState_BLOCKED){
        NSLog(@"MTAppCoreState_BLOCKED");
    }
    else if (_core.state == MTAppCoreState_DEAD){
        NSLog(@"MTAppCoreState_DEAD");
    }
    else if (_core.state == MTAppCoreState_NOT_INITIALIZED){
        NSLog(@"MTAppCoreState_NOT_INITIALIZED");
        
    }
    
}

-(void)obtenerOtroToken{
    
    if (_confirmacionST) {
        [_confirmacionST.txtNIP resignFirstResponder];
    } else if (_autenticacionSTViewController != nil) {
        [_autenticacionSTViewController.txtNIP resignFirstResponder];
    }
    [self inicializaCore];
    if(_core.state == MTAppCoreState_READY)
    {
        if([_core usingPassword]){
            NSLog(@"%@", ([_core loginWithPassword:[[APIToken getInstance].apiTokenDataSource getIumST]]?@"ok":@"no"));
        }
        
        id <MTEnterpriseList> eList;
        id <MTAccountList> aList;
        //obtener empresas
        eList = [_core loadEnterpriseList];
        
        if (!eList.count>0) {
            NSLog(@"Lista de empresas vacia");
            
        }else {
            NSLog(@"Lista de empresas mayor a cero");
            
            aList = [_core loadAccountListForEnterprise: [eList enterpriseAt: 0]];
            
            if (!aList.count>0) {
               NSLog(@"Lista de tokens vacia");
                
            }else{
                id <MTAccount> a = [aList accountAt:0];
                NSLog(@"%@",[a name]);
                t2 =   [_core loadTokenForAccount: a];
                [t2 updateTimeOTP];
            }
        }
    }
    
    
    if ([t1.otp isEqualToString:t2.otp]) {
        NSLog(@"OTPs iguales (OTP1=%@, OTP2=%@), se inicia el proceso de generación de otro token", t1.otp, t2.otp);
        timerObtenerToken =  [NSTimer scheduledTimerWithTimeInterval: 8
                                                              target: self
                                                            selector: @selector(obtenerOtroToken)
                                                            userInfo: nil
                                                             repeats: NO];
        
        
    } else {
        NSLog(@"OTPs generadas: OTP1=%@, OTP2=%@", t1.otp, t2.otp);
        if(timerObtenerToken && timerObtenerToken.isValid){
            [timerObtenerToken invalidate];
        }
        
        if(direcionarreactivar2x1 ){
            [ self reactivar2x1 ];
        }else{
            [self finalizaSincronizacion:t1.otp aOTP2:t2.otp];
        }
    }
}



-(void)finalizaSincronizacion:(NSString*)otp1 aOTP2:(NSString*)otp2{
    NSLog(@"finalizaSincronizacion");

    NSInteger downloadId;
    if ((_confirmacionST) && (_confirmacionST.tiposolicitud != REACTIVACION)) {
       
        _tipoOperacion=2;
        downloadId= [[ServerToken getInstance] sincronizaToken: _objetoSofttoken.nombreToken
                                                         aOTP1: otp1
                                                         aOTP2: otp2
                                                     forClient: self]; //client:_confirmacionST
        
        if (downloadId > 0) {
            //[_confirmacionST startActivityIndicator];
        } else {
            [_confirmacionST showCommunicationErrorAlert];
        }
        
    } else if (_claveActivacionViewController) { // linea comentada, se cambio _claveActivacionSTViewController
        _tipoOperacion=4;
        downloadId= [[ServerToken getInstance] sincronizaToken: _objetoSofttoken.nombreToken
                                                         aOTP1: otp1
                                                         aOTP2: otp2
                                                     forClient: self];
                                                    //client:_claveActivacionSTViewController
        
        if (downloadId > 0) {
            //[_claveActivacionSTViewController startActivityIndicator];
        } else {
            [_claveActivacionViewController showCommunicationErrorAlert]; // linea comentada, se cambio _claveActivacionSTViewController
        }
        
    } else if (_autenticacionSTViewController) {
       
        _tipoOperacion=3;
        downloadId= [[ServerToken getInstance] sincronizaToken:_objetoSofttoken.nombreToken
                                                         aOTP1:otp1
                                                         aOTP2:otp2
                                                     forClient:self];
                                                    //client:_autenticacionSTViewController
        
        if (downloadId > 0) {
            //[_autenticacionSTViewController startActivityIndicator];
        } else {
            [_autenticacionSTViewController showCommunicationErrorAlert];
        }
    }
}

-(void)guardarEstatusBMovil:(BOOL)status {
    if (status) {
        NSLog(@"guardando bmovil activado");
    } else {
        NSLog(@"guardando bmovil desactivado");
    }
    
    NSString* estatusAplicacionesFilePath = [ToolsAPIToken getAppStatusStorageFilePath];
    
    NSMutableDictionary* estatusAplicacionesDictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:estatusAplicacionesFilePath];
    
    [estatusAplicacionesDictionary setObject:[NSNumber numberWithBool:status] forKey:BMOVIL_ESTATUS_KEY];
    [estatusAplicacionesDictionary writeToFile: estatusAplicacionesFilePath atomically: YES];
   
    [[APIToken getInstance].apiTokenDelegate setApplicationActivated:status];
    [[APIToken getInstance].apiTokenDelegate storeSession];
   
}

+(void)guardaEstatusTokenMovil:(BOOL)status {
    if (status) {
        NSLog(@"guardando token móvil activado");
    } else {
        NSLog(@"guardando token móvil desactivado");
    }
    
    NSString* estatusAplicacionesFilePath = [ToolsAPIToken getAppStatusStorageFilePath];
    
    NSMutableDictionary* estatusAplicacionesDictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:estatusAplicacionesFilePath];
    
    [estatusAplicacionesDictionary setObject:[NSNumber numberWithBool:status] forKey:SOFTOKEN_ESTATUS_KEY];
    [estatusAplicacionesDictionary writeToFile: estatusAplicacionesFilePath atomically: YES];
    
    [[APIToken getInstance].apiTokenDelegate storeSession];
}

-(BOOL)consultaArchivoPendienteDescarga {
    NSFileManager* fileManager = [NSFileManager defaultManager];
    NSString* pendienteDescargaFilePath = [ToolsAPIToken getPendienteDescargaFilePath];
    
    return [fileManager fileExistsAtPath: pendienteDescargaFilePath];
}


//METODO ANTERIOR
-(void)creaArchivoPendienteDescarga {
    
    NSString* pendienteDescargaFilePath = [ToolsAPIToken getPendienteDescargaFilePath];
    NSMutableDictionary* pendienteDescargaDictionary = [NSMutableDictionary dictionaryWithCapacity: 8];
    
    [pendienteDescargaDictionary setObject:_objetoSofttoken.numTarjeta forKey:PENDIENTE_DESCARGA_NUMERO_TARJETA_KEY];
    [pendienteDescargaDictionary setObject:_objetoSofttoken.numCelular forKey:PENDIENTE_DESCARGA_NUMERO_TELEFONO_KEY];
    [pendienteDescargaDictionary setObject:_objetoSofttoken.numeroCliente forKey:PENDIENTE_DESCARGA_NUMERO_CLIENTE_KEY];
    [pendienteDescargaDictionary setObject:_objetoSofttoken.tipoSolicitud forKey:PENDIENTE_DESCARGA_TIPO_SOLICITUD_KEY];
    [pendienteDescargaDictionary setObject:_objetoSofttoken.nombreToken forKey:PENDIENTE_DESCARGA_NOMBRE_TOKEN_KEY];
    [pendienteDescargaDictionary setObject:_objetoSofttoken.numeroSerie forKey:PENDIENTE_DESCARGA_NUMERO_SERIE_KEY];
    [pendienteDescargaDictionary setObject:_objetoSofttoken.correo forKey:PENDIENTE_DESCARGA_CORREO_KEY];
    [pendienteDescargaDictionary setObject:_objetoSofttoken.companiaCelular forKey:PENDIENTE_DESCARGA_COMPANIA_CELULAR_KEY];
    
    [pendienteDescargaDictionary writeToFile: pendienteDescargaFilePath atomically: YES];
    
    NSLog(@"Archivo pendienteDescarga creado");
}

//METODO NUEVO SOFTTOKEN
-(void)creaArchivoPendienteDescargaN {
    
    NSString* pendienteDescargaFilePath = [ToolsAPIToken getPendienteDescargaFilePath];
    NSMutableDictionary* pendienteDescargaDictionary = [NSMutableDictionary dictionaryWithCapacity: 8];
    
    [pendienteDescargaDictionary setObject:_objetoSofttoken.numTarjeta forKey:PENDIENTE_DESCARGA_NUMERO_TARJETA_KEY];
    [pendienteDescargaDictionary setObject:_objetoSofttoken.numCelular forKey:PENDIENTE_DESCARGA_NUMERO_TELEFONO_KEY];
    [pendienteDescargaDictionary setObject:_objetoSofttoken.numeroCliente forKey:PENDIENTE_DESCARGA_NUMERO_CLIENTE_KEY];
    [pendienteDescargaDictionary setObject:_objetoSofttoken.tipoSolicitud forKey:PENDIENTE_DESCARGA_TIPO_SOLICITUD_KEY];
    [pendienteDescargaDictionary setObject:_objetoSofttoken.nombreToken forKey:PENDIENTE_DESCARGA_NOMBRE_TOKEN_KEY];
    [pendienteDescargaDictionary setObject:_objetoSofttoken.numeroSerie forKey:PENDIENTE_DESCARGA_NUMERO_SERIE_KEY];
    [pendienteDescargaDictionary setObject:_objetoSofttoken.correo forKey:PENDIENTE_DESCARGA_CORREO_KEY];
    
    [pendienteDescargaDictionary writeToFile: pendienteDescargaFilePath atomically: YES];
    
    NSLog(@"Archivo pendienteDescarga creado");
}

-(void)cargaArchivoPendienteDescarga {
    NSString* pendienteDescargaFilePath = [ToolsAPIToken getPendienteDescargaFilePath];
    NSDictionary* pendienteDescargaDictionary = [NSDictionary dictionaryWithContentsOfFile: pendienteDescargaFilePath];
    NSLog(@"pendienteDescargaDictionary%@",pendienteDescargaDictionary);
    
    _objetoSofttoken = [[Softtoken alloc] initWithNumCelular:[pendienteDescargaDictionary objectForKey:PENDIENTE_DESCARGA_NUMERO_TELEFONO_KEY]
                                          andCompaniaCelular:[pendienteDescargaDictionary objectForKey:PENDIENTE_DESCARGA_COMPANIA_CELULAR_KEY]
                                               andNumTarjeta:[pendienteDescargaDictionary objectForKey:PENDIENTE_DESCARGA_NUMERO_TARJETA_KEY]
                                            andNumeroCliente:[pendienteDescargaDictionary objectForKey:PENDIENTE_DESCARGA_NUMERO_CLIENTE_KEY]
                                            andTipoSolicitud:[pendienteDescargaDictionary objectForKey:PENDIENTE_DESCARGA_TIPO_SOLICITUD_KEY]
                                              andNombreToken:[pendienteDescargaDictionary objectForKey:PENDIENTE_DESCARGA_NOMBRE_TOKEN_KEY]
                                              andNumeroSerie:[pendienteDescargaDictionary objectForKey:PENDIENTE_DESCARGA_NUMERO_SERIE_KEY]
                                                   andCorreo:[pendienteDescargaDictionary objectForKey:PENDIENTE_DESCARGA_CORREO_KEY]
                                            andNombreCliente:nil];
}

-(void)borraPendienteDescarga {
    NSString* pendienteDescargaFilePath = [ToolsAPIToken getPendienteDescargaFilePath];
    [[NSFileManager defaultManager] removeItemAtPath: pendienteDescargaFilePath error: NULL];
}

-(void) mostrarMensaje:(NSString*) mensaje {
    UIAlertView *alerta = [[UIAlertView alloc]initWithTitle: AVISO
                                                    message: mensaje
                                                   delegate: nil
                                          cancelButtonTitle: COMMON_ACCEPT
                                          otherButtonTitles: nil];
    [alerta show];
}

-(void)muestraAlertBienvenida{
    NSLog(@"muestraAlertaBienvenida");
    
    NSString* title = @"";
    
    if (!_esReactiva2x1) {
        title = ST_TITLE_ALERT_BIENVENIDA;
    }
    
    if (_mensajeBienvenida == nil) {
        if (!_esReactiva2x1) {
            _mensajeBienvenida = ST_MSG_ALERT_BIENVENIDA_TOKEN_MOVIL;
        } else {
            _mensajeBienvenida = ST_MSG_ALERT_BIENVENIDA_REACTIVA_2X1;
        }
    }
    
    UIAlertView *alerta = [[UIAlertView alloc]initWithTitle: title
                                                    message: _mensajeBienvenida
                                                   delegate: self
                                          cancelButtonTitle: COMMON_ACCEPT
                                          otherButtonTitles: nil];
    [alerta show];
}

- (void)mostrarAlertCambioNumeroCompaniaDesdeReenviarCodigo {
    UIAlertView *alert = [[UIAlertView alloc] init];
    
    alert.tag = TAG_ALERTA_CAMBIO_NUMERO_COMPANIA_DESDE_REENVIAR_CODIGO;
    [[alert initWithTitle: AVISO
                  message: TEXTO_CAMBIO_NUMERO_COMPANIA
                 delegate:self
        cancelButtonTitle:NO_KEY
        otherButtonTitles: SI, nil]
     show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSLog(@"Hollaaaaaaaaaaaaa alertview");
    if (alertView.tag == TAG_ALERTA_CAMBIO_NUMERO_COMPANIA_DESDE_REENVIAR_CODIGO) {
        if (buttonIndex == 0) {
            // El usuario selecciona NO: se ejecuta P026 Activación Softtoken, EA#2
            NSLog(@"Reenviar touch");
            [self borraToken];
            
        } else if(buttonIndex == 1) {
            // El usuario selecciona SI: se ejecuta P026 Activación Softtoken, EA#15
            UIAlertView *alert = [[UIAlertView alloc] init];
            
            alert.tag = TAG_ALERTA_ACTUALIZAR_ALERTAS_DESDE_REENVIAR_CODIGO;
            [[alert initWithTitle: AVISO
                          message: TEXTO_ACTUALIZAR_ALERTAS
                         delegate: self
                cancelButtonTitle: OK_KEY
                otherButtonTitles: nil]
             show];
            
        }
        
    } else if (alertView.tag == TAG_ALERTA_CAMBIO_NUMERO_COMPANIA_DESDE_CODIGO_ERROR_CNE1506) {
        if (buttonIndex == 0) {
            // El usuario selecciona NO: se ejecuta P026 Activación Softtoken, EA#16
            UIAlertView *alert = [[UIAlertView alloc] init];

            alert.tag = TAG_ALERTA_VERIFICAR_DATOS;
            [[alert initWithTitle: AVISO
                          message: TEXTO_VERIFICAR_DATOS
                         delegate: self
                cancelButtonTitle: OK_KEY
                otherButtonTitles: nil]
             show];
            
        } else if(buttonIndex == 1) {
            // El usuario selecciona SI: se ejecuta P026 Activación Softtoken, EA#17
            UIAlertView *alert = [[UIAlertView alloc] init];
            
            alert.tag = TAG_ALERTA_ACTUALIZAR_ALERTAS_DESDE_CODIGO_ERROR_CNE1506;
            [[alert initWithTitle: AVISO
                          message: TEXTO_ACTUALIZAR_ALERTAS
                         delegate: self
                cancelButtonTitle: OK_KEY
                otherButtonTitles: nil]
             show];
        }
        
    } else if (alertView.tag == TAG_ALERTA_VERIFICAR_DATOS) {
        //_controladorIngresaDatosST.txtNumCelular.text = @""; //LINEA COMENTADA
        //_controladorIngresaDatosST.txtNumTarjeta.text = @""; //LINEA COMENTADA
        _controladorIngresaDatosST.txtTarjeta.text = @"";
        _controladorIngresaDatosST.lblGetNumero.text = @"";
        
    } else if (alertView.tag == TAG_ALERTA_ACTUALIZAR_ALERTAS_DESDE_REENVIAR_CODIGO) {
        // Continuación P026 Activación Softtoken, EA#15
        [self borraToken];
        
    } else if (alertView.tag == TAG_ALERTA_ACTUALIZAR_ALERTAS_DESDE_CODIGO_ERROR_CNE1506) {
        // Continuación P026 Activación Softtoken, EA#17
      /*  SuiteAppDelegate *suiteApp =(SuiteAppDelegate*)[UIApplication sharedApplication].delegate;
        [suiteApp.controladorSuite.controladorDeNavegacion popToRootViewControllerAnimated:YES];*/
        
        [(UINavigationController *)([[UIApplication sharedApplication].delegate window].rootViewController) popToRootViewControllerAnimated:YES];
        
    } else if (alertView.tag == TAG_ALERTA_IVR_FALSE) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:SOLICITAR_CODIGO_TEL_NUMBER]];
        [[APIToken getInstance].softApp.controladorSofttoken showClaveActivacionST];
    } else if (alertView.tag == 1) {
        [self mostrarPantallaEmailST];
        
    } else {
        if(buttonIndex == 0) {
            
            if ([_viewController isKindOfClass:[claveActivacionViewController class]]){ // linea comentada, se cambio _claveActivacionSTViewController
                
                [_viewController.navigationController popToRootViewControllerAnimated:YES];
            
            } else if([_viewController isKindOfClass:[IngresaDatosSTViewController class]]
               || [_viewController isKindOfClass:[DefinicionContrasenaViewController class]]) {
            
                NSString* validacionAlertas = [[APIToken getInstance].apiTokenDataSource tipoSolicitud].validacionAlertas;
                
                if ([ESTATUS_VALIDACION_BMOVIL_CONTRATADO isEqualToString:validacionAlertas]) {
                    // Solicitud Softtoken, EA#2
                    NSString* estatusFilePath = [ToolsAPIToken getAppStatusStorageFilePath];
                    NSMutableDictionary* estatusDictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:estatusFilePath];
                    [estatusDictionary setObject:[NSNumber numberWithBool:NO] forKey:BMOVIL_ESTATUS_KEY];
                    [estatusDictionary writeToFile: estatusFilePath atomically: YES];
                    
                    [[APIToken getInstance].apiTokenDelegate resetSession];
                    [[APIToken getInstance].apiTokenDelegate storeSession];
                    
                    [[APIToken getInstance].apiTokenDelegate setApplicationActivated:NO];

                    [_viewController startActivityIndicator];
                    
                    [[ServerAplicacionDesactivada getInstance] consultaEstatusMantenimientoConNumeroTelefonico:_objetoSofttoken.numCelular
                                             conVersionCatalogoTelefono:@"0"
                                        conVersionCatalogoAutenticacion:@"0"
                                                  conVersionAppConsulta:[APIToken getInstance].appVersionConsulta
                                                         conIsEncriptar:[ServerTools isEncriptar]
                                                            paraCliente:_viewController];
                
                } else if ([ESTATUS_VALIDACION_SIN_ALERTAS_CONTRATADAS isEqualToString:validacionAlertas]) {
                    // Solicitud Softtoken, EA#5
                    
                } else if (([ESTATUS_VALIDACION_TELEFONO_INGRESADO_NO_COINCIDE isEqualToString:validacionAlertas])
                           || ([ESTATUS_VALIDACION_COMPANIA_INGRESADA_NO_COINCIDE isEqualToString:validacionAlertas])) {
                    // Solicitud Softtoken, EA#6 y EA#7
                    
                } else if ([ESTATUS_VALIDACION_TELEFONO_ASOCIADO_NO_COINCIDE isEqualToString:validacionAlertas]) {
                    // Solicitud Softtoken, EA#8
                    NSString* banderasBMovilFilePath = [ToolsAPIToken getBanderasBMovilFilePath];
                    NSMutableDictionary* banderasDictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:banderasBMovilFilePath];
                    [banderasDictionary setObject:[NSNumber numberWithBool:YES] forKey:CAMBIO_CELULAR_KEY];
                    [banderasDictionary writeToFile: banderasBMovilFilePath atomically: YES];
                    
                    [self crearTemporalCambioTelefono];
                    
                    // Solicitud Softtoken, escenario principal, paso 6
                    _objetoSofttoken.tipoSolicitud = INDICADOR_SOLICITUD_TOKEN_NUEVO;
                    
                    [self invokeSolicitudSofttoken];
                    
                } else if ([ESTATUS_VALIDACION_COMPANIA_ASOCIADA_NO_COINCIDE isEqualToString:validacionAlertas]) {
                    // Solicitud Softtoken, EA#9
                    NSString* banderasBMovilFilePath = [ToolsAPIToken getBanderasBMovilFilePath];
                    
                    NSMutableDictionary* banderasDictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:banderasBMovilFilePath];
                    
                    [banderasDictionary setObject:[NSNumber numberWithBool:YES] forKey:CAMBIO_CELULAR_KEY];
                    [banderasDictionary writeToFile: banderasBMovilFilePath atomically: YES];
                    
                    [self crearTemporalCambioTelefono];
                    
                    // Solicitud Softtoken, escenario principal, paso 6
                    _objetoSofttoken.tipoSolicitud = INDICADOR_SOLICITUD_TOKEN_NUEVO;
                    
                    [self invokeSolicitudSofttoken];
                    
                }
            } else if ([_viewController isKindOfClass:[EmailSTViewController class]]){ //se cambio EmailSTViewController por OperacionExitosaToken
                NSString* banderasBMovilFilePath = [ToolsAPIToken getBanderasBMovilFilePath];
                NSDictionary* banderasBMovilDictionary = [NSDictionary dictionaryWithContentsOfFile: banderasBMovilFilePath];
                NSLog(@"banderasBMovilDictionary%@",banderasBMovilDictionary);
                
                [[APIToken getInstance].apiTokenDelegate setUserName:_objetoSofttoken.numCelular];
                [[APIToken getInstance].apiTokenDelegate storeSession];
                //linea comentada, se comento la linea de abajo para probar modif 6:07 pm 11/07
                [_viewController.navigationController popToRootViewControllerAnimated:YES];
                
                // Si se cumple EA#7 mostramos pantalla inicial
                if ([[banderasBMovilDictionary objectForKey:CAMBIO_DE_PERFIL_KEY] boolValue]) {
                    [[APIToken getInstance].apiTokenDelegate bmovilLaunched];
                }
            
            }
        }
    }
}

-(void)crearTemporalCambioTelefono {
    NSLog(@"Creando el archivo TemporalCambioTelefono");
    
    NSString* temporalCambioTelefonoFilePath = [ToolsAPIToken getTemporalCambioTelefonoFilePath];
    NSMutableDictionary* dictionary = [NSMutableDictionary dictionaryWithCapacity: 1];
    
    [dictionary setObject:_objetoSofttoken.numTarjeta forKey:TARJETA_TEMPORAL_CAMBIO_TELEFONO_KEY];
    [dictionary writeToFile: temporalCambioTelefonoFilePath atomically: YES];
    
    NSLog(@"Contenido del diccionario de temporalCambioTelefono");
    NSLog(@"%@", dictionary);
}


- (void)dealloc
{
    NSLog(@"Dealloc delegateContratacion");
    if(timerObtenerToken && timerObtenerToken.isValid)
        [timerObtenerToken invalidate];
    timerObtenerToken = nil;
    if(_core != nil){
        _core = nil;
    }
    
    //[_objetoPendienteDescarga release];
}

-(void) reactivar2x1
{
    NSLog(@"Reactivacion 2X1");
    
    [_viewController startActivityIndicator];

   
    activationTimeBack = [[NSDate date] timeIntervalSince1970];

    [[APIToken getInstance].apiTokenDelegate setIUMWithUser:_objetoSofttoken.numCelular andActivationTime:activationTimeBack];
 
    
    float version = [APIToken getInstance].appVersion;
    NSString * ium = [[APIToken getInstance].apiTokenDataSource getIUM];
    version = version*100;
    _objetoSofttoken.versionApp = [NSString stringWithFormat:@"%.0f",version];
    [[ServerToken getInstance] sincronizeAndExportToken:_objetoSofttoken.numTarjeta
                                         numeroTelefono:_objetoSofttoken.numCelular
                                          tipoSolicitud:INDICADOR_SOLICITUD_REACTIVACION
                                            nombreToken:_objetoSofttoken.nombreToken
                                                   otp1:t1.otp
                                                   otp2:t2.otp
                                                    ium:ium
                                             versionApp:_objetoSofttoken.versionApp
                                              forClient:self];

}

- (void) finalizaReactivacion2x1
{
    NSLog(@"FINALIZNDO REACTIVACIÓN 2X1");
    
    if ([[APIToken getInstance].apiTokenDataSource isAppActivated]) {
        
        [[APIToken getInstance].apiTokenDelegate resetSession];
        
        [[APIToken getInstance].apiTokenDelegate setIUMWithUser:_objetoSofttoken.numCelular andActivationTime:activationTimeBack];
        
    }

    [ self guardarEstatusBMovil:YES];
    [ ContratacionSTDelegate guardaEstatusTokenMovil:YES];
    
    [[APIToken getInstance].apiTokenDelegate storeSession];
    
    // Guardar en KeyChain las variables número, seed y centro:
    [[APIToken getInstance] guardarEnKeyChain:[APIToken getInstance].apiTokenDataSource.userName clave:KEYCHAIN_TELEPHONE_KEY];
    [[APIToken getInstance] guardarEnKeyChain:[[NSNumber numberWithDouble: [APIToken getInstance].apiTokenDataSource.activationTime] stringValue] clave:KEYCHAIN_SEED_KEY];
    [[APIToken getInstance] guardarEnKeyChain:[[APIToken getInstance].apiTokenDataSource getAppName] clave:KEYCHAIN_CENTER_KEY];
    
    _esReactiva2x1 = YES;
    
    [ _viewController stopActivityIndicator ];
    
    [ self mostrarPantallaEmailST ];
}

- (void) rollbackMovil
{
    NSLog(@"EJECUTANDO ROLLBACK MOVIL");
    [[ APIToken getInstance].apiTokenDelegate resetSession];
    [ self guardarEstatusBMovil: NO ];
    [ ContratacionSTDelegate guardaEstatusTokenMovil: NO ];
    
    // Borrar de KeyChain las variables número, seed y centro
    [[APIToken getInstance] borrarDeKeyChain:KEYCHAIN_TELEPHONE_KEY];
    [[APIToken getInstance] borrarDeKeyChain:KEYCHAIN_SEED_KEY];
    [[APIToken getInstance]  borrarDeKeyChain:KEYCHAIN_CENTER_KEY];

    [_viewController stopActivityIndicator];
    
    [[[UIAlertView alloc] initWithTitle: AVISO message:TEXTO_ALERT_ROLLBACK delegate:self cancelButtonTitle:OK_KEY otherButtonTitles:nil, nil] show ];
}

//se cambio sp_AlertView por sp_AlertViewInc

- (void)popUpButtonTouchUpInside:(id)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSLog(@"Se ejecuta la accion de clic en allguno de los botones", buttonIndex);
    if (sp_AlertViewInc.tag == TAG_ALERTA_SOLICITAR_COD_ACTIVACION) {
        
    } else if (sp_AlertViewInc.tag == TAG_ALERTA_COD_ACTIVACION_INCORRECTO) {
        if (buttonIndex == 0) {
            //_claveActivacionSTViewController.txtCveActivacion.text = @""; se cambia esta linea por la de abajo
            _claveActivacionViewController.TextCodigo.text = @"";
            [_claveActivacionViewController activarBotonDerecho:NO]; // linea comentada, se cambio _claveActivacionSTViewController
        } else if (buttonIndex == 1) {
            [self borraToken];
            NSLog(@"EXITO!");
        }
    }
}




- (BOOL) mostrarBotonSlicitarClave {
    
    BOOL result = [[NSFileManager defaultManager] fileExistsAtPath: [ToolsAPIToken getApplicationSessionFilePath]] && [[[NSMutableDictionary dictionaryWithContentsOfFile: [ToolsAPIToken getApplicationSessionFilePath]] objectForKey:IVR_KEY] boolValue];
    return result;
}



#pragma mark - respuesta de servidor
 
 - (NSInteger) checkDownloadId: (NSInteger) aDownloadId {
     
     //IngresaDatosViewcontroller
     if (_tipoOperacion==1){
     
         return CONSULTA_TIPO_SOLICITUD_ST;
     }
     
     
     //ConfirmacionSTViewController
      else if (_tipoOperacion==2){
        return 32;
      }
     
     //AutenticacionSTViewController
      else if (_tipoOperacion==3){
    
          return _autenticacionSTViewController.operationDelivered;

      }
     //ClaveActivacionSTViewController
      else if (_tipoOperacion==4){
          
         return 44;
          
      }
     
     return 0;
     
 }

 -(void)networkResponse:(ServerResponse *)aServerResponse{
     NSInteger operationType = [self checkDownloadId: nil];
     //MARCADOR SERVERRESPONSE
     if ([[APIToken getInstance].softApp analyzeServerResponseN: aServerResponse forOperation: operationType] == NO)
     {
         [self serverError];
         //stopactivityindicator
         BaseViewController* currentViewController = (BaseViewController*) ((UINavigationController *)[[UIApplication sharedApplication].delegate window].rootViewController).viewControllers.lastObject;
         [currentViewController stopActivityIndicator];
         
     } else {
         
       //IngresaDatosViewcontroller
       if (_tipoOperacion==1){
     
         NSLog(@"Respuesta llega a ingresa datos st.");
         
         //[[APIToken getInstance].apiTokenDelegate setUserActivationST:self.controladorIngresaDatosST.txtNumCelular.text]; //LINEA COMENTADA CAMBIO
           [[APIToken getInstance].apiTokenDelegate setUserActivationST:self.controladorIngresaDatosST.lblGetNumero.text];
           //[[APIToken getInstance].apiTokenDelegate setUserNumTarjetaST :self.controladorIngresaDatosST.txtNumTarjeta.text]; //LINEA COMENTADA CAMBIO
           [[APIToken getInstance].apiTokenDelegate setUserNumTarjetaST :self.controladorIngresaDatosST.txtTarjeta.text];
           [[APIToken getInstance].apiTokenDelegate setUserCompaniaST:[self.controladorIngresaDatosST.selectedItem objectAtIndex:1]];
         //MARCADOR SERVERRESPONSE
         [self analyzeServerResponseN:aServerResponse];
           
        //ConfirmacionSTViewController
        }else if (_tipoOperacion==2){
        
           //[self stopActivityIndicator];
           [self setConfirmacionST:self.confirmacionST];
           [self analyzeServerResponseN:aServerResponse]; //MARCADOR SERVERRESPONSE
            
        }
         
         //AutenticacionSTViewController
       else if (_tipoOperacion==3){
           //[self stopActivityIndicator];
           if ([aServerResponse.responseHandler isKindOfClass:[SolicitudSTResult class]]) {
               _autenticacionSTViewController.operationDelivered = AUTENTICAR_TOKEN;
               //SE AGREGO EL NUEVO METODO DE CONSUTA AUTENTICACION
               [self consultaAutenticacionN:_autenticacionSTViewController.txtNIP.text andCVV:_autenticacionSTViewController.txt3DigitosTarjeta.text andOTP:@"" onViewController:_autenticacionSTViewController];
               
           } else if ([aServerResponse.responseHandler isKindOfClass:[AutenticacionTokenResult class]]) {
               [self setAutenticacionSTViewController:_autenticacionSTViewController];
               [self analyzeServerResponseN:aServerResponse];
           }
       }
         
         //ClaveActivacionSTViewController
       else if (_tipoOperacion==4){
           [self analyzeServerResponseN:aServerResponse];

       }
     }
      
 }




@end
