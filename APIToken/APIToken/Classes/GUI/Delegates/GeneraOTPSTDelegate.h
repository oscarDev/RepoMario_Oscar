//
//  GeneraOTPSTDelegate.h
//  Bancomer
//
//  Created by Francisco.Garcia on 28/05/13.
//
//

#import <Foundation/Foundation.h>
#import <mtcore-ios/MTDeployer.h>
#import <mtcore-ios/MTAppCore.h>
#import <mtcore-ios/MTCoreFactory.h>

@class GeneraOTPSTViewController;
@class MuestraOTPSTViewController;
@class RegistraCuentaSTViewController;
@class AltaRegistroViewController;  // Modificación 50683

typedef enum {
	claveConSemilla,
	claveSimple
} tipoDeToken;

@interface GeneraOTPSTDelegate : NSObject{

    id <MTDeployer> deployer;
    id <MTAppCore> _core;
    
}
@property (nonatomic) tipoDeToken seleccionToken;
@property (nonatomic, strong) GeneraOTPSTViewController* generaOTPSTViewController;
@property (nonatomic, strong) MuestraOTPSTViewController* muestraOTPSTViewController;
@property (nonatomic, strong) RegistraCuentaSTViewController *controladorRegistraCuentasST;
@property (nonatomic, strong) id <MTAppCore> core;
@property (nonatomic, strong) AltaRegistroViewController *altaRegistroViewController; // Modificación 50683



+(NSString*)generaOTPTiempo;
-(void)muestraOTP:(NSString*)otp;
+(NSString*)generaOTPChallenge:(NSString*)challenge;
-(BOOL)validaTextoChallenge;
-(void)borraToken;
-(void)cierraCore;
- (NSString *) dbPath;
-(void)muestraAlertConfirmacionBorrarDatos;

+ (NSString *) generaOTPTransactionSigning:(NSString *) qrCode; // Modificación 50683
-(void)muestraVistaAltaRegistro: (NSMutableDictionary *)operacion qrLeido:(NSString *)qr; // Modificación 50683
+ (NSMutableDictionary *) generaOperacionTransactionSigning:(NSString *) qrCode; // Modificación 50683

@end
