//
//  GeneraOTPSTDelegate.m
//  Bancomer
//
//  Created by Francisco.Garcia on 28/05/13.
//
//

#import "GeneraOTPSTDelegate.h"
#import "SoftokenViewsController.h"
#import "GeneraOTPSTViewController.h"
#import "ContratacionSTDelegate.h"
#import "ConstantsToken.h"
#import "MuestraOTPSTViewController.h"
#import "RegistraCuentaSTViewController.h"
#import "OpenOCSTesting.h"  // Modificación 50683
#import "SofttokenApp.h"
#import "APIToken.h"

#define PENDIENTE_DESCARGA                  @"ArchivoPendienteDescarga"
#define PENDIENTE_DESCARGA_KEY              @"pendienteDescarga"

@implementation GeneraOTPSTDelegate
@synthesize core= _core;
//@synthesize generaOTPSTViewController = _generaOTPSTViewController;
//@synthesize muestraOTPSTViewController = _muestraOTPSTViewController;
static GeneraOTPSTDelegate* _generaOTPSTDelegateInstance;

/*
 * Returns the singleton only instance
 */
+ (GeneraOTPSTDelegate*) getInstance {
	if (_generaOTPSTDelegateInstance == nil) {
		@synchronized([GeneraOTPSTDelegate class]) {
			if (_generaOTPSTDelegateInstance == nil) {
				_generaOTPSTDelegateInstance = [[GeneraOTPSTDelegate alloc] init];
			}
		}
	}	
	return _generaOTPSTDelegateInstance;
}

/*
 *
 */
-(void)inicializaCore{
    NSString* path = [self dbPath];
    if(!_core)
    {
        //Set Legacy mode ON
#ifdef MTCoreFactory_API_VERSION
        [MTCoreFactory setLegacyMode:YES];
#endif
        _core = [MTCoreFactory createCore: [[APIToken getInstance].apiTokenDataSource getSemillaST] usingDatabase: path];
    }
    
    if ([[Server getInstance] getEnvironment] == 0) {
        [_core setDownloadURL: @"https://www.bancomermovil.com/mbank/mtbrokerext/AccountDownload.aspx"];
    } else {
        [_core setDownloadURL: @"https://www.bancomermovil.net:11443/mbank/mtbrokerext/AccountDownload.aspx"];
    }
}

/*
 *
 */
-(void)cierraCore{
    if(_core){
        _core = nil;
    }
}

/*
 *
 */
- (NSString *) dbPath {
    //NSString* path = [NSString stringWithFormat:@"%@/Library/tokendb/%@",NSHomeDirectory(),[[UIDevice currentDevice] uniqueIdentifier]];
    NSString* path = [NSString stringWithFormat:@"%@/Library/tokendb/%@",NSHomeDirectory(),@"222222"];
    return path;
}

/*
 *
 */
+(NSString*)generaOTPTiempo{
    NSLog(@"Generando OTP de tiempo");
    
    id <MTToken> t1 = nil;
    GeneraOTPSTDelegate* delegate = [[[self class] alloc]init];//[[self class]getInstance];
    delegate.core = nil;
    [delegate inicializaCore];
    if([delegate validaPwdDB]){
        NSLog(@"PwdDB validada");
        
        id <MTAccountList> aList = [delegate obtenerCuentas];
        if(aList.count > 0){
            id <MTAccount> a = [aList accountAt:0];            
            t1 =   [delegate.core loadTokenForAccount: a];
        }
    }else{
        NSLog(@"PwdDB NO validada. Borrando token");
        [delegate borraToken];
    }
    
    if(t1){
        NSLog(@"t1 definida");
        [t1 updateTimeOTP];
        [delegate cierraCore];
        NSLog(@"TOKEN %@",[t1 otp]);
        return [t1 otp];
    }else
    {
        NSLog(@"t1 NO definida");
         [delegate cierraCore];
        return @"";
    }
}

/*
 *
 */
-(BOOL)validaPwdDB{
    NSString* password  = [[APIToken getInstance].apiTokenDataSource getIumST];
    
    return [_core loginWithPassword:password];     //JRMM, esto se comenta para poder continuar con el flujo de
}

/*
 *
 */
-(id <MTAccountList>)obtenerCuentas{
    id <MTEnterpriseList> eList;
    id <MTAccountList> aList = nil;
    eList = [_core loadEnterpriseList];
    
    if (!eList.count>0) {
        NSLog(@"Lista de empresas vacia");
        
    }else {
        NSLog(@"Lista de empresas mayor a cero");
        aList = [_core loadAccountListForEnterprise: [eList enterpriseAt: 0]];
    }
    return aList;
}

/*
 *
 */
-(void)muestraOTP:(NSString*)otp{
    if([otp isEqualToString:@""]) {
        [[APIToken getInstance].softApp showCommunicationErrorAlert];
        return;
    }
        
    [[APIToken getInstance].softApp.controladorSofttoken showMuestraOTPS:otp];
}

/*
 *
 */
-(void)borraToken{
    
    if ([MTCoreFactory resetDatabase: [self dbPath]]) {
        [ContratacionSTDelegate guardaEstatusTokenMovil:NO];
        [self cierraCore];
        NSLog(@"Token borrado");
    }
    
    [(UINavigationController *)([[UIApplication sharedApplication].delegate window].rootViewController) popToRootViewControllerAnimated:YES];
}



-(BOOL)validaTextoChallenge{
    BOOL challengeValido = YES;
    
    challengeValido = (_controladorRegistraCuentasST.txtCuenta.text.length == CHALLENGE_ST_REFERENCE_LENGTH);
    
    if(!challengeValido){
        [[APIToken getInstance].softApp showErrorMessage: ST_CHALLENGE_ERROR];
    }
    return challengeValido;
    
}

-(void)muestraAlertConfirmacionBorrarDatos{
    NSLog(@"Muestra alert confirmacion borrar datos.");
    NSString* title = @"Aviso";
    NSString* msg = @"Al borrar datos asociados inhabilitarás el funcionamiento de Token móvil y podrás reactivarlo una sola vez al día.";
    NSString* btnEliminar = @"Eliminar";
    NSString* btnCancelar = @"Cancelar";
    
    UIAlertView *alerta = [[UIAlertView alloc]initWithTitle: title
                                                    message: msg
                                                   delegate: self
                                          cancelButtonTitle: nil
                                          otherButtonTitles: btnEliminar, btnCancelar, nil];
    [alerta show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 0)
    {
        [self borraToken];
    }
    else if(buttonIndex == 1)
    {
        
    }
}

-(void)borraPendienteDescarga
{
    NSFileManager* fileManager = [NSFileManager defaultManager];
    NSString* documentsDirectory = [[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject] path];
    NSString* pendienteDescargaPath = [documentsDirectory stringByAppendingPathComponent:PENDIENTE_DESCARGA];
    [fileManager removeItemAtPath:pendienteDescargaPath error:nil];
}

+(NSString*)generaOTPChallenge:(NSString*)numTarjeta{
    NSString* challenge = @"";
    id <MTToken> t1 = nil;
    GeneraOTPSTDelegate* delegate = [[[self class] alloc]init];//[[self class]getInstance];
    [delegate inicializaCore];
    if([delegate validaPwdDB]){
        id <MTAccountList> aList = [delegate obtenerCuentas];
        if(aList.count > 0){
            id <MTAccount> a = [aList accountAt:0];
            t1 =   [delegate.core loadTokenForAccount: a];
        }
    }else{
        [delegate borraToken];
    }
    
    //if([t1 calculateResponseForChallenge:numTarjeta]){
     if([t1 calculateResponseForTS:numTarjeta]){ 
    challenge = [t1 otp];
        //[t1 updateTimeOTP];  // Modificación 50683
        [delegate cierraCore];
        NSLog(@"TOKEN %@",[t1 otp]);   // Modificación 50683
        
        return challenge;//[t1 otp];
    }else
    {
        [delegate cierraCore];
        return @"";
    }
    return challenge;
}

/** Modificación 50683
 * Se genera el OTP transaction Signing en base a la respuesta del
 * codigo QR.
 */
+ (NSString *) generaOTPTransactionSigning:(NSString *) qrCode {
    
    OpenOCSTesting *ocsTesting = [[OpenOCSTesting alloc] init];
    
    [ocsTesting generateRandom];
    [ocsTesting constructorOCSAlphabet];
    [ocsTesting constructorNoPaddingNoIgnore];
    [ocsTesting decodingNoPadding];
    
    NSLog(@"qrCode : %@", qrCode);
    
    NSData *data = [ocsTesting obfuscatorWithString:qrCode];
    NSLog(@"dataOrigen : %@", data);
    
    
    
    NSMutableData *mutableData = [[NSMutableData alloc] initWithData:data];
    int value = 02;
    [mutableData replaceBytesInRange: NSMakeRange(3,1) withBytes: &value];
    NSLog(@"dataMod : %@", mutableData);
    
    
    
    id <MTOpticalMessage> encodedMessage= [MTCoreFactory createOpticalMessageWithData:mutableData];
    NSLog(@"encodedMessage : %@", encodedMessage);
    
    id <MTTransactionSigning> transactionSigning = [MTCoreFactory createTransactionSigningForOpticalMessage:encodedMessage];
    
    NSMutableDictionary *dictionaryData = [[NSMutableDictionary alloc] init];
    
    [dictionaryData setObject:[transactionSigning getTagValueAt:0] forKey:@"0x02"];
    [dictionaryData setObject:[transactionSigning getTagValueAt:1] forKey:@"0x04"];
    
    NSLog(@"Operaciones OTP: %@",dictionaryData);
    
    NSLog(@"Transaction signing generada Challenge: %@", [transactionSigning getChallenge]);
    
    NSString *otp = [self generaOTPChallenge:[transactionSigning getChallenge]];
    
    NSLog(@"OTP generada : %@",otp);
    
    return otp;
}

/**
 * SofftokenOptica ODT2 -Modificación 50683
 * Muestra vista ALTA REGISTRO
 */
-(void)muestraVistaAltaRegistro: (NSMutableDictionary *)operacion qrLeido:(NSString *)qr
{
    [[APIToken getInstance].softApp.controladorSofttoken muestraAltaRegistro:operacion qrLeido:qr];
}

/**SofftokenOptica ODT2 - Modificación 50683
 * Se genera el OTP transaction Signing en base a la respuesta del
 * codigo QR.
 */
+ (NSMutableDictionary *) generaOperacionTransactionSigning:(NSString *) qrCode {
    
    OpenOCSTesting *ocsTesting = [[OpenOCSTesting alloc] init];
    
    [ocsTesting generateRandom];
    [ocsTesting constructorOCSAlphabet];
    [ocsTesting constructorNoPaddingNoIgnore];
    [ocsTesting decodingNoPadding];
    
    NSData *data = [ocsTesting obfuscatorWithString:qrCode];
    NSLog(@"data : %@", data);
    @try {
        id <MTOpticalMessage> encodedMessage= [MTCoreFactory createOpticalMessageWithData:data];
        
        id <MTTransactionSigning> transactionSigning = [MTCoreFactory createTransactionSigningForOpticalMessage:encodedMessage];
        
        
        NSMutableDictionary *dictionaryData = [[NSMutableDictionary alloc] init];
        
        /*
         0x07: Reference	Tipo de operación RN9
         0x80: Folio	Folio AST
         0x03: From account	Cuenta a firmar RN10
         0x04: To account	Descripción de tercero RN11
         */
        
        [dictionaryData setObject:[transactionSigning getTagValueAt:0] forKey:@"0x02"];
        [dictionaryData setObject:[transactionSigning getTagValueAt:1] forKey:@"0x04"];
        [dictionaryData setObject:[transactionSigning getTagValueAt:2] forKey:@"0x07"];
        [dictionaryData setObject:[transactionSigning getTagValueAt:3] forKey:@"0x05"];
        
        
        NSLog(@"Operaciones : %@",dictionaryData);
        
        return dictionaryData ;
    } @catch (NSException * e) {
        
        return nil;
    }
    
}

@end
