//
//  ContratacionSTDelegate.h
//  Bancomer
//
//  Created by Francisco.Garcia on 27/05/13.
//
//

#import <Foundation/Foundation.h>
#import <mtcore-ios/MTDeployer.h>
#import <mtcore-ios/MTAppCore.h>
#import "PopUpImageViewController.h"
#import "BaseDelegate.h"
#import "AlertaIncVC.h"
#import "AlertaEnvio.h"

@class ServerResponse;
@class BancomerViewController;
@class IngresaDatosSTViewController; //linea comentada 1er pantalla
@class ContratarTokenController; //linea nueva 1er pantalla
@class ConfirmacionSTViewController;
//@class ClaveActivacionSTViewController; linea comentada vc a quitar
@class AutenticacionSTViewController;
@class EmailSTViewController; // linea comentada VC a reemplazar
//@class OperacionExitosaToken; //linea comentada
@class Softtoken;
@class TipoSolicitudResult;
@class claveActivacionViewController; //linea comentada: se agrego esta linea

#define CODIGO_ERROR_CNE1506                @"CNE1506"
#define TAG_ALERTA_SOLICITAR_COD_ACTIVACION                             7
#define TAG_ALERTA_COD_ACTIVACION_INCORRECTO                            8

/**
 * Defines ivr key
 */
#define IVR_KEY									@"ivr"

@interface ContratacionSTDelegate : BaseDelegate<MTDeploymentListener, PopUpImageAlertViewDelegate>
{

    id <MTDeployer> deployer;
    id <MTAppCore> _core;
    id <MTToken> t1,t2;
    ConfirmacionSTViewController* __strong _confirmacionST;
    //ClaveActivacionSTViewController* __strong _claveActivacionSTViewController; linea comentada.
    
    AutenticacionSTViewController* __strong _autenticacionSTViewController;
    NSTimer* timerObtenerToken;
    
    claveActivacionViewController*__strong _claveActivacionViewController; //linea comentada, se agrego esta linea
}
@property (nonatomic,strong)ConfirmacionSTViewController* confirmacionST;
//@property (nonatomic,strong)ClaveActivacionSTViewController* claveActivacionSTViewController; linea comentada se quita lo del vc a reemplazar
@property (nonatomic,strong)AutenticacionSTViewController* autenticacionSTViewController;
@property (nonatomic,strong)IngresaDatosSTViewController   *controladorIngresaDatosST; //vc a reemplazar 1er pantalla
@property (nonatomic, strong)ContratarTokenController *controladorContratarST; //linea nueva 1er pantalla

@property (nonatomic,strong)EmailSTViewController   *controladorEmailST;     //Linea comentada VC a reemplazar
//@property (nonatomic, strong)OperacionExitosaToken *controladorExitosa;
@property (nonatomic,strong)Softtoken *objetoSofttoken;
@property (nonatomic,strong)NSString *tipoInstrumento;
@property (nonatomic,strong)BaseViewController *viewController;
@property (nonatomic,strong)TipoSolicitudResult *tempTipoSolicitud;
@property (nonatomic,strong)NSString *mensajeBienvenida;
@property BOOL es2x1;
@property BOOL esReactiva2x1;
@property(nonatomic) NSInteger tipoOperacion;
@property (nonatomic, strong)claveActivacionViewController* claveActivacionViewController; //se agrego esta linea

+(NSInteger) getOperacionSolicitudSofttoken;
+(BOOL)validaCampos:(id)viewController;

-(void)consultaSolicitud:(NSString*)numCelular andTarjeta:(NSString*)numTarjeta andCompaniaCelular:(NSString*) companiaCelular onViewController:(IngresaDatosSTViewController*)viewController; //linea comentada 1er pantalla
-(void)consultaSolicitudN: (NSString*)numCelular andTarjeta:(NSString*)numTarjeta onViewController:(ContratarTokenController*)viewController;  //linea agregagada 1er pantalla

//-(void)consultaAutenticacion:(NSString*)nip andOTP:(NSString*)otp onViewController:(ConfirmacionSTViewController*)viewController;

-(void)consultaAutenticacion:(NSString*)nip andCVV:cvv andOTP:(NSString*)otp onViewController:(BaseViewController*)viewController; //-- Transacción CVV
-(void)consultaAutenticacionN:(NSString*)nip andCVV:cvv andOTP:(NSString*)otp onViewController:(BaseViewController*)viewController; //-- Transacción CVV //METODO NUEVO

-(void)analyzeServerResponse:(ServerResponse *)aServerResponse;
+(void)guardaEstatusTokenMovil:(BOOL)status;
-(void)muestraAlertBienvenida;
-(void)mostrarAlertCambioNumeroCompaniaDesdeReenviarCodigo;
+(void)borrarToken;
-(void)borraToken;
-(void)continuarActivacion;
//-(void)cancelOperation;
-(void)setObjetoSofttoken:(NSString*)numeroCelular andCompaniaCelular:(NSString*)aCompaniaCelular andNumTarjeta:(NSString*)aNumTarjeta;
-(void)setObjetoSofttokenN:(NSString*)numeroCelular andNumTarjeta:(NSString*)aNumTarjeta;  
-(BOOL)consultaArchivoPendienteDescarga;
-(void)borraPendienteDescarga;
-(void)cargaArchivoPendienteDescarga;
-(void)invokeCambioTelefonoAsociado;
-(void)invokeSolicitudSofttoken;
-(void)invokeSolicitudST:(BaseDelegate*) client;
-(void)invokeOperacionIngreso:(BaseDelegate*)client andNumCelular:(NSString*)numCelular andTarjeta:(NSString*)numTarjeta andCompaniaCelular:(NSString*) companiaCelular;
-(void)analyzeServerResponseConsultaTarjetaST:(TipoSolicitudResult *)tipoSolicitud conCliente:(id<ServerClient>)cliente;
-(void)invokeContratacionEnrolamiento:(BaseDelegate*)client;

- (BOOL) mostrarBotonSlicitarClave;
+ (BOOL) isIVR;
+ (void) guardarEstatusIVR:(BOOL)estatusIVR;
@end
