//
//  SofttokenApp.h
//  SuiteBancomer
//
//  Created by JuanRa Marin  on 07/10/13.
//  Copyright (c) 2013 GoNet. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "BaseDelegate.h"

@class SoftokenViewsController;

@interface SofttokenApp : BaseDelegate

@property(nonatomic,strong)SoftokenViewsController *controladorSofttoken;
@property(nonatomic,strong)UIAlertView *errorAlert;
@property(nonatomic,strong)UIAlertView *informationAlert;

- (id)initWithStatus:(BOOL)status;
- (id)initWithStatus:(BOOL)status andMostrarPantallaToken:(BOOL) mostrarPantallaToken;
- (id)initAutenticacionST;

-(void)softLaunchedwithStatus:(BOOL)status;

- (void) showCommunicationErrorAlert;

- (BOOL) analyzeServerResponse: (ServerResponse*) aServerResponse forOperation: (NSInteger) anOperation;

//LINEA COMENTADA METODO NUEVO
- (BOOL) analyzeServerResponseN: (ServerResponse*) aServerResponse forOperation: (NSInteger) anOperation;


/**
 * Shows an error alert view
 *
 * @param aMessage The information message to show
 */
- (void) showErrorMessage: (NSString*) anErrorMessage;
/**
 * Shows an information alert view
 *
 * @param aMessage The information message to show
 */
- (void) showInformationWithMessage: (NSString*) aMessage;

@end
