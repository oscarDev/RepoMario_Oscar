//
//  TipoSolicitudResult.h
//  Bancomer
//
//  Created by Francisco.Garcia on 27/05/13.
//
//

#import <Foundation/Foundation.h>
#import "ParsingHandler.h"
@interface TipoSolicitudResult : NSObject<ParsingHandler,NSCopying>
    
@property (nonatomic, copy) NSString* estatusBmovil;
@property (nonatomic, copy) NSString* validacionAlertas;
@property (nonatomic, copy) NSString* numeroAlertas;
@property (nonatomic, copy) NSString* companiaAlertas;
@property (nonatomic, copy) NSString* numeroCliente;
@property (nonatomic, copy) NSString* indicadorContratacion;
@property (nonatomic, copy) NSString* numSerieToken;
@property (nonatomic, copy) NSString* tipoInstrumento;
@property (nonatomic, copy) NSString* correoElectronico;
@property (nonatomic, copy) NSString* nombreCliente;
@property (nonatomic, copy) NSString* switchEnrolamiento;
@property (nonatomic, copy) NSString* dispositivoFisico;
@property (nonatomic, copy) NSString* estatusDispositivo;
@end
