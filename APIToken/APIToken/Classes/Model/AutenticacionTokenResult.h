//
//  AutenticacionTokenResult.h
//  Bancomer
//
//  Created by Francisco.Garcia on 28/05/13.
//
//

#import <UIKit/UIKit.h>
#import "ParsingHandler.h"
@interface AutenticacionTokenResult : NSObject<ParsingHandler>

@property (nonatomic, copy) NSString* numeroSerie;
@property (nonatomic, copy) NSString* nombreToken;
@property(nonatomic)BOOL ivr;

@end
