//
//  FinalizarContratacionSTResult.h
//  Bancomer
//
//  Created by Alberto Martínez Fernández on 23/05/14.
//  Copyright (c) 2014 GoNet. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ParsingHandler.h"

@interface FinalizarContratacionSTResult : NSObject<ParsingHandler>

@property(nonatomic)BOOL isCorrect;

@end
