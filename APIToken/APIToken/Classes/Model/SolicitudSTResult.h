//
//  SolicitudSTResult.h
//  Bancomer
//
//  Created by Alberto Martínez Fernández on 16/05/14.
//

#import <Foundation/Foundation.h>
#import "ParsingHandler.h"

@interface SolicitudSTResult : NSObject<ParsingHandler,NSCopying>

@property(nonatomic)BOOL isCorrect;
@property (nonatomic, copy) NSString* codigoMensaje;
@property (nonatomic, copy) NSString* descripcionMensaje;

@end
