//
//  AutenticacionTokenResult.m
//  Bancomer
//
//  Created by Francisco.Garcia on 28/05/13.
//
//

#import "AutenticacionTokenResult.h"
#import "ParserJSON.h"

@implementation AutenticacionTokenResult

/**
 * Parses the raw data. Does nothing, as this class doesn't parse raw data
 *
 * @param aData The data received from the server to be parsed
 */
- (void) parseData: (NSData*) aData {
}

/**
 * Processes a server response
 *
 * @parameters aParser The parser containing the server response, ready to start parsing
 */
- (void) processParser: (Parser*) aParser {
    
    ParserJSON *parserJSON = (ParserJSON*)aParser;
    
    _numeroSerie = [[parserJSON parseNextValueForTag:@"numeroSerie"] copy];
    _nombreToken = [[parserJSON parseNextValueForTag:@"nombreToken"] copy];
     _ivr = [[parserJSON parseNextValueForTag:@"ivr"] isEqualToString:@"true"];
}

/**
 * Informs whether the parsing handler uses raw data or a parser to analyze data
 *
 * @return Always returns YES
 */
- (BOOL) usesParser {
	return YES;
}

@end
