//
//  SincronizacionExportacionTokenResult.m
//  Bancomer
//
//  Created by Driss on 07/04/15.
//  Copyright (c) 2015 GoNet. All rights reserved.
//

#import "SincronizacionExportacionTokenResult.h"
#import "ParserJSON.h"

@implementation SincronizacionExportacionTokenResult



/**
 * Parses the raw data. Does nothing, as this class doesn't parse raw data
 *
 * @param aData The data received from the server to be parsed
 */
- (void) parseData: (NSData*) aData {
}

-(void)processParser:(Parser *)aParser{
    
    ParserJSON *parserJSON = (ParserJSON*)aParser;
    
    _estado = [[parserJSON parseNextValueForTag:@"estado"] copy];
    _indReac2x1 = [[parserJSON parseNextValueForTag:@"indReac2x1"] copy];
    
   
    
}



-(BOOL)usesParser{
    return YES;
}


@end
