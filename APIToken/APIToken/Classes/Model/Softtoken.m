//
//  Softtoken.m
//  Bancomer
//
//  Created by Alberto Martínez Fernández on 15/05/14.
//  Copyright (c) 2014 GoNet. All rights reserved.
//

#import "Softtoken.h"

@implementation Softtoken

-(id)initWithNumCelular:(NSString*)aNumCelular andCompaniaCelular:(NSString*)aCompaniaCelular andNumTarjeta:(NSString*)aNumTarjeta andNumeroCliente:(NSString*)aNumeroCliente andTipoSolicitud:(NSString*)aTipoSolicitud andNombreToken:(NSString*)aNombreToken andNumeroSerie:(NSString*)aNumeroSerie andCorreo:(NSString*)aCorreo andNombreCliente:(NSString*)aNombreCliente
{
    
    self = [super init];
    if (self) {
        _numCelular = aNumCelular==nil?@"":[aNumCelular copy];
        _numTarjeta = aNumTarjeta==nil?@"":[aNumTarjeta copy];
        _numeroCliente = aNumeroCliente==nil?@"":[aNumeroCliente copy];
        _tipoSolicitud = aTipoSolicitud==nil?@"":[aTipoSolicitud copy];
        _nombreToken = aNombreToken==nil?@"":[aNombreToken copy];
        _numeroSerie = aNumeroSerie==nil?@"":[aNumeroSerie copy];
        _correo = aCorreo==nil?@"":[aCorreo copy];
        _nombreCliente = aNombreCliente==nil?@"":[aNombreCliente copy];
        _companiaCelular = aCompaniaCelular==nil?@"":[aCompaniaCelular copy];
        _versionApp=@"";
    }
    return self;
}

@end
