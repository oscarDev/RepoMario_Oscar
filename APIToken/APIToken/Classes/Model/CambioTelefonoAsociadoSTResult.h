//
//  CambioTelefonoAsociadoSTResult.h
//  Bancomer
//
//  Created by Alberto Martínez Fernández on 13/05/14.
//

#import <Foundation/Foundation.h>
#import "ParsingHandler.h"

@interface CambioTelefonoAsociadoSTResult : NSObject<ParsingHandler>

@property(nonatomic)BOOL isCorrect;

@end
