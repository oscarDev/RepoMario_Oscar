//
//  SincronizacionExportacionTokenResult.h
//  Bancomer
//
//  Created by Driss on 07/04/15.
//  Copyright (c) 2015 GoNet. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ParsingHandler.h"
@interface SincronizacionExportacionTokenResult : NSObject <ParsingHandler>

    @property(nonatomic, copy)NSString *estado;
    @property(nonatomic, copy)NSString *indReac2x1;



@end
