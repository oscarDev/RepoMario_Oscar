//
//  ContratacionEnrolamientoResult.h
//  Bancomer
//
//

#import <Foundation/Foundation.h>
#import "ParsingHandler.h"

@interface ContratacionEnrolamientoResult : NSObject<ParsingHandler>

@property(nonatomic)BOOL isCorrect;

@end
