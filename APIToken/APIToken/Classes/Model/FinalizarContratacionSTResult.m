//
//  FinalizarContratacionSTResult.m
//  Bancomer
//
//  Created by Alberto Martínez Fernández on 23/05/14.
//  Copyright (c) 2014 GoNet. All rights reserved.
//

#import "FinalizarContratacionSTResult.h"

@implementation FinalizarContratacionSTResult

-(void)processParser:(Parser *)aParser{
    _isCorrect = YES;
}

-(BOOL)usesParser{
    return YES;
}

-(void)parseData:(NSData *)aData{
    
}

@end