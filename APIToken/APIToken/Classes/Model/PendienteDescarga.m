//
//  PendienteDescarga.m
//  SuiteBancomer
//
//  Created by JuanRa Marin  on 15/10/13.
//  Copyright (c) 2013 GoNet. All rights reserved.
//

#import "PendienteDescarga.h"
#define NUMERO_TARJETA_KEY              @"NumeroTarjeta"
#define NUMERO_TELEFONO_KEY             @"NumeroTelefono"
#define NUMERO_CLIENTE_KEY              @"NumeroCliente"
#define TIPO_SOLICITUD_KEY              @"TipoSolicitud"
#define NOMBRE_TOKEN_KEY                @"NombreToken"
#define NUMERO_SERIE_KEY                @"NumeroSerie"
#define CORREO_KEY                      @"Correo"
#define COMPANIA_CELULAR_KEY            @"CompaniaCelular"

@implementation PendienteDescarga


- (id)initWithCoder:(NSCoder *)coder {
    self = [super init];
    _numeroTarjeta = [coder decodeObjectForKey:NUMERO_TARJETA_KEY];
    _numeroTelefono = [coder decodeObjectForKey:NUMERO_TELEFONO_KEY];
    _numeroCliente = [coder decodeObjectForKey:NUMERO_CLIENTE_KEY];
    _tipoSolicitud = [coder decodeObjectForKey:TIPO_SOLICITUD_KEY];
    _nombreToken = [coder decodeObjectForKey:NOMBRE_TOKEN_KEY];
    _numeroSerie = [coder decodeObjectForKey:NUMERO_SERIE_KEY];
    _correo = [coder decodeObjectForKey:CORREO_KEY];
    _companiaCelular = [coder decodeObjectForKey:COMPANIA_CELULAR_KEY];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
    [coder encodeObject:_numeroTarjeta forKey:NUMERO_TARJETA_KEY];
    [coder encodeObject:_numeroTelefono forKey:NUMERO_TELEFONO_KEY];
    [coder encodeObject:_numeroCliente forKey:NUMERO_CLIENTE_KEY];
    [coder encodeObject:_tipoSolicitud forKey:TIPO_SOLICITUD_KEY];
    [coder encodeObject:_nombreToken forKey:NOMBRE_TOKEN_KEY];
    [coder encodeObject:_numeroSerie forKey:NUMERO_SERIE_KEY];
    [coder encodeObject:_correo forKey:CORREO_KEY];
    [coder encodeObject:_companiaCelular forKey:COMPANIA_CELULAR_KEY];
}

@end
