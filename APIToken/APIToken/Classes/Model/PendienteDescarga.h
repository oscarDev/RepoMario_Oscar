//
//  PendienteDescarga.h
//  SuiteBancomer
//
//  Created by JuanRa Marin  on 15/10/13.
//  Copyright (c) 2013 GoNet. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PendienteDescarga : NSObject<NSCoding>

@property (nonatomic, copy) NSString* numeroTarjeta;
@property (nonatomic, copy) NSString* numeroTelefono;
@property (nonatomic, copy) NSString* numeroCliente;
@property (nonatomic, copy) NSString* tipoSolicitud;
@property (nonatomic, copy) NSString* nombreToken;
@property (nonatomic, copy) NSString* numeroSerie;
@property (nonatomic, copy) NSString* correo;
@property (nonatomic, copy) NSString* companiaCelular;
@end
