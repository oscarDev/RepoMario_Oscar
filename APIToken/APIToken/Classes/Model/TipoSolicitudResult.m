//
//  TipoSolicitudResult.m
//  Bancomer
//
//  Created by Francisco.Garcia on 27/05/13.
//
//

#import "TipoSolicitudResult.h"
#import "Tools.h"
#import "ParserJSON.h"

#define ESTATUS_BMOVIL_TAG                                  @"estatusBmovil"
#define VALIDACION_ALERTAS_TAG                              @"validacionAlertas"
#define NUMERO_ALERTAS_TAG                                  @"numeroAlertas"
#define COMPANIA_ALERTAS_TAG                                @"companiaAlertas"
#define NUMERO_CLIENTE_TAG                                  @"numeroCliente"
#define INDICADOR_CONTRATACION_TAG                          @"indicadorContratacion"
#define NUMERO_SERIE_TOKEN_TAG                              @"numSerieToken"
#define TIPO_INSTRUMENTO_TAG                                @"tipoInstrumento"
#define CORREO_ELECTRONICO_TAG                              @"correoElectronico"
#define NOMBRE_CLIENTE_TAG                                  @"nombreCliente"
#define SWITCH_ENROLAMIENTO_TAG                             @"switchEnrolamiento"
#define DISPOSITIVO_FISICO_TAG                              @"dispositivoFisico"
#define ESTATUS_DISPOSITIVO_TAG                              @"estatusDispositivo"

@implementation TipoSolicitudResult

/**
 * Deallocates used memory
 */

/**
 * Parses the raw data. Does nothing, as this class doesn't parse raw data
 *
 * @param aData The data received from the server to be parsed
 */
- (void) parseData: (NSData*) aData {
}


-(void)processParser:(Parser *)aParser{
    
    ParserJSON *parserJSON = (ParserJSON*)aParser;
    
    _estatusBmovil = [[parserJSON parseNextValueForTag:ESTATUS_BMOVIL_TAG] copy];
    _validacionAlertas = [[parserJSON parseNextValueForTag:VALIDACION_ALERTAS_TAG] copy];
    _numeroAlertas = [[parserJSON parseNextValueForTag:NUMERO_ALERTAS_TAG] copy];
    _companiaAlertas = [[parserJSON parseNextValueForTag:COMPANIA_ALERTAS_TAG] copy];
    _numeroCliente = [[parserJSON parseNextValueForTag:NUMERO_CLIENTE_TAG] copy];
    _indicadorContratacion = [[parserJSON parseNextValueForTag:INDICADOR_CONTRATACION_TAG] copy];
    _numSerieToken = [[parserJSON parseNextValueForTag:NUMERO_SERIE_TOKEN_TAG] copy];
    _tipoInstrumento = [[parserJSON parseNextValueForTag:TIPO_INSTRUMENTO_TAG] copy];
    _correoElectronico = [[parserJSON parseNextValueForTag:CORREO_ELECTRONICO_TAG] copy];
    _nombreCliente = [[parserJSON parseNextValueForTag:NOMBRE_CLIENTE_TAG] copy];
    _switchEnrolamiento = [[parserJSON parseNextValueForTag:SWITCH_ENROLAMIENTO_TAG] copy];
    _dispositivoFisico = [[parserJSON parseNextValueForTag:DISPOSITIVO_FISICO_TAG] copy];
    _estatusDispositivo = [[parserJSON parseNextValueForTag:ESTATUS_DISPOSITIVO_TAG] copy];
}

-(BOOL)usesParser{
    return YES;
}


@end
