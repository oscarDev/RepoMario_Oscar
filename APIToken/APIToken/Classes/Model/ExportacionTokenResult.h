//
//  ExportacionTokenResult.h
//  Bancomer
//
//  Created by Francisco.Garcia on 28/05/13.
//
//

#import <UIKit/UIKit.h>
#import "ParsingHandler.h"
@interface ExportacionTokenResult : NSObject<ParsingHandler>

@end
