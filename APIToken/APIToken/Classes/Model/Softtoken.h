//
//  Softtoken.h
//  Bancomer
//
//  Created by Borja Esteban Pascual on 15/05/14.
//  Copyright (c) 2014 GoNet. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Softtoken : NSObject

@property (nonatomic, readwrite, copy) NSString *numCelular;
@property (nonatomic, readwrite, copy) NSString *companiaCelular;
@property (nonatomic, readwrite, copy) NSString *numTarjeta;
@property (nonatomic, readwrite, copy) NSString *numeroCliente;
@property (nonatomic, readwrite, copy) NSString *tipoSolicitud;
@property (nonatomic, readwrite, copy) NSString *nombreToken;
@property (nonatomic, readwrite, copy) NSString *numeroSerie;
@property (nonatomic, readwrite, copy) NSString *correo;
@property (nonatomic, readwrite, copy) NSString *nombreCliente;
@property (nonatomic, readwrite, copy) NSString *estatusValidacion;
@property (nonatomic, readwrite, copy) NSString *numAlertas;
@property (nonatomic, readwrite, copy) NSString *companiaAlertas;
@property (nonatomic, readwrite, copy) NSString *switchEnrolamiento;
@property (nonatomic, readwrite, copy) NSString *versionApp;

-(id)initWithNumCelular:(NSString*)aNumCelular andCompaniaCelular:(NSString*)aCompaniaCelular andNumTarjeta:(NSString*)aNumTarjeta andNumeroCliente:(NSString*)aNumeroCliente andTipoSolicitud:(NSString*)aTipoSolicitud andNombreToken:(NSString*)aNombreToken andNumeroSerie:(NSString*)aNumeroSerie andCorreo:(NSString*)aCorreo andNombreCliente:(NSString*)aNombreCliente
;

@end
