/**
 * @file
 * This file contains the definition of the class MTCoreFactory.
 */
// Copyright (c) 2011 Open Communications Security S/A. All rights reserved.
// $Id$
#import <Foundation/Foundation.h>
#import <mtcore-ios/MTAppCore.h>
#import <mtcore-ios/MTSerialActivation.h>
#import <mtcore-ios/MTTransactionSigning.h>
#import <mtcore-ios/MTOpticalMessage.h>

/**
 * This macro contains the version string of the API.
 */
#define MTCoreFactory_API_VERSION "5.1.3.0"

/**
 * This static class implements the object factory used to create the instances of the
 * main classes of the @prodname.
 *
 * @note Do not create instances of this class.
 * @since 5.0
 * @author Fabio Jun Takda Chino
 * @author Rafael Teixeira
 * @author Cesar Ferraz 
 */
@interface MTCoreFactory : NSObject

/**
 * Creates a new instance of the application core for the specified device using
 * the specified database.
 *
 * @param[in] deviceId A string with that represents a unique identifier for the device.
 * This value must be the same all the times or the database will not work well.
 * @param[in] dataDir The directory that will be used by this core as the database.
 * @return The core instance or nil for failure.
 * @note The new instance is added to the autorelease pool, given that, do not forget to retain
 * this instance if required.
 * @warning Do not instantiate more than one core using the same database at same time.
 */
+ (id <MTAppCore>) createCore: (NSString *) deviceId usingDatabase: (NSString *) dataDir;


/**
 * Creates a new instance of the application core for the specified device using
 * the specified database and seting the maximum number allowed of login attempts.  
 * (anything between 1 and 10)
 *
 * @param[in] deviceId A string with that represents a unique identifier for the device.
 * This value must be the same all the times or the database will not work well.
 * @param[in] dataDir The directory that will be used by this core as the database.
 * @param[in] nMaxNumberLoginAttempts the maximum number allowed of login attempts. (anything between 1 and 10)
 * @return The core instance or nil for failure.
 * @note The new instance is added to the autorelease pool, given that, do not forget to retain
 * this instance if required.
 * @warning Do not instantiate more than one core using the same database at same time.
 */
+ (id <MTAppCore>) createCoreWithMaxLoginAttempts: (NSString *) deviceId usingDatabase: (NSString *) dataDir andMaxNumberLoginAttempts: (int) nMaxNumberLoginAttempts;


/**
 * Creates a non initialized instance of the protocol MTSerialActivation.
 *
 * @return A new non initialized instance of the a MTSerialActivation instance.
 * @note The new instance is added to the autorelease pool, given that, do not forget to retain
 * this instance if required.
 */
+ (id <MTSerialActivation>) createSerialActivation;

/**
 * Creates a new instance of the protocol MTTransactionSignign based on a given start code.
 *
 * @param[in] startCode The start code that describes the transaction signature.
 * @return The new instance or nil in case of failure (low memory or an invalid start code).
 * @note The new instance is added to the autorelease pool, given that, do not forget to retain
 * this instance if required.
 */
+ (id <MTTransactionSigning>) createTransactionSigningForStartCode: (NSString *) startCode;

/**
 * Deletes the database located on the given path. This method may be used to reset
 * the application when the database is dead or corrupted.
 *
 * @param[in] dataDir The path to the token database.
 * @return YES for success or NO otherwise.
 * @warning Do not invoke this method if an instance of the MTAppCore is using 
 * this database.
 */
+ (BOOL) resetDatabase: (NSString *) dataDir;

/**
 * Decodes an OCSBase32 string.
 *
 * @param[in] contents The OCSBase32 string.
 * @return An NSData instance with the decoded data or nil in case of failure.
 * @since 2013.02.26
 */
+ (NSData *) decodeOCSBase32: (NSString *) contents;

/**
 * Creates a new instance of optical message.
 *
 * @param[in] contents The message read from the QRCode.
 * @return A new instance of MTOpticalMessage or nil in case of failure.
 * @since 2013.02.26
 */
+ (id <MTOpticalMessage>) createOpticalMessageWithContents: (NSString *) contents;

/**
 * Creates an Optical Message with an array of bytes
 * @param the byte array used to create the Optical Message
 * @return an Optical Message
 * @since 2015.02.26
 */
+ (id <MTOpticalMessage>) createOpticalMessageWithData: (NSData *) data;

/**
 * Creates a new instance of the protocol MTTransactionSigning based on a given opticalMessage.
 *
 * @param[in] opticalMessage The optical message read from the QRCode used to display the Transaction Signing parameters
 * @return The new instance of the Transaction Signing or nil in case of failure .
 * @since 2013.02.26 
 */
+ (id <MTTransactionSigning>) createTransactionSigningForOpticalMessage: (id <MTOpticalMessage>) opticalMessage;

/**
 * Verifies if the library is running on 64 bit processor.
 * @return YES if the library is running on a 64 bit processor or false otherwise.
 * @since 2015.03.24
 */
+ (BOOL) is64Bit;

/**
 * Sets the legacy mode state. The legacy mode is set to NO by default.
 *
 * <p>The legacy mode enables the backward compatibility with the deprecated 
 * 5.0.x.y series. This mode must be enabled if and only if the previous version
 * of your application is using the deprecated 5.0.x.y API.</p>
 *
 * <p>This method must be called before the first execution of createCoreWith or 
 * createCoreWithMaxLoginAttempts.</p>
 *
 * @param[in] legacyMode Set to YES to enable the legacy mode or NO otherwise.
 * @since 2015.03.27
 * @warning Do not turn legacy mode on unless you receive explicity instructions
 * to do so in your application. Turning this mode on incorrectly will lead to 
 * the loss of all existing tokens after the upgrade of your application.
 */
+ (void) setLegacyMode: (BOOL) legacyMode;

@end
