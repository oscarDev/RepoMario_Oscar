/**
 * @file
 * This file contains the definition of the class MTOpticalMessageType.
 */
// Copyright (c) 2011-2013 Open Communications Security S/A. All rights reserved.
// $Id: MTCoreFactory.h 1291 2013-02-27 13:23:40Z cferraz@opencs.com.br $
#import <Foundation/Foundation.h>
#import <mtcore-ios/MTOpticalMessageTag.h>

/**
 * This type defines the known optical message types.
 *
 * @author Cesar Ferraz
 * @author Fabio Jun Takada Chino
 * @since 2013.02.19
 */
typedef enum MTOpticalMessageType {
    /**
     * Type of the transaction signing optical message.
     */
    MTOpticalMessageTS = 0,
    /**
     * Type of the OCRA optical message.
     */
    MTOpticalMessageOCRA = 1,
    /**
     * Unknown type of optical message.
     */
    MTOpticalMessageUnsupported = -1,
} MTOpticalMessageType;

/**
 * This interface defines the optical message. Instances of this interface may be aquired by calling MTCoreFactory.createOpticalMessageWithContents.
 *
 * @author Cesar Ferraz
 * @author Fabio Jun Takada Chino
 * @since 2013.02.19
 */
@protocol MTOpticalMessage  <NSObject>
@required
/**
 * The number of tags inside this optical message.
 */
@property (nonatomic, readonly) NSInteger tagCount;

/**
 * The 16-bit magic associated with this optical message.
 */
@property (nonatomic, readonly) NSInteger magic;

/**
 * The version associated with this optical message.
 */
@property (nonatomic, readonly) NSInteger version;

/**
 * The type of this optical message. This type is based on the value of magic found in the optical message.
 */
@property (nonatomic, readonly) MTOpticalMessageType type;

/**
 * This function retrieves an Optical message tag given an index
 *
 * @param[in] index The index of the searched tag.
 * @return an OpticalMessageTag type
 */
- (id <MTOpticalMessageTag>) getTagAt: (int) index;

@end
