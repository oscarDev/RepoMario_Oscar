/**
 * @file
 * This file contains the definition of the class MTOpticalMessageTag.
 */
// Copyright (c) 2011-2013 Open Communications Security S/A. All rights reserved.
// $Id: MTCoreFactory.h 1291 2013-02-27 13:23:40Z cferraz@opencs.com.br $
#import <Foundation/Foundation.h>

/**
 * This interface defines the optical message tag.
 *
 * @author Cesar Ferraz
 * @since 2013.02.19
 */
@protocol MTOpticalMessageTag  <NSObject>
@required

/**
 * The name of the tag.
 */
@property (nonatomic, readonly) NSInteger name;

/**
 * The value of the tag.
 */
@property (nonatomic, readonly) NSString * value;
@end
