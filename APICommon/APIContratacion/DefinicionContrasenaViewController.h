//
//  ContratacionClavesViewController.h
//  SuiteBancomer
//
//  Created by JuanRa Marin  on 10/07/13.
//  Copyright (c) 2013 GoNet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EditableViewController.h"

@class ContratacionDelegate;


#define DEFINICION_CONTRASENA_XIB     @"DefinicionContrasenaView"
/**
 * Internal service payment information being edited enumerate
 */
typedef enum {
	cciee_None = 0, //!<No information is being edited
	cciee_Cont, //!<Contraseña text is being edited
    cciee_ConfCont, //!<Confirmacion contraseña text is being edited
    cciee_Correo, //!<Correo text is being edited
    cciee_ConfCorreo //!<Confrimacion correo text is being edited
}ContratacionClavesInformationEditingEnum;

@interface DefinicionContrasenaViewController : EditableViewController
{
    ContratacionClavesInformationEditingEnum _infoEdited;
}
@property (strong, nonatomic) IBOutlet UILabel *lblCont;
@property (strong, nonatomic) IBOutlet UITextField *txtCont;
@property (strong, nonatomic) IBOutlet UILabel *lblConfCont;
@property (strong, nonatomic) IBOutlet UITextField *txtConfCont;
@property (strong, nonatomic) IBOutlet UILabel *lblCorreo;
@property (strong, nonatomic) IBOutlet UITextField *txtCorreo;
@property (strong, nonatomic) IBOutlet UILabel *lblInfo;
@property (strong, nonatomic) IBOutlet UILabel *lblConfCorreo;
@property (strong, nonatomic) IBOutlet UITextField *txtConfCorreo;
@property (nonatomic,strong) ContratacionDelegate *delegateContratacion;
@property (nonatomic) BOOL vieneDeContratacion;

- (id)initWithArray:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil array:(NSMutableArray*)array;

@end
