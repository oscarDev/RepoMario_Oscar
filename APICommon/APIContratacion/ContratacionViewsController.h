//
//  ContratacionViewsController.h
//  Bancomer
//
//  Created by Mike on 22/06/15.
//  Copyright (c) 2015 GoNet. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ContratacionDelegate;
@class InformacionBmovilConSinTokenViewController;
@class IngresaDatosViewController;
@class DefinicionContrasenaViewController;
@class ConsultaEstatus;

@interface ContratacionViewsController : NSObject

@property(nonatomic,strong)ContratacionDelegate *delegateContratacion;
@property(nonatomic,strong)IngresaDatosViewController *controladorIngresaDatos;
@property(nonatomic,strong)DefinicionContrasenaViewController *controladorDefinicionContrasena;
@property(nonatomic,strong)InformacionBmovilConSinTokenViewController *controladorInformacionBmovilConSinToken;

+ (ContratacionViewsController*) getInstance;

-(void)showIngresaDatosConConsultaEstatus:(ConsultaEstatus*)consultaEstatus
                            andBorraDatos:(BOOL)borraDatos;

-(void)showDefinicionContrasena;

-(void)showInformacionBmovilConSinToken;

@end
