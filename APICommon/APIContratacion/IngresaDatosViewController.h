//
//  ContratacionViewController.h
//  SuiteBancomer
//
//  Created by JuanRa Marin  on 09/07/13.
//  Copyright (c) 2013 GoNet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EditableViewController.h"
#import "SeleccionHorizontalViewController.h"
//#import "UICustomSwitch.h"

@class SeleccionHorizontalViewController;
@class ContratacionDelegate;
@class ListaDatosViewController;
#define INGRESA_DATOS_XIB                            @"IngresaDatosView"

@interface IngresaDatosViewController : EditableViewController <SeleccionHorizontalDelegate>

@property (strong, nonatomic) IBOutlet UILabel *lblTitulo;
@property(nonatomic,strong)ListaDatosViewController *controladorListaDatos;
@property (strong, nonatomic) IBOutlet UILabel *lblSeleccionHorizontal;
@property (strong, nonatomic) IBOutlet UILabel *lblTarjeta;
@property (strong, nonatomic) IBOutlet UITextField *txtTarjeta;
@property (strong, nonatomic) IBOutlet UIButton *btnNuevaCuenta;

@property (nonatomic, strong) SeleccionHorizontalViewController *controladorSeleccionHorizontal;
@property (nonatomic,strong) ContratacionDelegate *delegateContratacion;
//@property (strong, nonatomic) IBOutlet UILabel *lblOperarConToken;
@property (strong, nonatomic) IBOutlet UIButton *btnInfoContratacion;
//@property (strong, nonatomic) IBOutlet UIView *contenedorSwitchOperarConToken;

//- (void) showMessage;
- (void) showMessageWithText: (NSString*)text;
- (void) showMessageWithTextAndGoLogin: (NSString*)text;
- (IBAction)btnNuevaCuenta:(id)sender;
@end
