//
//  Contratacion.h
//  SuiteBancomer
//
//  Created by JuanRa Marin  on 13/08/13.
//  Copyright (c) 2013 GoNet. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
#import "ParsingHandler.h"

@interface Contratacion : NSObject<ParsingHandler,NSCopying>

@property (nonatomic, readwrite) Perfil perfil;
@property (nonatomic, readwrite, copy) NSString *companiaCelular;
@property (nonatomic, readwrite, copy) NSString *numCelular;
@property (nonatomic, readwrite, copy) NSString *numeroCuenta;

@property (nonatomic, readwrite, copy) NSString *emailCliente;
@property (nonatomic, readwrite, copy) NSString *estatusAlertas;
@property (nonatomic, readwrite, copy) NSString *tipoInstrumento;
@property (nonatomic, readwrite, copy) NSString *estatusInstrumento;
@property (nonatomic, readwrite, copy) NSString *fechaContratacion;
@property (nonatomic, readwrite, copy) NSString *fechaModificacion;
@property (nonatomic, readwrite, copy) NSString *cveAcceso;
@property (nonatomic, readwrite, copy) NSString *numeroTarjeta;
@property (nonatomic, readwrite, copy) NSString *tipoTarjeta;
@property (nonatomic, readwrite, copy) NSString *monoProducto;
@property (nonatomic, readwrite, copy) NSString *validacionAlertas;
@property (nonatomic, readwrite, copy) NSString *numeroAlertas;
@property (nonatomic, readwrite, copy) NSString *companiaAlertas;
@property (nonatomic, readwrite, copy) NSString *tipoPersona;
@property (nonatomic, readwrite, copy) NSString *contrasena;
@property(nonatomic)BOOL ivr;



-(id)initWithPerfil:(Perfil)aPerfil andCompaniaCelular:(NSString*)aCompaniaCelular andNumCell:(NSString*)aNumCell andNumeroCuenta:(NSString*)aNumeroCuenta andEmailCliente:(NSString*)aEmailCliente andEstatusAlertas:(NSString*)aEstatusAlertas andTipoInstrumento:(NSString*)aTipoInstrumento andEstatusInstrumento:(NSString*)aEstatusInstrumento andAFechacontratacion:(NSString*)aFechaContratacion andFechaModificacion:(NSString*)aFechaModificacion andNumeroTarjeta:(NSString*)aNumeroTarjeta andTipoPersona:(NSString*)aTipoPersona andValidacionAlertas: (NSString*) aValidacionAlertas
;

@end
