//
//  ContratacionDelegate.h
//  SuiteBancomer
//
//  Created by JuanRa Marin  on 12/08/13.
//  Copyright (c) 2013 GoNet. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DelegateBaseAutenticacion.h"
#import "PopUpImageViewController.h"
#import "Constants.h"
#import "AlertaIncVC.h"
#import "AlertaEnvio.h"

@class IngresaDatosViewController;
@class DefinicionContrasenaViewController;
@class Contratacion;
@class ConsultaEstatus;
@class ContratacionAutenticacionViewController;
@class KeyChainHandler;

@interface ContratacionDelegate : DelegateBaseAutenticacion<UIAlertViewDelegate, PopUpImageAlertViewDelegate>
{
    NSInteger _downloadId;
}

@property (nonatomic,strong) IngresaDatosViewController *controladorIngresaDatos;
@property (nonatomic,strong) DefinicionContrasenaViewController *controladorDefinicionContrasena;
@property (nonatomic,strong) Contratacion *objectContratacion;
@property (nonatomic,strong) Contratacion *contratacionTemporal;
@property (nonatomic,strong) ConsultaEstatus *objConsultaEstatus;
@property (nonatomic,strong) ContratacionAutenticacionViewController *controladorContratacionAutenticacion;
@property(nonatomic,strong)NSString *titulo;
@property(nonatomic,strong)NSMutableArray *datosLista;
@property(nonatomic)BOOL debePedirContrasena;
@property(nonatomic,strong)NSString *textoLblContrasena;
@property(nonatomic)BOOL debePedirNIP;
@property(nonatomic,strong)NSString *textoLblNIP;
@property(nonatomic,strong)NSString *textoAyudaNIP;
@property(nonatomic)BOOL debePedirInstrumento;
@property(nonatomic,strong)NSString* tipoInstrumentoSeguridad;
@property(nonatomic,strong)NSString *textoLblInstrumento;
@property(nonatomic,strong)NSString *textoAyudaInstrumento;
@property(nonatomic)BOOL debePedirCVV;
@property(nonatomic,strong)NSString *textoLblCVV;
@property(nonatomic,strong)NSString *textoAyudaCVV;
@property(nonatomic,strong)NSString *numeroTarjeta;
@property(nonatomic)BOOL debePedirTarjeta;
@property(nonatomic,strong)NSString *perfilDeterminado;
@property(nonatomic,strong)NSString *textoTarjeta;
@property(nonatomic,strong)NSString* nombreCompania;

@property(nonatomic) NSInteger tipoOperacion;


@property(nonatomic,strong)NSString *instrumentoPassword;
@property(nonatomic,strong)NSString *instrumentoNip;
@property(nonatomic,strong)NSString *instrumentoToken;
@property(nonatomic,strong)NSString *instrumentoCVV;
@property(nonatomic,strong)NSString *instrumentoNumerosTarjeta;
@property(nonatomic)TipoOtpAutenticacion tipoOTP;

@property (nonatomic, strong) KeyChainHandler *keychainHandler;

- (id)init;

-(void)doNetworkOperationType:(NSInteger)aType conPassword:(NSString*)aPassword nip:(NSString*)aNip token:(NSString*)aToken cvv:(NSString *)aCVV numerosTarjeta:(NSString *)numerosTarjeta;
-(void)analyzeServerResponse: (ServerResponse*) aServerResponse andType:(NSInteger)aType;
-(BOOL)validaTarjeta;
-(BOOL)validarDatosIngresaDatos;
-(BOOL)validaContrasena;
-(BOOL)validaCorreo;
-(void)realizarMantenimientoAlertas;
-(NSMutableArray*)getDatosListaIngresa;
-(NSMutableArray*)getListaTelefonicas;
-(NSString*)getTitulo;
-(NSMutableArray*)getDatosListaAutenticacion;
-(BOOL)getDebePedirContrasena;
-(NSString*)getTextoLblContrasena;
-(BOOL)getDebePedirNIP;
-(NSString*)getTextoLblNIP;
-(NSString*)getTextoAyudaNIP;
-(BOOL)getDebePedirInstrumento;
-(NSString*)getTextoLblInstrumento;
-(NSString*)getTextoAyudaInstrumento;
-(BOOL)getDebePedirCVV;
-(NSString*)getTextoLblCVV;
-(NSString*)getTextoAyudaCVV;
-(Contratacion*)getObjetoContratacion;
-(void)setCveAcceso:(NSString*)cveAcceso andMail:(NSString*)eMail;
-(void)borraDatos;
-(void)showInformationWithMessage:(NSString*)aMessage;

- (BOOL)activarTokenMovil;
@end
