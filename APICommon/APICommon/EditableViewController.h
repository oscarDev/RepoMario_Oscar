/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */


#import "BaseViewController.h"
#import "PopButtonsView.h"



/**
 * Base view controller to be used by view controllers containing editable elements.
 * Uses parent class scroller to make controls visible when the keyboard or
 * another obstructing view is present (keyboard or picker view).
 * A pop buttons view is also provided to allow dismissing the obstructing view, move from one editable
 * control to another, and show available texts similar to the one being edited. All the events related
 * to this actions must be implemented by subclasses
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface EditableViewController : BaseViewController <PopButtonsViewDelegate, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource> {
@private
	/**
	 * Current text field being edited. Used when switching from keyboard to picker on predictive text events
	 */
	UITextField* _currentTextView;
	
	/**
	 * Predictive text array to show
	 */
	NSArray* _predictiveText;
	
	/**
	 * Current text from text view
	 */
	NSString* _currentTextViewText;
	
@protected
    
	/**
	 * Pop buttons view to complement the keyboard
	 */
	PopButtonsView* _popButtonsView;
	
	/**
	 * Showing predictive information flag. YES when picker containing predictive information is shown
	 */
	BOOL _showingPredictiveInfo;
	
	/**
	 * View currently being edited. Child view must set this attribute before animation starts to
	 * allow the subview to automatically move the view up or down
	 */
	UIView* _editedView;
    
	/**
	 * General picker used by child classes
	 */
	UIPickerView* _picker;
}

-(void) removeAvailableTextsButtonInPopButtonsView;
- (void) subviewAppear;
- (void) subviewDisappear;
@end
