//
//  Compania.h
//  SuiteBancomer
//
//  Created by Rubén Jacobo on 04/06/13.
//  Copyright (c) 2013 GoNet. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
	Compania_Tiempo_Aire,
	Compania_Dinero_Movil
} TiposCompania;

@interface Compania : NSObject<NSCoding,NSCopying>

/**
 * Provides read-write access to the nombre
 */
@property (nonatomic, readwrite, copy) NSString *nombre;

/**
 * Provides read-write access to the imagen
 */
@property (nonatomic, readwrite, copy) NSString *imagen;

/**
 * Provides read-write access to the clave
 */
@property (nonatomic, readwrite, copy) NSString *clave;

/**
 * Provides read-write access to the importes
 */
@property (nonatomic, readwrite, copy) NSMutableArray *importes;

/**
 * Provides read-write access to the orden
 */
@property (nonatomic, readwrite) int orden;

-(id)initWithNombre:(NSString*)aNombre imagen:(NSString*)aImagen clave:(NSString*)aClave importes:(NSMutableArray*)aImportes orden:(int)aOrden;

@end
