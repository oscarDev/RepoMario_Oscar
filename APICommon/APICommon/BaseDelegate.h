//
//  BaseDelegate.h
//  SuiteBancomer
//
//  Created by Rubén Jacobo on 20/05/13.
//  Copyright (c) 2013 GoNet. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Server.h"

@class ServerResponse;

@interface BaseDelegate : NSObject <ServerClient>

- (void)networkResponse:(ServerResponse *)aServerResponse;
- (void)serverError;
- (void)doGenericErrorAction:(ServerResponse *)aServerResponse;
- (void)doGenericErrorActionWithoutErrorCode:(ServerResponse *)aServerResponse;

@end
