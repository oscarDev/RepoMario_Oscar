/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */


/**
 * Defines a check account type
 */
#define CHECK_TYPE										@"CH"

/**
 * Defines a "libreton" account type
 */
#define LIBRETON_TYPE									@"LI"

/**
 * Defines a savings account type
 */
#define SAVINGS_TYPE									@"AH"

/**
 * Defines a credit account type
 */
#define CREDIT_TYPE										@"TC"

/**
 * Defines a debit account type
 */
#define DEBIT_TYPE										@"TD"

/**
 * Defines a debit account type
 */
#define CLABE_TYPE										@"CL"

/**
 * Defines a prepaid account type
 */
#define PREPAID_TYPE									@"TP"

/**
 * Defines a express account type
 */
#define EXPRESS_TYPE									@"CE"


/**
 * Wraps the dat needed to define a Bancomer account in MBanking application
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface Account : NSObject<NSCopying> {
@private
	/**
	 * Account number
	 */
	NSString* _number;
	
	/**
	 * Current balance
	 */
	NSString* _balance;
	
	/**
	 * Current balance date
	 */
	NSString* _date;
	
	/**
	 * Flag to determine if account is visible
	 */
	BOOL _visible;
	
	/**
	 * Account currency
	 */
	NSString* _currency;
	
	/**
	 * Account type
	 */
	NSString* _type;
	
	/**
	 * Account concept
	 */
	NSString* _concept;
    
    /**
	 * Account alias
	 */
	NSString* _alias;
    
    /**
     * Número de teléfono móvil
     */
    NSString* _celularAsociado;
    
    /**
     * Clave de la compañía
     */
    NSString* _codigoCompania;
    
    /**
     * Nombre de la compañía
     */
    NSString* _descripcionCompania;
    
    /**
     * Última fecha de modificación
     */
    NSString* _fechaUltimaModificacion;
    
    /**
     * Indicador de Servicio SPEI activo
     */
    NSString* _indicadorSPEI;
}

/**
 * Provides read-write access to the account number
 */
@property (nonatomic, readwrite, copy) NSString* number;

/**
 * Provides read-only access to the account full number
 */
@property (nonatomic, readonly, copy) NSString* fullNumber;

/**
 * Provides read-only access to the account public name, composed of account type, currency
 * and account last digits
 */
@property (nonatomic, readonly, copy) NSString* publicName;

/**
 * Provides read-write access to the account balance
 */
@property (nonatomic, readwrite, copy) NSString* balance;

/**
 * Provides read-write access to the account balance date
 */
@property (nonatomic, readwrite, copy) NSString* date;

/**
 * Provides read-write access to the accout visible flag
 */
@property (nonatomic, readwrite) BOOL isVisible;

/**
 * Provides read-write access to the account currency
 */
@property (nonatomic, readwrite, copy) NSString* currency;

/**
 * Provides read-write access to the account type
 */
@property (nonatomic, readwrite, copy) NSString* type;

/**
 * Provides read-write access to the account concept
 */
@property (nonatomic, readwrite, copy) NSString* concept;

/**
 * Provides read-write access to the account alias
 */
@property (nonatomic, readwrite, copy) NSString* alias;

@property (nonatomic, readwrite, copy) NSString* cuentaOrigenPublicName;

/**
 * Provides read-write access to cellphone number
 */
@property (nonatomic, readwrite, copy) NSString* celularAsociado;

/**
 * Provides read-write access to company id
 */
@property (nonatomic, readwrite, copy) NSString* codigoCompania;

/**
 * Provides read-write access to company name
 */
@property (nonatomic, readwrite, copy) NSString* descripcionCompania;

/**
 * Provides read-write access to last update
 */
@property (nonatomic, readwrite, copy) NSString* fechaUltimaModificacion;

/**
 * Provides read-write access to SPEI indicator
 */
@property (nonatomic, readwrite, copy) NSString* indicadorSPEI;


/**
 * Desiganted initialized. Initializes the Account instance with provided information
 *
 * @param aNumber The account number to store
 * @param aBalance The account balance to store
 * @param aDate The account balance date to store
 * @param anIsVisible The account visible flag to store
 * @param aCurrency The account currency to store
 * @param aType The account type to store
 * @param aConcept The account concept to store
 * @param anAlias The account alias to store
 * @return The initialized Account instance
 */
- (id) initWithNumber: (NSString*) aNumber balance: (NSString*) aBalance date: (NSString*) aDate isVisible: (BOOL) anIsVisible
             currency: (NSString*) aCurrency type: (NSString*) aType andConcept: (NSString*) aConcept andAlias:(NSString*)anAlias cellphone:(NSString*)aCellpone companyCode:(NSString*)aCompanyCode companyName:(NSString*)aCompanyName lastUpdate:(NSString*)aLastUpdate speiIndicator:(NSString*)aSpeiIndicator;

-(BOOL) isExpressType;
-(NSString*)getName;



@end
