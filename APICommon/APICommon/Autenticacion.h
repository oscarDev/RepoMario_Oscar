//
//  Autenticacion.h
//  SuiteBancomer
//
//  Created by ARTEMIO OLVERA MEDINA on 14/06/13.
//  Copyright (c) 2013 GoNet. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
#import "PerfilAutenticacion.h"

@interface Autenticacion : NSObject<NSCoding,NSCopying>

@property (nonatomic)float limiteOperacion;
@property (nonatomic, strong) NSString *version;
//@property (nonatomic, strong) NSArray *operaciones;

@property (nonatomic, strong) NSArray *operacionesRecortado;
@property (nonatomic, strong) NSArray *operacionesBasico;
@property (nonatomic, strong) NSArray *operacionesAvanzado;

//-(id)initWithVersion:(NSString*)aVersion operaciones:(NSArray*)aOperaciones limite:(float)aLimite;
-(BOOL)mostrarContraseniaDeOperacion:(Operacion)operacion conPerfil:(Perfil)perfil;
-(BOOL)mostrarContraseniaDeOperacion:(Operacion)operacion conPerfil:(Perfil)perfil conImporte:(float)importe;
-(BOOL)mostrarNIPDeOperacion:(Operacion)operacion conPerfil:(Perfil)perfil;
-(BOOL)mostrarNIPDeOperacion:(Operacion)operacion conPerfil:(Perfil)perfil conImporte:(float)importe;
-(TipoOtpAutenticacion)tokenAMostrarDeOperacion:(Operacion)operacion conPerfil:(Perfil)perfil;
-(TipoOtpAutenticacion)tokenAMostrarDeOperacion:(Operacion)operacion conPerfil:(Perfil)perfil conImporte:(float)importe;
-(BOOL)mostrarCVVDeOperacion:(Operacion)operacion conPerfil:(Perfil)perfil;
-(BOOL)mostrarCVVDeOperacion:(Operacion)operacion conPerfil:(Perfil)perfil conImporte:(float)importe;
-(NSString*)getCadenaAutenticacionDeOperacion:(Operacion)operacion conPerfil:(Perfil)perfil;
-(NSString*)getCadenaAutenticacionDeOperacion:(Operacion)operacion conPerfil:(Perfil)perfil conImporte:(float)importe;
-(PerfilAutenticacion*)leeCredencialesDeOperacion:(Operacion)operacion conPerfil:(Perfil)perfil;
-(BOOL)validaRegistroDeOperacion:(Operacion)operacion conPerfil:(Perfil)perfil;
-(BOOL) isOperacionVisible:(Operacion)operacion conPerfil:(Perfil)perfil;
-(BOOL) isOperacionPermitida:(Operacion)operacion conPerfil:(Perfil)perfil;

@end
