//
//  PopUpImageViewController.h
//  Bancomer
//
//  Created by Driss on 12/11/2015.
//  Copyright (c) 2013-2015 Driss.
//

#import <UIKit/UIKit.h>


@protocol PopUpImageAlertViewDelegate

- (void)popUpButtonTouchUpInside:(id)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;

@end

@interface PopUpImageViewController : UIView<PopUpImageAlertViewDelegate>

@property (nonatomic, retain) UIView *parentView;    // The parent view this 'dialog' is attached to
@property (nonatomic, retain) UIView *dialogView;    // Dialog's container view
@property (nonatomic, retain) UIView *containerView; // Container within the dialog (place your ui elements here)
@property (nonatomic, retain) UILabel *lblTitulo;


@property (nonatomic, assign) id<PopUpImageAlertViewDelegate> delegate;
@property (nonatomic, retain) NSArray *buttonTitles;
@property (nonatomic, assign) BOOL useMotionEffects;
@property (nonatomic, assign) BOOL closable;

@property (copy) void (^onButtonTouchUpInside)(PopUpImageViewController *alertView, int buttonIndex) ;

- (id)init;
- (id)initWithTitle:(NSString*) aTitle andImageName:(NSString*) aImage andMessage:(NSString*) aMessage closable:(BOOL) aClosable;


//- (id)initWithTitle:(NSString*) aTitle andImageName:(NSString*) aImage andMessage:(NSString*) aMessage closable:(BOOL) aClosable aButton1:(UIButton*) aGenerar aButton2:(UIButton*) aCancelar;

- (void)show;
- (void)close;

- (IBAction)popUpButtonTouchUpInside:(id)sender;
- (void)setOnButtonTouchUpInside:(void (^)(PopUpImageViewController *alertView, int buttonIndex))onButtonTouchUpInside;

- (void)deviceOrientationDidChange:(NSNotification *)notification;
- (void)dealloc;
- (void)setColorTitulo:(UIColor *)color;

@end
