//
//  DelegateBaseAutenticacion.h
//  SuiteBancomer
//
//  Created by ARTEMIO OLVERA MEDINA on 11/06/13.
//  Copyright (c) 2013 GoNet. All rights reserved.
//

typedef enum tipoInstrumentoAutenticacion
{
    INSTRUMENTO_DP270_AUTENTICACION,
    INSTRUMENTO_OCRA_AUTENTICACION,
    INSTRUMENTO_SOFTTOKEN_ACTIVADO_AUTENTICACION,
    INSTRUMENTO_SOFTTOKEN_INACTIVO_AUTENTICACION,
    INSTRUMENTO_CVV_AUTENTICACION,
    INSTRUMENTO_DP270_AYUDA_REGISTRO,
    INSTRUMENTO_OCRA_AYUDA_REGISTRO
} TipoInstrumentoAutenticacion;

#import "BaseDelegate.h"

@interface DelegateBaseAutenticacion : BaseDelegate

-(NSString*)getEtiquetaCVV;//******************
-(NSString*)getTextoAyudaCVV;//************
-(void)realizaOperacion;
-(NSString*)getTextoAyudaIstrumentoSeguridad:(TipoInstrumentoAutenticacion) tipoInstrumento;
-(NSString*)getEtiquetaCampoNip;
-(NSString*)getEtiquetaCampoContrasenia;
-(NSString*)getEtiquetaCampoOCRA;
-(NSString*)getEtiquetaCampoDP270;
-(NSString*)getEtiquetaCampoSoftokenActivado;
-(NSString*)getEtiquetaCampoSoftokenDesactivado;
-(NSString*)getTextoEspecialResultados;
-(NSString*)getTextoPantallaResultados;
-(void)borraRapido;
-(NSString*)getTextoTituloResultado;
-(NSString*)getTextoTituloConfirmacion;
-(NSArray*)getDatosTablaConfirmacion;
-(NSArray*)getDatosTablaResultados;
-(NSString*)getTextoAyudaNIP;
-(BOOL)mostrarContrasenia;
-(NSString*)tokenAMostrar;
-(BOOL)mostrarNIP;
-(NSArray*)getDatosTablaAltaFrecuentes;
-(NSArray*)getDatosTablaAltaRapidos;
-(NSString*)getNombreImagenEncabezado;
-(NSString*)getTextoEncabezado;
-(NSString*)getTextoBotonOperacionNueva;

@end
