//
//  BaseViewController.h
//  SuiteBancomer
//
//  Created by Rubén Jacobo on 20/05/13.
//  Copyright (c) 2013 GoNet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HeaderView.h"

@class HeaderView;

static NSString *const IMAGEN_BOTON_MENU         = @"ios_btn_Men.png";
static NSString *const IMAGEN_BOTON_SIGUIENTE     = @"btn-14.png";
static NSString *const IMAGEN_BOTON_CONFIRMAR     = @"ios_btn_Confirmar.png";
static NSString *const IMAGEN_BOTON_ACTIVAR       = @"IoBtActivar.png";
static NSString *const IMAGEN_BOTON_DERECHO_SALIR = @"Iobtsalir.png";
static NSString *const IMAGEN_BOTON_SALIR         = @"btn-Salir.png";
#define IMAGEN_BOTON_ACEPTAR        @"IoBt_Aceptar.png"
#define IMAGEN_BOTON_CALCULAR       @"IoBtCalcular.png"
#define IMAGEN_BOTON_MEINTERESA     @"IoBtMeinteresa.png"
#define IMAGEN_BOTON_HAMBURGUESA     @"ios_Ic_menu.png"

@interface BaseViewController : UIViewController {
@private
	/**
	 * Activity indicator to show when a long term operation is running
	 */
	UIActivityIndicatorView* _activityIndicator;
	
	/**
	 * Flag indicating whether view header is present or not
	 */
	BOOL _isHeaderPresent;

@protected
	/**
	 * Transparent view to hold all subviews created from NIB file
	 */
	UIView* _transparentView;
	
	/**
	 * Transparent scroll to provide scrolling capabilities
	 */
	UIScrollView* _transparentScroll;
    
    /**
     * View header. It can be visible or hidden (default is hidden)
     */
    HeaderView* _headerView;
    
    UIAlertController *_alert;
    
    BOOL configuracionVista;
    
    NSInteger btnPulsado;
}

/**
 * Starts the activity indicator
 */
- (void)startActivityIndicator;

/**
 * Stops the activity indicator
 */
- (void)stopActivityIndicator;

//STF 20100719-INI FP
/*
 * Determines if a process is running by checking if the activity indicator is animating.
 */
- (BOOL)isProcessing;
//STF 20100719-FIN

/**
 * Sets a given text to a label, aligning the text vertically to the label top.
 * Resizes the label to fit the text in height
 *
 * @param aText The text to set
 * @param aLabel The label to show the text
 * @param aFontSize The font size
 */
+ (void)setText:(NSString *)aText toLabel:(UILabel *)aLabel withFontSize:(CGFloat)aFontSize;

/**
 * Recalculates the scroll
 */
- (void)recalculateScroll;

/**
 * Shows or hides the view header depending on the parameter provided
 *
 * @param aShowHeader YES to show header, NO otherwise
 */
- (void)showHeader:(BOOL)aShowHeader;

/**
 * Sets the header title. It will only be displayed if header is made visible
 *
 * @param aHeaderTitle The title to show in the header
 */
- (void)setHeaderTitle:(NSString *)aHeaderTitle;

/**
 * Sets the header image. It will only be displayed if header is made visible
 *
 * @param aHeaderImage The image to show in the header. A 16x16 pixels image is
 * the best resolution to use
 */
- (void)setHeaderImage:(UIImage *)aHeaderImage;

/**
 * Returns the header height
 *
 * @return The heaader height
 */
- (CGFloat)getHeaderHeight;

/**
 * Child classes must return the offset from the last control in the view to the
 * view bottom. Default is 0
 *
 * @return The offset between the last control in the view and the view bottom
 */
- (CGFloat)distanceFromLastControlToViewBottom;

/**
 * Establece la configuración inicial  de la vista
 */
- (void)configurarVista;
- (void)ocultarBotonAtras : (BOOL) aOcultar;
//modificacion
- (void)ocultaBotonDerecho:(BOOL)aOcultar;
//fin
- (void)muestraBotonDerecho:(TipoBotonDerecho)aTipo;
- (void)muestraBotonIzquierdo:(NSString *)imageName;
- (void)muestraBotonDerechoWithImagen:(NSString *)imageName;
- (void)accionBotonIzquierdo;
- (void)accionBotonDerecho;
- (void)ejecutaAccionBotonDerecho;
- (void)botonAtrasImagenSalir;
- (void)habilitarBotonIzquierdo:(BOOL)enable;
- (void)habilitarBotonDerecho:(BOOL)enable;
- (void)activarBotonDerecho:(BOOL)enable;
- (void)ocultaEncabezado: (BOOL)oculta;
- (void)setHeaderTitleTextColor: (UIColor*) aHeaderTitleColor;

- (void)showCommunicationErrorAlert;
- (void)showErrorMessage:(NSString *)anErrorMessage;
- (void)showErrorMessage:(NSString *)anErrorMessage withErrorCode:(NSString *)anErrorCode;
- (void)showInformationWithMessage:(NSString *)message;

@end
