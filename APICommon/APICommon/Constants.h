/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

/*
 * STF 20100719 ODT1 - Added all the constants needed to implement the four ODT1 requirements, each added constant has the STF 20100719 mark.
 *
 * STF ODT2 - Constants for the delete, add, or consult frequent or fast payment operations-
 *				Constant for the frequent and fast payment nickname lenght
 */

#import <Foundation/Foundation.h>

/**
 * Defines the character to mask the invisible digits in a number as a string
 */
#define MASK_STRING														@"*"


/**
 * Defines the number of visible digits in a number
 */
#define VISIBLE_NUMBER_CHARCOUNT										5


/**
 * Defines the number of visible digits in a number
 */
#define VISIBLE_NUMBER_CHARCOUNT_MIS_CUENTAS                            10

/**
 * Defines the text field default text size
 */
#define TEXT_FIELD_TEXT_SIZE											16.0f

/**
 * Defines the telephone number length (user field)
 */
#define TELEPHONE_NUMBER_LENGTH											10

/**
 * Defines the buttons title size
 */
#define BUTTONS_TITTLE_SIZE												14.0f



/**
 * Define formato de archivo tipo png
 */
#define FORMATO_PNG                                                     @"png"

/**
 * Define fondo en cuenta origen una cuenta
 */
#define FONDO_CUENTA_ORIGEN_UNA_CUENTA                                  @"imgCuentaOrigen"

/**
 * Define fondo en cuenta origen varias cuentas
 */
#define FONDO_CUENTA_ORIGEN_VARIAS_CUENTAS                              @"imgCuentaOrigenFlechas"

#define ICONO_CUENTA_ORIGEN_LIST                                        @"icon-55.png"
/*
 * Define el nombre de la fuente utilizada en la aplicación
 */
#define HELVETICA_NEUE_LT_STD_55_ROMAN                                  @"HelveticaNeueLTStd-Roman"
#define FUENTE_CUERPO                           [UIFont fontWithName:HELVETICA_NEUE_LT_STD_55_ROMAN size:16.0F];
#define FUENTE_CUERPO_2                         [UIFont fontWithName:HELVETICA_NEUE_LT_STD_55_ROMAN size:12.0F];
/*
 * Tableta colores guia de estilos.
 */
#define COLOR_1ER_AZUL                                      [UIColor colorWithRed:9.0f/255.0f green:79.0f/255.0f blue:164.0f/255.0f alpha:1.0f]
#define COLOR_2DO_AZUL                                      [UIColor colorWithRed:0.0f/255.0f green:110.0f/255.0f blue:193.0f/255.0f alpha:1.0f]
#define COLOR_3ER_AZUL                                      [UIColor colorWithRed:0.0f/255.0f green:158.0f/255.0f blue:229.0f/255.0f alpha:1.0f]
#define COLOR_4TO_AZUL                                      [UIColor colorWithRed:82.0f/255.0f green:188.0f/255.0f blue:236.0f/255.0f alpha:1.0f]
#define COLOR_5TO_AZUL                                      [UIColor colorWithRed:137.0f/255.0f green:209.0f/255.0f blue:243.0f/255.0f alpha:1.0f]
#define COLOR_6TO_AZUL                                      [UIColor colorWithRed:181.0f/255.0f green:229.0f/255.0f blue:249.0f/255.0f alpha:1.0f]

#define COLOR_MAGENTA                                       [UIColor colorWithRed:200.0f/255.0f green:23.0f/255.0f blue:94.0f/255.0f alpha:1.0f]
#define COLOR_NARANJA                                       [UIColor colorWithRed:246.0f/255.0f green:137.0f/255.0f blue:30.0f/255.0f alpha:1.0f]
#define COLOR_AMARILLO                                      [UIColor colorWithRed:253.0f/255.0f green:189.0f/255.0f blue:44.0f/255.0f alpha:1.0f]
#define COLOR_VERDE                                         [UIColor colorWithRed:134.0f/255.0f green:200.0f/255.0f blue:45.0f/255.0f alpha:1.0f]
#define COLOR_VERDE_LIMON                                   [UIColor colorWithRed:183.0f/255.0f green:194.0f/255.0f blue:4.0f/255.0f alpha:1.0f]
#define COLOR_TURQUESA                                      [UIColor colorWithRed:62.0f/255.0f green:182.0f/255.0f blue:187.0f/255.0f alpha:1.0f]

#define COLOR_GRIS_TEXTO_1                                  [UIColor colorWithRed:60.0f/255.0f green:60.0f/255.0f blue:60.0f/255.0f alpha:1.0f]
#define COLOR_GRIS_TEXTO_2                                  [UIColor colorWithRed:77.0f/255.0f green:77.0f/255.0f blue:77.0f/255.0f alpha:1.0f]
#define COLOR_GRIS_TEXTO_3                                  [UIColor colorWithRed:102.0f/255.0f green:102.0f/255.0f blue:102.0f/255.0f alpha:1.0f]
#define COLOR_GRIS_TEXTO_4                                  [UIColor colorWithRed:176.0f/255.0f green:176.0f/255.0f blue:176.0f/255.0f alpha:1.0f]
#define COLOR_GRIS_FONDO                                    [UIColor colorWithRed:245.0f/255.0f green:245.0f/255.0f blue:245.0f/255.0f alpha:1.0f]
#define COLOR_AZUL_FONDO                                    [UIColor colorWithRed:240.0f/255.0f green:249.0f/255.0f blue:254.0f/255.0f alpha:1.0f]

#define COLOR_GRIS_BORDE_TABLA                              [UIColor colorWithRed:128.0f/255.0f green:128.0f/255.0f blue:128.0f/255.0f alpha:1.0f]

/*
 * Macro para definir colores RGB en Hexadecimal
 */
#define UIColorFromRGBWithAlpha(rgbValue,a) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:a]

//enumeraciones de BaseAutenticacionDelegate
typedef enum operacion {
    contratacion,
    reactivacion,
    desbloqueo,
    login,
    transferirpropias,
    transferirbancomer, transferirbancomerf, transferirbancomerr,
    transferirinterbancaria, transferirinterbancariaf, transferirinterbancariar,
    dineromovil, dineromovilf,
    pagoservicios, pagoserviciosf, pagoserviciosr, pagoserviciosp,
    compratiempoaire, compratiempoairef, compratiempoairer,
    compracomercios,
    cancelardineromovil,
    altafrecuente,
    altarapido,
    bajafrecuente,
    bajarapido,
    cambiotelefono,
    cambiocuenta,
    cambiolimites,
    administraralertas,
    suspendercancelar,
    cambioperfil,
    configurarCorreo,
    configurarAlertas, //contratacionAlertas,
    quitarsuspension,
    MantenimientoSpeiMovil,
    solicitaAlertas,
    contratacionLink,
    cambioperfilr,
    consultalimites,
    oneClickBmovilConsumo,
    consultaTransferenciaSPEI, //Added to consultaTransferenciaSpei
    pagarTarjetaCredito,
    retirosintarjeta,
    cancelarRetirosintarjeta,
    consultarCreditos,
    // consultaCtasAlertas,
    altaModServicioAlertas,
    detalleCuentasAlertas,
    bajaServicioAlertas,
    consultaTextoPaperless,//JPD
    inhibirEnvioEstadoCuenta,
    actualizarEstatusEnvioEC,
    consultarEstadoCuenta,
    oneClicSeguros,//APISegurosGF
    consultaMovInterbancarios,
    pagoCreditoCH,
    consultarInversiones,
    oneClicDomiTDC, //APITDCDomiciliacion
    oneClicTDCAdicional, //Contratacion TDC Adicional
    posicionPat // Posición patrimonial
} Operacion;

typedef enum perfil{
    recortado,basico,avanzado
}Perfil;

typedef enum tipoOtpAutenticacion{
    ninguno,codigo,registro
}TipoOtpAutenticacion;

#define TIPO_IPHONE                             @"iPhone"

#define PERFIL_BASICO                   @"MF01"
#define PERFIL_RECORTADO                @"MF02"
#define PERFIL_AVANZADO                 @"MF03"

#define SCREEN_WIDTH                            320
#define LAT_MARGIN                              20
#define COMPONENT_WIDTH                         280
#define LBL_HEIGHT                              21
#define CORNER_RADIOUS                          3
#define BORDER_WIDTH                            1.0

#define LABEL_1_TAG 1
#define LABEL_2_TAG 2
#define LABEL_3_TAG 3

#define ANTERIOR                        @"Anterior"
#define SIGUIENTE                       @"Siguiente"
#define LISTA                           @"Lista"
#define TECLADO                         @"Teclado"
#define OK_KEY                          @"OK"

#define SI                              @"SI"
#define NO_KEY                          @"NO"
#define AVISO                           @"Aviso"
#define ACEPTAR                         @"Aceptar"
#define CANCELAR                        @"Cancelar"

#define ATRAS                           @"Atrás"

#define ERROR                           @"Error"
#define ERROR_UPPERCASE                 @"ERROR"
#define ERROR_COMUNICACIONES            @"Error de comunicaciones"

#define SELECCIONA_CUENTA               @"Selecciona cuenta"
#define CUENTA                          @"Cuenta"
#define LIBRETON                        @"Libretón"
#define TARJETA_CREDITO                 @"T. de crédito"
#define CUENTA_EXPRESS                  @"Cuenta Express"
#define CLABE                           @"Clabe"
#define PREPAGO                         @"Prepago"
#define CELULAR                         @"Celular"