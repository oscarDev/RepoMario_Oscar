//
//  SeleccionHorizontalViewController.h
//  SUITE2
//
//  Created by JuanRa Marin  on 15/05/13.
//  Copyright (c) 2013 JuanRa Marin . All rights reserved.
//

#import <UIKit/UIKit.h>

#define SELECCION_HORIZONTAL_XIB @"SeleccionHorizontalView"

@class SeleccionHorizontalViewController;

@protocol SeleccionHorizontalDelegate <NSObject>

-(void)getServicio;

@end

@interface SeleccionHorizontalViewController : UIViewController <UIScrollViewAccessibilityDelegate, UIScrollViewDelegate>
{
    NSString *selectedItem;
}

@property (strong, nonatomic) NSMutableArray *lista;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (nonatomic,strong) id<SeleccionHorizontalDelegate>delegate;

-(void)setViewFrame:(float)x andOriginY:(float)y;
-(id) getSelectedItem;
-(void)changeSelection:(UIButton *)button;
@end