//
//  EncripcionUtilities.h
//
//  Created by Driss on 15/09/15.
//

#import <Foundation/Foundation.h>

@interface EncripcionUtilities : NSObject

+ (NSString*) encriptarDatos:(NSString*) txt;

@end
