/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */


#import "EditableViewController.h"
//#import "Session.h"


/**
 * EditableViewController protected category
 */
@interface EditableViewController(protectedCategory)

/**
 * Fades in the pop buttons view to show it at a given height. If pop buttons view is already in view, only
 * transparent view position is calculated
 *
 * @param aFinalHeight The pop buttons view bottom final height, measured from window bottom
 * @param anAnimationCurve The animation curve to use
 * @param anAnimationDuration The animation duration
 */
- (void) fadeInPopToShowAtHeight: (CGFloat) aFinalHeight withAnimationCurve: (UIViewAnimationCurve) anAnimationCurve
					 andDuration: (double) anAnimationDuration;

/**
 * Fades out the pop buttons view to hide it. If pop buttons view is not in view, no operation is performed
 *
 * @param anAnimationCurve The animation curve to use
 * @param anAnimationDuration The animation duration
 */
- (void) fadeOutPopToHideWithAnimationCurve: (UIViewAnimationCurve) anAnimationCurve
								andDuration: (double) anAnimationDuration;

/**
 * Moves the pop buttons view to show from an initial height to a final height position. If pop buttons view is already
 * in view, only transparent view position is calculated
 *
 * @param anInitHeight The pop buttons view bottom initial height, measured from window bottom
 * @param aFinalHeight The pop buttons view bottom final height, measured from  window bottom
 * @param anAnimationCurve The animation curve to use
 * @param anAnimationDuration The animation duration
 */
- (void) movePopToShowFromHeight: (CGFloat) anInitHeight toHeight: (CGFloat) aFinalHeight
			  withAnimationCurve: (UIViewAnimationCurve) anAnimationCurve andDuration: (double) anAnimationDuration;

/**
 * Moves the pop buttons view to hide at a final height position. If pop buttons view is not in view no operation is performed
 *
 * @param aFinalHeight The pop buttons view bottom final height, measured from window bottom
 * @param anAnimationCurve The animation curve to use
 * @param anAnimationDuration The animation duration
 */
- (void) movePopToHideAtHeight: (CGFloat) aFinalHeight withAnimationCurve: (UIViewAnimationCurve) anAnimationCurve
				   andDuration: (double) anAnimationDuration;

/**
 * Moves the pop buttons view to a final height position. If pop buttons view is not in view no operation is performed
 *
 * @param aFinalHeight The pop buttons view bottom final height, measured from window bottom
 * @param anAnimationCurve The animation curve to use
 * @param anAnimationDuration The animation duration
 */
- (void) movePopToHeight: (CGFloat) aFinalHeight withAnimationCurve: (UIViewAnimationCurve) anAnimationCurve
			 andDuration: (double) anAnimationDuration;

/**
 * Shows the picker in case it is hidden, The picker is animated from the window bottom to the final position
 */
- (void) movePickerToShow;

/**
 * Hides the picker in case it is visible, The picker is animated to the window bottom. In case it is necesary,
 * the pop up is also hiden
 *
 * @param aHidePopUp YES to hide also the pop up, NO otherwise
 */
- (void) movePickerToHideAndPopUp: (BOOL) aHidePopUp;

/**
 * Makes the given view visible inside the scroller
 *
 * @param aView The view to make visible
 */
- (void) makeViewVisible: (UIView*) aView;

/**
 * Child classes must implement this selector to inform the maximum
 * size allowed for a given UITextField
 *
 * @param aTextField The text field to check
 * @return A positive (or zero) integer setting the maximumn size allowed, or a negative
 * intenger in case there is no limit
 */
- (NSInteger) maxSizeForTextField: (UITextField*) aTextField;

/**
 * Invoked by framework when animation finishes. If animation is a pop buttons view hide animation,
 * pop buttons view is removed from view
 *
 * @parm animationID An NSString containing an optional application-supplied identifier
 * @param finished An NSNumber object containing a Boolean value. The value is YES if the animation ran to completion before it stopped or NO if it did not
 * @param context An optional application-supplied context
 */
- (void) animationDidStop: (NSString*) animationID finished: (NSNumber*) finished context: (void*) context;

/**
 * Child classes informs editable view controller whether the provided text field contains
 * an integer number
 *
 * @param aTextField The text field to test
 * @return YES in case the text field contains an integer number, NO otherwise
 */
- (BOOL) textFieldIsInteger: (UITextField*) aTextField;

/**
 * Child classes informs editable view controller whether the provided text field contains
 * a currency value
 *
 * @param aTextField The text field to test
 * @return YES in case the text field contains a currency, NO otherwise
 */
- (BOOL) textFieldIsCurrency: (UITextField*) aTextField;

- (BOOL) textFieldContainsAlfa: (UITextField*) aTextField;

/**
 * Child classes informs editable view controller whether currency field can contain cents
 *
 * @param aTextField The text field to test
 * @return YES in case the text field currency can contain cents, NO otherwise
 */
- (BOOL) textFieldCanContainCents: (UITextField*) aTextField;

/**
 * Queries the child class about the prediction type for the current editing control
 *
 * @return The prediction type for the current editing control
 */
//- (PredictionInformationType) currentPredictionType;

/**
 * Resets the text field provided to contain a zero currency value
 *
 * @param aTextField The text field to reset
 */
- (void) resetTextFieldCurrency: (UITextField*) aTextField;

@end
