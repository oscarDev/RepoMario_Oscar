/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */


#import <UIKit/UIKit.h>


//Forward declarations
@class HeaderView;
@class BaseViewController;

typedef enum
{
    BOTON_MENU,
    BOTON_SIGUIENTE,
    BOTON_CONFIRMAR,
    BOTON_ACTIVAR,
    BOTON_DERECHO_SALIR,
    BOTON_ACEPTAR,
    BOTON_CALCULAR,
    BOTON_MEINTERESA,
}   TipoBotonDerecho;

/**
 * Provides new header views created from a NIB file
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface HeaderViewProvider : NSObject {
@private
	/**
	 * Auxiliary header view to contruct from the NIB file
	 */
	HeaderView* __strong _auxHeaderView;
}

/**
 * Provides read-write access to the auxiliary header view
 */
@property (nonatomic, readwrite, strong) IBOutlet HeaderView* auxHeaderView;

/**
 * Creates and returns a new header view constructed from a NIB file
 *
 * @return The newly created header view
 */
+ (HeaderView*) newHeaderView;

@end



/**
 * Header view used in view controllers accessed from the main menu view controller. It contains an
 * image on the left side (16x16 pixels) and a label
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface HeaderView : UIView {
@private
	/**
	 * Header image view to show header image
	 */
	IBOutlet UIImageView* _imageView;
	
	/**
	 * Header label to show header title
	 */
	IBOutlet UILabel* _titleLabel;
    
    
}

/**
 * Provides read-write access to the header image
 */
@property (strong, nonatomic) IBOutlet UIButton *btnAtras;
@property (strong, nonatomic) IBOutlet UIButton *btnMenu;
@property (nonatomic, readwrite, strong) UIImage* headerImage;
@property (nonatomic, readwrite, strong) UIImage* btnDerechoImage;
@property (strong, nonatomic) IBOutlet UIImageView *lineaHeader;
@property (nonatomic, readwrite, strong) UIImage* btnIzquierdoImage;

/**
 * Provides read-write access to the header title
 */
@property (nonatomic, readwrite, copy) NSString* headerTitle;

@property (nonatomic, strong) BaseViewController *controladorBase;
@property (nonatomic) TipoBotonDerecho _tipoBotonDerecho;

- (IBAction)touchAtras:(id)sender;
- (IBAction)touchMenu:(id)sender;
- (void)ocultaEncabezado :(BOOL)aOcultaEncabezado;
-(void)colorTitulo:(UIColor*)aColor;

@end
