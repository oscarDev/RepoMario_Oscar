/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

#import <UIKit/UIKit.h>
#import "Constants.h"

@interface Tools : NSObject

/**
 * Calculates the given data MD5 digest and returns it as an hexadecimal string capitalized
 *
 * @param aData The data to calculate its MD5
 * @return The MD5 digest as hexadecimal string
 */
+ (NSString *)getMD5StringFromData:(NSData *)aData;

/**
 * Hides part of the account number, showing the account number first digists as an asterisk (*),
 * and only leaving visible a certain number of digist. For example:
 * Account 12345678901234567890 would turn into *7890
 *
 * @param anAccountNumber The account number
 * @return A string with the masked account number
 */
+ (NSString *)hideAccountNumbers:(NSString *)anAccountNumber;

+ (NSString *)hideAccountNumbersMisCuentas:(NSString *)anAccountNumber;

/**
 * Returns YES if first parameter string starts with second parameter string
 *
 * @param aContainerString The string to check whether it starts with other string
 * @param aContainedString The string to check whether is the begining of other string
 * @return YES if aContainerString starts with aContainedString, NO otherwise
 */
+ (BOOL)string:(NSString *)aContainerString startsWith:(NSString *)aContainedString;

/**
 * Formats an amount received from the server
 *
 * @param anAmount The amount to format
 * @param aNegative YES if the amount is negative, NO otherwise
 * @return The formatted amount
 */
+ (NSString *)formatAmount:(NSString *)anAmount isNegative:(BOOL)aNegative;

/**
 * Formats an amount entered by the usr to show the decimal and thousand separator. Examples:
 * 22 returns 22.00
 * 22.3 returns 22.30
 * 1230 returns 1,230.00
 * 1234567.8 returns 1,234,567.80
 *
 * @param anAmount The amount to format
 * @return The formatted amount
 */
+ (NSString *)formatUserAmount:(NSString *)anAmount;

/*
 * Same as above with the locale currency symbol prepended
 * gramirez
 */
+ (NSString *)formatUserAmountWithLocaleCurrencySymbol:(NSString *)anAmount;

/**
 * Formats an date received from the server
 *
 * @param aDate The date to format
 * @return The formatted date
 */
+ (NSString *)formatDate:(NSString *)aDate;

/**
 * Formats an date received from the server (aaaa-mm-dd)
 *
 * @param aDate The date to format
 * @return The formatted date
 */
+ (NSString *)formatDateTDC:(NSString *)aDate;

/**
 * Formats a time interval since 1.970 into a string
 *
 * @param aTimeInterval The time interval to format
 * @return The formatted date
 */
+ (NSString *)formatTimeInterval:(NSTimeInterval)aTimeInterval;

/**
 * Formats a date recived from the server as DD/MM
 *
 * @param aDate The date to format
 * @return The formatted date
 */
+ (NSString *)formatShortDate:(NSString *)aDate;

/**
 * Obtains a time interval since Jan 1, 1.970, from a date and time expressed as string
 *
 * @param aDate The date as DDMMYYYY
 * @param aTime The time as HHMMSS
 * @return a time interval since Jan 1, 1.970, spefifying the date and time passed. Returns 0 if the date or time cannon be parsed
 */
+ (NSTimeInterval)parseDate:(NSString *)aDate andTime:(NSString *)aTime;

/**
 * Formats a amount posibly including separators for server representation
 *
 * @param anAmount The amount to format
 * @return The formatted amount
 */
+ (NSString *)formatAmountForServer:(NSString *)anAmount;


//STF 20100719-INI QP
/*
 * Formats a amount returned by server to an amount with cent separators
 *
 * @param anAmount The amount to format
 * @return the formatted amount
 */
+ (NSString *)formatAmountFromServer:(NSString *)anAmount;
//STF 20100719-FIN

/**
 * Formatea el string con formato $1,000.00 a un double para poder realizar operaciones con saldos
 */
+ (double)formatBalanceToDouble:(NSString *)balance;

/**
 * Formatea el string con formato $1,000.00 a un double para poder realizar operaciones con saldos
 */
+ (NSString *)convertAcountTagInText:(NSString *)tag;

+ (NSString *)convierteClaveANomenclatura:(NSString *)tag;

/**
 * Retorna la clave
 */
+ (NSString *)convierteNomenclaturaAClave:(NSString *)tag;

/**
 * Valida y devuelve el nombre del nib correspondiente al tamaño de la pantalla
 */
+ (NSString *)getNibName:(NSString *)nibName;

/**
 * Devuelve true si es un iphone 5
 */
+ (BOOL)isIphone5;

+ (NSString *)FormatCurrentTimeFromServer:(NSString *)aTime;
+ (float)formatBalanceToFloat: (NSString *)balance;

/*
 *  Formats date from dd/mm/aaaa to aaaa-mm-dd
 */
+ (NSString *)formatDateForServer:(NSString *)aDate;

/**
 * Formatea el saldo eliminando caracteres especiales
 */
+ (NSString *)formatBalanceWithoutSpecialCharacters:(NSString *)balance;

+ (NSString *)hideCreditAccountNumbers:(NSString *)anAccountNumber;

+ (NSString *)formatStringSpecialCharactersForString:(NSString *)string andIgnoreCharacters:(NSString *)caracteres;

/*
 * Le da formato a una fecha AAAA-MM-DD a DDMMAAAA
 */
+ (NSString *)formateaFechaContratacion:(NSString *)aDate;

/**
 * Elimina espacios y saltos de linea de una cadena
 */
+ (NSString *)eliminarSaltosEspacios:(NSString *)string;

+ (BOOL)isVaildMail:(NSString *)checkString;
/*
 *
 */
+ (NSString *)createPUK;

+ (NSString *)convertirAMAyusculas:(NSString *)cadena;

+ (Perfil)calcularPerfilEnum:(NSString *)perfilMF;
+ (NSString *)calcularPerfilMF:(Perfil)perfilEnum;
+ (NSString *)enmascararNumero:(NSString *)numero nDigitosVisibles:(NSInteger)numDigitosVisibles;
+ (Operacion)getOperationIdForString:(NSString *)operation;
+ (BOOL)is24hFormat:(NSString *)stringHora andDateFormatter:(NSDateFormatter *)dateFormatter;
+ (NSString *)convertir12a24h:(NSString *)horaFormato12 andDateFormatter:(NSDateFormatter *)dateFormatter;
+ (NSString *)formatoFechaSpei: (NSString *)aDate;
+ (NSString *)formateaFechaConBarras:(NSString *)dateString;
+ (NSDate *)parseDate:(NSString *)stringDate inFormat:(NSString *)inFormat;
+ (NSString *)formatearHora:(NSString *)hora;

//Inicia BBVA Credit
+ (NSString *)getMoneyString:(NSString *)price;
+ (NSString *)formatearImporteConPunto:(NSString *)importeConPunto;
+ (NSString *)getConceptoDePago:(NSString *)clave;
+ (NSString *)formatFechaSimulador:(NSString *)fechaS;
//Termina BBVA Credit

+ (BOOL)isAplicacionInstalada:(NSString *)urlApp;

+ (BOOL)networkReachable;
+ (NSString *)decodeHTMLEntities:(NSString *)string;

+ (NSString *)getTimeFromDouble:(double)time;

+ (NSString *)formatPuntosBancomer:(NSString *)aPuntos;

//APISegurosGF
/**
 * Dar formato a un NSString de cantidad, a tipo 100,000
 *
 * @param anAmount   Un NSString para darle formato
 * @param aNegative  YES si el monto es negativo, NO en otro caso
 * @return The formatted amount
 */
+ (NSString *)format2Amount:(NSString *)anAmount isNegative:(BOOL)aNegative;

+ (NSString *)formatDateString:(NSDate *)aDate;

+ (NSMutableString *)formatearNumeroDeAgenda:(NSMutableString *)phone;

/*
 * Computes the application IUM for the given user and application activation time
 */
+ (BOOL)isEncriptacionWithCadAutenticacion:(NSString*)cadAutenticacion;

@end
