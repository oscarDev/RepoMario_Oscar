//
//  PerfilAutenticacion.h
//  SuiteBancomer
//
//  Created by ARTEMIO OLVERA MEDINA on 14/06/13.
//  Copyright (c) 2013 GoNet. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"

@interface PerfilAutenticacion : NSObject<NSCoding,NSCopying>

@property (nonatomic) BOOL contrasenia;
@property (nonatomic) BOOL nip;
@property (nonatomic) BOOL cvv2;
@property (nonatomic) TipoOtpAutenticacion token;
@property (nonatomic) BOOL registro;

@property (nonatomic) BOOL operar;
@property (nonatomic) BOOL visible;

@end
