//
//  ConsultaEstatus.h
//  SuiteBancomer
//
//  Created by ARTEMIO OLVERA MEDINA on 05/09/13.
//  Copyright (c) 2013 GoNet. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ParsingHandler.h"
#import "Autenticacion.h"
#import "Constants.h"

@class TelefonicasMantenimiento;

@interface ConsultaEstatus : NSObject<ParsingHandler,NSCopying>
{
    /**
	 * Reglas de autenticación
	 */
	Autenticacion *_reglasAutenticacion;
    
    /**
	 * Telefonicas Mantenimiento
	 */
	TelefonicasMantenimiento *_telefonicasMantenimiento;
    
}

@property(nonatomic,readwrite,copy)NSString *numeroCliente;
@property(nonatomic,readwrite,copy)NSString *estatusServicio;
@property(nonatomic,readwrite,copy)NSString *companiaCelular;
@property(nonatomic,readwrite,copy)NSString *fechaContratacion;
@property(nonatomic,readwrite,copy)NSString *fechaSistema;
@property(nonatomic,readwrite)Perfil perfilCliente;
@property(nonatomic,readwrite,copy)NSString *tipoInstrumento;
@property(nonatomic,readwrite,copy)NSString *estatusInstrumento;
@property(nonatomic,readwrite,copy)NSString *emailCliente;
@property(nonatomic,readwrite,copy)NSString *versionCatAutenticacion;
@property(nonatomic,readwrite,copy)NSString *versionCatTelefonicas;
@property(nonatomic,readwrite,copy)NSMutableArray *telefonicasMantenimientoArray;
@property(nonatomic,readwrite,copy)NSString *numeroCelular;
@property (nonatomic, readwrite, strong) Autenticacion* reglasDeAutenticacion;
@property (nonatomic, readwrite, strong) TelefonicasMantenimiento* telefonicasMantenimiento;
@property(nonatomic,readwrite,copy)NSString *perfilAST;
@property(nonatomic,readwrite,copy)NSString *nombreCliente;
@property(nonatomic,readwrite,copy)NSString *estatusAlertas;
@property(nonatomic,readwrite,copy)NSString *numTarjeta;

-(id)initWithNumeroCliente:(NSString*)numeroCluente conEstatusServicio:(NSString*)estatusServicio conCompaniaCelular:(NSString*)companiaCelular conFechaContratacion:(NSString*)fechaContratacion conFechaSystema:(NSString*)fechaSistema conPerfil:(Perfil)perfil conTipoInstrumento:(NSString*)tipoInstrumento conEstatusInstrumento:(NSString*)estatusInstrumento conEmail:(NSString*)emailcliente conVersionCatAutenticacion:(NSString*)verCatAutenticacion conVersionCatTelefonicas:(NSString*)verCatTelefonicas conArregloTelefonicas:(NSMutableArray*)arregloTelefonicas conNumeroCelular:(NSString*)numeroCelular conAutenticacion:(Autenticacion*)autenticacion conTelefonicasMantenimiento:(TelefonicasMantenimiento*)telefonicasMantenimiento conPerfilAST:(NSString*)perfilAST conNombreCliente:(NSString*)nombreCliente conEstatusAlertas:(NSString*)estatusAlertas;

@end
