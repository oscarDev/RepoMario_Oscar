//
//  ServerAplicacionDesactivada.h
//  Bancomer
//
//  Created by Mike on 28/10/15.
//  Copyright (c) 2015 GoNet. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Server.h"

/**
 * Códigos de URLs
 */
#define OPERATION_CODE_PARAMETER									@"OPERACION"
#define OPERATION_LOCALE_PARAMETER									@"LOCALE"
#define OPERATION_LOCALE_VALUE										@"es_ES"
#define OPERATION_DATA_PARAMETER									@"PAR_INICIO.0"
#define PARAMETER_SEPARATOR											@"*"

#define OPERATION_CODE_VALUE										@"BAN2O01"
#define OPERATION_CODE_VALUE_JSON                                   @"BAN2O05"

#define CONSUTA_ESTATUS_MANTENIMIENTO                                   37

@interface ServerAplicacionDesactivada : NSObject

@property NSString *codeValueJson;

+ (ServerAplicacionDesactivada*) getInstance;

+ (NSString*) operationCodeForOperationIndex:(NSUInteger)anOperationIndex;

-(NSInteger)consultaEstatusMantenimientoConNumeroTelefonico:(NSString*)numeroTelefono
                                 conVersionCatalogoTelefono:(NSString*)versionCatTelefonico
                            conVersionCatalogoAutenticacion:(NSString*)versionCatAutenticacion
                                      conVersionAppConsulta:(float)versionAppConsulta
                                             conIsEncriptar:(BOOL)isEncriptar
                                                paraCliente:(id<ServerClient>)cliente;

@end
