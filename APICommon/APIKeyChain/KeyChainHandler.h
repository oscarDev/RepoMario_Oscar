//
//  KeyChainHandler.h
//  Bancomer
//
//  Created by Mike on 08/06/15.
//  Copyright (c) 2015 GoNet. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KeyChainItemWrapper.h"

#define KEYCHAIN_STORENAME                      @"BConnectKeyChain"
#define KEYCHAIN_DATA_KEY                       @"acct"
#define KEYCHAIN_SEED_KEY                       @"seed"
#define KEYCHAIN_TELEPHONE_KEY                  @"numCel"
#define KEYCHAIN_CENTER_KEY                     @"centro"
#define KEYCHAIN_CONTRATACION_BT_KEY            @"contratacionBT"
#define KEYCHAIN_ACTIVA_TOKEN_KEY               @"activaToken"

@interface KeyChainHandler : NSObject {
    @private KeychainItemWrapper *keychainItemWrapper;
}

-(void) inicializarKeyChain;
-(NSString*) leerDeKeyChain:(NSString *)clave;
-(void) guardarEnKeyChain:(NSString *)valor clave:(NSString *)clave;
- (void) borrarDeKeyChain:(NSString*)clave;
-(void) borrarKeyChain;

@end
