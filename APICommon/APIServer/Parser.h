/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

#import <Foundation/Foundation.h>


/**
 * Response status successful
 */
#define STATUS_OK										@"OK"

/**
 * Response status warning
 */
#define STATUS_WARNING									@"AVISO"

/**
 * Response status error
 */
#define STATUS_ERROR									@"ERROR"

/**
 * Response optional aplication update
 */
#define STATUS_OPTIONAL_UPDATE							@"AC"
@class Result;

/**
 * Wraps th result data of a response (status, error code, error message)
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
//@interface Result: NSObject {
//@private
//	/**
//	 * The status code
//	 */
//	NSString* _status;
//	
//	/**
//	 * The error code
//	 */
//	NSString* _code;
//	
//	/**
//	 * The error message
//	 */
//	NSString* _message;
//	
//	/**
//	 * The URL for mandatory application update
//	 */
//	NSString* _updateURL;
//}
//
///**
// * Provides read-only access to the status code
// */
//@property (nonatomic, readonly, copy) NSString* status;
//
///**
// * Provides read-only access to the error code
// */
//@property (nonatomic, readonly, copy) NSString* code;
//
///**
// * Provides read-only access to the error message
// */
//@property (nonatomic, readonly, copy) NSString* message;
//
///**
// * Provides read-only access to the URL for mandatory application update
// */
//@property (nonatomic, readonly, copy) NSString* updateURL;
//
///**
// * Initializes a resut instance with a given status code, error code and error message
// *
// * @param aStatus The status code to store
// * @param aCode The error code to store
// * @param aMessage The error message to store
// * @return The initialized Result insntance
// */
//- (id) initWithStatus: (NSString*) aStatus code: (NSString*) aCode andMessage: (NSString*) aMessage;
//
///**
// * Designated initializer. Initializes a resut instance with a given status code, error code, error message and update URL
// *
// * @param aStatus The status code to store
// * @param aCode The error code to store
// * @param aMessage The error message to store
// * @param anUpdateURL The application update URL to store
// * @return The initialized Result insntance
// */
//- (id) initWithStatus: (NSString*) aStatus code: (NSString*) aCode message: (NSString*) aMessage andUpdateURL: (NSString*) anUpdateURL;
//
//@end


/**
 * Parses operation responses from Bancomer serer
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface Parser : NSObject {
@protected
	/**
	 * Response received
	 */
	NSString* _response;
	
	/**
	 * Next index to parse
	 */
	NSInteger _nextIndex;
	
	/**
	 * Response received size
	 */
	NSInteger _responseSize;
	
	/**
	 * Read buffer
	 */
	NSMutableString* _readBuffer;
	
	/**
	 * Code buffer
	 */
	NSMutableString* _codeBuffer;
	
	/**
	 * Flag to indicate that the reader should not go forward due to unfound optional tags
	 */
	BOOL _pauseReader;
	
	/**
	 * Last entity read
	 */
	NSString* _lastEntity;
}

/**
 * Designated initializer. Initializes the Parser instance using the provided response string
 *
 * @param aResponse The response string to store and parse
 * @return The initialized Parser instance
 */
- (id) initWithResponse: (NSString*) aResponse;

/**
 * Parses the operation result from received message
 *
 * @return The operation result as a Result instance
 */
- (Result*) parseResult;

/**
 * Obtains the next entity
 *
 * @return The next entity as an NSString
 */
- (NSString*) parseNextEntity;

/**
 * Obtanins the next value in the whole entity
 *
 * @return The next value as an NSString
 */
- (NSString*) parseNextValue;

/**
 * Obtains the next value for a tag
 *
 * @param aTag The target tag
 * @return The value for the tag as an NSString
 */
- (NSString*) parseNextValueForTag: (NSString*) aTag;

/**
 * Obtains the next value for a tag, indicating whether the tag is mandatoy or not
 *
 * @param aTag The target tag
 * @param aMandatory YES in case the tag is mandatory, NO otherwise
 * @return The value for the tag as an NSString, or nil in case of a parsing error
 */
- (NSString*) parseNextValueForTag: (NSString*) aTag isMandatory: (BOOL) aMandatory;

/**
 * Gets the value for a tag in an entity
 *
 * @param anEntity The entity to extract the value from
 * @param aTag The tag to look for
 */
+ (NSString*) entityValueFromEntity: (NSString*) anEntity andTag: (NSString*) aTag;

/**
 * Try to parse a string as an integer
 *
 * @param aValue The value to convert
 * @param aDefaultValue The value to use f the conversion cannot be accomplished
 * @return The converted integer, or aDefaultValue if the conversion was not successful
 */
+ (NSInteger) value: (NSString*) aValue asIntWithDefault: (NSInteger) aDefaultValue;

@end
