//
//  ServerTools.h
//  APIServer
//
//  Created by bbva on 28/3/16.
//  Copyright © 2016 CGI. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ServerTools : NSObject

+ (void)writeLog:(NSString *)string;
+ (BOOL)isEncriptar;

@end
