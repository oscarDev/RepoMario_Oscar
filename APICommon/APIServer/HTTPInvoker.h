/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */
#import <Foundation/Foundation.h>

/**
 * Flag to indicate HTTP POST usage (1 = use HTTP POST, otherwise use HTTP GET)
 */
#define USE_HTTP_POST											1


//Forward declarations
@protocol ParsingHandler;


/**
 * Protocol implemented by HTTPInvoker clients to be informed asynchronously when download is finished or cancelled
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@protocol HTTPInvokerClient <NSObject>

/**
 * The download has finished correctly. The informtion has already being parsed by the provider parser
 *
 * @param aDownloadId The download identification that has finished
 */
- (void) downloadFinished: (NSInteger) aDownloadId;

/**
 * The download finished beacuse of an error
 *
 * @param aDownloadId The download identification that has finished because of an error
 */
- (void) downloadError: (NSInteger) aDownloadId;

/**
 * The download was cancelled by user
 *
 * @param aDownloadId The download identification that was cancelled
 */
- (void) downloadCancelled: (NSInteger) aDownloadId;

@end


/**
 * Sends operation using HTTP protocol
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface HTTPInvoker : NSObject {
@private
	/**
	 * Dictionary of active connections
	 */
	NSMutableDictionary* _activeDownloads;
	
	/**
	 * Next download process id
	 */
	NSInteger _nextDownloadProcessId;
}

/**
 * Returns the singleton only instance
 *
 * @result The singleton only instance
 */
+ (HTTPInvoker*) getInstance;

/**
 * Invokes a network operation to the given URL
 *
 * @param aURLString The URL string to invoke the operation
 * @param aParametersString The parameters string used in the operation
 * @param aHandler The response handler to analyze the server response
 * @param aClient The client that starts the nerwokr operation
 * @param aManageCookiesFlag When YES, the cookies are managed by this class, when NO, the cookies are handled by NSMutableURLRequest object
 * @param aStoreCookiesFlag When YES, the cookies are stored in persistent memory, when NO, the cookies are not stored
 * @param aUseStoredCookiesFlag When YES, the cookies stored in persistent memory are sent, when NO, the default behaviour is used
 * @return A positive transaction number, or a negative number in case an error is detected
 */
- (NSInteger) invokeOperationToURL: (NSString*) aURLString withParameters: (NSString*) aParametersString
						andHandler: (id<ParsingHandler>) aHandler forClient: (id<HTTPInvokerClient>) aClient
					  storeCookies: (BOOL) aStoreCookiesFlag andUseStoredCookies: (BOOL) aUseStoredCookiesFlag;

/**
 * Cancels the given download process and notifies the client. The associated
 * DonwloadInformation instance is removed from the pending downloads list
 *
 *
 * @param aDownloadId The download process Id that is going to be stopped
 */
- (void) cancelDownload: (NSInteger) aDownloadId;

/**
 * Simulates a network operation
 *
 * @param aDownloadId The download simulated
 * @param aResponse The simulated response as a string
 * @param aHandler The handler to analyze the response
 * @param aClient The client to simulate the operation for
 */
- (void) simulateOperationId: (NSInteger) aDownloadId withResponse: (NSString*) aResponse
				  andHandler: (id<ParsingHandler>) aHandler forClient: (id<HTTPInvokerClient>) aClient;

@end
