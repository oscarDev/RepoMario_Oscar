/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

/*
 * STF 20100719 ODT1 - Added the new methods and protocols needed to use the fast and frequent payments, as well as the get service name opearation needed
 *							for services payment requirement.
 *
 * STF ODT2 - Added the methods to support adding and deleting fast and frequent payments.
 */


#import "HTTPInvoker.h"


/**
 * Defines Movilok infrastructure
 */
#define MOVILOK														0

/**
 * Defines Bancomer infrastructure
 */
#define BANCOMER														1

//STF-ODT2-GRL 20100830 INI
/**
 * Defines Stefanini Infrastructure
 */
#define STEFANINI													2

//STF-ODT2-GRL 20100830 FIN


/**
 * Defines current operation provider
 */
#define PROVIDER												BANCOMER

/**
 * Defines the use development server flag (0 = use production, otherwise use development)
 */
#define USE_DEVELOPMENT_SERVER									1
// 1 Test, 0 Produccion

//Forward declarations
@class ServerResponse;
@class Server;

/**
 * Protocol implemented by Server class clients. Protocol implementers are informed when the network request
 * finishes correctly, are cancel or finish with an error
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@protocol ServerClient <NSObject>

/**
 * The request finished correctly
 *
 * @param aServer The server triggering the event
 * @param aDownloadId The download identification that finished correctly
 * @param aServerResponse The server response information
 */
- (void) server: (Server*) aServer download: (NSInteger) aDownloadId finishedWithResponse: (ServerResponse*) aServerResponse;

/**
 * The request was cancelled
 *
 * @param aServer The server triggering the event
 * @param aDownloadId The download identification that was cancelled
 */
- (void) cancelledServer: (Server*) aServer download: (NSInteger) aDownloadId;

/**
 * The request finished with an error
 *
 * @param aServer The server triggering the event
 * @param aDownloadId The download identification that finished with an error
 */
- (void) erroredServer: (Server*) aServer download: (NSInteger) aDownloadId;

@end

/**
 * Server is responsible of acting as an interface between the application business logic and the HTTP operations.
 * It receives the network operation requests and builds a network operation suitable for HTTPInvoker class, which
 * eventually will perform the network connection.
 * Server returns the server response as a ServerResponse object, so that the application logic can perform high
 * level checks and processes
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface Server : NSObject <HTTPInvokerClient> {
@private
	/**
	 * HTTP invoker instance to perform network connections
	 */
	HTTPInvoker* _httpInvoker;
	
	/**
	 * Pending operations dictionary
	 */
	NSMutableDictionary* _pendingOperations;

	/**
	 * First login simulates a deactivation flag
	 */
	BOOL _simulateDeactivation;
}

@property BOOL isJSON;

/**
 * Returns the singleton only instance
 *
 * @result The singleton only instance
 */
+ (Server*) getInstance;

/**
 * Cancels all pending operations
 */
- (void) cancelPendingOperations;

#pragma mark - OneClic FaseII

#pragma mark - SPEI Operations

//Paperless

-(NSInteger) doGenericNetworkRequestWithURL:(NSString*) urlString
                           andUrlParameters:(NSString*) urlParameters
                       andSimulatedResponse:(NSString*) simulatedResponse
                         andResponseHandler:(id<ParsingHandler>) aResponseHandler
                                  forClient:(id<ServerClient>)aClient;

-(NSInteger) doGenericNetworkRequestForOperation:(NSString*) operacion
                                  withParameters:(NSDictionary*) parameters
                              andResponseHandler:(id<ParsingHandler>) aResponseHandler
                                       forClient:(id<ServerClient>)aClient;

-(NSInteger) getProvider;
-(NSInteger) getEnvironment;
-(NSInteger) getAppVersionConsulta;

//APITOKE
+(NSInteger)getSimulationParam;
+(NSInteger)getDevelopmentServerParam;
//Fin

@end
