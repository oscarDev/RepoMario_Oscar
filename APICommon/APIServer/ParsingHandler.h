/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */
#import <Foundation/Foundation.h>

@class Parser;


/**
 * Protocol to be implemented by those interested in building respnse objects fom Bancomer server responses
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@protocol ParsingHandler <NSObject>

/**
 * Informs whether the parsing handler uses raw data or a parser to analyze data
 *
 * @return YES in case the parsing handler uses a parser to analyze data, NO if raw data is needed
 */
- (BOOL) usesParser;

/**
 * Processes a server response
 *
 * @parameters aParser The parser containing the server response, ready to start parsing
 */
- (void) processParser: (Parser*) aParser;

/**
 * Parses the raw data
 *
 * @param aData The data received from the server to be parsed
 */
- (void) parseData: (NSData*) aData;

@end
